// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bytes"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	pb "siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

type fmtSummary struct {
	name string
}

func registerSummary() {
	f := &fmtSummary{name: "summary"}
	asgmtFormatters[f.name] = f
}

type wlIndex struct {
	app, wl int
}

func (m model) workload(an, wn string) wlIndex {
	for i, app := range m.apps.Applications {
		if app.Id == an {
			for j, w := range app.Workloads {
				if w.Id == wn {
					return wlIndex{i, j}
				}
			}
			break
		}
	}
	return wlIndex{-1, -1}
}

type byteSize int64

func (b byteSize) String() string {
	if b >= 1024 {
		f := byteSize(1024)
		for _, p := range "KMGTPE" {
			if b < f*byteSize(1024) {
				return fmt.Sprintf("%.4g%ciB", float64(b)/float64(f), p)
			}
			f *= byteSize(1024)
		}
	}
	return strconv.FormatInt(int64(b), 10)
}

func wrapWords(ws []string, indent, width int) string {
	is := strings.Repeat(" ", indent-1)
	var buf bytes.Buffer
	inline := 0
	for i, w := range ws {
		sep := ","
		if i == len(ws)-1 {
			sep = ""
		}
		n := utf8.RuneCountInString(w) + len(sep) + 1
		if inline == 0 {
			fmt.Fprintf(&buf, "%s %s%s", is, w, sep)
			inline = indent + n
		} else if inline+n <= width {
			fmt.Fprintf(&buf, " %s%s", w, sep)
			inline += n
		} else {
			fmt.Fprintf(&buf, "\n%s %s%s", is, w, sep)
			inline = indent + n
		}
	}
	return buf.String()
}

type nodeInfo struct {
	cpu types.Resource[int32]
	ram types.Resource[byteSize]
	ws  []wlIndex
}
type nodeInfos struct {
	m  *model
	is []nodeInfo
}

func (nis nodeInfos) String() string {
	var buf bytes.Buffer
	for i, ni := range nis.is {
		if len(ni.ws) == 0 {
			continue
		}
		node := nis.m.infra.Nodes[i]
		if node.NodeType != pb.Node_COMPUTE {
			continue
		}
		fmt.Fprintf(&buf, "%s: cpu:%s, ram:%s, workloads: %d\n",
			node.Id, &ni.cpu, &ni.ram, len(ni.ws))
		apps := make([]string, len(ni.ws))
		for i, wi := range ni.ws {
			app := nis.m.apps.Applications[wi.app]
			apps[i] = fmt.Sprintf("%s•%s", app.Id, app.Workloads[wi.wl].Id)
		}
		fmt.Fprintln(&buf, wrapWords(apps, 3, 80))
	}
	return buf.String()
}

func (m *model) nodeInfos() nodeInfos {
	ni := nodeInfos{m: m, is: make([]nodeInfo, len(m.infra.Nodes))}
	for i, n := range m.infra.Nodes {
		if n.NodeType == pb.Node_COMPUTE {
			ni.is[i].cpu.Cap = int32(n.CPU.Capacity)
			ni.is[i].cpu.Load(int32(n.CPU.Used))
			ni.is[i].ram.Cap = byteSize(n.Memory.Capacity)
			ni.is[i].ram.Load(byteSize(n.Memory.Used))
		}
	}
	for _, wa := range m.asgmts[0].Workloads {
		if n := m.infra.FindNode(wa.Node); n >= 0 {
			if wi := m.workload(wa.Application, wa.Workload); wi.app >= 0 {
				w := m.apps.Applications[wi.app].Workloads[wi.wl]
				ni.is[n].cpu.Use(w.CPU)
				ni.is[n].ram.Use(byteSize(w.Memory))
				ni.is[n].ws = append(ni.is[n].ws, wi)
			}
		}
	}
	return ni
}

type linkInfo struct {
	bw types.Resource[types.Bandwidth]
	cs []int
}
type linkInfos struct {
	m  *model
	is []linkInfo
}

func getCapacity(infra *pb.Network, name string) int64 {
	for _, m := range infra.Media {
		if m.Id == name {
			return m.Bandwidth.Capacity
		}
	}
	return 0
}

func (lis linkInfos) String() string {
	var buf bytes.Buffer
	base, channels := 0, lis.m.apps.Channels
	for _, net := range lis.m.infra.Networks {
		empty := true
		for i := range net.Links {
			if len(lis.is[base+i].cs) > 0 {
				empty = false
				break
			}
		}
		if empty {
			continue
		}
		fmt.Fprintf(&buf, "\nNetwork “%s”\n", net.Id)
		for i, link := range net.Links {
			li := lis.is[base+i]
			if len(li.cs) == 0 {
				continue
			}
			fmt.Fprintf(&buf, "%s: lat:%s, cap:%s",
				link.Id, time.Duration(link.Latency), &li.bw)
			fmt.Fprintf(&buf, ", channels: %d\n", len(li.cs))
			cs := make([]string, len(li.cs))
			for i, ci := range li.cs {
				cs[i] = channels[ci].Id
			}
			fmt.Fprintln(&buf, wrapWords(cs, 3, 80))
		}
		base += len(net.Links)
	}
	return buf.String()
}

func findLinkByName(n *pb.Network, name string) int {
	for i, l := range n.Links {
		if l.Id == name {
			return i
		}
	}
	return -1
}

func findLinkReverse(n *pb.Network, name string) int {
	if k := findLinkByName(n, name); k >= 0 {
		src, dst := n.Links[k].Source, n.Links[k].Target
		for i, l := range n.Links {
			if l.Source == dst && l.Target == src {
				return i
			}
		}
	}
	return -1
}

func pathNodes(n *pb.Network, p *pb.Network_Path) (src string, dst string) {
	if k := findLinkByName(n, p.Links[0]); k >= 0 {
		src = n.Links[k].Source
	}
	if k := findLinkByName(n, p.Links[len(p.Links)-1]); k >= 0 {
		dst = n.Links[k].Target
	}
	return
}

func (m *model) pathLinks(name, src, dst string) []int {
	base := 0
	for _, net := range m.infra.Networks {
		for _, p := range net.Paths {
			getLinks := func(findLink func(*pb.Network, string) int) []int {
				links := make([]int, len(p.Links))
				for i := range links {
					if k := findLink(net, p.Links[i]); k >= 0 {
						links[i] = base + k
					} else {
						return nil
					}
				}
				return links
			}
			if p.Id == name {
				return getLinks(findLinkByName)
			} else if textformat.ImplicitReversePath {
				pSrc, pDst := pathNodes(net, p)
				if pSrc == dst && pDst == src {
					return getLinks(findLinkReverse)
				}
			}
		}
		base += len(net.Links)
	}
	return nil
}

var channelName = regexp.MustCompile("^If_(?:" +
	"([[:alnum:]-]+_[[:alnum:]-]+)[_-]([[:alnum:]-]+_[[:alnum:]-]+)|" +
	"([[:alnum:]-]+)_([[:alnum:]-]+))$")

func splitChannelName(name string) (bool, string, string) {
	if m := channelName.FindStringSubmatch(name); m != nil {
		switch {
		case m[1] != "":
			return true, m[1], m[2]
		case m[3] != "":
			return true, m[3], m[4]
		}
	}
	return false, "", ""
}

func (m *model) findChannel(name string) int {
	if match, w1, w2 := splitChannelName(name); match {
		for i, ch := range m.apps.Channels {
			if ch.SourceWorkload == w1 && ch.TargetWorkload == w2 {
				return i
			}
		}
	}
	return -1
}

func (m *model) findChannelByName(name string) int {
	for i, ch := range m.apps.Channels {
		if ch.Id == name {
			return i
		}
	}
	return -1
}

func (m *model) linkInfos() linkInfos {
	lc := 0
	for _, net := range m.infra.Networks {
		lc += len(net.Links)
	}
	lis, base := linkInfos{m: m, is: make([]linkInfo, lc)}, 0
	for _, net := range m.infra.Networks {
		for i, n := range net.Links {
			lis.is[base+i].bw.Cap = types.Bandwidth(getCapacity(net, n.Medium))
		}
		base += len(net.Links)
	}
	for _, pa := range m.asgmts[0].Channels {
		ci := m.findChannelByName(pa.Channel)
		if ci < 0 {
			continue
		}
		ls := m.pathLinks(pa.Path, pa.Source, pa.Target)
		for _, l := range ls {
			lis.is[l].bw.Use(types.Bandwidth(m.apps.Channels[ci].Bandwidth))
			lis.is[l].cs = append(lis.is[l].cs, ci)
		}
	}
	return lis
}

func (f *fmtSummary) formatAsgmt(m *model) ([]byte, error) {
	var buf bytes.Buffer
	for i := range m.asgmts {
		if len(m.asgmts) > 1 {
			if i == 0 {
				fmt.Fprintf(&buf, "Assignment %d\n", i+1)
			} else {
				fmt.Fprintf(&buf, "\nAssignment %d\n", i+1)
			}
		}
		if len(m.asgmts[i].Workloads) > 0 {
			fmt.Fprintf(&buf, "Node Usage\n%s", m.nodeInfos())
		}
		if len(m.asgmts[i].Channels) > 0 {
			fmt.Fprintf(&buf, "\nLink Usage%s", m.linkInfos())
		}
	}
	return buf.Bytes(), nil
}
