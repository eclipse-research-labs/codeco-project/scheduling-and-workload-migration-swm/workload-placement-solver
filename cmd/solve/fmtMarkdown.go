// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strings"
)

type fmtMarkdown struct {
	*fmtText
	name string
}

func registerMarkdown() {
	f := &fmtMarkdown{
		fmtText: modelConverters["text"].(*fmtText),
		name:    "markdown"}
	modelConverters[f.name] = f
	queryFormatters[f.name] = f
	asgmtFormatters[f.name] = f
}

type mdConverter struct {
	md        *bufio.Reader
	tf        bytes.Buffer
	buf       bytes.Buffer
	inTable   bool
	row       int
	fields    []string
	tableLine func(*mdConverter, []string) string
}

func newMdConverter(data []byte) *mdConverter {
	return &mdConverter{md: bufio.NewReader(bytes.NewReader(data))}
}

var (
	reKeepLine = *regexp.MustCompile(`^$|^#|^[|]|^<!-- workload`)
	reWlAsgmt  = *regexp.MustCompile(`^<!-- workload +assignment +(v\d+) *-->`)
	reHeadline = *regexp.MustCompile(`^#+ +`)
	reTable    = *regexp.MustCompile(
		`^[|] +(?i:workload|channel|node|option|link|find|path)`)
	reCleanUp = *regexp.MustCompile(`\n\n+`)
)

func columns(line string) []string {
	if line = strings.TrimSpace(line); line == "" {
		return nil
	}
	cs := strings.Split(line, "|")
	for i := range cs {
		cs[i] = strings.TrimSpace(cs[i])
	}
	return cs[1 : len(cs)-1]
}

func toLower(cs []string) []string {
	for i := range cs {
		cs[i] = strings.ToLower(cs[i])
	}
	return cs
}

func (mc *mdConverter) unknown([]string) string {
	return ""
}

func (mc *mdConverter) statement(fs []string, object, word string) string {
	if fs == nil {
		return ""
	}
	var stmt bytes.Buffer
	showWord := true
	for i, val := range fs {
		if i == 0 {
			fmt.Fprintf(&stmt, "%s %s", object, val)
		} else if len(val) > 0 {
			if showWord {
				fmt.Fprintf(&stmt, " %s", word)
				showWord = false
			}
			if field := mc.fields[i]; field == "labels" {
				fmt.Fprintf(&stmt, " %s:(%s)", field, val)
			} else {
				fmt.Fprintf(&stmt, " %s:%s", field, val)
			}
		}
	}
	return stmt.String()
}

func (mc *mdConverter) workload(fs []string) string {
	return mc.statement(fs, "workload", "needs")
}

func (mc *mdConverter) channel(fs []string) string {
	return mc.statement(fs, "channel", "needs")
}

func (mc *mdConverter) node(fs []string) string {
	return mc.statement(fs, "node", "provides")
}

func (mc *mdConverter) link(fs []string) string {
	return mc.statement(fs, "link", "with")
}

func (mc *mdConverter) option(fs []string) string {
	if fs == nil {
		return mc.buf.String()
	}
	if mc.row == 1 {
		fmt.Fprint(&mc.buf, "with")
	}
	fmt.Fprintf(&mc.buf, " %s:%s", fs[0], fs[1])
	return ""
}

func (mc *mdConverter) nameWithList(name string, fs []string) string {
	if fs == nil {
		return ")"
	}
	if mc.row == 1 {
		return fmt.Sprintf("%s (\n   %s", name, fs[0])
	}
	return fmt.Sprintf("   %s", fs[0])
}

func (mc *mdConverter) nodes(fs []string) string {
	return mc.nameWithList("nodes", fs)
}

func (mc *mdConverter) paths(fs []string) string {
	return mc.nameWithList("paths", fs)
}

func (mc *mdConverter) findPaths(fs []string) string {
	return mc.nameWithList(mc.fields[0], fs)
}

func (mc *mdConverter) table(line string) string {
	switch {
	case !mc.inTable:
		if reTable.MatchString(line) {
			mc.fields, mc.inTable, mc.row = toLower(columns(line)), true, 0
			switch mc.fields[0] {
			case "workload":
				mc.tableLine = (*mdConverter).workload
			case "channel":
				mc.tableLine = (*mdConverter).channel
			case "node":
				mc.tableLine = (*mdConverter).node
			case "option":
				mc.tableLine = (*mdConverter).option
			case "link":
				mc.tableLine = (*mdConverter).link
			case "nodes":
				mc.tableLine = (*mdConverter).nodes
			case "paths":
				mc.tableLine = (*mdConverter).paths
			default:
				if strings.HasPrefix(mc.fields[0], "find") {
					mc.tableLine = (*mdConverter).findPaths
				} else {
					mc.tableLine = (*mdConverter).unknown
				}
			}
		} else {
			mc.tableLine = (*mdConverter).unknown
		}
		return ""
	case len(line) > 2 && line[1] == '-':
		return ""
	default:
		mc.row++
		stmt := mc.tableLine(mc, columns(line))
		if line == "" {
			mc.buf.Reset()
			mc.inTable = false
		}
		return stmt
	}
}

func (mc *mdConverter) processLine(line string) {
	if len(line) == 0 && !mc.inTable {
		goto print
	}
	if m := reWlAsgmt.FindStringSubmatch(line); m != nil {
		line = "workload assignment " + m[1]
		goto print
	}
	switch {
	case len(line) > 0:
		switch line[0] {
		case '#':
			line = reHeadline.ReplaceAllString(line, "")
		case '|':
			if line = mc.table(line); line == "" {
				return
			}
		}
	case mc.inTable:
		if line = mc.table(line); line == "" {
			return
		}
	}
print:
	fmt.Fprintln(&mc.tf, line)
}

func (f *fmtMarkdown) convertModel(name string, md []byte) (*model, error) {
	mc := newMdConverter(md)
	var err error
	for err == nil {
		var line string
		line, err = mc.md.ReadString('\n')
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return nil, err
		}
		if line[len(line)-1] == '\n' {
			line = line[0 : len(line)-1]
		}
		if reKeepLine.MatchString(line) {
			mc.processLine(line)
		}
	}
	if mc.inTable {
		mc.processLine("")
	}
	data := reCleanUp.ReplaceAll(mc.tf.Bytes(), []byte{'\n'})
	return f.fmtText.convertModel(name, data)
}
