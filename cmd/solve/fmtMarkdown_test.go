// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestColumns(t *testing.T) {
	assert.Equal(t, []string{"a", "b"}, columns("|  a   | b  |"))
	assert.Equal(t, []string{"a", "b"}, columns("|a|b|"))
	assert.Equal(t, []string{"a", ""}, columns("|  a   |   |"))
	assert.Equal(t, []string{"a", ""}, columns("|a||"))
	assert.Equal(t, []string{"", "b"}, columns("|     | b  |"))
	assert.Equal(t, []string(nil), columns(""))
}

func TestToLower(t *testing.T) {
	assert.Equal(t, []string{"a", "b"}, toLower([]string{"A", "b"}))
}

func TestMdConverterStatement(t *testing.T) {
	mc := mdConverter{fields: []string{"workload", "cpu", "ram"}}
	assert.Equal(t,
		"workload w needs cpu:1 ram:2",
		mc.statement(columns("|w|1|2|"), "workload", "needs"))
	assert.Equal(t,
		"workload w",
		mc.statement(columns("|w|||"), "workload", "needs"))
	mc = mdConverter{fields: []string{"workload"}}
	assert.Equal(t,
		"workload w",
		mc.statement(columns("|w|"), "workload", "needs"))
}

func TestMdConverterTable_statements(t *testing.T) {
	mc := mdConverter{}
	assert.Equal(t, "", mc.table("| workload | cpu |"))
	assert.True(t, mc.inTable)
	assert.Equal(t, "", mc.table("|-|-|"))
	assert.Equal(t, "workload w", mc.table("|w||"))
	assert.Equal(t, 1, mc.row)
	assert.Equal(t, "workload w needs cpu:1", mc.table("|w|1|"))
	assert.Equal(t, 2, mc.row)
	assert.Equal(t, "", mc.table(""))
}

func TestMdConverterTable_nameWithList(t *testing.T) {
	mc := mdConverter{}
	assert.Equal(t, "", mc.table("| find paths |"))
	assert.True(t, mc.inTable)
	assert.Equal(t, "", mc.table("|-|"))
	assert.Equal(t, "find paths (\n   p1 p2", mc.table("|p1 p2|"))
	assert.Equal(t, 1, mc.row)
	assert.Equal(t, "   p3", mc.table("|p3|"))
	assert.Equal(t, 2, mc.row)
	assert.Equal(t, ")", mc.table(""))
}

func TestMdConverterTable_options(t *testing.T) {
	mc := mdConverter{}
	assert.Equal(t, "", mc.table("| Option | Value |"))
	assert.True(t, mc.inTable)
	assert.Equal(t, "", mc.table("|o1|v1|"))
	assert.Equal(t, 1, mc.row)
	assert.Equal(t, "", mc.table("|o2|v2|"))
	assert.Equal(t, 2, mc.row)
	assert.Equal(t, "with o1:v1 o2:v2", mc.table(""))
}
