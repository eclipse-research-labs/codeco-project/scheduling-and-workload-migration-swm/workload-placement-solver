// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"google.golang.org/protobuf/encoding/protojson"
	pb "siemens.com/qos-solver/internal/port/grpc"
)

type fmtJson struct {
	name string
}

func registerJson() {
	f := &fmtJson{name: "json"}
	modelConverters[f.name] = f
	queryFormatters[f.name] = f
	asgmtFormatters[f.name] = f
}

func (f *fmtJson) convertModel(name string, data []byte) (*model, error) {
	var qm pb.Model
	if err := protojson.Unmarshal(data, &qm); err != nil {
		return nil, err
	}
	m := model{
		name:  name,
		apps:  qm.AppGroup,
		infra: qm.Infrastructure,
		costs: qm.Costs,
		recs:  qm.Recommendations}
	return &m, nil
}

func (f *fmtJson) formatQuery(m *model) ([]byte, error) {
	M := &pb.Model{
		AppGroup:        m.apps,
		Infrastructure:  m.infra,
		Costs:           m.costs,
		Recommendations: m.recs}
	return protojson.Marshal(M)
}

func (f *fmtJson) formatAsgmt(m *model) ([]byte, error) {
	return protojson.Marshal(m.asgmts[0])
}
