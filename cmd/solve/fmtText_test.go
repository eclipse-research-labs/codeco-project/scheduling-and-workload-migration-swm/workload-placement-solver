// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestANameString(t *testing.T) {
	assert.Equal(t, `abc`, aName("abc").String())
	assert.Equal(t, `"a→c"`, aName("a→c").String())
}
func TestFmtTextConvertModel(t *testing.T) {
	text := fmtText{}
	model, err := text.convertModel("", []byte("17"))
	assert.ErrorContains(t, err, `expected: "workload"`)
	assert.Nil(t, model)
}

func TestStripAppName(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	assert.Equal(t, "a1_w1", stripAppName("a1_w1"))
	assert.Equal(t, "w1", stripAppName("w1"))

	textformat.CompoundNames = true
	assert.Equal(t, "w1", stripAppName("a1_w1"))
	assert.Equal(t, "w1", stripAppName("w1"))
}

func parseApplications(t *testing.T, clauses string) []*grpc.Application {
	asgmt, err := textformat.Parse("test", fmt.Sprintf(`
		workload assignment v1 model test
		%s
		infrastructure node c`,
		clauses))
	require.NoError(t, err)
	return asgmt.AppGroup.Applications
}

func TestFmtTextFormatWorkload(t *testing.T) {
	text := fmtText{}
	check := func(in, exp string) {
		app := parseApplications(t, in)
		ws := app[0].Workloads
		assert.Equal(t, exp, text.formatWorkload(ws[len(ws)-1]))
	}
	check(
		"application a workload w needs ram:2 cpu:1",
		"workload w needs cpu:1 ram:2")
	check(
		"application a workload w needs ram:2 cpu:1 labels:(A ~B)",
		"workload w needs cpu:1 ram:2 labels:(A ~B)")
	check(
		"application a workload w needs cpu:1 ram:2 labels:(~B)",
		"workload w needs cpu:1 ram:2 labels:(~B)")
}

func TestFmtTextFormatApps(t *testing.T) {
	text := fmtText{}
	apps := func(n int) []*grpc.Application {
		var buf bytes.Buffer
		for i := 0; i < n; i++ {
			fmt.Fprintf(&buf,
				"application %c workload w needs cpu:1 ram:2MiB\n",
				'a'+i)
		}
		return parseApplications(t, buf.String())
	}
	assert.Equal(t,
		"applications\n",
		text.formatApps(nil))
	assert.Equal(t,
		"applications\napplication a\nworkload w needs cpu:1 ram:2MiB\n",
		text.formatApps(apps(1)))
	assert.Equal(t,
		"applications\napplication a\nworkload w needs cpu:1 ram:2MiB\n\n"+
			"application b\nworkload w needs cpu:1 ram:2MiB\n",
		text.formatApps(apps(2)))
}

func TestFmtTextFormatChannels(t *testing.T) {
	text := fmtText{}
	assert.Equal(t, "", text.formatChannels(nil))
	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model test
		application a workload w
		channel a•w→b•v needs lat:1ms bw:2Mb 
		infrastructure node c`)
	require.NoError(t, err)
	assert.Equal(t,
		"channel a•w→b•v needs lat:1ms bw:2Mb\n",
		text.formatChannels(asgmt.AppGroup.Channels))
}

func TestFmtTextFormatNode(t *testing.T) {
	text := fmtText{}
	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model test
		application a workload w
		channel a•w→b•v needs lat:1ms bw:2Mb 
		infrastructure
		node n1 provides cpu:1 ram:2MiB
		node n2 provides cpu:1 ram:2MiB labels:(A B)
		node n3 provides cpu:2-1 ram:4MiB-3MiB
		node n4`)
	require.NoError(t, err)
	infra := asgmt.Infrastructure
	assert.Equal(t,
		"node n1 provides cpu:1 ram:2MiB",
		text.formatNode(infra.Nodes[0]))
	assert.Equal(t,
		"node n2 provides cpu:1 ram:2MiB labels:(A B)",
		text.formatNode(infra.Nodes[1]))
	assert.Equal(t,
		"node n3 provides cpu:2-1 ram:4MiB-3MiB",
		text.formatNode(infra.Nodes[2]))

	assert.Equal(t,
		"node n4",
		text.formatNode(infra.Nodes[3]))
}

func TestFmtTextFormatLinks(t *testing.T) {
	text := fmtText{}
	assert.Equal(t, "", text.formatLinks(nil, 0))

	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model test
		application a workload w
		infrastructure
		node n1,n2 provides cpu:1
		network net link n1•n2 with lat:1000000 bw:2000000`)
	require.NoError(t, err)
	assert.Equal(t,
		"link n1→n2 with lat:1ms bw:2Mb\n",
		text.formatLinks(asgmt.Infrastructure, 0))
}

func TestFmtTextFormatPaths(t *testing.T) {
	text := fmtText{}
	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model test
		application a workload w
		infrastructure
		node n1,n2,n3,n4 provides cpu:1
		network n1 link n1•n2,n2•n3
		paths (n1•n2•n3)
		network n2 link n1•n2,n2•n3,n2•n4
		paths (n1•n2•n3 n1•n2•n4)`)
	require.NoError(t, err)
	assert.Equal(t,
		`paths (n1→n2→n3)`,
		text.formatPaths(asgmt.Infrastructure.Networks[0]))
	assert.Equal(t,
		"paths (\n   n1→n2→n3\n   n1→n2→n4\n)",
		text.formatPaths(asgmt.Infrastructure.Networks[1]))
}

func TestFmtTextFormatQuery(t *testing.T) {
	text := fmtText{}
	asgmt, err := textformat.Parse("src", `
		workload assignment v1
		model M
		application a
			workload w1 needs cpu:1 ram:2MiB
			workload w2 needs cpu:1 ram:2MiB
			channel a•w1→a•w2 needs lat:1 bw:2Mb
		node n1 provides cpu:1 ram:2GiB
		network Ethernet
		link n1→n2,n2→n3 with lat:1 bw:2Mb
		paths (n1→n2→n3)`)
	require.NoError(t, err)
	model := &model{
		name:  asgmt.Name,
		opts:  asgmt.Options,
		apps:  asgmt.AppGroup,
		infra: asgmt.Infrastructure}
	bs, err := text.formatQuery(model)
	assert.NoError(t, err)
	assert.Equal(t, strings.Join([]string{
		`workload assignment v1`,
		"model M options compound-names:false implicit-reverse-path:false " +
			"shortest-path-only:true\n",
		`applications`,
		`application a`,
		`workload w1 needs cpu:1 ram:2MiB`,
		"workload w2 needs cpu:1 ram:2MiB\n",
		"channel a•w1→a•w2 needs lat:1ns bw:2Mb\n",
		"infrastructure",
		"node n1 provides cpu:1 ram:2GiB\n",
		"network Ethernet",
		"link n1→n2 with lat:1ns bw:2Mb",
		"link n2→n3 with lat:1ns bw:2Mb\n",
		"paths (n1→n2→n3)\n",
	}, "\n"), string(bs))
}

func TestFmtTextFormatQuery_twoNetworks(t *testing.T) {
	text := fmtText{}
	asgmt, err := textformat.Parse("src", `
		workload assignment v1
		model M
		application a workload w1
		node n1,n2 provides ram:2GiB
		network loopback
		link n1→n1,n2→n2 with lat:1 bw:10Gb path:yes
		network Ethernet
		link n1→n2 with bw:2Mb`)
	require.NoError(t, err)
	model := &model{
		name:  asgmt.Name,
		opts:  asgmt.Options,
		apps:  asgmt.AppGroup,
		infra: asgmt.Infrastructure}
	bs, err := text.formatQuery(model)
	assert.NoError(t, err)
	assert.Equal(t, strings.Join([]string{
		`workload assignment v1`,
		"model M options compound-names:false implicit-reverse-path:false " +
			"shortest-path-only:true\n",
		`applications`,
		`application a`,
		"workload w1 needs cpu:100 ram:1GiB\n",
		"infrastructure",
		"node n1 provides cpu:1000 ram:2GiB",
		"node n2 provides cpu:1000 ram:2GiB\n",
		"network loopback",
		"link n1→n1 with lat:1ns bw:10Gb",
		"link n2→n2 with lat:1ns bw:10Gb\n",
		"paths (\n   n1→n1\n   n2→n2\n)\n",
		"network Ethernet",
		"link n1→n2 with lat:100µs bw:2Mb\n\n",
	}, "\n"), string(bs))
}

func TestFmtTextFormatAsgmts(t *testing.T) {
	text := fmtText{}
	asgmt, err := textformat.Parse("src", `
		workload assignment v1
		model M
		application a workload w1
		node n1,n2 provides ram:2GiB
		assignment workload a•w1 on n1
		assignment workload a•w1 on n2`)
	require.NoError(t, err)
	model := &model{
		name:   asgmt.Name,
		apps:   asgmt.AppGroup,
		infra:  asgmt.Infrastructure,
		asgmts: asgmt.Assignment}
	bs, err := text.formatAsgmt(model)
	assert.NoError(t, err)
	assert.Equal(t, strings.Join([]string{
		`assignments`,
		"assignment",
		`workload a•w1 on n1`,
		`assignment`,
		"workload a•w1 on n2\n",
	}, "\n"), string(bs))
}
