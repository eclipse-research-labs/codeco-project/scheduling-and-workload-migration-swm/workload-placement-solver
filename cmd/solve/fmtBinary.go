// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"google.golang.org/protobuf/proto"
	pb "siemens.com/qos-solver/internal/port/grpc"
)

type fmtBinary struct {
	name string
}

func registerBinary() {
	f := &fmtBinary{name: "binary"}
	modelConverters[f.name] = f
	queryFormatters[f.name] = f
	asgmtFormatters[f.name] = f
}

func (f *fmtBinary) convertModel(name string, data []byte) (*model, error) {
	var qm pb.Model
	if err := proto.Unmarshal(data, &qm); err != nil {
		return nil, err
	}
	m := model{
		name:  name,
		apps:  qm.AppGroup,
		infra: qm.Infrastructure,
		costs: qm.Costs,
		recs:  qm.Recommendations}
	return &m, nil
}

func (f *fmtBinary) formatQuery(m *model) ([]byte, error) {
	M := &pb.Model{
		AppGroup:        m.apps,
		Infrastructure:  m.infra,
		Costs:           m.costs,
		Recommendations: m.recs}
	return proto.Marshal(M)
}

func (f *fmtBinary) formatAsgmt(m *model) ([]byte, error) {
	return proto.Marshal(m.asgmts[0])
}
