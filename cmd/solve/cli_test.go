// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/rogpeppe/go-internal/testscript"
	"github.com/stretchr/testify/assert"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestMain(m *testing.M) {
	os.Exit(testscript.RunMain(m, map[string]func() int{
		"solve": solveMain,
		"solveE": func() int {
			defer restore(&textformat.ImplicitReversePath, false)()
			return solveMain()
		},
		"solveI": func() int {
			defer restore(&textformat.ImplicitReversePath, true)()
			return solveMain()
		},
	}))
}

func setEnvVarFolder(env *testscript.Env) error {
	path, err := filepath.Abs("testdata/textformat")
	if err == nil {
		env.Setenv(envVarFolder, path)
	}
	return err
}

func TestOptionHandling(t *testing.T) {
	testscript.Run(t, testscript.Params{
		Dir:   "testdata/cmd/solve/cliOptions",
		Setup: setEnvVarFolder,
	})
}

func TestArgumentHandling(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	testscript.Run(t, testscript.Params{
		Dir: "testdata/cmd/solve/argHandling",
	})
}

func TestBuiltIns(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	testscript.Run(t, testscript.Params{
		Dir:   "testdata/cmd/solve/builtIns",
		Setup: setEnvVarFolder,
	})
}

func TestOutputFormats(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	testscript.Run(t, testscript.Params{
		Dir: "testdata/cmd/solve/outputFormats",
	})
}

func TestInputData(t *testing.T) {
	testscript.Run(t, testscript.Params{
		Dir: "testdata/cmd/solve/inputData",
	})
}

func TestMarkdown(t *testing.T) {
	testscript.Run(t, testscript.Params{
		Dir:   "testdata/cmd/solve/markdown",
		Setup: setEnvVarFolder,
	})
}

func TestRegression(t *testing.T) {
	testscript.Run(t, testscript.Params{
		Dir:   "testdata/cmd/solve/regression",
		Setup: setEnvVarFolder,
	})
}

func restore[T any](who *T, value T) func() {
	store := *who
	*who = value
	return func() {
		*who = store
	}
}

func TestParseFlagFormat(t *testing.T) {
	type result struct {
		in, out string
		code    int
	}
	tests := []struct {
		flag   string
		result result
		errMsg string
	}{
		{" binary ", result{"binary", "binary", 0}, ""},
		{" binary , text ", result{"binary", "text", 0}, ""},
		{"summary", result{"summary", "summary", 0}, ""},
		{"", result{"", "", 1}, "empty format"},
		{"unknown", result{"", "", 1}, "illegal format: unknown"},
		{"unknown,text", result{"", "", 1}, "illegal format: unknown"},
		{"text,unknown", result{"", "", 1}, "illegal format: unknown"}}
	registerFormats()
	for _, tc := range tests {
		var buf bytes.Buffer
		defer restore(&stderr, func(format string, a ...interface{}) {
			fmt.Fprintf(&buf, format, a...)
		})()
		var r result
		r.in, r.out, r.code = parseFlagFormat(tc.flag)
		assert.Equal(t, tc.result, r, "flag: %q", tc.flag)
		assert.Contains(t, buf.String(), tc.errMsg)
	}
}
