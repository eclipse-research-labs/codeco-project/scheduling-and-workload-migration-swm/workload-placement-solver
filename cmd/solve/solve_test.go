// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestNameToArgs(t *testing.T) {
	assert := assert.New(t)
	name, args := nameToArgs("name")
	assert.Equal("name", name)
	assert.Equal(([]string)(nil), args)

	name, args = nameToArgs("name(eins)")
	assert.Equal("name", name)
	assert.Equal([]string{"eins"}, args)

	name, args = nameToArgs("name(eins, zwei , drei)")
	assert.Equal("name", name)
	assert.Equal([]string{"eins", "zwei", "drei"}, args)
}

func TestReadModel(t *testing.T) {
	registerFormats()
	path := "testdata/textformat/tiny.asgmt"
	formatIn = "text"
	opts := textformat.DefaultModelOptions()
	assert, require := assert.New(t), require.New(t)
	m, err := readModel(path)
	require.NoError(err)
	assert.Equal(opts, m.opts)
	assert.Len(m.apps.Applications, 1)

	defer restore(&textformat.ImplicitReversePath, true)()
	opts.ImplicitReversePath = true
	m, err = readModel(path)
	require.NoError(err)
	assert.Equal(opts, m.opts)
	assert.Len(m.apps.Applications, 1)

	m, err = readModel("unknown")
	assert.ErrorContains(err, "no such file or directory")
	assert.Nil(m)
}

func TestBuiltInModel(t *testing.T) {
	opts := textformat.DefaultModelOptions()
	assert := assert.New(t)
	m, err := builtInModel("multiple-apps")
	assert.Equal(opts, m.opts)
	assert.NoError(err)
	assert.Len(m.apps.Applications, 5)

	defer restore(&textformat.ImplicitReversePath, true)()
	opts.ImplicitReversePath = true
	m, err = builtInModel("multiple-apps")
	require.NoError(t, err)
	assert.Equal(opts, m.opts)
	assert.Len(m.apps.Applications, 5)

	m, err = builtInModel("unknown")
	assert.ErrorContains(err, "unknown model: unknown")
	assert.Nil(m)
}
