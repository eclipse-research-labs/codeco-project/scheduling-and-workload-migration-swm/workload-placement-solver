// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"runtime/pprof"
	"strings"
	"unicode/utf8"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data"
	"siemens.com/qos-solver/pkg/solve"
	"siemens.com/qos-solver/pkg/textformat"
)

var toolName string = "solve"
var toolVersion string = "0.0dev"

type printer func(string, ...interface{})

var stdout printer = func(format string, a ...interface{}) {
	fmt.Fprintf(os.Stdout, format, a...)
}
var stderr printer = func(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
}

type modelConverter interface {
	convertModel(string, []byte) (*model, error)
}
type queryFormatter interface{ formatQuery(*model) ([]byte, error) }
type asgmtFormatter interface{ formatAsgmt(*model) ([]byte, error) }

var modelConverters map[string]modelConverter = make(map[string]modelConverter)
var queryFormatters map[string]queryFormatter = make(map[string]queryFormatter)
var asgmtFormatters map[string]asgmtFormatter = make(map[string]asgmtFormatter)

func showBuiltIns() {
	ms, w1, w2 := data.ListModels(), 0, 0
	for i := range ms {
		if l := utf8.RuneCountInString(ms[i].Name); l > w1 {
			w1 = l
		}
		if l := utf8.RuneCountInString(ms[i].Note); l > w2 {
			w2 = l
		}
	}
	stdout("\n %-*s │ %s\n", w1, "Name", "Note")
	stdout("%s┼%s\n", strings.Repeat("─", w1+2), strings.Repeat("─", w2+2))
	for _, m := range ms {
		stdout(" %-*s │ %s\n", w1, m.Name, m.Note)
	}
	stdout("\n")
}

type model struct {
	name   string
	opts   textformat.ModelOptions
	apps   *pb.ApplicationGroup
	infra  *pb.Infrastructure
	costs  *pb.Cost
	recs   *pb.Recommendation
	asgmts []*pb.Assignment
}

func nameToArgs(name string) (string, []string) {
	var args []string
	nameWithArgs := regexp.MustCompile(`^([a-zA-Z0-9_-]+)\((.+)\)$`)
	if ps := nameWithArgs.FindStringSubmatch(name); ps != nil {
		name = ps[1]
		if as := strings.Split(ps[2], ","); len(as) > 0 {
			args = make([]string, len(as))
			for i, a := range as {
				args[i] = strings.TrimSpace(a)
			}
		}
	}
	return name, args
}

func readModel(name string) (m *model, err error) {
	var data []byte
	if name == "-" {
		data, err = io.ReadAll(bufio.NewReader(os.Stdin))
	} else {
		data, err = os.ReadFile(name)
		if os.IsNotExist(err) && !path.IsAbs(name) {
			dirs := strings.Split(flagFolder, string(filepath.ListSeparator))
			for i := 0; i < len(dirs) && os.IsNotExist(err); i++ {
				data, err = os.ReadFile(path.Join(dirs[i], name))
			}
		}
	}
	if err != nil {
		return
	}
	converter, ok := modelConverters[formatIn]
	if !ok {
		err = fmt.Errorf("unsupported format: %s", formatIn)
		return
	}
	m, err = converter.convertModel(name, data)
	return
}

func builtInModel(name string) (*model, error) {
	name, args := nameToArgs(name)
	if flagShowSource {
		text := data.GetSource(name, args...)
		if text == "" {
			return nil, fmt.Errorf("unknown model: %s", name)
		}
		if showFileSep {
			stdout("-- %s --\n", name)
		}
		stdout("%s", text)
		return nil, nil
	}
	m := data.GetModel(name, args...)
	if m == nil {
		return nil, fmt.Errorf("unknown model: %s", name)
	}
	bm := &model{
		name:  name,
		opts:  textformat.DefaultModelOptions(),
		apps:  m.AppGroup,
		infra: m.Infrastructure,
		costs: m.Costs,
		recs:  m.Recommendations}
	return bm, nil
}

func displayStatistics() {
	if flagShowStats {
		stats := solve.Statistics()
		stdout("Statistics:\nCombinations: %d\nLink Checks: %d\n",
			stats.Combinations, stats.CheckLinks)
	}
}

func outputData(data []byte) (err error) {
	if flagOutput == "-" || flagOutput == "" {
		_, err = os.Stdout.Write(data)
	} else {
		err = os.WriteFile(flagOutput, data, 0644)
	}
	return
}

func outputQuery(m *model) (err error) {
	var out []byte
	if out, err = queryFormatters[formatOut].formatQuery(m); err == nil {
		err = outputData(out)
	}
	return
}

func queryAssignment(m *model) (err error) {
	conn, err := grpc.Dial(flagServer,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return
	}
	defer conn.Close()

	client := pb.NewSchedulerClient(conn)
	qm := &pb.Request{Model: &pb.Model{
		AppGroup:        m.apps,
		Infrastructure:  m.infra,
		Recommendations: m.recs}}
	stream, err := client.Schedule(context.Background(), qm)
	if err != nil {
		return
	}
	for {
		var reply *pb.Reply
		reply, err = stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			return
		}
		if len(reply.Errors) > 0 {
			return errors.New(reply.Errors[0].Details)
		}
		m.asgmts = []*pb.Assignment{reply.Assignment}
	}
	if m.asgmts == nil {
		return errors.New("no assignment returned")
	}
	return nil
}

func computeAssignment(m *model) (err error) {
	for i := 1; err == nil && i <= flagRepeat; i++ {
		var asgmt *pb.Assignment
		model := pb.Model{
			AppGroup:        m.apps,
			Infrastructure:  m.infra,
			Costs:           m.costs,
			Recommendations: m.recs}
		if asgmt, err = solve.Solve(&model); err == nil {
			m.asgmts = []*pb.Assignment{asgmt}
		}
	}
	return
}

func process(name string) (err error) {
	var m *model
	if m, err = readModel(name); os.IsNotExist(err) {
		if m, err = builtInModel(name); err != nil {
			return
		}
	} else if err != nil {
		return
	}
	if flagCheckOnly || flagShowSource {
		return
	}
	if flagQueryOnly {
		return outputQuery(m)
	}
	if flagServer != "" {
		err = queryAssignment(m)
	} else {
		err = computeAssignment(m)
	}
	if err == nil {
		var out []byte
		if out, err = asgmtFormatters[formatOut].formatAsgmt(m); err == nil {
			if showFileSep {
				header := fmt.Sprintf("-- %s --\n", name)
				out = append([]byte(header), out...)
			}
			err = outputData(out)
		}
		displayStatistics()
	}
	return
}

func registerFormats() {
	registerBinary()
	registerJson()
	registerSummary()
	registerText()
	registerMarkdown()
}

func solveMain() int {
	registerFormats()
	if stop, code := parseCommandLine(); stop {
		return code
	}
	if flagCpuProfile != "" {
		f, err := os.Create(flagCpuProfile)
		if err != nil {
			stderr("%s: profiling: %s\n", toolName, err)
			return 1
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	for _, arg := range flag.Args() {
		if err := process(arg); err != nil {
			stderr("%s: failure: %s\n", toolName, err.Error())
			return 1
		}
	}
	if flagMemProfile != "" {
		f, err := os.Create(flagMemProfile)
		if err != nil {
			stderr("%s: profiling: %s\n", toolName, err)
			return 1
		}
		defer f.Close()
		runtime.GC()
		if err := pprof.WriteHeapProfile(f); err != nil {
			stderr("%s: profiling: %s\n", toolName, err)
			return 1
		}
	}
	return 0
}

func main() {
	if code := solveMain(); code != 0 {
		os.Exit(code)
	}
}
