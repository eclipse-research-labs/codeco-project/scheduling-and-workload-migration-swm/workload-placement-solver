// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFmtBinaryConvertModel(t *testing.T) {
	binary := fmtBinary{}
	model, err := binary.convertModel("", []byte{255})
	assert.Error(t, err)
	assert.Nil(t, model)
}
