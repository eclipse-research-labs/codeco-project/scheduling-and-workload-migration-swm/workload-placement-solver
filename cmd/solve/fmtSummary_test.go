// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestByteSizeString(t *testing.T) {
	testCases := []struct {
		b byteSize
		s string
	}{{123, "123"},
		{1536, "1.5KiB"},
		{65536, "64KiB"},
		{2 * 1024 * 1024, "2MiB"},
		{1023 * 1024 * 1024 * 1024, "1023GiB"}}
	for _, tc := range testCases {
		assert.Equal(t, tc.s, tc.b.String())
	}
}

func TestWrapWords(t *testing.T) {
	assert.Equal(t, "", wrapWords([]string{}, 3, 10))
	assert.Equal(t, "   eins", wrapWords([]string{"eins"}, 3, 10))
	assert.Equal(t,
		"   eins,\n   zwei,\n   drei",
		wrapWords([]string{"eins", "zwei", "drei"}, 3, 10))
	assert.Equal(t,
		"   eins,\n   zweiMalZwei,\n   drei",
		wrapWords([]string{"eins", "zweiMalZwei", "drei"}, 3, 10))
}

func TestSplitChannelName(t *testing.T) {
	assert := assert.New(t)
	match, w1, w2 := splitChannelName("If_w1_w2")
	assert.True(match)
	assert.Equal("w1", w1)
	assert.Equal("w2", w2)

	match, w1, w2 = splitChannelName("If_a1_w1_a1_w2")
	assert.True(match)
	assert.Equal("a1_w1", w1)
	assert.Equal("a1_w2", w2)

	match, w1, w2 = splitChannelName("If_w1_a1_w2")
	assert.False(match)
	assert.Equal("", w1)
	assert.Equal("", w2)
}

func parse(input string) *grpc.Model {
	var model grpc.Model
	if err := textformat.Convert("varList", input, &model); err != nil {
		panic("cannot convert model: " + err.Error())
	}
	return &model
}

func TestModelFindChannel(t *testing.T) {
	check := func(compound bool, name string) {
		defer restore(&textformat.CompoundNames, compound)()
		gm := parse(`
		workload assignment v1
		model find-channel
		application a1
			workload w1
			workload w2
			channel w1→w2
		node dummy`)
		m := model{apps: gm.AppGroup}
		assert.Equal(t, 0, m.findChannel(name))
		assert.Equal(t, -1, m.findChannel("If_x_y"))
	}
	check(false, "If_w1_w2")
	check(true, "If_a1_w1_a1_w2")
}

func _MiB(n int) int64 { return int64(n) * 1024 * 1024 }
func _Mb(n int) int64  { return int64(n) * 1_000_000 }

func newTestModel() *model {
	asgmt, err := textformat.Parse("test", `
		workload assignment v1
		model test-model options compound-names:no
		application A1
			workload W1 needs cpu:1000 ram:8MiB
			workload W2 needs cpu:1000 ram:8MiB
			channel W1—W2 needs lat:10 bw:1Mb,2Mb
		application A2
			workload W3 needs cpu:1000 ram:8MiB
			workload W4 needs cpu:1000 ram:8MiB
			channel W3—W4 needs lat:10 bw:2Mb,3Mb
		node C1 provides cpu:2000 ram:16MiB
		node C2,C3 provides cpu:1000 ram:8MiB
		node N
		network net
		link C1—C2,C1—C3 with lat:1 bw:100Mb path:yes
		assignment
		workload A1•W1 on C1
		workload A1•W2 on C1
		workload A2•W3 on C2
		workload A2•W4 on C3
		channel "A1•W1→A1•W2" on net•"C1→C2"
		channel "A1•W2→A1•W1" on net•"C2→C1"
		channel "A2•W3→A2•W4" on net•"C1→C3"
		channel "A2•W4→A2•W3" on net•"C3→C1"`)
	if err != nil {
		panic("cannot parse: " + err.Error())
	}
	return &model{name: asgmt.Name,
		apps:   asgmt.AppGroup,
		infra:  asgmt.Infrastructure,
		asgmts: asgmt.Assignment}
}

func mkRsrcCPU(inUse, cap int) types.Resource[int32] {
	rsrc := types.Resource[int32]{Cap: int32(cap)}
	rsrc.Use(int32(inUse))
	return rsrc
}

func mkRsrcRAM(inUse, cap int) types.Resource[byteSize] {
	rsrc := types.Resource[byteSize]{Cap: byteSize(_MiB(cap))}
	rsrc.Use(byteSize(_MiB(inUse)))
	return rsrc
}

func TestModelNodeInfos(t *testing.T) {
	model := newTestModel()
	assert.Equal(t,
		nodeInfos{m: model, is: []nodeInfo{
			{cpu: mkRsrcCPU(2000, 2000),
				ram: mkRsrcRAM(16, 16),
				ws:  []wlIndex{{0, 0}, {0, 1}}},
			{cpu: mkRsrcCPU(1000, 1000),
				ram: mkRsrcRAM(8, 8),
				ws:  []wlIndex{{1, 0}}},
			{cpu: mkRsrcCPU(1000, 1000),
				ram: mkRsrcRAM(8, 8),
				ws:  []wlIndex{{1, 1}}},
			{cpu: types.Resource[int32]{},
				ram: mkRsrcRAM(0, 0)}}},
		model.nodeInfos())
}

func TestNodeInfosString(t *testing.T) {
	m := newTestModel()
	assert.Equal(t,
		"C1: cpu:2000/2000, ram:16MiB/16MiB, workloads: 2\n   A1•W1, A1•W2\n"+
			"C2: cpu:1000/1000, ram:8MiB/8MiB, workloads: 1\n   A2•W3\n"+
			"C3: cpu:1000/1000, ram:8MiB/8MiB, workloads: 1\n   A2•W4\n",
		m.nodeInfos().String())
}

func TestModelLinkInfos(t *testing.T) {
	bw := func(inUse, cap int) types.Resource[types.Bandwidth] {
		rsrc := types.Resource[types.Bandwidth]{Cap: types.Bandwidth(_Mb(cap))}
		rsrc.Use(types.Bandwidth(_Mb(inUse)))
		return rsrc
	}

	model := newTestModel()
	assert.Equal(t,
		linkInfos{m: model, is: []linkInfo{
			{bw: bw(1, 100), cs: []int{0}},
			{bw: bw(2, 100), cs: []int{1}},
			{bw: bw(2, 100), cs: []int{2}},
			{bw: bw(3, 100), cs: []int{3}}}},
		model.linkInfos())
}

func TestLinkInfosString(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	m := newTestModel()
	assert.Equal(t,
		"\nNetwork “net”\n"+
			"C1→C2: lat:1ns, cap:1Mb/100Mb, channels: 1\n   A1•W1→A1•W2\n"+
			"C2→C1: lat:1ns, cap:2Mb/100Mb, channels: 1\n   A1•W2→A1•W1\n"+
			"C1→C3: lat:1ns, cap:2Mb/100Mb, channels: 1\n   A2•W3→A2•W4\n"+
			"C3→C1: lat:1ns, cap:3Mb/100Mb, channels: 1\n   A2•W4→A2•W3\n",
		m.linkInfos().String())
}

func TestModelDisplayAssignment(t *testing.T) {
	check := func(flag bool) {
		defer restore(&textformat.ImplicitReversePath, flag)()
		summary := fmtSummary{}
		bs, err := summary.formatAsgmt(newTestModel())
		require.NoError(t, err)
		assert.Equal(t,
			"Node Usage\n"+
				"C1: cpu:2000/2000, ram:16MiB/16MiB, workloads: 2\n"+
				"   A1•W1, A1•W2\n"+
				"C2: cpu:1000/1000, ram:8MiB/8MiB, workloads: 1\n"+
				"   A2•W3\n"+
				"C3: cpu:1000/1000, ram:8MiB/8MiB, workloads: 1\n"+
				"   A2•W4\n"+
				"\nLink Usage\n"+
				"Network “net”\n"+
				"C1→C2: lat:1ns, cap:1Mb/100Mb, channels: 1\n"+
				"   A1•W1→A1•W2\n"+
				"C2→C1: lat:1ns, cap:2Mb/100Mb, channels: 1\n"+
				"   A1•W2→A1•W1\n"+
				"C1→C3: lat:1ns, cap:2Mb/100Mb, channels: 1\n"+
				"   A2•W3→A2•W4\n"+
				"C3→C1: lat:1ns, cap:3Mb/100Mb, channels: 1\n"+
				"   A2•W4→A2•W3\n",
			string(bs))
	}
	check(false)
	check(true)
}

func TestModelDisplayAssignment_multiple(t *testing.T) {
	model, summary := newTestModel(), fmtSummary{}
	model.asgmts = append(model.asgmts, model.asgmts[0], model.asgmts[0])
	bs, err := summary.formatAsgmt(model)
	require.NoError(t, err)
	asgmtStr := "Node Usage\n" +
		"C1: cpu:2000/2000, ram:16MiB/16MiB, workloads: 2\n" +
		"   A1•W1, A1•W2\n" +
		"C2: cpu:1000/1000, ram:8MiB/8MiB, workloads: 1\n" +
		"   A2•W3\n" +
		"C3: cpu:1000/1000, ram:8MiB/8MiB, workloads: 1\n" +
		"   A2•W4\n" +
		"\nLink Usage\n" +
		"Network “net”\n" +
		"C1→C2: lat:1ns, cap:1Mb/100Mb, channels: 1\n" +
		"   A1•W1→A1•W2\n" +
		"C2→C1: lat:1ns, cap:2Mb/100Mb, channels: 1\n" +
		"   A1•W2→A1•W1\n" +
		"C1→C3: lat:1ns, cap:2Mb/100Mb, channels: 1\n" +
		"   A2•W3→A2•W4\n" +
		"C3→C1: lat:1ns, cap:3Mb/100Mb, channels: 1\n" +
		"   A2•W4→A2•W3\n"
	assert.Equal(t,
		"Assignment 1\n"+asgmtStr+
			"\nAssignment 2\n"+asgmtStr+
			"\nAssignment 3\n"+asgmtStr,
		string(bs))
}
