// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"
	"time"

	pb "siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

type aName string

var plainName *regexp.Regexp = regexp.MustCompile(`^[a-zA-Z0-9.-]+$`)

func (n aName) String() string {
	if plainName.MatchString(string(n)) {
		return string(n)
	}
	return `"` + string(n) + `"`
}

type fmtText struct {
	name string
}

func registerText() {
	f := &fmtText{name: "text"}
	modelConverters[f.name] = f
	queryFormatters[f.name] = f
	asgmtFormatters[f.name] = f
}

func (f *fmtText) convertModel(name string, data []byte) (*model, error) {
	if flagShowSource {
		if showFileSep {
			stdout("-- %s --\n", name)
		}
		stdout("%s", data)
		return nil, nil
	}
	asgmt, err := textformat.Parse(name, string(data))
	if err != nil {
		return nil, err
	}
	m := model{
		name:  name,
		opts:  asgmt.Options,
		apps:  asgmt.AppGroup,
		infra: asgmt.Infrastructure,
		costs: asgmt.Costs,
		recs:  asgmt.Recommendations}
	return &m, nil
}

var appPrefix *regexp.Regexp = regexp.MustCompile(`^[^_]+_`)

func stripAppName(name string) string {
	if textformat.CompoundNames {
		name = appPrefix.ReplaceAllString(name, "")
	}
	return name
}

func (f *fmtText) formatWorkload(w *pb.Workload) string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "workload %s needs cpu:%d ram:%s",
		aName(stripAppName(w.Id)), w.CPU, byteSize(w.Memory))
	if len(w.RequiredLabels) > 0 || len(w.ForbiddenLabels) > 0 {
		buf.WriteString(" labels:")
		sep := '('
		for _, l := range w.RequiredLabels {
			fmt.Fprintf(&buf, "%c%s", sep, aName(l))
			sep = ' '
		}
		for _, l := range w.ForbiddenLabels {
			fmt.Fprintf(&buf, "%c~%s", sep, aName(l))
			sep = ' '
		}
		buf.WriteRune(')')
	}
	return buf.String()
}

func (f *fmtText) formatApps(as []*pb.Application) string {
	var buf bytes.Buffer
	buf.WriteString("applications\n")
	for i, a := range as {
		if i > 0 {
			buf.WriteString("\n")
		}
		fmt.Fprintf(&buf, "application %s\n", aName(a.Id))
		for _, w := range a.Workloads {
			fmt.Fprintln(&buf, f.formatWorkload(w))
		}
	}
	return buf.String()
}

func (f *fmtText) formatChannels(cs []*pb.Channel) string {
	var buf bytes.Buffer
	for _, c := range cs {
		fmt.Fprintf(&buf, "channel %s•%s→%s•%s needs lat:%s bw:%s\n",
			aName(c.SourceApplication), aName(stripAppName(c.SourceWorkload)),
			aName(c.TargetApplication), aName(stripAppName(c.TargetWorkload)),
			time.Duration(c.Latency), types.Bandwidth(c.Bandwidth))
	}
	return buf.String()
}

func (f *fmtText) formatNode(n *pb.Node) string {
	if n.NodeType == pb.Node_NETWORK {
		return fmt.Sprintf("node %s", aName(n.Id))
	}
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "node %s provides cpu:%d",
		aName(n.Id), n.CPU.Capacity)
	if n.CPU.Used > 0 {
		fmt.Fprintf(&buf, "-%d", n.CPU.Used)
	}
	fmt.Fprintf(&buf, " ram:%s", byteSize(n.Memory.Capacity))
	if n.Memory.Used > 0 {
		fmt.Fprintf(&buf, "-%s", byteSize(n.Memory.Used))
	}
	if len(n.Labels) > 0 {
		buf.WriteString(" labels:")
		sep := '('
		for _, l := range n.Labels {
			fmt.Fprintf(&buf, "%c%s", sep, aName(l))
			sep = ' '
		}
		buf.WriteRune(')')
	}
	return buf.String()
}

func (f *fmtText) formatLinks(infra *pb.Infrastructure, net int) string {
	if infra == nil || net < 0 || net >= len(infra.Networks) {
		return ""
	}
	var buf bytes.Buffer
	nw := infra.Networks[net]
	for _, l := range nw.Links {
		if m := infra.FindMedium(l.Medium); m != nil {
			fmt.Fprintf(&buf, "link %s→%s with lat:%s bw:%s\n",
				aName(l.Source), aName(l.Target), time.Duration(l.Latency),
				types.Bandwidth(m.Bandwidth.Capacity))
		}
	}
	return buf.String()
}

func (f *fmtText) linksToNodes(net *pb.Network, ls []string) string {
	ns := make([]string, len(ls)+1)
	var l *pb.Network_Link
	for i, ln := range ls {
		if j := findLinkByName(net, ln); j >= 0 {
			l = net.Links[j]
			ns[i] = aName(l.Source).String()
		} else {
			ns[i] = "?"
		}
	}
	if l != nil {
		ns[len(ns)-1] = aName(l.Target).String()
	} else {
		ns[len(ns)-1] = "?"
	}
	return strings.Join(ns, "→")
}

func (f *fmtText) formatPaths(net *pb.Network) string {
	var buf bytes.Buffer
	ps := net.Paths
	switch {
	case len(ps) == 0:
		// nothing to do
	case len(ps) == 1:
		fmt.Fprintf(&buf, "paths (%s)", f.linksToNodes(net, ps[0].Links))
	default:
		buf.WriteString("paths (\n")
		for _, p := range ps {
			fmt.Fprintf(&buf, "   %s\n", f.linksToNodes(net, p.Links))
		}
		buf.WriteString(")")
	}
	return buf.String()
}

func (f *fmtText) formatQuery(m *model) ([]byte, error) {
	var buf bytes.Buffer
	fmt.Fprintf(&buf,
		"workload assignment v1\nmodel %s options %s\n\n",
		m.name, m.opts)
	fmt.Fprintln(&buf, f.formatApps(m.apps.Applications))
	if str := f.formatChannels(m.apps.Channels); str != "" {
		fmt.Fprintln(&buf, str)
	}
	fmt.Fprintf(&buf, "infrastructure\n")
	for _, n := range m.infra.Nodes {
		fmt.Fprintln(&buf, f.formatNode(n))
	}
	for i, net := range m.infra.Networks {
		fmt.Fprintf(&buf, "\nnetwork %s\n", aName(net.Id))
		fmt.Fprintln(&buf, f.formatLinks(m.infra, i))
		if str := f.formatPaths(m.infra.Networks[i]); str != "" {
			fmt.Fprintln(&buf, str)
		}
	}
	return buf.Bytes(), nil
}

func (f *fmtText) formatAsgmt(m *model) ([]byte, error) {
	var buf bytes.Buffer
	if len(m.asgmts) > 1 {
		buf.WriteString("assignments\n")
	}
	for _, asgmt := range m.asgmts {
		buf.WriteString("assignment\n")
		for _, wa := range asgmt.Workloads {
			fmt.Fprintf(&buf, "workload %s•%s on %s\n",
				wa.Application, wa.Workload, wa.Node)
		}
		for _, ca := range asgmt.Channels {
			fmt.Fprintf(&buf, "channel %s on %s\n", ca.Channel, ca.Path)
		}
	}
	return buf.Bytes(), nil
}
