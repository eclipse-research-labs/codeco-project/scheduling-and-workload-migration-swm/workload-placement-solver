// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"unicode"

	"siemens.com/qos-solver/internal/types"
)

const envVarServer string = "SOLVE_SERVER"
const envVarFolder string = "SOLVE_FOLDER"

const defaultFormat string = "text,summary"

var ( // command line flags and options
	flagVersion     bool
	flagHelp        bool
	flagList        bool
	flagShowStats   bool
	flagShowSource  bool
	flagQueryOnly   bool
	flagCheckOnly   bool
	flagShowPercent bool
	flagRepeat      int
	flagFormat      string
	flagOutput      string
	flagServer      string
	flagCpuProfile  string
	flagMemProfile  string
	flagFolder      string
)
var ( // derived variables
	formatIn    string
	formatOut   string
	showFileSep bool
)

func init() {
	flag.BoolVar(&flagVersion, "version", false,
		"Show version and exit.")
	flag.BoolVar(&flagHelp, "help", false,
		"Show this help text and exit.")
	flag.BoolVar(&flagList, "list", false,
		"Show built-in models and exit.")
	flag.BoolVar(&flagShowStats, "stats", false,
		"Show some statistics about solving process.")
	flag.BoolVar(&flagShowSource, "source", false,
		"Show text of source files.")
	flag.BoolVar(&flagQueryOnly, "query", false,
		"Prepare query data only.")
	flag.IntVar(&flagRepeat, "repeat", 1,
		"Repeat solving the given number of times")
	flag.StringVar(&flagCpuProfile, "cpu-profile", "",
		"Write CPU profile to file")
	flag.StringVar(&flagMemProfile, "mem-profile", "",
		"Write memory profile to file")
	flag.StringVar(&flagServer, "server", flagServer,
		"Address of server to use.")
	flag.StringVar(&flagFormat, "format", defaultFormat,
		"Input and output formats for data.")
	flag.StringVar(&flagOutput, "o", "",
		"Target file for marshalled data.")
	flag.StringVar(&flagFolder, "folder", "",
		"Folders for assignment files.")
	flag.BoolVar(&flagCheckOnly, "check", false,
		"Perform syntax check of input file only.")
	flag.BoolVar(&flagShowPercent, "percent", false,
		"Show resource usage in percent.")
}

func usage() {
	text := `
Usage: %s [option...] name...

Solve the problems given on the command line or in a file.
For reading and writing data supported formats are binary,
JSON, and text.  Use “-” to read from standard input.
The following flags are supported:

`
	envVarsHelp := `
The environment variables below are available to set options.
Command-line flags take precedence over environment variables.

  %s
    	Folder for assignment files
  %s
    	Address of server

`
	fmt.Fprintf(flag.CommandLine.Output(), text, toolName)
	flag.PrintDefaults()
	fmt.Fprintf(flag.CommandLine.Output(), envVarsHelp,
		envVarFolder, envVarServer)
}

func notEnoughArguments() int {
	stderr("%s: not enough arguments\n", toolName)
	stderr("Usage: %s [options...] archive ", toolName)
	return 1
}

func repeatOutOfBounds() int {
	stderr("%s: repeat number out of bounds (1..100): %d",
		toolName, flagRepeat)
	return 1
}

func emptyFormat() int {
	stderr("%s: empty format", toolName)
	return 1
}

func illegalFormat(format string) int {
	stderr("%s: illegal format: %s", toolName, format)
	return 1
}

func unsupportedFormat(format string) int {
	stderr("%s: option 'query' does not support format: %s",
		toolName, format)
	return 1
}

func parseFlagFormat(flag string) (string, string, int) {
	flag = strings.Map(
		func(r rune) rune {
			if unicode.IsSpace(r) {
				return -1
			}
			return r
		},
		strings.ToLower(flag))
	if flag == "" {
		return "", "", emptyFormat()
	}
	fs := strings.Split(flag, ",")
	for _, f := range fs {
		_, ok1 := queryFormatters[f]
		_, ok2 := asgmtFormatters[f]
		if !ok1 && !ok2 {
			return "", "", illegalFormat(f)
		}
	}
	if len(fs) == 1 {
		return fs[0], fs[0], 0
	}
	return fs[0], fs[1], 0
}

func parseCommandLine() (bool, int) {
	flag.Parse()
	if flagVersion {
		stdout("%s version %s\n", toolName, toolVersion)
	}
	if flagHelp {
		flag.CommandLine.SetOutput(os.Stdout)
		usage()
	}
	if flagList {
		showBuiltIns()
	}
	if flagVersion || flagHelp || flagList {
		return true, 0
	}
	if flagRepeat <= 0 || 100 < flagRepeat {
		return true, repeatOutOfBounds()
	}
	var code int
	if formatIn, formatOut, code = parseFlagFormat(flagFormat); code != 0 {
		return true, code
	}
	if _, ok := queryFormatters[formatIn]; flagQueryOnly && !ok {
		return true, unsupportedFormat(formatIn)
	}
	if flag.NArg() < 1 {
		return true, notEnoughArguments()
	}
	showFileSep = flag.NArg() > 1
	types.ResourceShowPercent = flagShowPercent
	if ev := os.Getenv(envVarServer); flagServer == "" && ev != "" {
		flagServer = ev
	}
	if ev := os.Getenv(envVarFolder); flagFolder == "" && ev != "" {
		flagFolder = ev
	}
	return false, 0
}
