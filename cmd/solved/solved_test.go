// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"os"
	"testing"

	"github.com/rogpeppe/go-internal/testscript"
	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(testscript.RunMain(m, map[string]func() int{
		"solved": solvedMain,
	}))
}

func TestOptionHandling(t *testing.T) {
	testscript.Run(t, testscript.Params{
		Dir: "testdata/cmd/solved/cliOptions",
	})
}

func TestSetDefaultValues(t *testing.T) {
	assert := assert.New(t)
	tests := []struct {
		varHost, varPort string
		host             string
		port             int
	}{
		{"nil", "nil", "localhost", 5000},
		{"", "", "localhost", 5000},
		{" host ", "nil", "host", 5000},
		{"host", "17", "host", 17},
		{"nil", "17", "localhost", 17},
		{"nil", "??", "localhost", 5000},
		{"h1", " h2 :17", "h2", 17},
		{"nil", "h2:17", "h2", 17},
		{"h1", ":17", "h1", 17},
		{"nil", ":17", "localhost", 17},
	}
	for _, tc := range tests {
		os.Unsetenv(envVarHost)
		os.Unsetenv(envVarPort)
		if tc.varHost != "nil" {
			os.Setenv(envVarHost, tc.varHost)
		}
		if tc.varPort != "nil" {
			os.Setenv(envVarPort, tc.varPort)
		}
		setDefaultValues()
		assert.Equal(
			tc.host, flagHost, "params: %q, %q", tc.varHost, tc.varPort)
		assert.Equal(
			tc.port, flagPort, "params: %q, %q", tc.varHost, tc.varPort)
	}
}
