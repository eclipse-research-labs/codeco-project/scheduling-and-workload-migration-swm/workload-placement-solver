// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"

	"google.golang.org/grpc"
	pb "siemens.com/qos-solver/internal/port/grpc"
)

var toolName string = "solved"
var toolVersion string = "0.0dev"

const envVarHost string = "SOLVED_HOST"
const envVarPort string = "SOLVED_PORT"

type requestLimit struct {
	set   bool
	count uint64
}

func (rl *requestLimit) String() string {
	if !rl.set {
		return "—"
	}
	return strconv.FormatUint(rl.count, 10)
}

func (rl *requestLimit) Set(v string) error {
	if n, err := strconv.ParseUint(v, 0, 64); err != nil {
		return err
	} else {
		rl.set, rl.count = true, n
		return nil
	}
}

var ( // command line flags and options
	flagVersion bool
	flagHelp    bool
	flagVerbose bool
	flagHost    string
	flagPort    int
	flagMaxReqs requestLimit
)

type printer func(string, ...interface{})

var stdout printer = func(format string, a ...interface{}) {
	fmt.Fprintf(os.Stdout, format, a...)
}
var stderr printer = func(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
}

func setDefaultValues() {
	flagHost = "localhost"
	if str := os.Getenv(envVarHost); str != "" {
		flagHost = strings.Trim(str, " \t")
	}
	flagPort = 5000
	if str := os.Getenv(envVarPort); str != "" {
		if strings.ContainsRune(str, ':') {
			ps := strings.Split(str, ":")
			if str := strings.Trim(ps[0], " \t"); str != "" {
				flagHost = str
			}
			str = ps[1]
		}
		if n, err := strconv.Atoi(str); err == nil {
			if 1 <= n && n <= 65535 {
				flagPort = n
			}
		}
	}
}

func init() {
	setDefaultValues()
	flag.BoolVar(&flagVersion, "version", false,
		"Show version and exit.")
	flag.BoolVar(&flagHelp, "help", false,
		"Show this help text and exit.")
	flag.BoolVar(&flagVerbose, "verbose", false,
		"Enable verbose logging.")
	flag.StringVar(&flagHost, "host", flagHost,
		"Address to bind server to.")
	flag.IntVar(&flagPort, "port", flagPort,
		"Port to listen for incoming requests.")
	flag.Var(&flagMaxReqs, "max-requests",
		"Maximum number request to handle.")
}

func usage() {
	text := `
Usage: %s [option...] name...

Return an assignment of workloads to nodes considering CPU, RAM,
and communication constraints (latency, bandwidth, and connectivity).
Problem specification and computed assignment are sent via gRPC.
The following options are supported:

`
	fmt.Fprintf(flag.CommandLine.Output(), text, toolName)
	flag.PrintDefaults()
}

func parseCommandLine() (bool, int) {
	flag.Parse()
	if flagVersion {
		stdout("%s version %s\n", toolName, toolVersion)
	}
	if flagHelp {
		flag.CommandLine.SetOutput(os.Stdout)
		usage()
	}
	if flagVersion || flagHelp {
		return true, 0
	}
	stdout("%s: options: verbose:%v, host:%s, port:%d, max-requests:%s\n",
		toolName, flagVerbose, flagHost, flagPort, &flagMaxReqs)
	return false, 0
}

func listenAndServe() int {
	addr := fmt.Sprintf("%s:%d", flagHost, flagPort)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		stderr("%s: failed to listen: %v", toolName, err)
		return 1
	}

	s := grpc.NewServer()
	pb.RegisterSchedulerServer(s, newAssigner(s))
	if !flagMaxReqs.set || flagMaxReqs.count > 0 {
		s.Serve(lis)
	}
	return 0
}

func solvedMain() int {
	if stop, code := parseCommandLine(); stop {
		return code
	}
	return listenAndServe()
}

func main() {
	if code := solvedMain(); code != 0 {
		os.Exit(code)
	}
}
