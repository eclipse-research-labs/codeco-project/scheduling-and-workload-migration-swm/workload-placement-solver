// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"google.golang.org/grpc"
	pb "siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/solve"
)

type assigner struct {
	pb.UnimplementedSchedulerServer
	server *grpc.Server
	count  uint64
}

func newAssigner(server *grpc.Server) *assigner {
	return &assigner{server: server}
}

func (s *assigner) Schedule(
	r *pb.Request, stream pb.Scheduler_ScheduleServer,
) (err error) {
	reply := pb.Reply{}
	if flagVerbose {
		stdout("%s\n", r)
	}
	reply.Assignment, err = solve.Solve(r.Model)
	if flagVerbose {
		if reply.Assignment != nil {
			stdout("%s\n", reply.Assignment)
		} else {
			stdout("Failure: %s\n", err.Error())
		}
	}
	if err != nil {
		reply.Errors = []*pb.Reply_Error{
			{Kind: "assigner", Details: err.Error()}}
	} else {
		reply.Feasible = true
	}
	err = stream.Send(&reply)
	if s.count++; flagMaxReqs.set && s.count >= flagMaxReqs.count {
		s.server.Stop()
	}
	return
}
