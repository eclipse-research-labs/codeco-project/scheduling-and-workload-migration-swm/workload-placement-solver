// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
	pb "siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/textformat"
)

func newTestAssigner(
	t *testing.T, ctx context.Context,
) (pb.SchedulerClient, func()) {
	t.Helper()
	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	pb.RegisterSchedulerServer(server, newAssigner(server))
	go server.Serve(listener)
	conn, err := grpc.DialContext(ctx, "",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return listener.Dial()
		}),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	require.NoError(t, err, "grpc.DialContext()")
	return pb.NewSchedulerClient(conn),
		func() {
			t.Helper()
			require.NoError(t, listener.Close(), "close listener")
			server.Stop()
		}
}

func workloadAsgmt(app, wl, node string) *pb.Assignment_Workload {
	return &pb.Assignment_Workload{
		Application: app, Workload: wl, Node: node}
}
func pathAsgmt(name, net, src, dst string) *pb.Assignment_Channel {
	return &pb.Assignment_Channel{
		Channel: name, Network: net, Path: textformat.PathName(src, dst, 1)}
}

func setUpRequest() *pb.Request {
	input := `/* two apps, two nodes*/
	workload assignment v1
	model setup
	application A1
		workload W1.1 needs cpu:100 ram:1000
		workload W1.2 needs cpu:100 ram:1000
	application A2
		workload W2.1 needs cpu:100 ram:1000
		workload W2.2 needs cpu:100 ram:1000
	channel A1•W1.1→A1•W1.2 needs lat:10 bw:1000
	channel A2•W2.1→A2•W2.2 needs lat:10 bw:1000
	channel A1•W1.1→A2•W2.2 needs lat:10 bw:500
	infrastructure
	node C1,C2 provides cpu:1000 ram:8000
	network loopback
	link C1→C1 with lat:1 bw:100000 path:yes
	network ethernet
	link C1→C2 with lat:5 bw:5000`

	var model pb.Model
	if err := textformat.Convert("setup", input, &model); err != nil {
		panic("Cannot convert model: " + err.Error())
	}
	return &pb.Request{Model: &model}
}

func getReplies(t *testing.T, data *pb.Request) []*pb.Reply {
	ctx := context.Background()
	client, closer := newTestAssigner(t, ctx)
	defer closer()

	receiver, err := client.Schedule(ctx, data)
	require.NoError(t, err)
	var replies []*pb.Reply
	for {
		reply, err := receiver.Recv()
		if errors.Is(err, io.EOF) {
			break
		}
		replies = append(replies, reply)
	}
	return replies
}

func TestAssigner_happyCase(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	data := setUpRequest()
	replies := getReplies(t, data)

	assert.Len(t, replies, 1)
	assert.Equal(t,
		&pb.Assignment{
			Workloads: []*pb.Assignment_Workload{
				workloadAsgmt("A1", "W1.1", "C1"),
				workloadAsgmt("A1", "W1.2", "C1"),
				workloadAsgmt("A2", "W2.1", "C1"),
				workloadAsgmt("A2", "W2.2", "C1")},
			Channels: []*pb.Assignment_Channel{
				pathAsgmt("A1•W1.1→A1•W1.2", "loopback", "C1", "C1"),
				pathAsgmt("A1•W1.1→A2•W2.2", "loopback", "C1", "C1"),
				pathAsgmt("A2•W2.1→A2•W2.2", "loopback", "C1", "C1")}},
		replies[0].Assignment)
	var exp []*pb.Reply_Error
	assert.Equal(t, exp, replies[0].Errors)
}

func restore[T any](who *T, value T) func() {
	store := *who
	*who = value
	return func() {
		*who = store
	}
}

func TestRestore(t *testing.T) {
	flag := false
	func() {
		defer restore(&flag, true)()
		assert.True(t, flag)
	}()
	defer assert.False(t, flag)
}

func TestAssigner_happyCase_verbose(t *testing.T) {
	defer restore(&flagVerbose, true)()
	defer restore(&textformat.CompoundNames, false)()
	var buf bytes.Buffer
	defer restore(&stdout, func(format string, a ...interface{}) {
		fmt.Fprintf(&buf, format, a...)
	})()
	data := setUpRequest()
	replies := getReplies(t, data)

	match, err := regexp.MatchString(`^.+\n.+\n$`, buf.String())
	assert.NoError(t, err)
	assert.True(t, match)

	assert.Len(t, replies, 1)
	assert.Equal(t,
		&pb.Assignment{
			Workloads: []*pb.Assignment_Workload{
				workloadAsgmt("A1", "W1.1", "C1"),
				workloadAsgmt("A1", "W1.2", "C1"),
				workloadAsgmt("A2", "W2.1", "C1"),
				workloadAsgmt("A2", "W2.2", "C1")},
			Channels: []*pb.Assignment_Channel{
				pathAsgmt("A1•W1.1→A1•W1.2", "loopback", "C1", "C1"),
				pathAsgmt("A1•W1.1→A2•W2.2", "loopback", "C1", "C1"),
				pathAsgmt("A2•W2.1→A2•W2.2", "loopback", "C1", "C1")}},
		replies[0].Assignment)
	var exp []*pb.Reply_Error
	assert.Equal(t, exp, replies[0].Errors)
}

func TestAssigner_unknownApp(t *testing.T) {
	data := setUpRequest()
	data.Model.AppGroup.Channels[0].TargetApplication = "X"
	replies := getReplies(t, data)

	assert.Len(t, replies, 1)
	var expAsgmt *pb.Assignment
	assert.Equal(t, expAsgmt, replies[0].Assignment)
	assert.Equal(t,
		[]*pb.Reply_Error{
			{Kind: "assigner", Details: "unknown application: X"}},
		replies[0].Errors)
}
