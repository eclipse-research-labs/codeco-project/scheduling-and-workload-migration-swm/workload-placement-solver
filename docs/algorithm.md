<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Solver’s Backtracking Algorithm

When a user deploys an application group in a cluster, Solver is the component
that decides which workload is assigned to which worker node.  We call the
task of placing workloads in a cluster an *assignment problem*.  This section
is about the algorithm Solver uses to find a solution to an assignment
problem.

Solver follows the principles below when assigning pods to nodes.  In contrast
to Kubernetes’ standard scheduler, Solver considers applications and not only
workloads and knows about the network within a cluster.  It is not necessary
to specify network requirements for an application.

- Solver tries to assign all workloads of all applications in an application
  group to a cluster’s worker nodes considering both computing and network
  resources.
- Solver places as many pods as possible in the cluster.  An application is
  handled in its entirety.
- Solver distributes workloads using the same rules as Kubernetes (i.e., it
  computes the same node priority).  The set of rules is simplified with
  respect to Kubernetes as Solver does not consider for instance node or pod
  affinity.
- Solver does not try to find the “best” solution for a set of application
  but finds a suitable solution (the first is encounters).

## Overview

Solver works with a set of applications each having a set of workloads.  There
are channels connecting two workloads.  This logical description of an
application is mapped to a set of (compute) nodes connected by network paths,
i.e., the physical infrastructure of a Kubernetes cluster.  A workload and a
channel specify criteria that a node or network path must support.

Solver consider for each workload the set of nodes it may be assigned to (with
respect to CPU and RAM requirements) and for each channel the set of network
paths it may use (with respect to bandwidth and latency requirements).  For
each workload and channel there is a decision variable defining the set of
options available.

There is a single list with all decision variables.  Variables for workloads
are ordered so that workloads with few options are checked first.  The
variable for a channel is inserted right after the variables for its two
workloads.  Consider two workloads A and B checked in this order.  The
variable for a channel connecting these workloads follows directly the
variable for workload B.  When the workloads were placed, Solver can select a
suitable path between the nodes.

Solver traverse the list of decision variables to find a solution.  If the
currently selected set of nodes and paths is not suitable, Solver tries
another set until a solution is found.  Solver may not be able to place all
applications is a cluster.  In this case it drops the last application and
tries again.

## Adapted Algorithm B

Algorithm B (see [Knuth, 2019]) is a basic backtracking algorithm.  This
section describes an adapted version of algorithm B assigning the workloads of
a set of applications to compute nodes while maintaining the applications’
network requirements (latency and bandwidth).

A network is defined by links between two nodes having a given latency and
bandwidth.  There may be network nodes (i.e., nodes that do not run workloads)
to build a certain topology.  A network path consists of one or more links and
connects two compute nodes.  A path’s bandwidth is the minimum bandwidth of
its links and its latency the sum of the latency of all links.  There may be
more than one path between two nodes.  A set of nodes may be connected by more
than one network.  Network paths do not span networks, i.e., all links in a
path are of the same network “type”.

The domains $D_k$ for algorithm B are subsets of the compute nodes for
workloads and subsets of the network paths for channels.  These subsets are
not static as they change (i.e., get smaller) as workloads are assigned to
nodes and channels to paths.  A property $P_l$ is fulfilled if for all nodes
the workloads assigned to the node do not exceed its capacity and for all
paths the channels using it do not exceed its bandwidth.

Visiting an assignment means that a solution was found that fulfills all
workload and channel requirements.  No further checks are necessary to accept
this solution.

The algorithm does *not* traverse all assignments.  It stops at the first
suitable solution found.  There is no optimization with respect to the number
of total workload assigned to the cluster.  If it is not possible to assign
the workloads of all applications, the algorithm reduces the set of
applications considered.

**Algorithm B** (Basic backtrack).  Given the domains $D_l$ (set of nodes
suitable for a workload and path suitable for channels between workloads) and
properties $P_l$ (all workloads or channels $W_i$ placed so far do not
exceed nodes’ or paths’ capacities), visit all sequences $x_1 x_2\dots x_n$.

1. [Initialize] Set $l ← 1$.  Initialize auxiliary data structures:
   - For all channels determine the set of possible paths. Go to step 6 if
     there is a channel without any path.
   - Determine the network requirements for all workloads.
   - For all workloads determine the set of nodes.  A node is available for a
     workload if the workload’s compute and network requirements are
     satisfied.  For each node keep the list of nodes that the workload talks
     to when assigned to this node.  Go to step 6 if there is a workload
     without nodes.
   - Sort the list of workloads with respect to the number of available nodes.
     This assigns workloads with few possible nodes early and leads to a fast
     failure.
   - Insert the variables for channels into the list of variables for workload
     so that a channel is inserted when its two workloads were placed.
2. [Enter level $l$.] (Now $P_{l-1}(x_1,\dots,x_{l-1})$ holds.) If $l > n$,
   visit $x_1 x_2 \dots x_n$ and stop if the solution is suitable or go
   to step 5.  Otherwise (i.e., $l ≤ n$) set $D_l$ to the set of nodes or
   network paths that can currently host $W_l$.  If the set of nodes or
   channels is empty, go to step 5.  Sort nodes in $D_l$ with respect to
   Kubernetes’ node priority or sort channels with respect to latency.  Set
   $j_l ← 1$ and $x_l ← D_l[j_l]$, the node with the highest priority in
   $D_l$ or the path with the lowest latency.
3. [Try $x_l$.] If $P_l(x_1,\dots,x_l)$ holds, assign $W_l$ to node
   $x_l$ or to path $x_l$, set $l ← l + 1$, and go to step 2.
4. [Try again.] Unassign $W_l$ to node or path $x_l$.  If $j_l < |D_l|$,
   set $j_l ← j_l + 1$, $x_l ← D_l[j_l]$, and go to step 3.
5. [Backtrack.] Set $l ← l - 1$.  If $l > 0$, go to step 4.
6. [Reduce problem size.] If there is more than one application, remove the
   last application, and go to step 1. (Otherwise stop without a solution). ▌

## Network Communication

A path describes how two nodes can communicate and defines bandwidth and
latency between the two nodes on this path.  If the protocol requires to
acknowledge messages there is also a path in the reverse direction needed.
The reverse path must have sufficient resources for the acknowledgement
information.  It is not necessary that the path in the reverse direction is
the reversed path.

Solver assumes that the network’s routing can be configured with respect to
the channels.  If there are two channels between two nodes, it is assumed that
the traffic moves on the appropriate channel.  If the network does not support
cannel-based routing, the assignment might not work properly as network
requirements might be violated due to “unexpected” routing.

Especially any communication in the “reverse” direction might be problematic.
For some network types it may not be possible to specify a particular path for
the reverse direction (e.g., for automatic acknowledgements) as they always
“reverse” the path of the incoming traffic.  If may be necessary to extend
Solver’s model so that different network types are supported.

This applies not only to routing of traffic but also to how a network type
makes use of its bandwidth.  Collision detection might eat some bandwidth due
to the delays it introduces.  For properly routing traffic it might be
important to know a channel’s chunk size.  Using small chunks for a given
bandwidth may decrease the available bandwidth by more than the required
bandwidth due to the delay introduced by the chunks.  Solver does not take
this into account.

## Workload Representation

A workload is currently represented by a tuple with the workload requirements:
CPU, RAM, required and forbidden labels.  This is sufficient to solve the
assignment problem, but has a problem as Solver is not able to take advantage
when there are identical workloads.  If two workloads have the same set of
requirements, they still may be different workloads as they may use different
images.

Adding an image identifier to workloads tuple allows Solver to avoid visiting
of identical combinations.  In the SEMIoTICS projects the workloads for
handling ambient information (e.g., microphone or vibration sensor) are
deployed multiple times: once for each windmill.  It is however irrelevant
which workload runs on which windmill.  Solver will try all combinations
ignoring any symmetry.
