<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

<!-- workload assignment v1 -->

# Model “annotated-example”

This is an annotated example for Solver’s text format in Markdown format,
i.e., this file can be directly passed to Solver to check for instance whether
the infrastructure supports all applications. It shows a reasonable set up for
a project but is *not* taken from an actual project. Consider a complex
application running in an inhomogeneous cluster with nodes in a DC operated by
a CSP, nodes in a “local” DC, and nodes next to some machines being supervised
or controlled.

The network shown in the figure below is defined in multiple sections. Using
multiple sections makes collaboration easier as different teams can work on
“their” section. The text format does not support using multiple files for a
single model.

![ ](images/example-network.svg)  
*Network for example application*

**Note:** The machines in the factory controlled by the application are *not*
  part of the infrastructure. There is one node per device (e.g., node M1 is
  responsible to control machine M1) that talks to the machine. The software
  on this node is deployed via Kubernetes. We assume that we cannot deploy
  software directly on a machine’s controller using Kubernetes.

## Applications

The application suite contains applications for monitoring and controlling the
machines in the factory, applications providing an interface for the user to
check status of applications, machines, and production, and applications that
provide data for statistical purposes to an analysis tool running in a CSP’s
DC. The CSP’s DC also holds a repository where the system can download new
versions of all components.

To limit the example to a reasonable size, we include only one or two services
per application. A real-world application suite might have more services per
application and include more applications.

### Application “HMI”

This application provides factory operators an interface to monitor and
control the machines in the factory. This application runs in the factory’s
local DC.

A table’s header defines the type of a set of objects and their properties.
The properties must “match” the type of the object. Property names are
converted lo lower case, i.e., column “CPU” below corresponds to property
`cpu:`.

| Workload | CPU  | RAM    | Labels |
|--------- |----- |------- |------- |
| FEnd     | 100  | 512MiB | DC     |
| BEnd     | 1000 | 1GiB   | DC     |
| DB       | 1000 | 1GiB   | DC     |

There are channels between front end and back end and between back end and
database. The front end does not access the database. All channels are
bidirectional and have names so it is easy to refer to them. If no name is
given for a channel, Solver determines a name from the names of the workloads
the channel connects (e.g., the default name of channel ch01 is
“HMI•Fend→HMI•BEnd”).

The first column in a table of channels contains the channel’s specification
as defined by production `ChSpecs` in the grammar. If there no explicit name
given, Solver creates a name for the channel from the names of the workload’s
it connects. It is OK to refer to workloads that were not yet defined. If a
certain propertyis not given in a table, Solver uses a default value.

| Channel             | Latency | Bandwidth  |
|-------------------- |-------- |----------- |
| ch01,ch02:FEnd—BEnd | 75ms    | 100KB,1MB  |
| ch03,ch04:BEnd—DB   | 25ms    | 20KB,500KB |

### Application “Monitor”

This application collects log data from different components and stores it. It
analyzes incoming data and propagates them appropriately to the HMI. This
application runs in the factory’s DC.

As the two workloads have the same needs, a single row is sufficient to
describe them.

| Workload  | CPU  | RAM     | Labels |
|---------- |----- |-------- |------- |
| Prom,Graf | 1000 | 1536MiB | DC     |

One channel connects the two logging components and another is for forwarding
“important” logging data to the HMI’s backend.

| Channel             | Latency | Bandwidth |
|-------------------- |-------- |---------- |
| ch11,ch12:Prom—Graf | 50ms    | 500KB     |
| ch13:Prom→HMI•BEnd  | 100ms   | 100KB     |

### Application “Flux”

This application is responsible to check at regular intervals for new versions
of the components used for this application. The repository with component
version is hosted in the cloud. The application runs in the factory’s DC and
the CSP’s DC. The factory fetches new versions from the repository. A new
version is never pushed into the factory.

| Workload | CPU | RAM    | Labels |
|--------- |---- |------- |------- |
| Flux     | 100 | 512MiB | DC     |
| Repo     | 100 | 512MiB | CDC    |

The latency for the channel between the two components is high as the
corresponding link represents the connection between the factory and the CSP’s
DC. This is not a single physical link but a sequence of links. The network
topology for this link is usually not known.

The channel’s bandwidth requirements are low as downloading versions should
not disturb the transfer of more important business data. When there is a
transfer, it normally uses the available bandwidth.

| Channel             | Latency | Bandwidth |
|-------------------- |-------- |---------- |
| ch31,ch32:Flux—Repo | 10s     | 100Kb,1Mb |

### Application “Analysis”

This application provides analysis of aggregated data from application and
machines in the factory and determines business-relevant data. It runs in the
CSP’s DC (e.g., at AWS).

| Workload | CPU | RAM    | Labels |
|--------- |---- |------- |------- |
| UI       | 250 | 750MiB | CDC    |
| Analysis | 750 | 2GiB   | CDC    |

We specify network requirements for the application even if the network in the
CDC is not configurable and usually fast enough. The requirements are handy,
when we want to move the application to the local data center.

| Channel               | Lat  | BW        |
|---------------------- |----- |---------- |
| ch41,ch42:UI—Analysis | 50ms | 100KB,1MB |

## Infrastructure

This section defines the infrastructure supporting the services defined so
far.

| Node     | CPU  | RAM   | Labels |
|--------- |----- |------ |------- |
| C1,C2,C3 | 2000 | 8GiB  | CDC    |
| SF1      | 4000 | 16GiB | DC     |

### Network “eth”

All nodes are connected to a single network, but the links in that network
have different bandwidths.

The network options given below are the default values that we specify
explicitly only for demonstration purposes. The table with the options for a
network has to specified “immediately” after the network’s headline.

| Option | Value       |
|------- |------------ |
| qos    | best-effort |
| type   | wire        |

The network introduces a couple of network nodes. They define the topology of
the network. Network nodes can have properties similar to links.

| Node        |
|------------ |
| N1,N2,N3,N4 |

The links have different bandwidths. Links L7 and L8 resemble a “virtual” link
that represents the unknown network path in the Internet from the factory to
the CSP’s CDC. All links are named and bidirectional. If a link has no name,
Solver creates a name from the names of the nodes it connects (e.g., the
default name for link L1 is “C1→N1”).

| Link                                | Latency | Bandwidth |
|------------------------------------ |-------- |---------- |
| L1,L2:C1—N1,L3,L4:C2—N1,L5,L6:C3—N1 | 100ns   | 10Gb      |
| L7,L8:N1—N2                         | 250ms   | 50Mb      |
| L9,L10:N2—N3,L15,L16:N3—N4          | 200ns   | 1Gb       |
| L11,L12:SF1—N3                      | 300ns   | 100Mb     |

We have to define the network paths. All worker nodes defined so far can talk
to each other.

| Find Paths                             |
|--------------------------------------- |
| C1—C2 C1—C3 C1—SF1 C2—C3 C2—SF1 C3—SF1 |

### Network “lo”

Our worker nodes support a loopback interface that enables processes on the
node to talk to each other. The loopback interface is unidirectional and has a
high bandwidth. A loopback link is also a network path.

| Link                      | Latency | Bandwidth | Path |
|-------------------------- |-------- |---------- |----- |
| C1→C1,C2→C2,C3→C3,SF1→SF1 | 10ns    | 100Gb     | yes  |

**Note:** If there is no loopback interface, Solver *cannot* put workloads
  connected by a channel on the same node.

## Section “Machine Type 1”

This section defines applications and infrastructure needed for machines of
the first type. The machines themselves are not part of the infrastructure,
only the controllers that talk to them. For each machine there is an
application monitoring and controlling the machine.

### Application “M1”

This application monitors and controls machine machines of type 1. It must run
on the controller dedicated for a particular machine. In a factory there is
usually more than one machine of a particular type. The user can use a single
section for multiple machines or define one section with details about
multiple machines.

| Workload | CPU | RAM    | Labels |
|--------- |---- |------- |------- |
| M1       | 500 | 512MiB | M1     |

A machine provides operating data for further analysis to the monitoring
component. It is sent twice per second.

| Channel             | Latency | Bandwidth |
|-------------------- |-------- |---------- |
| cm1:M1→Monitor•Prom | 10ms    | 2×50KB    |

### Infrastructure

For controlling and monitoring a machine of type 1, we need a worker node
dedicated to a particular machine. If there are more machines, we define more
nodes as needed.

| Node | CPU  | RAM  | Labels |
|----- |----- |----- |------- |
| M1   | 2000 | 4GiB | M1     |

#### Network “eth” (continued)

The machine-specific worker nodes must be connected to the network.

| Link          | Latency | Bandwidth |
|-------------- |-------- |---------- |
| L15,L16:M1—N4 | 300ns   | 100Mb     |

The worker nodes can only talk to the machines in the factory’s DC.

| Find Paths |
|----------- |
| M1—SF1     |

#### Network “lo” (continued)

Worker node M1 has a loopback interface.

| Link  | Latency | Bandwidth | Path |
|------ |-------- |---------- |----- |
| M1→M1 | 10ns    | 100Gb     | yes  |

## Section “Machine Type 2”

This section defines applications and infrastructure needed for machines of
type 2. This is a different kind of machine that also needs some back end
support in the factory’s local DC in addition to the machine-specific
applications.

### Application “MAux”

This application is the back-end support needed for applications controlling
machines of type 2. If the number of machines is large, more than instance of
the application may be necessary.

| Workload | CPU  | RAM  | Labels |
|--------- |----- |----- |------- |
| App      | 2000 | 2GiB | DC     |

We do no specify channels for this application. The channels involving this
application are defined for the machine-specific applications.

### Application “M2”

This application monitors and controls machines of type 2. It must run on the
controller dedicated for a particular machine. In a factory there is usually
more than one machine of a particular type.

| Workload | CPU | RAM    | Labels |
|--------- |---- |------- |------- |
| M2       | 500 | 512MiB | M2     |

A machine provides operating data for further analysis to the monitoring
component. It is sent twice per second. The application also talks to its
supporting application “MAux”.

| Channel               | Latency | Bandwidth |
|---------------------- |-------- |---------- |
| cm31:M2→Monitor•Prom  | 10ms    | 2×50KB    |
| cm32,cm33:M2—MAux•App | 10ms    | 200KB     |

### Infrastructure

For controlling and monitoring machines of type 2, we need additional worker
nodes. There is one node dedicated to a particular machine. For the back-end
support we need an additional worker node in the factory’s DC.

| Node | CPU  | RAM  | Labels |
|----- |----- |----- |------- |
| M2   | 1000 | 2GiB | M3     |
| SF2  | 4000 | 8GiB | DC     |

#### Network “eth” (continued)

The worker nodes must be connected to the network.

| Link           | Latency | Bandwidth |
|--------------- |-------- |---------- |
| L17,L18:SF2—N3 | 300ns   | 100Mb     |
| L19,L20:M2—N4  | 300ns   | 100Mb     |

Machine-specific worker nodes can talk only to machines in the factory’s DC.
Node M1 can talk to SF2 as well. Node SF2 can talk to SF1 and the nodes in the
CSP’s DC.

| Find Paths                   |
|----------------------------- |
| M2—SF2 M2—SF1                |
| M1—SF2 M2—SF2                |
| SF2—SF1 SF2—C1 SF2—C2 SF2—C3 |

#### Network “lo” (continued)

The worker nodes also have a loopback interface.

| Link          | Latency | Bandwidth | Path |
|-------------- |-------- |---------- |----- |
| SF2→SF2,M2→M2 | 10ns    | 100Gb     | yes  |

## Assignment

This section shows the solution that Solver determines for the problem defined
in the previous sections (as of version 0.3.13). In most problems there is no
section with a solution. Specifying a solution may be handy for testing and
documentation purposes. The table below shows how the application are assigned
to worker nodes.

| Workload          | on  |
|------------------ |---- |
| HMI•FEnd          | SF1 |
| HMI•BEnd          | SF2 |
| HMI•DB            | SF1 |
| Monitor•Prom      | SF2 |
| Monitor•Graf      | SF1 |
| Flux•Flux         | SF1 |
| Flux•Repo         | C1  |
| Analysis•UI       | C2  |
| Analysis•Analysis | C3  |
| M1•M1             | M1  |
| M2•M2             | M2  |
| MAux•App          | SF2 |
| M3•M3             | M3  |
| M4•M4             | M4  |
| M5•M5             | M5  |
| M6•M6             | M6  |

The table below shows which network paths the applications’ channels use.
Solver outputs paths as a sequence of links to cope with networks where there
is more than one link between two nodes.

| Channel        | on       |
|--------------- |--------- |
| ch01           | L11∘L14  |
| ch02           | L13∘L12  |
| ch03           | L13∘L12  |
| ch04           | L11∘L14  |
| ch13           | SF2→SF2  |
| ch11           | L13∘L12  |
| ch12           | L11∘L14  |
| ch31           | L11∘³L2  |
| ch32           | L1∘³L12  |
| ch41           | L3∘L6    |
| ch42           | L5∘L4    |
| cm1            | L17∘²L14 |
| cm1            | L19∘²L14 |
| cm33           | L13∘²L22 |
| MAux•App→M4•M4 | L13∘²L24 |
| MAux•App→M5•M5 | L13∘²L26 |
| MAux•App→M6•M6 | L13∘²L28 |
| cm31           | L21∘²L14 |
| cm32           | L21∘²L14 |
| cm41           | L23∘²L14 |
| cm42           | L23∘²L14 |
| cm51           | L25∘²L14 |
| cm52           | L25∘²L14 |
| cm61           | L27∘²L14 |
| cm62           | L27∘²L14 |
