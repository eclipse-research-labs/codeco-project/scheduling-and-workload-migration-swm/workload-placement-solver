<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Solver’s Pipeline

GitLab CI builds Solver’s deliverables and executes all test cases whenever someone
commits changes to the repository.  This helps the team to ensure that it is
possible to build all deliverables and required reports for Solver at any time.  A
script in Solver’s pipeline is usually only a single command executing [Solver’s Makefile](makefile.md).

## Overview

Solver’s pipeline builds all deliverables and stores them in the appropriate
place: container images are put into Solver’s registry for container images
and other deliverables are either stored as artifacts for some time or put
into Solver’s registry for packages.

The pipeline itself contains virtually no code as it builds the desired
objects running [Solver’s Makefile](makefile.md).  This eliminates a problem often found in
projects that there are two sets of build scripts: one for a developer’s local
machine and another for the pipeline.  Solver’s setup ensures that every local
build tests also building in the pipeline.

All objects the pipeline builds are specific to a branch.  If the pipeline
builds for instance a container image for branch “feat-A”, this image is
different from an image built for branch “main”, i.e., Solver’s container
registry contains one entry for branch “main” and a second one for branch
“feat-A”—even if those builds refer to the same set of sources.  All settings
for tools (e.g., version of Go) are specific to a branch.  This allows the
team to change a library’s version in a feature branch without disturbing any
other branch especially branch “main”.

In GitLab CI each job executes as a container.  This container uses a
container image with the tools needed to execute the job’s script (e.g., Go
environment).  Solver’s pipeline has two phases: one creates some images used
by the pipeline itself and the second phase creates Solver’s “real” artifacts
and deliverables.  Solver uses pipeline-specific images for two reasons:

- *Fix version of tools.* By building an image the set of tools used (e.g.,
  `sed`, `bash`, or `make` and their versions) is known and documented in
  the image’s Dockerfile.  The build does not fail due to changes in the
  environment (e.g., a different version of `make` is installed).  Switching
  to another version of a tool or library requires a change to the
  corresponding image and gets recorded in the repository.
- *Accelerate building.* By putting everything needed for a given task
  (e.g., Go with all dependencies of Solver) into an image building is
  faster as there is no need to manage a cache with files that do not change
  between builds.  When there is a change to a library version in Go, it is
  sufficient and usually faster to rebuild the container.

When building for instance Solver’s executable `solve`, the corresponding job
uses image “goenv” that provides the required Go version and has all
dependencies for Solver installed.  The build is fast as all Go packages are
available and nothing has to be installed when building.

Solver’s pipeline specifies dependencies between jobs using keyword `needs`
and does not use keyword `dependencies`.  When there is more than one runner
available for building, using keyword `needs` reduces pipeline execution time.
A job can start as soon as all jobs it depends on are ready—no matter to which
stage those jobs belong.

## Timestamps

`Make` uses timestamps to check whether a file has to be rebuilt with respect
to its prerequisites.  After the user synchronizes Solver from its repository,
`make` rebuilds everything and the user modifies the files as necessary.  A
file’s modification date changes only when the user explicitly changes it and
`make` rebuilds only those files that need rebuilding.

In the pipeline the situation is however different as pipeline jobs run in
containers and the container for each job is a “fresh” container with all
files synchronized from Solver’s repository.  When rebuilding a package this is not
a problem as all files read from the repository are “new” and `make` rebuilds
everything.

If a job depends on one more other jobs, the situation is more complex as the
set of files to consider contains files from different sources: files from the
repository and files provided by GitLab’s artifact mechanism.  The timestamps
of these files are different.  Files read from the repository have the
“current” time and files provided by the artifact mechanism have the creation
date of the archive for a job’s artifacts.  This causes problems with `make`
as `make` considers some files as “old” and tries to rebuild them.  Rebuilding
may fail as necessary tools may not be available (e.g., compiling a Go
executable in a container based on image “script”).

We solve this problem by setting the modification date of all files in Solver’s
project folder to one specific date.  This is done in a job’s `before_script`.
All files are set to the date of file “README.md” in the project’s main
folder.  After setting the modification dates, all files read from Solver’s
repository or provided to a job via GitLab’s artifact mechanism have the same
date and `make` does not rebuild any of those files.

We set the `before_script` for all jobs in special job `default`.  Its keys
are added to all jobs of a pipeline.  A job can redefine the keys provided by
job `default`.  We also export a number of variables defined for the pipeline
that are needed by the Makefile as well.

Tag “DOCKER” tells GitLab to use hosts with architecture “amd64” for Solver’s
pipeline.  This convention may not apply to all GitLab instance.  Change the
tag appropriately For other GitLab instances.

    default:
      before_script:
        - find . -type f
          | sed -e /.\\/\\.git\\//d -e "s|^\\./||"
          | xargs -n 200 touch -r README.md
        - export CI CI_DEFAULT_BRANCH CI_COMMIT_REF_SLUG CI_PIPELINE_ID
          CI_REGISTRY CI_REGISTRY_IMAGE CI_REGISTRY_USER CI_REGISTRY_PASSWORD

## Job Dependencies

Solver’s pipeline consists of several stages shown below and each stage has one or
more jobs.  The order of the stages defines when a job is executed.  By
default a job may start when all jobs of its previous stage are completed
without an error.

    stages:
      - ci-images
      - build
      - test
      - pr-images
      - documentation

A job may define artifacts (i.e., a set of files) that GitLab puts into an
archive associated with the pipeline.  When no dependencies are defined
between jobs, job artifacts collected so far are provided to *all* jobs in
later stages.  This effort can be reduced by explicitly specifying the
dependencies between jobs using keyword `dependencies`, i.e., job B needs the
artifacts of job A.

In Solver’s pipeline we use keyword `needs` instead of keyword `dependencies` to
define dependencies between jobs.  The reason is that keyword `needs` enables
execution of jobs as early as possible.  Jobs from different stages may
execute in parallel and this reduces the pipeline’s total runtime.

Solver’s pipeline has two phases: the first builds the images used by the pipeline
itself for the current branch (see section “[Images](#images)”) and the second phase
builds all executables (see section “[Executables](#executables)”) and other deliverables.
Dependencies are specified using stages between stages “script” and
“ci-contexts” and between stages “ci-images” and “build”.  Jobs in other
stages specify dependencies using keyword `needs`.

That Solver uses two mechanisms to specify dependencies between pipeline jobs has
historical and not technical reasons.  All dependencies between jobs should
eventually be specified using keyword `needs`.  With this setup stages may no
longer be needed, i.e., we have a so-called *stage-less pipeline*.

## Executables

Solver’s pipeline builds a number of executables (i.e., CLI tools).  Some of them
are deliverables intended for deployment for instance in a Kubernetes cluster
(properly [packaged as an image](#images)) and some are used by the pipeline for instance
to get results for unit-tests.  The pipeline builds the following types of
executables:

- Executables for running unit tests
- Executables for running in a Kubernetes cluster
- Executables for Windows users

The Makefile does the heavy-lifting of actually building an image, so the jobs
for built-in images are short.  We use keyword `parallel:matrix:` to group the
jobs so that there are not so many jobs visible in the pipeline.  Executables
are stored as artifacts that expired after 48 hours.  Job `test-runner` builds
executables for running unit tests.  The executables are for architecture
"amd64" and are executed only by the pipeline to determine whether all unit
tests work as expected.

    test-runner:
      extends: .image-goenv
      stage: build
      script: make bin/$PKG.test
      artifacts:
        expire_in: 48h
        name: bin/$PKG.test
        paths:
          - deliv/bin/
          - tmp/img/
      parallel:
        matrix:
          - PKG:
              - cmd-solve
              - cmd-solved
              - internal-port-grpc
              - internal-types
              - pkg-data-new
              - pkg-textformat
              - pkg-data
              - pkg-solve

The jobs below create executables for Kubernetes for architectures “amd64” and
“arm64“.  These executables are put into the images that Kubernetes later
refers to.  There is one for each target architecture.  We use one job per
architecture so that a job can be built on the “proper" architecture if the
GitLab instance supports this (see section “[GitLab Instances](#gitlab-instances)”).  This may be
more faster than building all executables on a single architecture.

However, all jobs producing executables can run in containers for a single
architecture (e.g., “amd64”) even when they build an executable for another
architecture (e.g., “arm64”).  Go’s tool chain makes it easy to compile a
package for a different architecture or platform.  Cross compilation of
packages may take a little bit longer but for Solver this is not a problem.

    .tool:
      extends: .image-goenv
      stage: build
      artifacts:
        expire_in: 48h
        name: bin/$ARCH/$PKG
        paths:
          - deliv/bin/
          - tmp/img/
      parallel:
        matrix:
          - PKG: [cmd-solve, cmd-solved]
    tool-amd64:
      extends: [.tool, .build-for-amd64]
      script: make bin/amd64/$PKG
    tool-arm64:
      extends: [.tool, .build-for-arm64]
      script: make bin/arm64/$PKG

As a convenience for some Windows users that cannot run the executables for
packages “cmd/solve” and cmd/solved”, we build native executables for Windows.
They are only available for architecture "amd64".

    tool-amd64-windows:
      extends: [.tool, .build-for-amd64]
      script: make bin/amd64-windows/$PKG

## Images

As outlined in section “[Overview](#overview)” the pipeline builds two sets of images: one
set used by the pipeline and one set for deploying Solver for instance to a
Kubernetes cluster.  All images are branch-specific.  See section “[Naming
Convention](makefile.md/#naming-convention)” for further details on names for goals and jobs.

Almost all jobs producing images run in containers for architecture “amd64”
even if they create an image for another architecture.  We can specify an
image’s target architecture for both Docker and Kaniko when building an image
and both tools can produce images for another architecture.  The only jobs
that must run on an ARM64 host are those that build images used by the
pipeline for ARM64 hosts.

This scheme works also for Solver’s production images.  They are very simple as they
contain only the corresponding executable.  The corresponding Dockerfile
contains only commands that copy files or set a container’s entry point or
command.

If we need to execute commands are executed while building the container
(i.e., we use command `RUN` in a Dockerfile), it is probably necessary to
build such images on hosts with proper architecture.  Adapting Solver’s build system
to use images for different architectures is not too complicated.

### Defining Jobs

Except for image “script” each image for Solver is built in two steps (i.e.,
jobs in the pipeline): the first step prepares the image’s context and the
second step uses that context to build the image.  This separation is due to
Kaniko.  In the pipeline we cannot use Docker to build images as running
Docker within a container is impossible—each pipeline job runs as a container.
Kaniko’s image does not contain the tools we need for executing Solver’s
Makefile and we do not want to build a special image with Kaniko for some
technical reasons.  Therefore, there are two jobs when building an image and
an image’s context is a pipeline artifact.

Image “script” is special as it is built without creating a dedicated folder
for its context first.  The pipeline uses this image for all jobs running just
some shell script (defined by Solver’s Makefile).  As there are no additional
files needed for the context, folder “build/ci/script” with the image’s source
files can be used directly for building the image.

All images for the pipeline should be built only when one of their constituent
files change.  For pipeline images some information is put into separate files
so GitLab CI can properly check whether it is necessary to rebuild the image.
Consider for instance the rules for `make` used for image “goenv” defined in
files “build/ci/goenv/MakeFile” and “build/ci/images.mk”.  If one of those
files changes, GitLab CI rebuilds image “goenv”.  If these rules would be
defined in Solver’s main Makefile, GitLab would build image “goenv” even for
an “unrelated” change like a modification of a source file or adding a new
command-line tool to Solver.

### Building Image Contexts

The commands for building an image’s context and for building the image itself
using Docker or Kaniko are defined in Solver’s Makefile.  The names of the
jobs building an image and the corresponding goals in the Makefile have the
same name so it is easy to define a job for building an image.  Consider for
instance job “cc-goenv-amd64” building the context for image “goenv” shown in
the snippet below.

There is a common part that does not change between different images kept in
hidden key “.context-builder”.  The script for building an image’s context has
two commands: one for building the context and another creating a script file
with the commands for actually building the image.  This file is also part of
the context folder and copied to job “ci-goenv-amd64” building the image via
the artifact mechanism.

We have to store the commands for building an image as a file, as `make`
creates this sequence of commands but we cannot use `make` in the container
using Kaniko.

    .context-builder:
      script: make $CI_JOB_NAME
      artifacts:
        name: $CI_JOB_NAME
        paths:
          - tmp/img/

The image-specific part in job “cc-goenv-amd64” gets the list of files to
consider to determine whether image “goenv-amd64” should be rebuilt from
hidden key “.goenv-rules”.  Using a hidden key makes reusing the list of files
a bit easier.  Keyword “needs” in a job requires that the list of files given
in keyword “rules:changes:” are identical between the two jobs.

We have to create the condition for job “cc-goenv-amd64” ourselves as the
condition constructed by using keyword “extends” is wrong.  With keyword
“extends” we get *either* the condition from hidden key “.build-for-on-amd64"
*or* the condition from hidden key “.goenv-rules”.  We need both of them as a
so-called “complex rule”, i.e., a conjunction of the two conditions.  Anchors
make it easy to reuse the conditions without duplicating them.

    .goenv-rules:
      rules:
        - &goenv-rules-changes
          changes:
            - build/images.mk
            - build/ci/goenv/Dockerfile
            - build/ci/goenv/Makefile
            - build/ci/goenv/VERSION
            - go.mod
            - go.sum
            - scripts/build-image.sh
    cc-goenv-amd64:
      extends: [.cc-image, .build-for-on-amd64]
      rules:
        - <<: [*build-for-on-amd64-if, *goenv-rules-changes]

The job above builds images for both the main branch and all feature branches.
It has to use the proper image for a branch.  Variable `VERSION_SCRIPT` holds
version of the image a job should use.  It is set with respect to the current
branch as shown in the snippet below.

    workflow:
      rules:
        - if: $CI_COMMIT_REF_SLUG == $CI_DEFAULT_BRANCH
          variables:
            VERSION_SCRIPT: "1.0.2"
        - if: $CI_COMMIT_REF_SLUG != $CI_DEFAULT_BRANCH
          variables:
            VERSION_SCRIPT: $CI_COMMIT_REF_SLUG

To use image “goenv” for a given job, we get some keys from hidden key
“.image-goenv”.  The keys set a job’s image and specify that the job depends
on the job building the image.  The latter job is optional so that the
pipeline runs even if the job is not present as there is no need to rebuild
the image.

    .image-goenv:
      image: $CI_REGISTRY_IMAGE/goenv:$VERSION_GOENV$TAG_SUFFIX
      needs:
        - job: ci-goenv
          optional: true

Building the image context for one of Solver’s executables works like building
a context for a pipeline image.  The task is simpler as there is no list of
files GitLab CI has to consider for changes. There is no dedicated job to
build the context; the job creating an executable prepares also the context
for building the image without the executable.  This makes the pipeline a bit
faster.

### Building Images

The jobs for building an image are conceptually simple as they just execute
the script from the image’s context with the commands for building the image.
As with the job for building the context there is a part common to all jobs
building images (hidden key `.image-builder`) and a small part specific to the
image.

As jobs for building an image might need to download data from the Internet,
we have to ensure that variables `http_proxy`, `https_proxy`, and `no_proxy`
are properly set for a network proxy.  If the GitLab instance uses a network
proxy, we have to set those variables properly in a container so that tools
running *in* this container can access the Internet.  If the variables are not
set and the GitLab instance uses a network proxy, downloading files from the
Internet fails.  Note that the name of the variables with the proxy
information might differ between GitLab instances.

We must use the debugging image of Kaniko as this image contains a shell.
GitLab needs a shell in the container when it runs a job.  Kaniko’s release
image does not contain a shell and can therefore not be used in a pipeline.
We have to redefine the container’s entrypoint as this is by default set to
Kaniko and GitLab needs to run a shell.

    .image-builder:
      variables:
        http_proxy: $CODE_PROXY
        https_proxy: $CODE_PROXY
        no_proxy: $CODE_NO_PROXY
      image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
      # cannot use make in Kaniko container
      script: . tmp/img/${CI_JOB_NAME#*-}/build.sh

Job “ci-goenv-amd64” actually building image “goenv-amd64” depends on job
"cc-goenv-amd64" building the image’s context.  GitLab CI requires us to set
key “rules” to the same set of files as for the job given in keyword “needs”.

    ci-goenv-amd64:
      extends: [.ci-image, .build-for-on-amd64]
      rules:
        - <<: [*build-for-on-amd64-if, *goenv-rules-changes]
      needs: [cc-goenv-amd64]

Building the image for one of Solver’s executables works like building a
pipeline image.  The job for such an image is simpler as there is no list of
files GitLab CI has to consider for changes.  The job for the image runs
whenever the pipeline builds a new version of the executable.  The jobs for
building the image for tool “cmd-solved” are shown below.

    .pi:
      extends: .image-builder
      stage: pr-images
    pi-solved-amd64: {extends: .pi, needs: ["tool-amd64: [cmd-solved]"]}

### Multi-architecture Images

Solver’s executables are available for architectures “amd64” and “arm64” both based
on Linux.  Docker (and other container run-times) support so-called
multi-architecture images (MA-images for short) that make it easier to work
with images available for different architectures.  When the user specifies
the name of an MA-image, Docker checks the image for available architectures
and systems and picks the most appropriate one.  Under the hood an MA-image is
not really an image but a registry that refers to its images (i.e., a small
JSON structure with links).

Building an MA-image is therefore one additional step after all images for the
desired target architectures were built.  We have to create the registry that
refers to the constituent images and push that into the container registry.
Note that the user can also refer to the constituent images directly as they
are also stored in the container registry.

For building an MA-image (i.e., the registry referring the constituent
images), we use tool `manifest-tool` that creates the registry referring to
the different images we want to combine.  Hidden key `.ma` contains the parts
common to all MA-images we build for Solver.  We set the variables for a
network proxy so that access to the Internet from with the container being
built is possible.

    .ma:
      extends: .image-manifest
      stage: pr-images
      script: make $CI_JOB_NAME

Using hidden key `.ma`, it is simple to build an MA-image referring to some
other images.  The jobs producing the constituent images are listed in keyword
`needs:`.  The MA-image is only built, when all constituent images build
without an error.  The [Makefile](makefile.md) issues the actual commands for creating the
MA-image.

    pi-solve:
      extends: .ma
      needs:
        - job: ci-manifest
          optional: true
        - pi-solve-amd64
        - pi-solve-arm64
    pi-solved:
      extends: .ma
      needs:
        - job: ci-manifest
          optional: true
        - pi-solved-amd64
        - pi-solved-arm64

Note that *no* artifacts are copied between jobs when jobs produce images or
MA-images.  All images are written directly to the container registry.  The
dependency between jobs just defines that the constituent images must all
build successfully before the MA-image referring to them is added to the
registry.

## Documentation

Folder “docs” in Solver’s project folder contains some documents describing
different aspects of Solver.  The pipeline has a job to create EPUB document
“solver.epub” with Solver’s documentation for easy off-line reading.  Job “epub” for
building this document is shown below.  It uses the Makefile to create the
file and stores it as an artifact with the pipeline.

The job depends on job `ci-image-docenv-amd64` to provide image “docenv-amd64”
containing the tools needed to create the EPUB document.  Images are only
rebuilt when one of their constituent files changes (see section “[Building
Image Contexts](#building-image-contexts)”) so the dependency to the image-building job is optional.
If the image is not built, the job has no dependency and may execute as soon
as possible even if it is part of stage “documentation” that is a “late”
stage.

    epub:
      extends: .image-docenv
      stage: documentation
      script: make $CI_JOB_NAME
      artifacts:
        name: $CI_JOB_NAME
        paths:
          - deliv/docs/

## GitLab Instances

For historical reasons Solver’s pipeline supports more than one GitLab instance:
[code.siemens.com](https://code.siemens.com/itp_cloud_research/qos-solver.git) (CSC) and [gitlab.eclipse.org](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/scheduling-and-workload-migration-swm/workload-placement-solver.git) (GEO).  The team uses the former
instance for development and updates the latter when functionality is
published for general use.  While the pipeline’s syntax is the same for both
instances, we have to address the issues below to make the pipeline work on
both instances.

- **Job tags:** The instances have different conventions for job tags.
- **Container registry:** GEO has no container registry for Solver.
- **Building for ARM:** There is no runner available for GEO with an ARM64
  architecture.

### Job Tags

On CSC a job must have a tag DOCKER to use a container runtime.  The pipeline
assigns tag DOCKER to a job to make it run on an AMD64 host and tag
DOCKER\_AARCH64 to run it on an ARM64 host.  On GEO there is no tag necessary
for jobs using the AMD64 architecture and there are no runner for ARM&4.
Keyword `tags` is present on CSC but not present on GEO.  GitLab does not
support conditional fields in a job.

Considering only CSC we would define a job’s tag using keyword `default` as it
applies to all jobs and redefine it for those jobs that need to run on an
ARM64 host.  This causes problems with GEO, so we use a second file and
keyword `include` with a condition to set the tag for CSC.  We define a job’s
default tag in file “build/ci/defaults-siemens.yml”.

    # In file “build/ci/defaults.siemens.yml”
    default:
      tags:
        - DOCKER

The pipeline includes that file when it runs on CSC.  GitLab merges the two
definitions of keyword `default` and has a definition with a default tag for
all jobs.  On GEO the file is not read and there is no default tag for a job.

    include:
      - local: build/ci/defaults-siemens.yml
        rules:
          - if: $CI_SERVER_HOST =~ /siemens/
            when: always

Some jobs need to run on an ARM64 host—especially those building images for
ARM64 (see section “[Images](#images)”).  On CSC, we must specify tag DOCKER\_AARCH64 for
jobs running on ARM64 hosts.  We use hidden keys to provide any fields
necessary for architecture-specific jobs and control whether jobs should be
executed or not.

Hidden keys `.build-for-on-amd64` and `.build-for-amd64` indiicate that a job
produces something for architecture “amd64”.  The former also needs to run on
a host with that architecture.  For each architecture we have these two hidden
keys “binding” a job to a particular architecture.

    .build-for-amd64:
      rules:
        - &build-for-amd64-if
          if: $target_archs =~ /amd64/
    .build-for-arm64:
      rules:
        - &build-for-arm64-if
          if: $target_archs =~ /arm64/
    .build-for-on-amd64:
      rules:
        - &build-for-on-amd64-if
          if: $target_archs =~ /amd64/ && $runner_archs =~ /amd64/
    .build-for-on-arm64:
      rules:
        - &build-for-on-arm64-if
          if: $target_archs =~ /arm64/ && $runner_archs =~ /arm64/

On CSC, the hidden keys above need to define also the tag needed for a host
with the proper architecture.  GitLab merges the definition below with the
global one above.  This yields the desired tag for jobs running on an ARM64
host.

    # In file “build/ci/defaults.siemens.yml”
    .build-for-amd64:
      tags:
        - DOCKER
    .build-for-on-arm64:
      tags:
        - DOCKER-AARCH64

Jobs that need to run on an ARM64 host refer to the hidden key using keyword
`extends`.  With such a “base class” we can define any additional fields in
one place.  The job below builds an image with Go for ARM64.

    ci-goenv-arm64:
      extends: [.ci-image, .build-for-on-arm64]
      rules:
        - <<: [*build-for-on-arm64-if, *goenv-rules-changes]
      needs: [cc-goenv-arm64]

### Container Registry

GEO does not provide a container registry for Solver.  We use the registry of
CODECO’s private GitLab instance for images produced on GEO.  For the time
being, we do not push images for the main branch to Docker Hub.  Pushing
release images from CODECO’s repository to Docker Hub is a manual step.

Script “script/build-image.sh” creates an image with Docker or Kaniko.  For
the pipeline it uses always Kaniko.  The script prepares a configuration file
for Kaniko that contains the credentials needed for accessing the registry.
It uses the variables CI\_REGISTRY, CI\_REGISTRY\_USER, and CI\_REGISTRY\_PASSWD
provided by the GitLab instance.

    printf '{"auths":{"%s":{"username":"%s","password":"%s"}}' \
        "${CI_REGISTRY}" "${CI_REGISTRY_USER}" \
        "${CI_REGISTRY_PASSWORD}" >/kaniko/.docker/config.json

When building on GEO, we redefine those variables so that the pipeline pushes
the images to the proper spot.  For each variable provided by GtLab, we have a
private variable for our target registry (“FI” stands for Foreign Instance).
We define these variables in the project’s settings.  We do *not* define them
directly in the pipeline as they contain sensitive information.

The password used to access the registry is usually not a password but a
project access token or a deploy token.  A user on the target instance has to
define such a token on the project or group level.  The token needs to grant
read and write access to a container registry.

    .registry: &registry
      # Define FI_* variables for project or group
      CI_REGISTRY: $FI_REGISTRY
      CI_REGISTRY_IMAGE: $FI_REGISTRY_IMAGE
      CI_REGISTRY_USER: $FI_REGISTRY_USER
      CI_REGISTRY_PASSWD: $FI_REGISTRY_PASSWD

The script uses variable CI\_REGISTRY\_IMAGE when it builds an image’s tags.
The tag contains the registry’s location (i.e., a DNS name).

    /kaniko/executor -c . ${platform} ${KFLAGS} \
        $(buildArgs "${5}" "${BUILD_ARGS}") \
        $(imageTags -d "${CI_REGISTRY_IMAGE}/${2}" "${3}") \
        --image-name-tag-with-digest-file iid.txt

Instead of a container registry on another GitLab instance, we could also use
Harbour as a registry for the pipeline on GEO.  We do not do this as it
requires us to set up and maintain a machine with Harbour.  For the time
being, this is not an option.

### Building for ARM64

GEO does not support runners for architecture “arm64”.  It is not possible to
build images for that architecture if the image’s Dockerfile contains `RUN`
commands.  Jobs for architecture “arm64” therefore should be conditional and
be part of the pipeline only if there are appropriate runners available.
Similarly we might want to control the set of target architectures, i.e., for
which architecture(s) the pipeline should buld images.  We use two variables
`runner_archs` and `target_archs` to specify the architectures available and
desired.

    runner_archs:
      description: "Architectures of runners (default: “amd64,arm64”)."
      value: "amd64,arm64"
      options:
        - "amd64"
        - "arm64"
        - "amd64,arm64"
    target_archs:
      description: "Architectures to build (default: “amd64,arm64”)."
      value: "amd64,arm64"
      options:
        - "amd64"
        - "arm64"
        - "amd64,arm64"

Using these variables, it is simple to “bind” a job builds to an architecture.
Hidden keys `.build-for-arm64` and `.build-for-on-arm64` (see also section
“[Job Tags](#job-tags)”) indicate that a job produces something for architecture “amd64”
and that a job must run on a runner with architecture “amd64” and produces
something for it.

    .build-for-arm64:
      rules:
        - &build-for-arm64-if
          if: $target_archs =~ /arm64/
    .build-for-on-arm64:
      rules:
        - &build-for-on-arm64-if
          if: $target_archs =~ /arm64/ && $runner_archs =~ /arm64/

On CSC it is necessary to specify a tag for a job.  For Solver it is possible to
run some jobs producing something for architecture “arm64” also on hosts for
architecture “amd64”.  Tag DOCKER\_AARCH64 in job `.build-for-arm64` could be
replaced by tag DOCKER.  We use the former tag to increase parallelism when
executing jobs.

GitLab combines the two job definitions and has a base job for CSC with both a
condition and a tag.

    # In file “build/ci/defaults-siemens.yml”
    .build-for-arm64:
      tags:
        - DOCKER-AARCH64
    .build-for-on-arm64:
      tags:
        - DOCKER-AARCH64

An example for a job producing something for architecture “amd64” is job
`tool-amd64`.  Keyword “extends” defines that the job produces something for
architecture “amd64” but that this job can also run on a host with a different
architecture.

    tool-amd64:
      extends: [.tool, .build-for-amd64]
      script: make bin/amd64/$PKG

An example of a job producing something for an architecture that must also run
on a host with that architecture is job `ci-script-amd64`.  This is job also
shows that fields provided via keyword “extends” sometimes needs some further
consideration.  In this case we need a complex rule consisting of a check for
an architecture *and* a check whether one of several files were changed.

    ci-script-amd64:
      extends: [.image-builder, .build-for-on-amd64]
      stage: ci-images
      rules:
        - <<: [*build-for-on-amd64-if, *script-rules-changes]
      before_script:
        - export CI
          CI_REGISTRY CI_REGISTRY_IMAGE CI_REGISTRY_USER CI_REGISTRY_PASSWORD
      script:
        - . scripts/build-image-script.sh $CI_JOB_NAME $TAG_SUFFIX
