<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Overview of Solver

Project "Solver" provides a solver for an assignment problem.  An assignment
problem is about placing workloads on nodes in a cluster considering their
compute and networking requirements.  Solver is intended to be used in a
cluster managed by Kubernetes in combination with Siemens’ QoS Scheduler.

**Note:** This section is a bit sketchy.  Stay tuned for more information.

## Sequence Diagram

The target environment for Solver is a Kubernetes cluster where Solver returns
a solution for an assignment problem to the Siemens’ QoS Scheduler.  The
figure below shows the interaction between Solver and Scheduler.  In this
setup only Solver’s component “solved” is involved.  Component “solve” is not
used.

To keep the figure simple the user talks directly with Scheduler to request a
deployment.  In a real cluster the user talks to Kubernetes’ API that reaches
out to Scheduler as appropriate.

![ ](images/seq-k8s.svg)  
*Interaction between QoS Scheduler and Solver*

In a development setting Solver’s components “solve” and “solved” are used as
shown below.  The user uses component “solve” to send requests to component
“solved” running as a background process.  In this setup the server can be
located on the same machine or on another machine.  The user installs the
command-line applications of “solve” and “solved” as appropriate.

The first part in the figure below shows how an assignment problem given in
“native” form (e.g., as a JSON file) is handled.  The users passes the file to
`solve` that sends it to `solved` via the gRPC interface.  `solved` computes
an solution for the assignment and returns the solution via the gRPC interface
to `solve` that displays it to the user.

The second part shows how `solve` handles an assignment problem given in
Solver’s special text format for assignment problems (see Solver’s [Text Format
for Assignment Problems](textformat.md) for further details).  An assignment problem is
easier to specify in this format.

If Solver talks to a server in this way the server can be running as a
background process or in a Kubernetes cluster.  The user passes the process’
address on the command-line when invoking `solve`.

![ ](images/seq-solver.svg)  
*Interaction between Solver’s components*

For testing purposes it is sometimes convenient to “just” determine a
solutions for an assignment problem.  If `solve` is not given an address of a
server to talk to, It solves the given assignment problem itself using exactly
the same algorithm as `solved`.  The figure below shows the appropriate event
sequence.

![ ](images/seq-solve-only.svg)  
*Solver computing a solution itself*

## Component Diagrams

The figure below shows how the components of Solver and Scheduler work
together.  Solver’s component “solved” provides an interface for solving
assignment problems.  Solver’s component “solve” and Scheduler use this
interface to solve an given assignment problem.  Scheduler needs to solve
assignment problems that stem from deployments a user requested.

![ ](images/components.svg)  
*Interaction between Solver’s components and Scheduler*

The figure below shows the dependencies between Solver’s packages.  Only
packages defined by Solver are shown.  Third-party packages or packages from
Go’s standard library are not shown.

![ ](images/packages.svg)  
*Dependencies between Solver’s packages*

## Deployment Diagram

The figure below shows how a deployment of Solver on a node looks like.  A
Solver’s component `solved` is a statically linked executable that includes
all libraries.  A deployment of Solver is simple as just the executable needs
to be deployed on a node.  If the node is a worker node in a Kubernetes
cluster, the deployment is simple as well, as in the container’s image only
Solver’s component `solved` is present.

When Solver component `solved` is running, it provides a gRPC interface for
solving assignment problems.  It supports deployments of applications in
Kubernetes-style (i.e., workloads are only added to the already scheduled
workloads) as well as “general” deployments where existing workloads may move
within a cluster to a new node.

![ ](images/deployment.svg)  
*Deployment of Solver on a node*
