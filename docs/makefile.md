<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Solver’s Makefile

With Go only a single command is needed to build an executable for a package.
At the first glance using `make` seems to be overkill.  In project like Solver
there is however more to do than just building a single executable.  Solver
has a Makefile performing the following tasks (among others):

- Build executables `solve` and `solved`
- Build container images for deployment in Kubernetes
- Run static code analysis and linters
- Run tests and coverage analysis
- Generate sources dynamically

[Solver’s pipeline](pipeline.md) relies on the Makefile to get rid of the problem with two
sets of scripts: one for building locally and one for building in the
pipeline.  A pipeline job in Solver just executes `make` to get things done.
If building a pipeline job’s goal works locally, it is likely that it also
works in the pipeline.  The only source of truth when it comes to building in
Solver is its Makefile.

## Role of the Makefile

Solver’s Makefile creates every deliverable be it a command-line tool, an
image for a container, or some test report.  With the Makefile developers can
locally build deliverables in the same way as Solver’s pipeline does.
Solver’s pipeline is special as its jobs “just” use the Makefile to perform
the task at hand.  The script for a job is usually just a single command
executing `make`; it is the same command a developer uses to build the
deliverable locally.

The Makefile’s goal `help` lists the goals intended for developers.  It is the
default goal, so just executing `make` shows this list as well.

    Use one of the following goals:
    
       help       Show this help text
       solver     Create all deliverables and documentation
       defaults   Update user’s settings file using
                     mode=(local|docker)
       c-images   Create images for pipeline
       tools      Build all executables
       sources    Create generated sources
       lint       Perform static checks on source code
       reuse      Check REUSE compliance
       check      Run tests and get coverage
       coverage   Get total code coverage
       p-images   Build images with executables
       epub       Produce EPUB documentation
       version    Increment version number (default: revision)
                     kind=(major|minor|revision)
                     image=(script|goenv|reuse|manifest|docenv|shellcheck)
       release    Create release from current version
       export     Push current or given release to target repository
                     target=(codeco|eclipse)
                     version=version-number (default: see VERSION)
       clean      Remove images and generated files

When a developer runs command `make` `solver`, all deliverables of Solver are
built locally.  As the same commands are used locally and in the pipeline, a
successful run of `make` “implies” that the pipeline also succeeds.  However,
there is no guarantee as the two environments are not identical.  Within the
pipeline each job runs in a container and gets a fresh copy of the project’s
sources.

The Makefile supports two modes called `local` and `docker` where the latter
makes this gap smaller.  In mode `local` the Makefile uses locally installed
tools (e.g., Go) and in mode `docker` it uses the same containers as the
pipeline for building Solver’s deliverables.  The default mode is `docker` to
make the first contact with Solver as easy as possible.  In this mode the
local machine needs only `make` and `docker` (and some common tools like
`sed`) installed for building Solver.  The command sequence below builds a
fresh copy of Solver in mode `docker`:

    git clone https://code.siemens.com/itp_cloud_research/qos-solver solver
    cd solver
    make solver

Using default mode is fine with respect to dependencies to install but has a
disadvantage when it comes to speed.  Running tools in a container takes more
time and is sometimes more inconvenient when it comes to debugging.  The
commands below switch the Makefile to mode `local` and build everything.  The
Makefile reads a user-specific file with the name `${USER}.mk` that defines
the desired mode.  This file can be used by developer to define additional
setting or goals.

The first command undoes the effects of the previously shown commands.  It
even deletes any containers built for Solver from the local image cache.  Goal
`defaults` with variable `mode` can be used to switch back to mode `docker`
later on.

    make clean
    make defaults mode=local
    make solver

Goal `clean` deletes all files `make` generates from the “real“ source files.
Sometimes it is not necessary to ditch everything but only the files created
by a special goal.  Most developer-level goals define a cleaning goal that
undoes the goal’s effect.  Goal `clean-tools` for instance deletes all files
generated for goal `tools` but leaves for instance all files generated for
goal `epub` alone.

### Using an IDE

Using the Makefile for development is not the first choice for some
developers.  They prefer to use an IDE like Visual Studio Code (VSCode for
short) to work on a project.  This is perfectly fine as an IDE’s settings are
typically stored in a file (e.g., file “.vscode/.settings” for VSCode) that
the Makefile does not consider.

Before submitting something to the repository, it is recommended to execute
the Makefile to check whether a local build still works.  When this build is
done in mode `local`, it is a fast operation.

The files used by an IDE (e.g., folder “.vscode”) are typically not part of
the repository unless the whole team uses the same IDE.  These files are
therefore excluded from the repository.  Solver’s file “.gitignore” already
excludes settings for VSCode.  For other IDEs like LiteIDE or GoLand it might
be necessary to enhance this file to ignore additional files or folders.

### Private Goals

A developer might want to add private goals to the Makefile to extend Solver’s
build process.  This can be done by putting them into the user-specific file
with the name `${USER}.mk`.  The Makefile considers any file included from
this file also for goal `help`, i.e., the developer can provide some help for
the private goals.

Private goals are handy when something should be automated that should not go
into the repository for some reason.  If there is for instance some polishing
or enhancement of the Markdown files happening on a developer machine only the
processed files should end up in the repository and not the developer’s
private scripts.

## Goal `help`

The Makefile’s goal `help` shows the list of goals intended for a developer
with some descriptive text.  The text is retrieved from the Makefile and the
mechanism applies also to a private Makefile (see section “[Private Goals](#private-goals)”) a
developer might use.

To extract the text shown in goal `help`, the Makefile is read and all lines
starting with `##` are formatted for goal `help`.  All lines are shown in the
order extracted from the file.  If a line contains “::” it is for a goal.  A
goal’s descriptive text is indented.  The following snippet shows some general
text followed by a description of goal `hello` spanning two lines.

    ## Some general text
    ##
    ## hello :: First line of description
    ##+ Second line properly indented

When the snippet above is output as part of goal `help`, it displays as shown
below.  There is no limit on the amount of text for goal `help` but it is
recommended to keep descriptions concise.

    Some general text
    
       hello      First line of description
                  Second line properly indented

Goal `help` is the Makefile’s default goal, i.e., if the user does not specify
a goal on the command line, the help text is shown.  If this is inconvenient,
it can be changed by setting variable `.DEFAULT_GOAL` to the desired default
goal in the user-specific file.

## Naming Convention

Solver follows a convention when it comes to building executables and images.
It eases defining rules for an executable or an image and building the image
in the pipeline without code clones.  When building for instance image
“solved” the following steps are done:

- Create folder “deliv/bin” for all executables of Solver.
- Create executable “cmd-solved” in folder “deliv/bin” from source files in
  folder “cmd/solved”.
- Create folder “tmp/img/solved” for the image’s context.
- Populate context folder with files from folder “build/package/solved”,
  executable “cmd-^solved”, and other files (e.g., file “.dockerignore”).
- Use Docker (or Kaniko in the pipeline) to build the image and put it into
  the appropriate repository.

We use a tool’s name in all files contributing to it and for goals and jobs in
a consistent way.  The following list shows names for files, folders^, goals,
and jobs for image “solved” containing Solver’s tool “solved”.

This regularity makes it easy to define pipeline jobs for building different
tools and images.  We can write some macros for `make` generating rules for
building an executable or image from a set of files.  Due to the syntax of
`make` these macros are not exactly easy to read but they keep Solver’s
Makefile short and clear.

- `build/package/solved`: Folder with source files for image (e.g., its
  Dockerfile)
- `cmd/solved`: Folder with tool’s Go source files
- `cmd-solved`: Goal or job for building executable
- `deliv/bin/cmd-solved`: Executable
- `pr-context-solved`: Goal or job building image context
- `pr-image-solved`: Goal or job building image with tool
- `tmp/img/solved`: Folder with image context

For images used only within the pipeline some names are different: Makefile
goals and pipeline jobs use prefix “ci-” instead of “pr-”.  Image “goenv” for
instance has goals and jobs “ci-context-goenv” and “ci-image-goenv”.  Using
different names makes the pipeline definition with respect to images more
readable.

As names of goals in the Makefile and jobs in the pipeline are related, it is
easy to define scripts for pipeline jobs that just run `make` anyway.  Using
GitLab’s variable `CI_JOB_NAME`, we can write write the definition of a job
for a tool or an image on a single line.  This conciseness enhances the
readability of the pipeline definition substantially.

## CPU Architectures

Different system use different CPUs that have different instruction sets
(e.g., architecture “amd64” or “arm64”).  Solver’s executables can be built
for more than architecture or platform.  For Solver we support architectures
“amd64” and “arm64”.  A platform like “linux/amd64” is a combination of
operating system and an instruction set.  Usually we build executables for
Linux (e.g., for platform “linux/amd64”) but executables `solve` and `solved`
are available also for Windows (architecture “amd64”) for convenience.

Usually building for architecture platform X has to be done on that platform.
Building for platform Y on platform X is called *cross-compilation* and
usually requires a complicated setup of the tool chain for platform Y on
platform X.  Not all combinations may be feasible and one has to resort to
virtual machines to provide the necessary building environments.

With Go building for platform Y on platform X is as simple as building for the
native platform.  The only difference is that Go needs to be told about the
desired target platform in environment variables `GOARCH` and `GOOS`.  Go
supports building for almost all supported platforms on every supported
platform.  This allows cross-compilation without a complicated setup.

    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/arm64,GOARCH=arm64 GOOS=linux)
    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/amd64,GOARCH=amd64 GOOS=linux)
    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/amd64-windows,GOARCH=amd64 GOOS=windows,.exe))

When considering Solver’s build system, one has to cope with two sets of
architectures: architectures of machines building things and the set of target
architectures.  For Solver a single architecture (e.g., architecture “amd64”)
is sufficient to build all deliverables for all target architectures (e.g.,
for architectures “amd64” and “arm46”).

The Makefile creates rules so that it can build a given artefact for for every
target platform.  The pipeline in GitLab CI assigns jobs for building an
artefact to runners.  It depends on the architecture of the runner and the one
of the artefact to be produce whether the job is cross-compiling or producing
something for the “native” architecture.

[Solver’s pipeline](pipeline.md) has two variables `runner-archs` and `target_archs` that
define the set of architectures of runners available and the set of
architectures for which executables should be built (see also section
“[Environment Variables](#environment-variables)”).  The Makefile uses these two variables to adapt
itself accordingly.  It uses internally variables `archs-ci` and `archs-pi` to
keep track of the set of architectures used for the machines runnning build
processes and for the set of target architectures.

    

Variable `archs-ci` is used when we build an image for the pipeline.  As a job
may be assigned to any runner, the corresponding image has to contain all
architectures in `archs-ci`.  Consider building the image with the tool
`reuse`.  For each architecture in `archs-ci` we crate an image for this
architecture and then combine all these images into a multi-arch image for
eays reference.

If all runners available on a GitLab instance have the same architecture the
multi-arch image is not needed.  Using it nevertheless makes the whole process
simpler as we do not have to cope with conditionals.

    $(call ci-images,reuse,$(VERSION_REUSE),amd64 arm64,,,$(VERSION_ALPINE))
    $(call multi-arch-image,ci,reuse,$(VERSION_REUSE),amd64 arm64)

In the same manner we build an image for a deliverable like `solve` for all
target architectures using variable `archs-pi`.  For each architecture there
is an image with the tool built for that architecture and all those images are
combined into a multi-arch image.

    $(call tool-images,solve,amd64 arm64,cmd-solve)
    $(call multi-arch-image,pi,solve,$(moduleV),amd64 arm64)

Some executables (e.g., for running unit tests) are executed by a pipeline job
and must be built for one of the architectures in variable `archs-ci`.  In
most cases `archs-ci` and `archs-pi` have one or more architectures in common.
The pipeline should be able to handle the case where `archs-ci` and `archs-pi`
have no common elements.  In this case, there are runners for instance for
architecture “ppc64” that produce dliverables for architectures “amd64” and
“arm64”.  The executables with unit-tests run in a job and need to be built
for architecture “ppc64”.

## Building Container Images

Solver uses two types of images: images used exclusively by Solver’s pipeline
and images with Solver’s executables deployed for instance to a Kubernetes
cluster.  All images are specific to the branch the corresponding build is
done for.  We use custom images in Solver’s pipeline to reduce build times.

### Files and Tags

Building an image is a two-step process: prepare the image’s context and build
the image from that context (see also section “[Naming Convention](#naming-convention)”).  The
Makefile puts all context folders in folder “tmp/img” (e.g., “tmp/img/solved”
for image “solved”).  For each image to be built, Solver uses a dedicated
folder for the image’s context and copies everything needed for the image into
that folder.  Using a dedicated folder seems to be complex at the first
glance, but makes it easy to check what goes into an image and clean up
manually without carefully considering files in various folders.

Images in feature branches have a tag based on the branch’s name (e.g.,
“solved:17-feature” for image “solved” in branch “17-feature”) and images on
the main branch have the tag with the version number (e.g., “solved:0.5.0” for
image “solved” for version 0.5.0).  The tag “latest” refers to the most recent
version of an image in the main branch.  There is only one tag of an image in
a feature branch but there are multiple tags for an image in the main branch.

For each image there is a corresponding folder in folder “build/package”
(e.g., “build/package/solved” for image “solved”) with its “source” files.
For an image there are the following files:

- File “Dockerfile”: The file used by Docker or Kaniko to build the image.
  It defines the contents of the image.
- File “jobs.yml”: The jobs in the pipeline for this image.  There is one
  job for building the context and another for building the image itself.
  An image is built only when its constituent files change.
- File “Makefile”: Rules and variables for building the corresponding image.
  Solver’s main Makefile includes this file.

There are two steps (i.e., two jobs in the pipeline) for building an image due
to Kaniko used in the pipeline.  We use the Makefile to prepare the context
for an image but Kaniko’s image does not support running `make`.  So we run
`make` in a separate job, prepare the context and the script building the
image in that job, and copy the context as an artifact to the job building the
image.  The job building the image just executes the script prepared in the
previous step.

File “jobs.yml” is used only for images that the pipeline uses.  Such images
should be built only when one of its constituent files change as building the
image may take a while.  For this check everything contributing to an image
should be put into a file that the system can check.  If one of the files
listed above changes, the image must be built.  Images with Solver’s
executables do not use that file as they need to be built every time the
pipeline produces an executable.

The following files contribute to all images built for Solver.  Solver’s main
Makefile or pipeline definition includes or uses them.

- File “scripts/build-image.sh” is a shell script using Docker or Kaniko to
  build an image.  It determines the image’s tags and creates the control
  file for Kaniko with credentials and other information.
- File “build/image-building.yml” defines hidden keys with common variables
  and scripts for the jobs building a context for an image and the image
  itself.  These hidden keys make definition of a job for a context or image
  short.
- File “build/images.mk” defines macros for `make` that ease definition of
  rules for an image.  Using macros `ci-image` or `production-image` it is
  easy to define the rules needed to prepare an image’s context or build the
  image from its context.

### Images for the Pipeline

Solver’s pipeline uses dedicated images for its jobs.  Those images usually
are based on “official” images (e.g., image “golang:1.20-alpine” for image
“goenv”) but contain additional tools and libraries Solver needs.  Building
special images avoids downloading those additional files for every build
saving a lot of resources and quite some time.

The images for the pipeline have their own version numbers different from
Solver’s version.  This reduces the number of images in the repository as the
images change not as frequently as Solver’s source code.

#### Image “script”

Some pipeline jobs “just” execute a small shell script invoking `make`.  We
use a special image for such jobs so that the version of all tools is known
and under control.  Image “script” is the first image the pipeline builds for
a branch.  The build of image “script” differs from those of the other images
as there is no dedicated job to build its context.

Image “script” uses Alpine Linux as its base and installs `make` and some
auxiliary tools needed by jobs later in the pipeline.  We use Alpine Linux to
have smaller images.

#### Image “goenv”

Solver is written in the programming language Go.  Image “goenv” contains
everything needed for building Solver’s executables using the tool chain of
Go.  We use the official image with Go’s tool chain for Alpine Linux, install
all packages and tools that Solver uses, and install also a C compiler.

The C compiler is needed for building stand-alone executables and tracking
code coverage when executing tests.  We want to build stand-alone images so
that the images for an executable like “solved” can use image “scratch” as its
base.  This reduces the size of both the images itself and the attack surface
as there are no tools (e.g., a shell like `bash`) an attacker might exploit.

When looking at the Dockerfile of image “goenv” an astute reader will notice
that the building process involves multiple images, i.e., the file is a
so-called multi-stage Dockerfile.  We do not use such multi-stage Dockerfiles
as the Makefile or the pipeline do the orchestration otherwise.  Within the
pipeline a multi-stage Dockerfile is usually not as effective as on a
developer’s machine as there are multiple hosts involved when running jobs.

The Dockerfile for image “goenv” has two stages: the first one builds some
tools we use when building Solver and the second installs the libraries and
tools needed to compile Solver itself (e.g., like the support for ProtoBuf).
This stage also adds the C compiler to the images so that the images can be
made stand-alone.

Using a separate stage for the tools that we want to use (e.g., `staticcheck`)
reduces the final images substantially.  When installing a tool, Go downloads
all libraries needed by that tool, compiles the tool, and puts the tool’s
binary into the folder for binaries.  We only need the tool’s binary and not
the libraries specific to it and the temporary files produced while building
it.  Getting rid of those files by deleting them explicitly possible but using
a second stage keeps our Dockerfile simple and lean.

There is another way of preserving files in a image: the caching mechanism
provided by GitLab.  We do not use this as it is way slower than building and
using an image.  The caching mechanism puts all file to preserve into an
archive and stores that archive.  Building and extracting that archive
(containing thousands of files in our case) takes however a lot more time than
loading an image with all files installed.

#### Image “reuse”

For compliance reasons we check using tool `reuse` whether all files are
properly marked with copyright and licensing information.  We put this tool
into a dedicated image containing also Git and `make`.  With these tools
installed we can execute the same command on a developer’s machine and in the
corresponding job in the pipeline: `make` `reuse`.

Using another image is a slight complication but the job executing `reuse`
does not need any of the other tools in image “goenv”.  Installing `reuse` in
image “goenv” is possible but it increases the size of the already large image
and when compiling Go files, we do not need `reuse`.  The dedicated image for
`reuse` is small and starts quickly when executed in the pipeline.  Keep in
mind that downloading of images is more frequent within the pipeline as there
are usually multiple runners executing a pipeline’s jobs.

#### Image “manifest”

Solver’s distribution images are so-called multi-architecture images.  Such an
image contains multiple “subimages” for instance for the architectures “amd64”
and “arm64”.  A container runtime like Docker can load the proper subimage for
the current host’s architecture.  Multi-arch images simplify working with
images as there is one image name (e.g., “cmd-solve:1.0.0”) that refers to
different images (e.g., to architectures “amd64” and “arm64”).

Technically a multi-arch image is no real image but a directory of images
(i.e., a small JSON structure).  Docker uses that directory to determine the
proper image and loads that image.

Image “manifest” contains the tools needed to create multi-arch images.  The
image is based on Alpine Linux with a few common tool like `make` installed.
After downloading the archive with the executables of tool `manifest-tool`, we
copy the proper binary to folder “/usr/bin” and delete unneeded files.

#### Image “docenv”

For off-line reading we provide Solver’s documentation in EPUB format.  An
EPUB document is an archive with some HTML documents for the different
“chapters”.  Image “docenv” contain all tool needed to create an EPUB document
from Solver’s documentation in Markdown format.  We use the tool `pandoc` to
convert Solver’s Markdown documents into a single EPUB document.

Image “docenv” contains the tools needed to create an EPUB document.  The
image is based on Alpine Linux with a few common tool like `make` installed.
We must install just tool `pandoc` to convert Markdown to HTML. No additional
tools like LaTeX are needed.

### Distribution Images

There are two images for Solver intended for deployment: image “solve” and
image “solved”.  The former has an executable for talking to a server instance
of Solver and the latter has Solver’s executable for a server.  The former
image is mainly used when working on an application’s description or
collecting statistics about Solver’s behavior with different applications.

#### Image “solve”

Image “solve” contains Solver’s executable “solve” that allows a user to send
requests to a running server instance of Solver or determine a solution for an
assignment problem using the built-in mechanism.  This mechanism is the same
used by a server instance of Solver.  The image contains only executable
“solve” and nothing else.

**Note:** For debugging and technical reasons the image is currently based on a
version of Alpine Linux.

We build two variants of this image: one for architecture “amd64” and one for
architecture “arm64”.  Both images assume Linux as the host’s operating
system.  The architecture is part of the image name, i.e., the images for
different architectures are kept in different container registries (i.e.,
image “solve-amd64” and “solve-arm64”).

#### Image “solved”

Image “solved” contains Solver’s executable “solved” that listens on a port
for incoming assignment requests, computes a solution, and returns it.  Any
data sent to the server must be in the proper ProtoBuf format.  This image is
deployed in a cluster when Solver is the component that Scheduler should use
for solving assignment problems.  The image contains only executable “solved”
and nothing else.

**Note:** For debugging and technical reasons the image is currently based on a
version of Alpine Linux.

Similar to [image “solve”](#image-solve) we build actually two images for architectures
“amd64” and “arm64” with the names “solved-amd64” and “solved-arm64”
respectively.

#### Multi-architecture Images

For Solver we want to provide images for architecture “amd64” for IPCs or
typical desktop machines and architecture “arm64” for Raspberry PIs or desktop
machines with an ARM-based processor.  We use macro `multi-arch-image` to
combine architecture-specific constituent images into an multi-architecture
image (MA-image for short).

    $(call multi-arch-image,pi,solve,$(moduleV),amd64 arm64)
    $(call multi-arch-image,pi,solved,$(moduleV),amd64 arm64)

Macro `multi-arch-image` creates a goal for the MA-image having its
constituent images as prerequisites.  It uses marco `manifest-for` to create
the registry that refers to the constituent images.  The MA-image has the same
tag as the constituent images.  If the image is build for the main branch, tag
“latest” is set to refer to the MA-image just built.

Consider for instance the image “solve” for tool `cmd-solve` with version
0.3.4.  MA-image “solve:0.3.4” refers to its constituent images
“solve-amd64:0.3.4” and “solve-arm64:0.3.4”.  All images have also the tag
“latest”, i.e., a user can refer to image “solve:0.3.4” also as “solve” or
“solve:latest”.  Docker loads the constituent image suitable for the current
host.

For a local build we do *not* create MA-images as this is not a local
operation.  Setting variable `CI` to “local” creates an MA-image also for a
local build, but the image will reside in a remote repository.

    define multi-arch-image # prefix,image,version,archs
    $(eval .PHONY: $(1)-$(2)
    $(1)-$(2):$(if $(local-build), $(foreach a,$(4),$$($(2)-$(a)F)))$\
    $(if $(and $(local-build),$(if $(CI),,t)),
    >@echo "Skip creation of multi-arch image; use CI=local.",$\
    $(call manifest-for,$(2),$(3),$(3),$(4))$(if $(main-branch),$\
    $(call manifest-for,$(2),$(3),latest,$(4)),)))
    endef

Macro `manifest-for` creates an MA-image from a set of images.  MA-images
*cannot* be created locally, i.e., an MA-image is always created with a remote
repository.  The constituent images must already be part of the remote
repository or the operation fails.  When a pipeline in GitLab CI builds
Solver, the constituent images are part of the repository already as the jobs
building the constituent images did already complete.

    define manifest-for # image,src-tag,dst-tag,archs
    $(call manifest-for/cmd,$\
        $(if $(CI_REGISTRY_IMAGE),$\
            $(CI_REGISTRY_IMAGE)/)$(1)$(if $(CI),,-$(moduleNN)),$\
        $(2),$(3),$(4))
    endef

Macro `manifest-for/cmd` uses tool `manifest-tool` `in the pipeline to create
the MA-image referring to the architecture-specific images or ~docker
manifest` locally.  We specify the list of constituent images using option
`--platform`.  A platform (e.g., “linux/amd64”) is a combination of host
operating system and architecture.  We use always “linux” as the host
operating system as our primary deployment targets are Docker and Kubernetes
which both typically run on Linux hosts.

Option `--template` defines the template used to build names of the
constituent images where `ARCH` is replaced by the actual architecture of an
image.  The template must match the constituent images’ names or creation of
the MA-image fails.

Option `--target` defines name and tag of the MA-image.  The tag is the same
the tag used for the constituent images.

When building an MA-image “locally” (i.e., variable `CI` is set to “local”),
we must ensure that all constituent images are part to the remote remote
repository before creating the MA-image.  We use command `docker push` to copy
the images to the remote repository.  When using Docker to create an MA-image,
we assume that the user is properly logged into the target repository.

For command `docker manifest create` we have to specify the target image
followed by the list of constituent images.  Flag `--insecure` disables some
checks to allow communication with the remote repository.

    define manifest-for/cmd # image,src-tag,dst-tag,archs
    $(if $(local-build),$(foreach a,$(4),
    >docker push $(1):$(2)_$(a))
    >docker manifest create --insecure $(1):$(3) $\
        $(addprefix $(1):$(2)_,$(4)),
    >manifest-tool$(if $(strip $(CI_REGISTRY_IMAGE)), $\
        --username $$$${CI_REGISTRY_USER} --password $$$${CI_REGISTRY_PASSWORD}) $\
        push from-args $\
        --platforms $(subst $(space),$(comma),$(addprefix linux/,$(4))) $\
        --template $(1):$(call image-tag,$(2),ARCH) $\
        --target $(1):$(call image-tag,$(3),))
    endef

**Note:** Technically it is *not* necessary that the MA-image refers to
constituent images with the same tag (i.e., version).  An MA-image can combine
any set of constituent images.  Elusive defects result from using different
tags so this this should be avoided.

## Building Executables

Section “[Building Container Images](#building-container-images)” describes how we build container images
for Solver.  This section is about building the executables that go into those
container images (e.g., tool `cmd-solve` for container image “solve”).  Our
general strategy is to build executables early and reuse them later (e.g., to
run an integration test or build a container image).  The Makefile builds the
executables shown below and puts them in folder “deliv/bin”.  The pipeline
uses this folder to pass them as artifacts between jobs.

- `cmd-solve`: Command-line tool for solving an assignment problems using
  either the built-in solver (based on package “pkg/solve”) or by talking to
  a server supporting the gRPC interface.
- `cmd-solve.test`: Unit tests for command-line tool `cmd-solve`.
- `cmd-solved`: Command-line tool providing a server listening on a given
  port for gRPC requests for assignment problems to solve.  Scheduler talks
  to this executable when it needs an assignment for a set of workloads.
- `cmd-solved.test`: Unit tests for command-line tool `cmd-solved`.
- `internal-port-grpc.test`: Unit tests for some auxiliary functions for the
  types generated for the ProtoBuf interface between Scheduler and Solver.
- `internal-types.test`: Unit tests for some auxiliary types used in Solver.
- `pkg-data.test`: Solver comes with a couple of models built-in for testing
  purposes.  This package has some smoke tests for the functions that return
  those models.  See the manpage of tool “solve” for how to use these
  built-in packages.
- `pkg-data-new.test`: Unit tests for auxiliary functions for generating
  data structures for the gRPC interface manually for testing purposes.
  Instead of these functions, use functions that parse Solver’s text format
  for assignments.
- `pkg-solve.test`: Unit tests for Solver core functionality, i.e.,
  determining a solution for an assignment problem considering network
  requirements.
- `pkg-textformat.test`: Unit tests for the parser converting Solver’s text
  format for assignments into data suitable for the GRPC interface.  For
  further details about the text format refer to its [grammar description](textformat.md).

For each executable there is a corresponding package (i.e., a folder) and it has
the package’s path as its name.  All packages are stripped to reduce resource
requirements.  Stripping packages makes debugging harder, but the executables
are usually not used for that purpose directly.

For tools `cmd-solve` and `cmd-solved` we build also variants for Windows as
some users are not able to run Docker or install WSL.  The tools `cmd-solve`
and `cmd-solved` for Linux do not work with Windows.

### Macros Building Executables

For Solver we build ten executables and
for each executable we need to define a goal to build it.  The recipes for
them are pretty similar so we can use a macro to define the goal for building
an executable.  The lines below define the rules for Solver’s packages.  For
each package there is one line for its set of executables.  All packages build
an executable for unit tests and packages with argument “main” build also an
executable with “ordinary” functionality.

Consider for instance package “cmd/solve” providing tool `cmd-solve`.  We set
parameter “main” as this package defines CLI tool `cmd-solve`.  We define
goals to build a native tool (e.g., tool “deliv/bin/cmd-solve”), a tool for
architecture “amd64” (e.g., tool “deliv/bin/amd64/cmd-solve”), a tool for
architecture “arm64” (e.g., tool “deliv/bin/arm64/cmd-solve”), and a tool for
Windows.

The native package is for executing the package’s executable within the
pipeline or on a developer’s local machine.  We use the executables specific
to a particular architecture when building container images for these
architectures.  We build executables for Windows as a convenience for users
that cannot use Docker for some reason.  Note that it is necessary for the
executables on Windows to have extension “.exe” or the executable does not
work properly.

    $(call tools-for,internal/port/grpc,,,$$(grpcSources))
    $(call tools-for,internal/types,,,)
    $(call tools-for,pkg/data/new,,$$(grpcSources))
    $(call tools-for,pkg/textformat,,$$(grpcSources))
    $(call tools-for,pkg/data,,$$(grpcSources))
    $(call tools-for,pkg/solve,,$$(grpcSources))
    $(call tools-for,cmd/solve,main,$$(grpcSources),,solve)
    $(call tools-for,cmd/solved,main,$$(grpcSources),,solved)

In general Go’s tool chain takes care of dependencies to other packages when
building an executable.  Go includes and rebuilds them as necessary.  This
applies also to the package with the gRPC interface.  We specify the list of
gPRC source files explicitly as `make` should rebuild them (and any executable
that depends on then) when there is a change to the gRPC definition (see
section “[Sources for gRPC Interface](#sources-for-grpc-interface)”).

Macro `tools-for` forwards its arguments to macro `tools-for/pkg` and
construct a package’s name (e.g., “cmd-solve”) from its path (e.g.,
“cmd/solve”).  Doing this makes marco `tools-for/pkg` a bit shorter and more
readable.

    define tools-for # pkg,main?,prereqs,srcs,image
    $(eval $\
    $(call tools-for/pkg,$(call pkg-name,$(1)),$(1),$(2),$(3),$(4))
    $(call tools-for/ctx,$(call pkg-name,$(1)),amd64 arm64,$(5)))
    endef

Macro `tools-for/pkg` is simple and basically forwards its arguments to macro
`tools-for/arch` that actually defines the goals for a package and one target
architecture.  If macro `tools-for/pkg` is used for a package that provides a
command-line tool, we build the tool explicitly for architectures “amd64” and
“arm64” and for Windows.  For the Windows executable we do not build an image.
We must build explicitly for architectures “amd64” and “arm64” as we build
multi-arch images (see section “[Image ’manifest’](#image-manifest)”).

Consider for instance tool `cmd-solve`.  If we build this tool using the
native Go tool chain, we store it in folder “deliv/bin” as the tool it is for
the host’s architecture which may be “amd64” or “arm64” or something else.
For multi-arch images we build the tool explicitly for architectures “amd64”
and “arm64” and put these tools in folders “deliv/bin/amd64” and
“deliv/bin/arm64”.  When building an image for tool `cmd-solve` we refer to
these architecture-specific folders and *not* to folder “deliv/bin”.

    define tools-for/pkg # pkgN,pkgD,main?,prereqs,srcs
    $(call tools-for/vars,$(1),$(2),$(5))\
    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin)\
    $(if $(3),
    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/arm64,GOARCH=arm64 GOOS=linux)
    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/amd64,GOARCH=amd64 GOOS=linux)
    $(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/amd64-windows,GOARCH=amd64 GOOS=windows,.exe))
    endef

Macro `tools-for/arch` defines the variables and goals `make` needs to build
executables for a package.  For each executable it defines two variables with
source files.  Consider for instance package “cmd/solve”.  Variable
`cmd-solveGF` holds all Go files from the package’s folder and variable
`cmd-solveS` only the files that do not define unit tests.  Goal-specific
variable `to-remove` holds the paths of all executables and goal `clean-tools`
uses it to delete all executables.

When building an executable we provide flags that strip some debugging
information from the executable.  The source files for the gRPC interface are
used differently by different packages.  For package “internal/port/grpc” they
are regular source files and for the other packages they are dependencies that
cause a rebuild of a package when one of these files changes.  For the
executable with unit tests, we have to set the executable bit as some versions
of Go do not set this flag properly for test executables.

    define tools-for/arch # main?,pkgN,pkgD,prereqs,binD,goVars,ext
    $(2)B_$(notdir $(5)):=$(5)/$(2)$(7)
    $$(patsubst deliv/%,%,$(5)/$(2)): $(5)/$(2)$(7)
    $$(patsubst deliv/%,%,$(5)/$(2).test): $(5)/$(2).test$(7)
    $(if $(1),tools: $(5)/$(2)$(7)
    clean-tools: to-remove+=$(5)/$(2)$(7)
    fake-tools: tool-names+=$(5)/$(2)$(7)
    $(5)/$(2)$(7): $$($(2)S) $(4) $$(need-goenv) | $(5)/
    >$(call run,go,goenv-$(local-arch),,$(GOENVVARS) $(6)) $\
     build $$(GOFLAGS) -o $$@ $(moduleN)/$(3)
    )tools: $(5)/$(2).test$(7)
    clean-tools: to-remove+=$(5)/$(2).test$(7)
    fake-tools: tool-names+=$(5)/$(2).test$(7)
    $(5)/$(2).test$(7): $$($(2)GF) $(4) $$(need-goenv) | $(5)/
    >$(call run,go,goenv-$(local-arch),,$(GOENVVARS) $(6)) $\
     test -c -cover $$(GOFLAGS) -o $$@ $(moduleN)/$(3)
    >chmod +x $$@
    endef

Marco `run` returns a command that is either executed directly or in a
container depending on the value of variable `useDocker`.  The macro has four
parameters: command name, name of container image, a directory, and a list of
variable definitions.  The last two parameters may be empty.  The directory is
made the current directory before executing the command.

    define run # tool,image,dir,var(s)
    $(if $(useDocker),docker run$(if $(DRFLAGS), $(DRFLAGS)) --rm $\
    --volume $${PWD}:/project --workdir=/project$(if $(3),/$(3)) $\
    $(if $(4),$(addprefix --env ,$(4)) )$($(2)IN):$($(2)V) $(1),$\
    $(if $(3),cd $(3) ; )$(if $(4),$(strip $(4)) )$(1))
    endef

Consider for instance the command producing tool `cmd-solve` for operating
system “Linux” and architecture “arm64”.  Its recipe in macro `tools-for/arch`
is as follows.  Variables `GOENVVARS` and `GOFLAGS` hold some environment
variables and command-line flags telling Go to build statically linked
executables.

    $(call run,go,goenv-amd64,,$(GOENVVARS) $(6)) \
        build $$(GOFLAGS) -o $$@ $(moduleN)/$(3)

If the above recipe is eventually executed by `make`, the actual command
executed is shown below.  This command is for a locally installed Go tool
chain.

    CGO_ENABLED=0 CGO_LDFLAGS="-static" GOARCH=amd64 GOOS=windows \
    go build -tags=osusergo -ldflags "-s -w" \
       -o deliv/bin/amd64-windows/cmd-solve.exe \
       siemens.com/qos-solver/cmd/solve

If the command executes in a container, macro `run` converts the variable
definitions into options (e.g., `--env GOARCH=amd64`) and prefixes command
with `docker run`.  The project’s main folder is mounted into the container
and made the current working directory.

    docker run --interactive --tty --rm \
           --volume ${PWD}:/project --workdir=/project \
           --env CGO_ENABLED=0 --env CGO_LDFLAGS="-static" \
           --env GOARCH=amd64 --env GOOS=linux \
           goenv-solver:1.0.2_amd64 \
           go build -tags=osusergo -ldflags "-s -w" \
              -o deliv/bin/amd64/cmd-solve \
              siemens.com/qos-solver/cmd/solve

Macro `tools-for/vars` defines the variables with a package’s Go files.

    define tools-for/vars # pkgN,pkgD,srcs
    $(1)GF:=$$(wildcard $(2)/*.go) $(3)
    $(1)S:=$$(filter-out %_test.go,$$($(1)GF))
    
    endef

An executable with unit tests produces coverage information.  This slows down
execution of unit tests a bit.  If we want to run unit tests at maximum speed
we have to build separate executables for executing unit tests with and
without coverage information.  In Solver we use a single executable collecting
coverage data and accept the slight speed penalty.

### Sources for gRPC Interface

Solver’s executables depend on the source files of the gRPC interface.  Two
ProtoBuf files define the interface between Solver and its clients.  The files
generated from them using the ProtoBuf compiler for Go are part of Solver’s
repository to make it easier to work with Solver with an IDE without further
ado, i.e., without the need to generate any files first.

When generating the source files for the gRPC interface we use the variables
below.  They hold the list of ProtoBuf and generated Go source files and some
folder names.

    pkgGrpcN=grpc
    pkgGrpcD=internal/port/$(pkgGrpcN)
    grpcProtos=$(pkgGrpcD)/optimizer-service.proto \
        $(pkgGrpcD)/optimizer.proto
    grpcSources=$(addprefix $(pkgGrpcD)/,\
        optimizer.pb.go optimizer_deepcopy.pb.go\
        optimizer-service.pb.go optimizer-service_grpc.pb.go)
    tmpProtoD=tmp/proto/$(pkgGrpcN)

The recipe generating Go source files from the ProtoBuf files uses the
ProtoBuf compiler.  These files define a package name for Go that does not fit
in Solver’s environment.  We change that package name appropriately using
`sed`.  Generation of source files happens once in folder “tmp/proto/grpc”—due
to “&” in the goal definition).

All other goals in Solver’s Makefile do not modify Solver’s source tree but
place any generated or temporary files in folder “tmp”.  As we keep the Go
files for the gRPC interface in Solver’s repository, we copy any new files
into the proper folder and modify the source tree.

    $(grpcSources)&: $(grpcProtos) $(need-goenv) | $(tmpProtoD)
    >$(call changeGoPackage,optimizer.proto)
    >$(PROTOC) --go_out=. --deepcopy_out=. optimizer.proto
    >$(call changeGoPackage,optimizer-service.proto)
    >$(PROTOC) -I. --go_out=. --go-grpc_out=. optimizer-service.proto
    >cp $(addprefix $(tmpProtoD)/,$(notdir $(grpcSources))) $(@D)

Macro `changeGoPackage` returns the `sed` command for patching the package
name in a ProtoBuf file.  We patch the ProtoBuf files on thy fly as this makes
copying the master filer file from Scheduler easier.  Checking whether the
master files differs from Solver’s copy is trivial this way.

    changeGoPackage=sed -Ee '/go_package/s|"[^"]+"|"../'$(pkgGrpcN)'"|' $\
        $(pkgGrpcD)/$(1) >$(tmpProtoD)/$(1)

## Collecting Reports

Solver’s pipeline produces the reports below about Solver.  For each report
(i.e., job in the pipeline) there is a corresponding goal in the Makefile.
Except for the report about license information reports apply to a specific
package.

- *[Static code checks](#static-code-checks)*.  For each package we use tools `staticcheck` and `go
      vet` to check for a number of common problems with Go code like empty `if`
  statements or arguments to printing functions that do not match their verb
  (e.g., passing an integer to a string).  To ensure proper formatting of
  Solver’s code, we use tool `go fmt` to check whether code is formatted in
  a non-standard way.
- *[License information](#reuse-reports)*.  It is necessary to associate license information
  with each of Solver’s source files.  We use tool `reuse` to verify that
  all files in Solver have licensing information “attached”.
- *[Unit-test results](#test-reports)*.  For each package there are special test functions
  that check whether the package’s “real” functions work properly.  We
  collect the test results for each package and associate them with the
  currently executing pipeline.
- *[Coverage reports](#coverage-reports)*.  For the tests executed for each package Go’s testing
  framework determines which lines in the source files are hit.  The
  coverage report for a package is an HTML document showing which lines were
  hit running the tests.  The percentage of lines covered is also part of a
  unit-testing report.

For package of Solver there is one report with static code checks, one with
unit tests, and one with coverage information based on running the package’s
unit tests.

### Static Code Checks

We use tool `staticcheck` to check for common problems in code and produce an
appropriate report.  Each package has its own report that is formatted for
human consumption.  File “deliv/lint/staticcheck-cmd-solve.txt” contains for
instance the report for package `cmd/solve`.  Variable `go-pkg-paths` holds
the list of Solver’s packages.  We do not use `go list` to determine this list
as this tool is slow.

    go-pkg-paths:=$(shell find . -type f -name "*.go" \
        | sed -E -e "/\\/(tmp|private)\\//d" \
              -e '/_test.go$$/d' -e 's/\/[^/]+$$//' -e '/\/test\//d' \
        | sort -u)

Variable `go-check-paths` holds the packages that we want to pass to to the
checkers.  We do not remove any packages from the list of packages.  This
might change if there is a package with a lot of generated code that produces
a lot of warnings.

    go-pkg-paths:=$(shell find . -type f -name "*.go" \

The loop below passes for all packages that we want to check both a package’s
name and its folder to macro `staticcheck-of` that sets up everything for this
report.

    $(foreach d,$(go-check-paths),\
      $(eval $(call staticcheck-of,$(subst ./,,$(d)))))

Macro `staticcheck-of` creates the goals for generating a report for the given
package.  It generates a goal for the report with a recipe ensuring that the
report is properly generated even if `staticcheck` finds a problem.  Note that
the report depends on the package’s source files.  The variable with those
files was defined when setting up rules for creating executables (see section
“[Building Executables](#building-executables)”).

We also add the package’s report to the list of files to be deleted for goal
`clean-staticcheck` and as a dependency for goal `staticcheck`.  Those goals
are not used by the pipeline but are handy when working on Solver on the
command line using the Makefile.

    define staticcheck-of # pkgD
    clean-staticcheck: to-remove+=$(call staticcheck-of/file,$(1))
    staticcheck: $(call staticcheck-of/file,$(1))
    $(call staticcheck-of/file,$(1)): $\
        $$($(call pkg-name,$(1))GF) $$(grpcSources) $$(need-goenv) | deliv/lint/
    >printf "Lint Report for “$(1)”\n\n" >$$@ ;\
     $$(STATICCHECK) $$(STATICCHECK_OPTIONS) $(moduleN)/$(1) >>$$@ ;\
     exit 0
    endef

Auxiliary macro `staticcheck-of/for` returns the location of a package’s
report given its name.

    staticcheck-of/file=deliv/lint/staticcheck-$(call pkg-name,$(1)).txt

Generating a report with `go vet` is simpler as there is only one report for
all of Solver’s source code.  Goal `go-vet` creates the report for all
packages in variable `go-check-paths`.

    go-vet: sources | deliv/lint/
    >printf "Issues found by “go vet”\n\n" >$(vet-report) ;\
     go vet $(go-check-paths) 2>&1 >>$(vet-report) ;\
     txt=`sed -e "1,2d" $(vet-report)` ;\
     if [ -n "$${txt}" ] ; then cat $(vet-report) ; exit 1 ;\
        else printf "\033[1m%s\033[0m\n" "go vet found nothing" ; fi

There is also only a single report for tool `go fmt`.  There is an “error” if
`go fmt` would change a file’s formatting in some way.  Such changes do not
alter the code’s “meaning” but only its visual appearance.  Using `go fmt` for
this purpose is agreed upon in the Go community.

    go-fmt: sources | deliv/lint/
    >printf "Files changed by “gofmt”\n\n" >$(fmt-report) ;\
     gofmt -l $(go-pkg-paths) >>$(fmt-report) ;\
     fn=`sed -e "1,2d" $(fmt-report)` ;\
     if [ -n "$${fn}" ] ; then cat $(fmt-report) ; exit 1 ;\
        else printf "\033[1m%s\033[0m\n" "go fmt changes no files" ; fi

### Reuse Reports

We have to ensure for compliance reasons that each file in project Solver has
associated license and copyright information.  We use tool `reuse` to check
that each file has one of two licenses.  We use the Apache v2.0 license for
Solver’s source code and the CC-BY-SA v4.0 license for all documentation
files.  The tool reports an error if a file has no license at all or a license
that differs from the two allowed licenses.

The recipe shows the complete report if there is a problem or a short message
that Solver is compliant otherwise.  The exit code of tool `reuse` is the
recipe’s exit code to indicate to `make` whether the check was successful,
i.e., whether all files have licensing information.

    reuse: $(need-reuse) | deliv/lint/
    >LANGUAGE=C $(REUSE) lint >$(reuse-report) ; status=$$? ;\
     [ "$${status}" == 0 ] && echo "REUSE compliant" || cat $(reuse-report) ;\
     exit $${status}

### Test Reports

Each of Solver’s packages defines both the “real” functions and tests for
those functions.  Solver’s pipeline collects test results and stores them so
that Gitlab shows them for a pipeline.  Collecting test reports is a two-step
process: [building executables](#building-executables) with the test functions (see section “[Building
Executables](#building-executables)”) and running those executables to get test results.  Folder
“deliv/report” contains all test reports.

Generating a test report is simple as Go’s standard testing framework already
produces a report when executing a package’s tests.  This report contains the
name of each test executed, its result (pass or fail), and the execution time.
If the test produces some additional output, it is included in the test report
as well.

We build an executable with unit-tests for each of Solver’s packages and use
that executable to produce a test report.  A separate job produces the report
with test results.  Macro `tests-for` creates the goals for the pipeline jobs
collecting test reports.

    $(call tests-for,main,cmd/solve)
    $(call tests-for,main,cmd/solved)
    $(call tests-for,,internal/port/grpc)
    $(call tests-for,,internal/types)
    $(call tests-for,,pkg/data/new)
    $(call tests-for,,pkg/textformat)
    $(call tests-for,,pkg/data)
    $(call tests-for,,pkg/solve)

Macro `tests-for` creates variables, goals, and rules for a package given its
name and a flag indicating whether the package is for a command-line tool.  It
calls macros `test-report-for` and `coverage-report-for` to generate the
variables and goals for a test and coverage report for a package.

    define tests-for # main?,pkg
    $(eval $(call run-tool-cmd,$(call pkg-name,$(2))))
    $(eval $(call test-report-for,$(1),$(call pkg-name,$(2)),$(2)))
    $(eval $(call coverage-report-for,$(1),$(call pkg-name,$(2)),$(2)))
    endef

Macro `run-tool-cmd` sets two variables holding the path of the directory for
code coverage data and the command for executing a package’s unit tests.  We
have to take the Makefile’s mode (`local` or `docker`) into account.  If the
mode is `docker`, the tool is executed in an appropriate container.  Otherwise
it is executed directly from folder “deliv/bin”.  It is necessary to set
variable `GOCOVERDIR` to an absolute path being the folder used to store the
coverage information produced by Go’s testing framework.

    define run-tool-cmd # pkgN
    $(1)GCD:=tmp/coverage/$(1)
    $(1)Cmd=$$(call run,deliv/bin/$(1).test,script-amd64,,$\
        GOCOVERDIR=$$(if $$(useDocker),/project,$$$${PWD})/$$($(1)GCD))
    endef

Macro `test-report-for` produces the goals for a package’s test report.  The
test report produced by Go’s framework is however not the format that GitLab
can use to show the tests for a pipeline.  GitLab understands the JUnit format
and we have to convert Go’s report into that format.

To generate a test report, we run the package’s command for executing tests
and pass the report produced to tool `go-junit-report` that reformats it
appropriately.  Go writes the report to `stderr` so we have to redirect
`stderr` to `stdout`.  Developer-level goal `check` gets the report file as a
prerequisite and all reports are put into variable `check-to-remove` for goal
`clean-check`.

    define test-report-for # main?,pkgN,pkgD
    $(2)TR:=deliv/report/$(2)-tests.xml
    .PHONY: report/$(2)
    report/$(2): $$($(2)TR) | deliv/report/
    check: report/$(2)
    test-reports+=$$($(2)TR)
    clean-check: to-remove+=$$($(2)TR)
    $$($(2)TR): deliv/bin/$(2).test $$(need-script) $$(need-goenv) \
        | $$(dir $$($(2)TR)) $$($(2)GCD)/
    >$$($(2)Cmd) -test.v 2>&1 \
     | $$(GO-JUNIT-REPORT) $$(GO-JUNIT-OPTIONS) \
           -package-name $(moduleN)/$(3) >$$@
    endef

### Coverage Reports

The Makefile creates two kinds of coverage reports: one coverage report
showing lines covered when executing a package’s unit tests and a second
showing percentage of statements covered of all packages in project Solver.
The Go tool chain produces the first report from the coverage information
collected when executing a package’s unit tests.

Macro `coverage-report-for` creates a package’s coverage report.  The first
step is to run a package’s unit tests to collect coverage data.  Go produces a
number of files with coverage data for a package and for neatness’ sake we put
all those files in a dedicated folder.  Go’s tool `covdata` summarizes this
“raw” coverage information into an intermediate file “coverage.out” and tool
`cover` converts this file into an HTML report for the user.  The user can
check a package’s files in this report to see what lines are not yet covered
by tests.

Similar to a test report, a coverage report is also added as a prerequisite to
goal `check` and to variable `check-to-remove`.  We produce both all test and
coverage reports with goal `check`.

    define coverage-report-for # main?,pkgN,pkgD
    $(2)CR:=deliv/report/$(2)-coverage.html
    report/$(2): $$($(2)CR)
    clean-check: to-remove+=$$($(2)CR)
    $(2)CVI:=$$($(2)GCD)/coverage.out
    $$(call mkdir,$$(dir $$($(2)CVI)))
    $$($(2)CR): deliv/bin/$(2).test $$(need-script) $$(need-goenv) \
        | $$(dir $$($(2)CR)) $$($(2)GCD)/
    $(if $(1),>$$($(2)Cmd)
    >$$(GO) tool covdata textfmt -i=$$($(2)GCD) -o $$($(2)CVI)
    >$$(GO) tool cover -html=$$($(2)CVI) -o $$@,>$$($(2)Cmd) -test.coverprofile=$$($(2)CVI)
    >$$(GO) tool cover -html=$$($(2)CVI) -o $$@)
    endef

The second coverage report generated by goal `coverage` is a summary of the
code coverage of all statements in project Solver.  This report computes the
average code coverage from the unit-test report of all packages of project
Solver except package “internal/port/grpc”.  The latter package contains a lot
of generated code so the coverage information of this package is not so
meaningful.

On European systems the decimal point may be “,” instead of “.” to indicate a
decimal fraction.  GitLab expects “.” when reading a code coverage value.  We
transform any “,” in the summary report to a “.” via tool `tr` so that the
proper value is read.

    coverage: $(filter-out %port-grpc-tests.xml,$(test-reports))
    >awk -f scripts/total-coverage.awk $^ | tr , .

File “scripts/total-coverage.awk” contains statements for AWK to compute the
average coverage of statements where variable `N` holds the number of packages
and variable `T` the percentage of all packages combined.  For consistency’s
sake coverage information is shown in the same way as Go reports it, i.e., as
a percentage of the form 12.3%.

To avoid problems with the decimal separator when reading the value, we remove
the decimal point from the percentage value and use that value (e.g., 123
instead of 12.3).  When computing the average we have to account for this
scaling.  We instruct the pipeline to read this average as the coverage value
for job `coverage`.

The coverage value is stored differently with a newer version of tool
`go-junit-report`.  In a newer version the value is stored with two decimal
values in a property named “coverage.statements.pct”.  We handle both cases to
support newer versions on a developer’s machine.

    BEGIN { N=0;T=0;S=10 }
    /coverage: / {
        match($$0,"coverage: [0-9]+\\.[0-9]+");
        split(substr($$0,RSTART,RLENGTH),a,": ");
        sub("\\.","",a[2]);
        T+=a[2];N+=1
    }
    /coverage.statements.pct/ {
        match($$0,"value=\"[0-9]+\\.[0-9]+");
        split(substr($$0,RSTART,RLENGTH),a,"=\"");
        sub("\\.","",a[2]);
        T+=a[2];N+=1;S=100
    }
    END {
        if (S==10) {
            A=sprintf("%0.1f%%",T/(N*10))
        } else {
            A=sprintf("%0.2f%%",T/(N*100))
        }
        print "$(moduleN) coverage:",A,"of statements"
    }

GitLab shows coverage information as a graph showing the percentage for a
certain period of time.  There is a graph for each job with coverage data
attached.  GitLab maintains code coverage data for all branches in the project
independently.  The graphs show code coverage for a particular package.  Job
`coverage` shows combined coverage for *all* packages in project Solver.

## Documentation

GitLab renders Markdown documentation files in folder “docs” for easy viewing
but one needs a browser and an Internet connection to view those files.  For
easy off-line reading of Solver’s documentation the Makefile has goal `epub`
that creates file “deliv/docs/solver.epub” that can be used with e-book
readers like Amazon’s Kindle.

We use tool `pandoc` to create the EPUB document for Solver’s Markdown
documentation files.  Image “docenv” (see section “[Image ‘docenv’](#image-docenv)”) contains
everything needed for this conversion.

    $(epub): $(epubSrcs) $(manSrcs) $(epubMeta) $(need-docenv) | $(dir $(epub))
    >$(PANDOC) -f markdown -t epub --toc --metadata-file=$(epubMeta) \
     -o $@ $(epubSrcs) $(manSrcs)

Variable `epubSrcs` contains the Markdown files to be included in Solver’s
EPUB documentation.  As Solver’s man pages are currently in nroff format, we
have to convert them to Markdown to include them in the documentation.
Variable `manSrcs` contains man pages in Markdown format.

    epubSrcsT+=$(addprefix tmp/docs/,$(addsuffix .md,\
        annotated-example))
    epubSrcs:=$(addprefix docs/,$(addsuffix .md,algorithm textformat)) \
        $(epubSrcsT) \
        $(addprefix docs/,$(addsuffix .md,makefile pipeline))
    manSrcs:=$(addprefix tmp/docs/man-,$(addsuffix .md,\
        solve solved))

We use tool `pandoc` to convert from nroff format to Markdown.  All sections
in a manpage document are “chapters” which is not suitable for the EPUB
document.  Using `sed` we demote those headings one level and add a chapter
heading indicating the name of tool the man page refers to.

    tmp/docs/man-%.md: docs/%.1 | tmp/docs
    >$(PANDOC) -f man -t markdown $< \
     | sed -E -e "/^# /s/^/#/" -e "1{s/^/# Man Page $(basename $(<F))\n\n/;}" >$@

## Releases

Due to historical and administrative reasons Solver’s main development is done
on a private GitLab instance owned and managed by Siemens.  At times Solver’s
code is also published on a public GitLab instance (e.g., on the instance at
`gitlab.eclipse.org` managed by Eclipse).  The Makefile supports goals
`release` and `export` that help the team to publish a release.

Publishing a release is a two-step process: first we mark the release to be
published and then we copy that version to the target repository.  We use a
special branch “oss-release” to keep the versions be published.  For each
version published there is one commit in that branch.  The commad below show
how to make a release for the current release which defines the version number
in file “VERSION” and push that release to GitLab instance “codeco”.  A
release for Solver can be done only for branch “main”.

    make release
    make export target=codeco

Script “scripts/gitlab.infos.sh” holds information needed for each target
repository.  For repository “siemens” it contains the information shown below.
Parameters `host` and `group` describe the project location on the target
instance and parameters `scheduler` and `solver` are the project names used
for Scheduler and Solver.  Parameter `branch` defines the name for the branch
used to hold the releases (main repository) and the branch used for importing
(target repositories).

    siemens_host=code.siemens.com
    siemens_group=itp_cloud_research
    siemens_scheduler=qos-scheduler
    siemens_solver=qos-solver
    siemens_branch=oss-release

The branch with the releases is a so-called *orphaned* branch, i.e., its
history is separate from the one of the main branch.  We use an orphaned
branch to keep the number of submitters under tight control.  On some GitLab
instances *all* committers need to be a registered user for legal reasons.  We
do not want to disclose the employees working on Solver or force them to join
a particular instance, so we publish from that orphaned branch.

Goal `release` uses script “scripts/release.sh” to copy a release’s files to
the orphaned branch.  We create and archive with all files, switch to the
orphaned branch, and update it to hold the release’s files.  We do not merge
the commit of the release’s commit to the orphaned branch as this would
connect the histories of the two branches.

For the time being the orphaned branch it not directly used to push commits to
another repository.  But this may change for other repositories and then the
branch’s curated hostory is beneficial.

Once a release is marked using goal `release`, we use goal `export` to copy
that release to the target repository.  Script “scripts/export.sh” does the
heavy lifting copying a release.  It uses an archive to copy the release to
the target repository in a branch in the same way as when making a release.
To track which release was pushed to which repository, the script assigns the
release’s commit an appropriate tag.  A given release can be copied to more
than one repository.

## Environment Variables

The team uses the Makefile both for local builds on a developer’s machine and
builds done by GitLab CI (see section “[Role of the Makefile](#role-of-the-makefile)).  In the latter
case there are a number of variables that have influence on how the Makefile
behaves.  The variables below are environment variables that the Makefile
considers.  GitLab CI defines all variables starting with “CI\_” (see [GitLab’s
documentation](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) about variables defined for a pipeline).

- `CI`: If this variable is set the build is done by GitLab CI.  GitLab sets
  this variable to “true”.
- `CI_DEFAULT_BRANCH`: The name if the default branch for the project in
  GitLab.  This refers to the project main branch called “main” or “master”.
- `CI_COMMIT_REF_SLUG`: The name of the current branch suitable for
  including in a  DNS name for instance.
- `CI_PIPELINE_ID`: Identifier for the currently running pipeline.
- `CI_REGISTRY`: Host of the GitLab instance including any port number.
- `CI_REGISTRY_IMAGE`: Base address of the project’s container registry.
- `CI_REGISTRY_USER`: Username for accessing the project’s container
  registry.
- `CI_REGISTRY_PASSWORD`: Password for accessing the project’s container
  registry.
- `runner_archs`: The CPU architectures of runners available on the GitLab
  instance.  For Siemens’s instance for instance this is “amd64,arm64”.
- `target_archs`: The CPU architectures for which deliverables should be
  produced.  As we produce deliverables for architectures “amd64” and
  “arm64”, this is set to “amd64,arm64”.
- `SOLVER_IGNORE_SETTINGS`: If this variable is *not* set, the Makefile
  includes a file with the name “$(USER).mk” where variable `USER` is a
  variable defined by the shell containing the name of the current user (see
  section “[Private Goals](#private-goals)”).
- `SOLVER_BUILD_MODE`: The mode for the Makefile to use.  If this variable
  is set to “local”, the Makefile uses locally installed tools.  If this the
  variable is set to “docker”, the Makefile runs tools like Go n containers
  that are built in the same manner as in the pipeline.

Variable `CI` controls whether the Makefile is used for a pipeline job or not.
For a pipeline job all tools are expected to be available locally (see also
section “[Building Container Images](#building-container-images)”).  This is the same as build mode “local”.
For a pipeline build container images are not built using Docker but using
Kaniko.

The system uses variables starting with `CI_REGISTRY` when building an image
in the pipeline.  They define where the image should be stored and provide the
credentials for accessing the registry.  When building images locally with
Docker, the system uses the same tags as within the pipeline.  As there may be
more than one project producing the “same” images (e.g., project Scheduler
also produces an image “goenv”) local image names have suffix “-solver” (e.g.,
“goenv-solver” instead of “goenv”).

Variables `CI_COMMIT_REF_SLUG` and `DI_DEFAULT_BRANCH` determine the tags used
for images.  Whenn building for the main branch (the variables have the same
value), an image has the project version (as defined in file “VERSION”) as its
tag and the name of the current branch otherwise.

For a local build on a feature branch, we abbreviate the name of the tag if it
starts with a number.  When building for branch “999-festure-title” for
instance, image tags are “0.999” instead of the branch name.  This is done to
keep local image tags in Docker short.

We add variable `CI_PIPELINE_ID` to the version shown by tols `solve` and
`solved`.  The pipeline’s identifier is shown so that different version of
tools can be distinguished easily.  The system may produce more than one build
for any version and the pipeline identifier allows to distinguish excutables
produced for the same version by different pipelines.
