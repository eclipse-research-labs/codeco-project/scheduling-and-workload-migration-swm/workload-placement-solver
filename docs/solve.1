." SPDX-FileCopyrightText: 2023 Siemens AG
." SPDX-License-Identifier: CC-BY-SA-4.0
.TH "solve" "1" 
.SH "NAME"
.PP
\fBsolve\fP - solve an assignment problem

.SH "SYNOPSIS"
.PP
\fBsolve\fP [\fB-version\fP] [\fB-help\fP] [\fB-list\fP]

.PP
\fBsolve\fP [\fB-stats\fP] [\fB-query\fP] [\fB-source\fP] [\fB-server\fP \fIaddr\fP] 
.br
      [\fB-percent\fP] [\fB-format\fP \fIout\fP] [\fB-source\fP]
.br
      [\fB-o\fP \fIfile\fP] [\fB-folder\fP \fIpath\fP] \fIname\fP \fI...\fP

.PP
\fBsolve\fP [\fB-repeat\fP \fInum\fP] [\fB-cpu-profile\fP \fIpath\fP] [\fB-mem-profile\fP \fIpath\fP] \fIname\fP \fI...\fP

.SH "DESCRIPTION"
.PP
Tool \fBsolve\fP reads an assignment problem from a file \fIname\fP, solves it, and
shows the solution found.  An assignment problem is about placing workloads in
a cluster while considering network requirements defined by channels each
connecting two workloads.  The infrastructure of a cluster consists of compute
and network nodes connected by links.  A cluster may have more than one
network (e.g., ethernet and WLAN).  An assignment problem’s solution assigns a
compute node to each workload that can be placed in the cluster.

.PP
\fBsolve\fP understands the following options:

.TP
\fB\fB-version\fP\fP
Show version information and exit
.TP
\fB\fB-help\fP\fP
Show help text and exit
.TP
\fB\fB-list\fP\fP
Show list of built-in problems
.TP
\fB\fB-check\fP\fP
Perform a syntax check of the given problem file but do \fInot\fP
search for a solution.
.TP
\fB\fB-query\fP\fP
Do not determine a solution but output the query sent to a
server.
.TP
\fB\fB-source\fP\fP
Do not determine a solution but output the query’s source
code, i.e., the problem description in text format.
.TP
\fB\fB-percent\fP\fP
Show resource usage in summary in percent instead of a
fraction..
.TP
\fB\fB-folder\fP \fIpath\fP\fP
Search in folder \fIpath\fP for assignment files specified
as a relative path that were not found in the current working directory.
.TP
\fB\fB-format\fP \fIout\fP\fP
Define the format for output (default is `summary').
If a second format is specified (e.g., \fI-format json,binary\fP) the first
format sets the format for input and the second for output.
.TP
\fB\fB-o\fP \fIname\fP\fP
Write information to file \fIname\fP instead of standard
output.
.TP
\fB\fB-server\fP \fIaddr\fP\fP
Address of a server \fBsolve\fP should use to get a
solution for the given assignment problem.  If this option is not given,
\fBsolve\fP computes the solution using a backtracking algorithm.

.PP
The options below are useful when working on \fBsolve\fP itself:

.TP
\fB\fB-stats\fP\fP
Show statitics about the search for a solution
.TP
\fB\fB-repeat\fP \fInum\fP\fP
Repeat the search for a solution the given number of
times.  This is to assess the performance of \fBsolve\fP.
.TP
\fB\fB-cpu-profile\fP \fIfile\fP\fP
Write CPU profile to \fIfile\fP.
.TP
\fB\fB-mem-profile\fP \fIfile\fP\fP
Write memory profile to \fIfile\fP.

.PP
\fBsolve\fP supports the following formats when reading and writing information:

.IP \(em 4
\fItext\fP: Textual form of an asssignment problem as defined by \fBsolve\fP.
.IP \(em 4
\fIsummary\fP: Summary format showing the solution of an assignment in a
more compact format.
.IP \(em 4
\fIbinary\fP: Binary format of an assignment problem as defined by Scheduler’s
ProtoBuf specification.
.IP \(em 4
\fIjson\fP: JSON format of a assignment problem as defined by ProtoBuf
specification.

.PP
The text format defined for \fBsolve\fP allows to specify an assignment problem in
a concise way.  The example below shows how to place two applications each
with two workloads in a cluster with three nodes all connected to a switch.

.RS
.nf
\fCworkload assignment v1
model two-apps
application a1
  workload w1,w2 needs cpu:200 ram:500MiB
  channel w1—w2 needs bw:50*64KB,50*1Kb
application a2
  workload w1,w2 needs cpu:200 ram:500MiB
  channel w1-w2 needs bw:50*64Kb,50*128
channel a1•w2—a2•w1 needs bw:25*4Kb,25*128

node c1,c2,c3 provides cpu:2000 ram:6GiB
network eth
node sw
link c1—sw,c2—sw,c3—sw provides bw:1Gb
find paths (c1•c2 c1•c3 c2•c3)
\fP
.fi
.RE

.SH "ENVIRONMENT"
.PP
The following environment variables affect the execution of \fBsolve\fP:

.TP
\fBSOLVE_SERVER\fP
Address of a server \fBsolve\fP should use to get a solution for
an assignment from instead of computing it.  This corresponds to option
\fB-server\fP.  If an option is given, it takes precedence over the environment
variable.
.TP
\fBSOLVE_FOLDER\fP
Folder with assignment files where \fBsolve\fP looks for a file
when it is given a relative path.  This corresponds to option \fB-folder\fP.  If
an option is given, it takes precedence over the environment variable.

.SH "EXIT STATUS"
.PP
\fBsolve\fP returns exit code 0 if the problem description is valid and there is a
solution for the problem and a code >0 otherwise.

.SH "EXAMPLES"
.PP
Solve the assignment problem in file `problem.asgmt'.  The extension of a file
with an assignment problem is by convention `.asgmt'.

.RS
.nf
\fC$ solve problem.asgmt
\fP
.fi
.RE

.PP
Solve the problem in file `problem.asgmt' searching in folder `project' for
the file.

.RS
.nf
\fC$ solve --folder=project problem.asgmt
\fP
.fi
.RE

.PP
Same as before but specifying a complete path.

.RS
.nf
\fC$ solve project/problem.asgmt
\fP
.fi
.RE

.PP
List the problems built into \fBsolve\fP.

.RS
.nf
\fC$ solve --list
\fP
.fi
.RE

.PP
Solve the built-in problem `tiny'.

.RS
.nf
\fC$ solve tiny
\fP
.fi
.RE

.PP
Save the request in ProtoBuf format for built-in problem `tiny' in file
`tiny.pb'.

.RS
.nf
\fC$ solve -query -format text,binary -o tiny.pb tiny
\fP
.fi
.RE
