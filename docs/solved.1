." SPDX-FileCopyrightText: 2023 Siemens AG
." SPDX-License-Identifier: CC-BY-SA-4.0
.TH "solved" "1" 
.SH "NAME"
.PP
\fBsolved\fP - solve an assignment problem

.SH "SYNOPSIS"
.PP
\fBsolves\fP [\fB-version\fP] [\fB-help\fP]

.PP
\fBsolves\fP [\fB-verbose\fP] [\fB-host\fP \fIaddress\fP] [\fB-port\fP \fInum\fP] [\fB-max-requests\fP \fInum\fP]

.SH "DESCRIPTION"
.PP
Tool \fBsolved\fP receives an assignment problem from a port, solves it, and
returns the solution found.  An assignment problem is about placing workloads
in a cluster while considering network requirements defined by channels each
connecting two workloads.  The infrastructure of a cluster consists of compute
and network nodes connected by links.  A cluster may have more than one
network (e.g., ethernet and WLAN).  An assignment problem’s solution assigns a
compute node to each workload that can be placed in the cluster.

.PP
\fBsolved\fP understands the following options:

.TP
\fB\fB-version\fP\fP
Show version information and exit
.TP
\fB\fB-help\fP\fP
Show help text and exit
.TP
\fB\fB-verbose\fP\fP
Enable verbose logging
.TP
\fB\fB-host\fP \fIaddress\fP\fP
IP address \fBsolved\fP should bind to for incoming
requests.  The most common setting is "0.0.0.0" (allow any incoming
address).
.TP
\fB\fB-port\fP \fInum\fP\fP
The port \fBsolved\fP should use for listening for incoming
requests.
.TP
\fB\fB-max-requests\fP \fInum\fP\fP
The maximum number of requests that \fBsolved\fP
should handle.  This option is handy for testing.

.PP
The text format defined for \fBsolved\fP allows to specify an assignment problem
in a concise way.  The sample below shows how to place two applications each
with two workloads in a cluster with three nodes all connected to a switch.
Note that \fBsolved\fP only works with the binary representation of the assignment
problem

.RS
.nf
\fCworkload assignment v1
model two-apps
application a1
  workload w1,w2 needs cpu:200 ram:500MiB
  channel w1—w2 needs bw:50*64KB,50*1Kb
application a2
  workload w1,w2 needs cpu:200 ram:500MiB
  channel w1-w2 needs bw:50*64Kb,50*128
channel a1•w2—a2•w1 needs bw:25*4Kb,25*128

node c1,c2,c3 provides cpu:2000 ram:6GiB
network eth
node sw
link c1—sw,c2—sw,c3—sw provides bw:1Gb
find paths (c1•c2 c1•c3 c2•c3)
\fP
.fi
.RE

.SH "ENVIRONMENT"
.PP
The following environment variables affect the execution of \fBsolve\fP:

.TP
\fBSOLVED_SERVER\fP
IP-address \fBsolved\fP should bind to for incoming requests.
This corresponds to option \fB-host\fP.  If an option is given, it takes
precedence over the environment variable.
.TP
\fBSOLVED_PORT\fP
Port \fBsolve\fP should listen on for incoming requests.  This
corresponds to option \fB-port\fP.  If an option is given, it takes precedence
over the environment variable.

.SH "EXIT STATUS"
.PP
\fBsolved\fP returns exit code 0 if it could successfully open a port and handle
requests and a code >0 otherwise.

.SH "EXAMPLES"
.PP
Start \fBsolved\fP and listen on port 5000 for assignment problems.

.RS
.nf
\fC$ solves -port=5000
\fP
.fi
.RE
