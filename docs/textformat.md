<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Text Format for Assignment Problems

Solver supports a small DSL for easy definition of assignment problems.  A
problem description using the DSL is more concise and readable than the YAML
notation used by the custom resources in a cluster.  The text format is not
line-oriented or indentation-sensitive.  Below is a small example of an
assignment problem including a solution.

    workload assignment v1
    model tiny // used for reference only
    
    application a1 /* there may more than one application */
    workload w1 needs cpu:100 ram:20MiB
    workload w2 needs cpu:100 ram:100MiB
    
    channel w1→w2 needs lat:100µs bw:5MiBit
    
    infrastructure
    node c1 provides cpu:1000 ram:2GiB
    node c2 provides cpu:2000 ram:4GiB
    
    network Ethernet with qos:best-effort
    link c1↔c2 with lat:10µs bw:5MiBit
    find paths (c1•c2)
    
    assignment // this is a solution to the problem above
    workload a1•w1 on c2
    workload a1•w2 on c1
    channel a1•w1→a1•w2 from c2 to c1

The example is about one application “a1” with two workloads “w1” and “w2”
where workload “w1” sends data to workload “w2”.  The requirements of both
workloads and the channels connecting them are easy to see.  Support for
common units like “MiB” make it ease to specify memory, latency, bandwidth
requirements.

The cluster (or the part of a cluster to be used) is specified in section
“infrastructure”.  This section lists the worker nodes available and the
networks that connect the worker nodes.  A network description consists of a
number of links connecting two nodes and the paths in the network that connect
worker nodes.

Section “assignment” gives a solution for the assignment problem.  This
section is usually only present when the description is used for testing.
Subsequent sections describe the format in more detail.  An assignment file
has the general structure below.  There is a [header](#header) with information about the
assignment problem, one or more sections defining [applications](#applications), [channels](#channels), and
the cluster’s [infrastructure](#infrastructure) (consisting of [nodes](#nodes) and [networks](#networks)), any
node-specific [costs](#costs) for workloads, node [recommendations](#recommendations) for workloads, and an
optional section with [solutions](#assignments) to the problem.

    ProblemSpec ::= Header Section+ Assignments?
    Section ::= ("section" k8sName)? Applications Infrastructure WlInfos
    WlInfos ::= Costs? Recommendations?

A problem’s description can consist of multiple sections where each section
defines applications, channels, and the infrastructure they need.  Most
problems have only a single section.  Sections are handy when applications and
infrastructure are added as “modules”.  An example is wind park where the
number of wind mills changes.  The description of each wind mill can be
separate section and the common objects are given in the first section.

## Terminal Symbols

The syntax of an assignment description is simple.  It consists of words
separated by white space (e.g., space or new-line characters) or comments.
There is a one-line comment started with “//” and a comment that may span
multiple lines that starts with “/\*” and ends with “\*/”.  The number of
white-space characters between two words is irrelevant.

The following special symbols are used in the description of the text format.
Symbol “K8sName” stands for a valid name in Kubernetes.  This is a name
containing only letters, digits, “-”, and “.”.  The characters “-” and “.” may
only be “inner” characters.  To improve readability, there are the aliases
“AppName”, “WlName”, “ChannelName”, “NodeName”, “NetworkName”, “PathName”, and
“LinkName” for symbol “K8sName”.  For convenience names of channels, paths,
and links may be numbers as giving descriptive names might sometimes be a
burden.

Solver accepts a string like `"ident"` as an identifier.  Alternate notations
for quoted strings are `'ident'`, `“ident”`, and `«ident»`.  The identifier
returned by the scanner is the name without the enclosing quotes (e.g., `abc`
for `"abc"`).  These names are handy when using names in debugging that
contain “weird” characters.

At some places one can specify a numeric value (e.g., the amount of RAM or
bandwidth required).  For simplicity all those values are integer values.  A
value is often followed by a unit containing a scaling factor (e.g., “100Kb”
or “10µs).  The value “0.1s” has to specified as “100ms”.  Floating-point
values are allowed only for workload costs.  There are no white-space
characters between the components of a floating-point value.

    AppName ::= K8sName
    WlName ::= K8sName
    ChannelName ::= K8sName | Integer
    NodeName ::= K8sName
    NetworkName ::= K8sName
    PathName ::= K8sName | Integer
    LinkName ::= K8sName | Integer
    K8sName ::= "[a-zA-Z0-9]([a-zA-Z0-9.-]*[a-zA-Z0-9])?"
    Integer ::= "[0-9]+"
    Float ::= "-"? Integer ("." Integer)? ("e" "-"? Integer)?

## Header

Each assignment file starts with a header with some meta information about the
problem.  The version of the text format is currently 1.  A later version can
take this version into account and adapt the older format as appropriate.

For reference purposes there is the name of the model.  This name is not used
in the scheduling at all.  It is just used to provide some “identity” to the
data.  There is a small number of options controlling some aspects of creating
data for the assignment problem.  If an option is not given, its default-value
applies (see section “[Default Values](#default-values)”).

    Header ::= "workload" "assignment" Version Model
    Version ::= "v" Integer
    Model ::= "model" K8sName (("options" | "with") Option+)?
    Option ::= K8sName ":" (True | False)
    True ::= "true" | "yes" | "on"
    False ::=  "false" | "no" | "off"

Solver supports the following options for a model:

- `compound-names`: If this option is set, Solver prefix names of contained
  objects by the name of their containing objects to make names globally
  unique.  The name of workload W in application A is changed to “A\_W”.
  Note that is not a name compatible with Kubernetes’ naming rules.
- `implicit-reverse-path`: If this option is set, Solver include reverse
  paths in the request.  It is assumed that if there is path from node A to
  node B, then there is also a path from node B to node A using the same
  link in the reverse direction.  
  Note that this option is deprecated and will be deleted.  It is for an
  early version of Scheduler that requires also globally unique workload
  names.
- `shortest-path-only`: If this option is set, Solver includes only the
  shortest paths between two nodes in the request event if there is more
  than one path between two nodes.  The shortest path is the path with
  minimum *number* of link and not for instance the minimum total latency.

## Application Group

The first part of a section is the list of applications to be deployed.  It
contains at least one application.

### Applications

Each application has one or more workloads.  In Kubernetes a workload is a
pod.  All workloads of an application are deployed together in a cluster.  If
there is not enough room for one workload of an application, all workloads of
this application are not deployed.

    Applications ::= "applications"? Application (Application | Channel)*
    Application ::= "application" K8sName Workload+

A workload has certain requirements with respect to CPU and RAM. If CPU and
RAM not specified, default values apply.  A workload can have one or more
required or forbidden labels.  A workload can only run on those nodes that
have all required labels and none of the forbidden labels.  Label “A” is a
required label and label “~A” is a forbidden label.

An application’s workloads has the following properties and default values.
Requirements for CPU and memory are the limits specified in Kubernetes as they
define the maximum amount a workload needs.  The properties are similar to
those Kubernetes uses for pods.

- `cpu:` This property defines the amount of CPU the workload needs.
- `ram:` The amount of main memory the workload needs.
- `labels:` The labels the workload requires or does not tolerate.  There is
  no default value defines for this in package `internal/defaults`.

The amount of memory is specified in bytes.  For larger values the use of
common “units” like 4KiB for 4096 bytes is allowed.  There are no white-space
characters between a number and a unit, i.e., “4KiB” is allowed but “4 KiB” is
not.

    Workload ::= "workload" NameList ("on" NameList)? ("needs" WlNeeds+)?
    NameList ::= K8sName ("," K8sName)*
    WlNeeds ::= Labels | "cpu:" CPU | ("ram:" | "memory:") RAM
    CPU ::= Integer "mCPU"?
    RAM ::= Integer DataUnit?
    DataUnit ::= [KMG]? i? ("B" | "Byte"| "b" | "Bit")
    Labels ::= "labels:" "(" ([#x00AC!~-]? K8sName)+ ")"

The snippets below shows the definition of two workloads.  As the workloads
have the same needs, the third definition is equivalent to the first two.

    workload w1 needs cpu:1000 ram:8000
    workload w2 needs cpu:1000 memory:8000
    
    workload w1,w2 needs cpu:1000 ram:8000

### Channels

A channel states that two workloads communicate with each other.  There are
one-way and two-way channels.  If there is a one-way channel from workload A
to workload B, A sends data to B but B does not send any data to A.  Latency
bandwidth, and quality of service can be different for the two directions.  If
only one value is specified for a two-way channel, it applies to both
directions.

A channel can have an optional name given before the two workloads it connects
(e.g., “C1:W1→W2” is a channel named “C1” between workloads "W1” and “W2” of
the current application).  If the channel is a two-way channel there can be
two names.  If there is no name for a channel, Solver determines a name based
on the workload names.

    Channel ::= "channel" ChSpecs ("on" PathRefs)? ("needs" CommNeed+)?
    ChSpecs ::= Name? SrcDst ("," Name? SrcDst)*
    Name ::= (K8sName | Integer) ("," (K8sName | Integer))? ":"
    PathRefs ::= PathRef ("," PathRef)*
    PathRef ::= NetworkName "•" PathName
    SrcDst ::= WorkloadRef (OneWayOp | TwoWayOp) WorkloadRef
    WorkloadRef ::= (AppName "•")? WlName
    TwoWayOp ::= #x2194 /* "↔" */ | "<->" | #x2014 /* "—" */ | "--"
    OneWayOp ::= #x2192 /* "→" */ | "->"

A channel between two workload has the properties below.  Specifying a latency
for a best-effort channel is a only a soft requirement as there are no
guarantees for such channels.  Solver may select links that best match the
channel’s latency.

- `lat:` The latency the channel requires.  For an assured channel Solver
  uses only links having guaranteeing that latency.
- `bw:` The bandwidth the channel requires.  Solver ensures that all
  channels regardless of the type do not overload it.
- `qos:` The service class for the channel. The channel may use all networks
  having links with the required service class.  A best-effort channel can
  use links of types best-effort and assured.

The latency is measured in nanoseconds and the bandwidth in bits per second.
Some common units can be used to define “large” values (e.g., “1ms” and
“5MByte”).  Many workloads send data of a certain size periodically; a
workload capturing a video stream may send for instance 30 frames per second.
In such cases one can specify the bandwidth of channel as `bw:30×64KiB`.

    CommNeed ::= Latency | Bandwidth | QoS
    Latency ::= ("lat:" | "latency:") Duration ("," Duration)?
    Duration ::= Integer ([mµun]? "s")?
    Bandwidth ::= ("bw:" | "bandwidth:") Volume ("," Volume)?
    Volume ::= Integer ([*#x00D7] /* "×" */ Integer)? DataUnit?
    QoS ::= "qos:" QoSType ("," QoSType)? 
    QoSType ::= "best-effort" | "assured"

The majority of channels connect a workload to another workload in the same
application.  For such channels it is not necessary to specify the
application’s name, if the channel is defined “directly” after the
application.  In the example below channels `c1` and `c2` refer to the same
workloads, as a missing application name defaults to the name of the last
application.  The last definition is a more concise definition of channels
`c1` and `c2`.

    application a1
    workload w1
    workload w2
    
    channel c1:a1•w1→a1•w2
    channel c2:w1→w2
    
    channel c1:a1•w1→a1•w2,c2:w1→w2

A channel’s optional clause `on` tells Solver about network resources already
assigned to a channel.  It states which network paths the channels use.  It is
an error to specify more network paths than channels.  For a bidirectional
channel two path references must be given.

In the example below channel `c1` uses network path `n1•p4`, channel `c2` uses
paths `n2•p1`, channel `c3` (reverse channel of `c2`) uses path `n2•p3`, and
channel `c4` has no associated network path.  The last definition is a more
concise definition of the same channels.

    channel c1:w1→w2 on n1•p4 needs bw:30×10KB
    channel c2,c3:w1—w3 on n2•p1,n2•p3 needs bw:30×10KB
    channel c4:w4→w5 needs bw:30×10KB
    
    channel c1:w1→w2,c2,c3:w1—w3,c4:w4→w5
            on n1•p4,n2•p1,n2•p3
            needs bw:30×10KB

Channels are optional.  If there are no channels, the placement is done
without considering network requirements or resources.  Solver considers only
workload requirements.  The difference to Kubernetes’ standard scheduler is
that applications are still handled as a group.

## Infrastructure

Once applications and channels are defined, the infrastructure to be used is
specified.  The infrastructure need to contain everything available in a
cluster but only those subset that the application group should use.  It
contains at least one worker node and may contain one or more networks.
Specification of networks is needed only if channels are defined.

    Infrastructure ::= "infrastructure"? Node (Node | Network)*

### Nodes

A worker node provides a certain amount of CPU and RAM and may have one or
more labels.  There are no forbidden labels for nodes.  If some amount of CPU
or RAM on a worker node are used for already scheduled workloads or pods, the
amount can be given as “1000-500” where 1000 is total amount and 500 is the
amount already used.

    Node ::= "node" NameList ("provides" CompRsrc+ | "with" CommRsrc+)?
    CompRsrc ::= Labels
                 | "cpu:" CPU ("-" CPU)?
                 | ("ram:" | "memory:") RAM ("-" RAM)?

When there is list of names for a `node` declaration, Solver creates a node
for each name and all nodes have the same properties.  In the example below
the first two declarations specify the same nodes as the third.

    node N1 provides cpu:2000 ram:16GiB
    node N2 provides cpu:2000 memory:16GiB
    
    node N1,N2 provides cpu:2000 ram:16GiB

A node that does not provide CPU or RAM is a network node (e.g., a switch).
No workload can be placed on such nodes.  They exist only to build up network
topologies where nodes are not directly connected to each other.  For a
network node one can specify a latency introduced by that node and a maximum
bandwidth.

A worker node (i.e., a node used by Kubernetes for pods) has the properties
below.  The properties are similar to those used by Kubernetes for nodes.
Solver does not “overcommit” CPU nor memory.

- `cpu:` The amount of CPU available for workloads.
- `ram:` The amount main memory available for workloads.

Network nodes have a different set of properties.  These properties are **not**
yet supported when searching for a solution.  With network nodes the topology
of a network can be modelled.  In Kubernetes all nodes can talk directly to
all other nodes at high speed.  When using Kubernetes outside a data center
the network is typically more complex and contains switches and paths between
nodes of different length (i.e., latency).

- `lat:` The latency the node introduces when it forwards data.
- `bw:` The maximum bandwidth the node supports.  The all links using that
  node share that bandwidth.

If a network node is specified “outside” of a network, if may belong to
multiple networks.  An example is a node connected to both Ethernet and WLAN.
If a network node is attached to a single network, it should be defined in
that network.

    node worker provides cpu:1000 ram:8GiB
    node network
    node network with lat:10µs bw:40GiB

### Networks

A network consists of nodes and links.  The nodes may be a subset of the
workers nodes or network nodes defined “within” a network.  For a network the
following options may be given:

- `mtu:` The maximum size of a packet in the network.  Solver may use this
  value to better calculate bandwidth usage on a link.
- `bearer:` The name of the network the network is using.  All nodes and
  links of the base network are also part of the using network.  The two
  networks “share” the bandwidth of the links.
- `qos:` The service class the network supports.  The service class of a
  network using another network may be “higher” than the one of the base
  network (e.g., TSN (type assured) running on Ethernet (type best-effort)).
- `latency:` Default latency for links in the network.  If no latency is
  given for a link, Solver uses this latency.
- `bandwidth:` Default bandwidth for links in the network.  If no bandwidth
  is given for a link, Solver uses this bandwidth.
- `type:` The type of network.  A network of type “radio” has one medium for
  all its links and a network of type “wire” has one or two media per link.
- `links:` The type of links in a network.  If a network has links of type
  “duplex” it is possible to send and receive data on the same like at the
  same time.  For a half-duplex link one can only send or receive at any
  point in time.  In radio networks, links are usually half-duplex.

It is an error to specify an option more than once.  Option `bearer:` and
options `mtu:`, `type:`, and `links:` are exclusive as the network referred to
by option `bearer:` defines all latter options.  It is an error to specify
option `bearer:` with any of the options `mtu:`, `type:`, or `links:`.

A network can be the bearer for more than one network.  It is an error if a
network that has a bearer is the bearer for another network.

If options `qos:`, `latency:`, or `bandwidth:` are used with option `bearer:`
they set corresponding option to be different from the one from the bearer
network.  Not any value is valid.  Option `qos:` can be changed only from
“best-effort” to “assured” (i.e., upgraded).  A new value for `latency:` can
only be greater than or equal to the one from the bearer network. A new value
for option `bandwidth:` has to be smaller or equal to the one from the bearer
network.

The total bandwidth of a network node may not be available due so some already
deployed applications.  Similar to the CPU or RAM of a worker node the volume
can be given as a term “1000-500” where 1000 is the gross bandwidth and 500
the already allocated amount.

    Network ::= "network" NetworkName ("with" NetOption+ | "(" "continued" ")")?
                ("nodes" "(" NodeName+ ")")? (NetworkNode | Link)* (Paths)*
    NetOption ::= "mtu:" Integer DataUnit?
                  | "bearer:" NetworkName
                  | QoS | Latency | Bandwidth
                  | "type:" ("wire" | "radio")
                  | "links:" ("duplex" | "half-duplex")
    NetworkNode ::= "node" NameList ("with" CommRsrc+)?
    CommRsrc ::= Latency
                 | ("bw:" | "bandwidth:") VolumeTerm ("," VolumeTerm)?
    VolumeTerm ::= Volume ("-" Volume)?

When describing a model using multiple sections, each may contribute something
to particular network.  When there are new worker nodes they are normally part
of an earlier defined network.  The example below shows how a network from an
earlier section can be continued in a later section.

In the example, the first unnamed section defines network “eth” to have two
worker nodes “c1” and “c2” and a network node “sw” connected to each worker
node.  Section “sec-2” adds an additional worker node “c3” and extends network
“eth”.  The extension of network “eth” is indicated by “(continued)”.  Without
this indication, the definition of network “eth” raises a duplicate network
error.  The new worker node is connected to node “sw” and it can communicate
with the other worker nodes.

    workload assignment v1
    model two-sections
    
    application a1 workload w1,w2 channel w1--w2
    infrastructure
    node c1,c2 provides cpu:2000 ram:4GiB
    network eth with bw:100Mb
    node sw
    link c1--sw,c2--sw
    find paths (c1•c2)
    
    section sec-2
    application a2 workload w1,w2 channel w1--w2
    infrastructure
    node c3 provides cpu:4000 ram:16GiB
    network eth (continued)
    link c3--sw
    find paths (c1•c3 c2•c3)

#### Links

Links connect nodes within a network.  A link is directional, i.e., a link
from node A to node B connects the nodes and allows node A to send data to
node B but it does not allow node B to send data to node A.  A second link
must be specified for the “reverse” direction.  If there is a link between
node A and node\_B in network N, this does not mean that there is a link
between these nodes in network M.  The link exists in network M only if
network N is a bearer network of network M.

A network link has the properties below.  One may specify properties for a
link in a network using another network.

- `lat:` The latency when sending links over this link.
- `bw:` The bandwidth available on the link.

Similar to a channel latency and bandwidth of a two-way link can differ
between its two directions.  The network options `type:` and `links:`
influence how the bandwidth given for a link is used.  There is only one link
between two nodes in a network.

    Link ::= "link" LinkNames ("with" LinkRsrc+)?
    LinkNames ::= Name? NodePair ("," Name? NodePair)*
    NodePair ::= NodeName ("•" | OneWayOp | TwoWayOp) NodeName
    LinkRsrc ::= CommRsrc | "path:" True

In the example below the first pair of links is identical to the third
declaration of a two-way link.  This is an asymmetric link with some of it
gross bandwidth already used.  The fourth declaration shows how to define
multiple links having the same properties with a single declaration.

    link A→B with lat:50µs bw:1Gb-100Mb
    link B→A with lat:1ms bw:100Mb-8Mb
    
    link A↔B with lat:50µs,1ms bw:1Gb-100Mb,100Mb-8Mb
    
    link A↔B,C↔D,E→F with lat:75µs bw:100Mb

#### Paths

The final piece of a network definition is the list of paths in the network.
A path is a sequence of links in a network.  A link between two nodes does
*not* define a path between those nodes unless the clause “path:yes” is
specified for that link.  A path does not span multiple networks.  The list of
paths defines which nodes can communicate with each other.  If there is a path
between two worker nodes, these nodes can communicate within the network.

It is not an error if there is no path between two worker nodes in a network.
If a network of type “assured” for instance uses another network as its
bearer, typically not all nodes are part of the former network as it requires
usually some special network equipment.

The number of paths in a network even with only a few nodes is pretty large.
One can specify the list of paths explicitly or use command `find` to
determine the paths between two nodes.  Command `find` returns *all* paths
between two nodes.  It is not an error if a network has more than one path
between two nodes.

In a network there is a single link between two nodes so most paths are given
as a sequence of nodes (e.g., “A→B→C”).  Solver builds a path by considering
the path’s node pairs.  For each pair it searches for a link between the two
nodes and uses this link’s name in the path.  If there is no such link, Solver
computes a name (e.g., “A→B”).  This behaviour is useful when setting up
“broken” infrastructure for testing.

Sometimes it is more convenient to specify a path as a sequence of links given
by their names (e.g., “1,L2,3”).  In this case the path consists of the given
links without any further checks.  You must specify at least two links for a
path.  Use `path:yes` for a link that is also a path.

    Paths ::= "find" ("all" | "shortest")? "paths" "(" NodePair+ ")"
              | "paths" "(" Path+ ")"
    Path ::= Name? (NodePath | LinkPath)
    NodePath ::= NodeName ((OneWayOp NodeName)+ | (TwoWayOp NodeName)+)
    LinkPath ::= LinkName ("," LinkName)*

In the following example the three lists of paths are identical.  The network
has nodes A, B, C, and S, where node S is a switch that the other nodes are
connected to with two-way links.  A literal path is given by the list of
nodes.  A path should not contain any cycle.

    find paths (A•B A•C)
    
    paths (A↔S↔B A↔S↔C)
    
    paths (A→S→B B→S→A A→S→C C→S→A)

## Workload Information

The information available so far is sufficient to make a decision about where
a workload should be placed in the cluster considering also its network
requirements as definied by the channels.  When searching for a solution,
Solver orders the nodes with respect to a “priority” computed similar to the
one that Kubernetes uses.  It takes the amount CPU and and RAM of workload,
nodes, and other workloads on a node into account.

This default ordering might not always be the “best” way to order the nodes
for a given workload in certain scenarios.  One can specify [costs](#costs) or
[recommendations](#recommendations) for a workload to order the nodes differently and thus
influence the way Solver looks for a solution.  It is an error to specify both
recommendations and costs for a workload.  Note that a low cost or a high
recommendation does *not* force Solver to place the workload on this node.
All other requirements must be met as well (e.g., network bandwidth).

### Costs

Specifying costs for a workload inidcates how expensive it is to run a
workload on a node.  The cost for a workload is a positive value.  One can
specify a cost for running any workload on a particular node or for a specific
workload on different nodes.  If a cost is not specified, it defaults to 0.
When a cost is specified for both a node and a workload on a node, the cost
for the workload is used.

Solver uses the costs assigned to the nodes suitable to run a workload
(considering CPU and RAM requirements) to order them so that the node-specific
costs increases.  Solver tries cheaper nodes first.

For technical reasons Solver does not use the cost values directly but maps
them into the range [0,1000].  Solver uses the largest given cost value to
map the costs into that range.

    Costs ::= "costs" (NodeCosts | WorkloadValues) WorkloadValues*
    NodeCosts ::= "nodes" NodeValues
    WorkloadValues ::= "workload" WorkloadRef NodeValues
    NodeValues ::= "(" (K8sName ":" Float)+ ")"

In the example below the cost for running workloads on node N is 10 and the
cost for running workload A•W on nodes N and M is 5.  Hence, the cost for
running workload A•W on node N is 5.

    costs
    nodes (N:10)
    workload A•W (N:5 M:5)

### Recommendations

Costs as defined in the [previous section](#costs) are sometimes not appropriate and it
is better to specify a preference for different nodes for a workload, i.e.,
one specifies that a given node should have a strong preference (e.g., 90%) to
be used for a given workload.

One can specify a recommendation for one or more nodes for a workload.  The
value for a recommendation is from the range [0,100].  A recommendation for a
workload applies to *all* nodes in the cluster.  Nodes not explicitly listed
have a recommendation value 0.  Nodes with recommendation value 0 may still be
used for workloads, but Solver considers them last.

    Recommendations ::= "recommendations" WorkloadValues+

In the example below the recommendation states that workload A•W1 should run
more preferably on node N than on node M or on node O.  The second statement
for A•W2 gives the same recommendations with the role of M and N reversed.
Recommendations give a hint to Solver but do *not* enforce or prevent an
assignment as network and other criteria must be fulfilled.

    recommendations
    workload A•W1 (N:90 M:30)
    workload A•W2 (M:90 N:30 O:0)

## Assignments

For testing it is handy to be able to specify an assignment to be returned
by Solver.  An assignment consists of the list of assigned workloads followed
by the list of assigned channels.  A workload is assigned to a node and a
channel is assigned to a network path.  It is necessary to specify the network
explicitly as names of paths are unique only within a network, i.e., two
networks can both have a path named “c1→³c2”.

    Assignments ::= "assignments"? Assignment+
    Assignment ::= "assignment" WorkloadAsgmt+ ChannelAsgmt*
    WorkloadAsgmt ::= "workload" WorkloadRef "on" NodeName
    ChannelAsgmt ::= "channel" ChannelRef "on" PathRef ("," PathRef)?
    ChannelRef ::= SrcDst | ChannelName

The example below is an assignment of two workloads to node C.  The channel
between the two workloads uses a path for a link from node C to node C, i.e.,
it communicates using the loopback interface.  Note that it is necessary to
specify explicitly that the loopback interface is available and should be used
on a particular node.

    assignment
    workload A•W1 on C
    workload A•W2 on C
    channel A•W1→A•W2 on C→C

## Markdown Notation

Solver’s text format is a simple and concise notation for assignment problems,
but specifying many objects can be inconvenient at times.  A tabular format
for the objects in an assignment may be easier to use and check for
consistency and correctness.  Solver supports Markdown documents for this
purpose.  The user can specify objects like nodes or workloads in a tabular
notation and provide some explanations for the model.

When handling a Markdown document Solver converts it to text format and passes
this text to the function handling text format.  The Markdown document is
“preprocessed” to text format and *not* handled directly.  Solver just copies
table cells verbatim into the text format, i.e., they must have the proper
format defined in the grammar.

Below is a small example of an assignment problem in Markdown notation. The
Markdown text and the resulting text format is shown side by side.  All text
from the Markdown text that is not a headline or a table of a certain “class”
is dropped.

    <!-- workload assignment v1 --> │ workload assignment v1
    # Model “problem”               │ Model “problem”
                                    │
    Solver ignores descriptive text │
    when converting a problem from  │
    Markdown to text format.        │
                                    │
    ## Applications                 │ Applications
                                    │
    ### Application “App”           │ Application “App”
                                    │
    | Workload | CPU | RAM    |     │
    |----------|-----|--------|     │
    | w1       | 500 | 512MiB |     │ workload w1 needs cpu:500 ram:512MiB
    | w2       | 250 | 256MiB |     │ workload w2 needs cpu:250 RAM:256MiB
                                    │
    | Channel | Lat  | BW  |        │
    |---------|------|-----|        │
    | w1—w2   | 10ms | 1Mb |        │ channel w1—w2 needs lat:10ms bw:1Mb
                                    │
    ## Infrastructure               │ Infrastructure
                                    │
    | Node  | CPU  | RAM  |         │
    |-------|------|------|         │
    | c1,c1 | 1000 | 8GiB |         │ node c1,c2 provides cpu:1000 ram:8GiB
                                    │
    ### Network “eth“               │ Network “eth”
                                    │
    | Node   |                      │
    |--------|                      │
    | switch |                      │ node switch
                                    │
    | Link      | Lat   | BW    |   │
    |-----------|-------|-------|   │
    | c1–switch | 100µs | 100Mb |   │ link c1—switch with lat:100µs bw:100Mb
    | c2-switch | 100µs | 100Mb |   │ link c2—switch with lat:100µs bw:100Mb
                                    │
    | Find Paths |                  │
    |------------|                  │ find paths (
    | c1•c2      |                  │    c1•c2
                                    │ )

When converting a Markdown document to text format, Solver considers the
document line by line.  Only the comment with document type and version,
headlines and tables are converted to text format.  Solver converts tables of
types `workload`, `channel`, `node`, and `link` for objects.  A table row
defines an object where the first column has the object’s name and the other
columns additional properties.  The header of a table defines the object’s
type and properties.  Some tables are special:

- `option`: The table has two columns defining a list of options for a model
  or network.  It must follow “immediately” the headline it refers to.
- `find`: The table contains a single column with list of node pairs for
  which Solver should determine network paths.  The table’s type specifies
  the command’s type.
- `nodes`: The table contains a single column with the nodes that a network
  “selects” from its bearer network.
- `paths`: The table contains a single column with network paths.  Each path
  is a list of network links.

Note that Markdown is only supported as an input format.  Solver does not
create output in Markdown format.

An assigment problem in Markdown format can be treated as a “living”
specification for an assignment problem.  Users can use multiple sections for
improved readability and add text explaining the requirements for a particular
application or the network’s structure.  The rendered Markdown document is
more readable than the “pure” specifaction in Solver’s text format and be used
directly by Solver to check for instance whether the given infrastructure
supports the needs of all applications and channels.

## Default Values

This section is about default values Solver uses when reading an assignment
file.  Package `internal/defaults` defines all default values for Solver.

- Model
  - `compound-names`: `false`
  - `implicit-reverse-path`: `false`
  - `shortest-path-only`: `true`
- Workload
  - `cpu:` `100`
  - `ram:` `1073741824`
- Channel
  - `lat:` `1000000`
  - `bw:` `1000000`
  - `qos:` `"best-effort"`
- Worker nodes
  - `cpu:` `1000`
  - `ram:` `1073741824`
- Network
  - `qos:` `"best-effort"`
  - `type:` `"wire"`
  - `links:` `"duplex"`
- Network link
  - `lat:` `100000`
  - `bw:` `1000000000`

## Complete Grammar

For reference purposes the complete grammar for the text format of an
assignment problem is shown below.  The grammar does not define explicitly the
tokens the scanner returns (e.g., an `Integer` or a `K8sName`).  A terminal
symbol like `"workload"` in the grammar matches with the text in the file
without considering case.  The terminal `"workload"` matches “workload”,
“Workload”, “WORKLOAD”, or even “WoRkLoAd”.

    ProblemSpec ::= Header Section+ Assignments?
    Section ::= ("section" k8sName)? Applications Infrastructure WlInfos
    WlInfos ::= Costs? Recommendations?
    Header ::= "workload" "assignment" Version Model
    Version ::= "v" Integer
    Model ::= "model" K8sName (("options" | "with") Option+)?
    Option ::= K8sName ":" (True | False)
    True ::= "true" | "yes" | "on"
    False ::=  "false" | "no" | "off"
    
    Applications ::= "applications"? Application (Application | Channel)*
    Application ::= "application" K8sName Workload+
    Workload ::= "workload" NameList ("on" NameList)? ("needs" WlNeeds+)?
    NameList ::= K8sName ("," K8sName)*
    WlNeeds ::= Labels | "cpu:" CPU | ("ram:" | "memory:") RAM
    CPU ::= Integer "mCPU"?
    RAM ::= Integer DataUnit?
    DataUnit ::= [KMG]? i? ("B" | "Byte"| "b" | "Bit")
    Labels ::= "labels:" "(" ([#x00AC!~-]? K8sName)+ ")"
    Channel ::= "channel" ChSpecs ("on" PathRefs)? ("needs" CommNeed+)?
    ChSpecs ::= Name? SrcDst ("," Name? SrcDst)*
    Name ::= (K8sName | Integer) ("," (K8sName | Integer))? ":"
    PathRefs ::= PathRef ("," PathRef)*
    PathRef ::= NetworkName "•" PathName
    SrcDst ::= WorkloadRef (OneWayOp | TwoWayOp) WorkloadRef
    WorkloadRef ::= (AppName "•")? WlName
    TwoWayOp ::= #x2194 /* "↔" */ | "<->" | #x2014 /* "—" */ | "--"
    OneWayOp ::= #x2192 /* "→" */ | "->"
    CommNeed ::= Latency | Bandwidth | QoS
    Latency ::= ("lat:" | "latency:") Duration ("," Duration)?
    Duration ::= Integer ([mµun]? "s")?
    Bandwidth ::= ("bw:" | "bandwidth:") Volume ("," Volume)?
    Volume ::= Integer ([*#x00D7] /* "×" */ Integer)? DataUnit?
    QoS ::= "qos:" QoSType ("," QoSType)? 
    QoSType ::= "best-effort" | "assured"
    
    Infrastructure ::= "infrastructure"? Node (Node | Network)*
    Node ::= "node" NameList ("provides" CompRsrc+ | "with" CommRsrc+)?
    CompRsrc ::= Labels
                 | "cpu:" CPU ("-" CPU)?
                 | ("ram:" | "memory:") RAM ("-" RAM)?
    Network ::= "network" NetworkName ("with" NetOption+ | "(" "continued" ")")?
                ("nodes" "(" NodeName+ ")")? (NetworkNode | Link)* (Paths)*
    NetOption ::= "mtu:" Integer DataUnit?
                  | "bearer:" NetworkName
                  | QoS | Latency | Bandwidth
                  | "type:" ("wire" | "radio")
                  | "links:" ("duplex" | "half-duplex")
    NetworkNode ::= "node" NameList ("with" CommRsrc+)?
    CommRsrc ::= Latency
                 | ("bw:" | "bandwidth:") VolumeTerm ("," VolumeTerm)?
    VolumeTerm ::= Volume ("-" Volume)?
    Link ::= "link" LinkNames ("with" LinkRsrc+)?
    LinkNames ::= Name? NodePair ("," Name? NodePair)*
    NodePair ::= NodeName ("•" | OneWayOp | TwoWayOp) NodeName
    LinkRsrc ::= CommRsrc | "path:" True
    Paths ::= "find" ("all" | "shortest")? "paths" "(" NodePair+ ")"
              | "paths" "(" Path+ ")"
    Path ::= Name? (NodePath | LinkPath)
    NodePath ::= NodeName ((OneWayOp NodeName)+ | (TwoWayOp NodeName)+)
    LinkPath ::= LinkName ("," LinkName)*
    
    Assignments ::= "assignments"? Assignment+
    Assignment ::= "assignment" WorkloadAsgmt+ ChannelAsgmt*
    WorkloadAsgmt ::= "workload" WorkloadRef "on" NodeName
    ChannelAsgmt ::= "channel" ChannelRef "on" PathRef ("," PathRef)?
    ChannelRef ::= SrcDst | ChannelName
    
    Costs ::= "costs" (NodeCosts | WorkloadValues) WorkloadValues*
    NodeCosts ::= "nodes" NodeValues
    WorkloadValues ::= "workload" WorkloadRef NodeValues
    NodeValues ::= "(" (K8sName ":" Float)+ ")"
    
    Recommendations ::= "recommendations" WorkloadValues+
    
    AppName ::= K8sName
    WlName ::= K8sName
    ChannelName ::= K8sName | Integer
    NodeName ::= K8sName
    NetworkName ::= K8sName
    PathName ::= K8sName | Integer
    LinkName ::= K8sName | Integer
    K8sName ::= "[a-zA-Z0-9]([a-zA-Z0-9.-]*[a-zA-Z0-9])?"
    Integer ::= "[0-9]+"
    Float ::= "-"? Integer ("." Integer)? ("e" "-"? Integer)?
