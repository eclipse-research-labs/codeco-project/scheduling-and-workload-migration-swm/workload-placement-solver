// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package defaults

const (
	// Model Options
	CompoundNames       bool = false
	ImplicitReversePath bool = false
	ShortestPathsOnly   bool = true

	// Workload Properties
	CPU int32 = 100     // mCPU
	RAM int64 = 1 << 30 // 1GiB

	// Channel Properties
	Latency   int64 = 1000000 // 1ms
	Bandwidth int64 = 1000000 // 1Mb

	// Node Properties
	CPUNode int32 = 1000    // mCPU (one CPU)
	RAMNode int64 = 1 << 30 // 1GiB

	// Network Properties
	NetworkQoS   string = "best-effort"
	NetworkType  string = "wire"
	NetworkLinks string = "duplex"

	// Link Properties
	LatencyLink   int64 = 100000     // 100µs
	BandwidthLink int64 = 1000000000 // 1Gb
)
