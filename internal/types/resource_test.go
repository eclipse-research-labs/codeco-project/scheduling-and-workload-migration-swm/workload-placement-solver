// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package types

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func restore[T any](who *T, value T) func() {
	store := *who
	*who = value
	return func() {
		*who = store
	}
}

func TestResourceString(t *testing.T) {
	defer restore(&ResourceShowPercent, false)
	r1 := Resource[int]{Cap: 100, inUse: 50}
	assert.Equal(t, "50/100", r1.String())
	r2 := Resource[float64]{Cap: 100.0, inUse: 50.0}
	assert.Equal(t, "50/100", r2.String())
}

func TestResourceString_percent(t *testing.T) {
	defer restore[bool](&ResourceShowPercent, true)()
	r1 := Resource[int]{Cap: 100, inUse: 50}
	assert.Equal(t, "50.00%", r1.String())
	r2 := Resource[float64]{Cap: 100.0, inUse: 50.0}
	assert.Equal(t, "50.00%", r2.String())
}

func TestResourceBandwidthString(t *testing.T) {
	b := NewResource[Bandwidth](16 * 1000)
	b.Use(12 * 1000)

	defer restore(&ResourceShowPercent, false)()
	check := func(percent bool, exp ...string) {
		ResourceShowPercent = percent
		assert.Equal(t, exp[0], b.String())
		assert.Equal(t, exp[1], fmt.Sprintf("b:%s", b))
	}
	check(false, "12Kb/16Kb", "b:12Kb/16Kb")
	check(true, "75.00%", "b:75.00%")
}

func TestResourceIntMgmt(t *testing.T) {
	assert := assert.New(t)
	s := Resource[int]{Cap: 10}
	assert.Equal(10, s.Available())
	assert.True(s.Use(5))
	assert.Equal(5, s.Available())
	assert.True(s.Use(5))
	assert.Equal(0, s.Available())
	assert.False(s.Use(5))
	assert.Equal(0, s.Available())
	assert.Equal(15, s.inUse)
	s.Free(5)
	assert.Equal(10, s.inUse)
	s.Free(5)
	assert.Equal(5, s.inUse)
	s.Free(5)
	assert.Equal(0, s.inUse)
}

func TestResourceReset(t *testing.T) {
	assert := assert.New(t)
	r := Resource[int]{Cap: 10}
	assert.True(r.Load(5))
	assert.Equal(Resource[int]{Cap: 10, inUse: 5, load: 5}, r)
	assert.True(r.Use(3))
	assert.Equal(Resource[int]{Cap: 10, inUse: 8, load: 5}, r)
	assert.False(r.Use(3))
	assert.Equal(Resource[int]{Cap: 10, inUse: 11, load: 5}, r)
	r.Reset()
	assert.Equal(Resource[int]{Cap: 10, inUse: 5, load: 5}, r)

	r = Resource[int]{Cap: 10}
	assert.True(r.Use(3))
	assert.Equal(Resource[int]{Cap: 10, inUse: 3, load: 0}, r)
	r.Reset()
	assert.Equal(Resource[int]{Cap: 10, inUse: 0, load: 0}, r)
	assert.True(r.Use(3))
	assert.Equal(Resource[int]{Cap: 10, inUse: 3, load: 0}, r)
	assert.True(r.Load(5))
	assert.Equal(Resource[int]{Cap: 10, inUse: 8, load: 5}, r)
	r.Reset()
	assert.Equal(Resource[int]{Cap: 10, inUse: 5, load: 5}, r)
}

func TestResourceFloatMgmt(t *testing.T) {
	assert := assert.New(t)
	s := Resource[float64]{Cap: 10}
	assert.True(s.Use(5))
	assert.True(s.Use(5))
	assert.False(s.Use(5))
	assert.Equal(15.0, s.inUse)
	s.Free(5)
	assert.Equal(10.0, s.inUse)
	s.Free(5)
	assert.Equal(5.0, s.inUse)
	s.Free(5)
	assert.Equal(0.0, s.inUse)
}

func TestResourceWithBase(t *testing.T) {
	assert := assert.New(t)
	base := NewResource[int](100)
	r1 := base.Bear(50)
	require.Same(t, base, r1.base)
	assert.True(r1.Use(30))
	assert.Equal(30, r1.inUse)
	assert.Equal(30, base.inUse)

	assert.False(r1.Use(30))
	assert.Equal(60, r1.inUse)
	assert.Equal(60, base.inUse)

	r1.Free(30)
	assert.Equal(30, r1.inUse)
	assert.Equal(30, base.inUse)

	base = NewResource(50)
	r1 = base.Bear(100)
	assert.True(r1.Use(30))
	assert.Equal(30, r1.inUse)
	assert.Equal(30, base.inUse)

	assert.False(r1.Use(30))
	assert.Equal(60, r1.inUse)
	assert.Equal(60, base.inUse)

	base = NewResource(100)
	r1 = base.Bear(50)
	assert.True(r1.Use(25))
	assert.True(base.Use(60))
	assert.False(r1.Use(25))
	assert.Equal(110, base.inUse)
	assert.Equal(50, r1.inUse)
	r1.Free(25)
	assert.Equal(85, base.inUse)
	assert.Equal(25, r1.inUse)
}
