// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package types

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

var ResourceShowPercent bool

type Resource[U constraints.Integer | constraints.Float] struct {
	Cap   U
	inUse U
	load  U
	base  *Resource[U]
}

func NewResource[U constraints.Integer | constraints.Float](cap U) *Resource[U] {
	return &Resource[U]{Cap: cap}
}

func (r *Resource[U]) Bear(cap U) *Resource[U] {
	return &Resource[U]{Cap: cap, base: r}
}

func (r *Resource[U]) Used() U { return r.inUse }

func (r *Resource[U]) String() string {
	if ResourceShowPercent {
		return fmt.Sprintf("%.2f%%", float64(r.inUse)*100/float64(r.Cap))
	}
	return fmt.Sprintf("%v/%v", r.inUse, r.Cap)
}

func (r *Resource[U]) Load(l U) bool {
	r.load = l
	return r.Use(l)
}

func (r *Resource[U]) Available() U {
	d := r.Cap - r.inUse
	if d < 0 {
		d = 0
	}
	return d
}

func (r *Resource[U]) Use(x U) bool {
	r.inUse += x
	ok := r.inUse <= r.Cap
	if r.base != nil {
		return r.base.Use(x) && ok
	}
	return ok
}

func (r *Resource[U]) Free(x U) {
	r.inUse -= x
	if r.base != nil {
		r.base.Free(x)
	}
}

func (r *Resource[U]) Reset() {
	r.Free(r.inUse - r.load)
}
