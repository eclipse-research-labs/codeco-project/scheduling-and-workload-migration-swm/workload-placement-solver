// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package types

import (
	"fmt"
	"strconv"
)

type Bandwidth int64

func (b Bandwidth) String() string {
	if b >= 1000 {
		f := Bandwidth(1000)
		for _, p := range "KMGTPE" {
			if b < f*Bandwidth(1000) {
				return fmt.Sprintf("%.4g%cb", float64(b)/float64(f), p)
			}
			f *= Bandwidth(1000)
		}
	}
	return strconv.FormatInt(int64(b), 10)
}
