// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package types

type Option[T any] struct {
	value T
	set   bool
}

func NewOption[T any](v T) Option[T] {
	return Option[T]{value: v}
}

func (o *Option[T]) Set(v T) {
	o.value, o.set = v, true
}

func (o *Option[T]) WasSet() bool {
	return o.set
}

func (o *Option[T]) Value() T {
	return o.value
}
