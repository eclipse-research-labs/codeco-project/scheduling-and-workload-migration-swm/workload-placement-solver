// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOptionMgmt(t *testing.T) {
	assert := assert.New(t)
	option := Option[int]{}
	assert.Equal(0, option.value)
	assert.False(option.WasSet())
	option.Set(17)
	assert.Equal(17, option.value)
	assert.True(option.WasSet())

	option = NewOption(17)
	assert.Equal(17, option.value)
	assert.False(option.WasSet())
	option.Set(19)
	assert.Equal(19, option.value)
	assert.True(option.WasSet())
}
