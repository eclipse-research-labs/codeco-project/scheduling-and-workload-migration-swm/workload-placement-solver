// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBandwidthString(t *testing.T) {
	testCases := []struct {
		b Bandwidth
		s string
	}{{123, "123"},
		{1500, "1.5Kb"},
		{10_000_000, "10Mb"},
		{10_000_000_000, "10Gb"}}
	for _, tc := range testCases {
		assert.Equal(t, tc.s, tc.b.String())
	}
}
