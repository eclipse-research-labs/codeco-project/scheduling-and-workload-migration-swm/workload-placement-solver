// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package grpc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInfraModelFindNode(t *testing.T) {
	im := &Infrastructure{
		Nodes: []*Node{{Id: "N1"}, {Id: "N2"}}}
	assert.Equal(t, 0, im.FindNode("N1"))
	assert.Equal(t, 1, im.FindNode("N2"))
	assert.Equal(t, -1, im.FindNode("Nx"))
}

func TestInfraModelFindLink(t *testing.T) {
	im := &Infrastructure{Networks: []*Network{{Links: []*Network_Link{
		{Source: "A", Target: "B"},
		{Source: "A", Target: "C"},
		{Source: "B", Target: "D"}}}}}
	assert.Equal(t, 0, im.FindLink("A", "B"))
	assert.Equal(t, 1, im.FindLink("A", "C"))
	assert.Equal(t, 2, im.FindLink("B", "D"))
	assert.Equal(t, -1, im.FindLink("X", "D"))
	assert.Equal(t, -1, im.FindLink("A", "X"))
}

func TestCostEmpty(t *testing.T) {
	var c Cost
	assert.True(t, (*Cost).Empty(nil))
	assert.True(t, c.Empty())

	c.Nodes = []*NodeValue{}
	c.Workloads = []*Cost_Workload{}
	assert.True(t, c.Empty())

	c.Workloads = []*Cost_Workload{{Application: "A", Workload: "W"}}
	assert.True(t, c.Empty())

	nv := &NodeValue{Node: "X", Value: 1}
	c.Nodes = []*NodeValue{nv}
	c.Workloads = []*Cost_Workload{}
	assert.False(t, c.Empty())

	c.Nodes = []*NodeValue{}
	c.Workloads = []*Cost_Workload{
		{Application: "A", Workload: "W", Nodes: []*NodeValue{nv}}}
	assert.False(t, c.Empty())
}

func TestRecommendationEmpty(t *testing.T) {
	var r Recommendation
	assert.True(t, (*Cost).Empty(nil))
	assert.True(t, r.Empty())

	r.Workloads = []*Recommendation_Workload{}
	assert.True(t, r.Empty())

	r.Workloads = []*Recommendation_Workload{{Application: "A", Workload: "W"}}
	assert.True(t, r.Empty())

	nv := &NodeValue{Node: "X", Value: 1}
	r.Workloads = []*Recommendation_Workload{
		{Application: "A", Workload: "W", Nodes: []*NodeValue{nv}}}
	assert.False(t, r.Empty())
}
