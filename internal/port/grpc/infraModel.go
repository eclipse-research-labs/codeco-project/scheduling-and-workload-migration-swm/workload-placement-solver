// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package grpc

type NodeLoad struct {
	Id  string
	CPU int32
	RAM int64
}

type NetworkNodeLoad struct {
	Id  string
	Cap int64 // bit/s
}

type LinkLoad struct {
	Src string // node name
	Dst string // node name
	Cap int64  // bit/s
}

func (im *Infrastructure) FindNode(name string) int {
	for i := range im.Nodes {
		if im.Nodes[i].Id == name {
			return i
		}
	}
	return -1
}

func (im *Infrastructure) FindMedium(name string) *Medium {
	for _, net := range im.Networks {
		for _, m := range net.Media {
			if m.Id == name {
				return m
			}
		}
	}
	return nil
}

func (im *Infrastructure) FindLink(src, dst string) int {
	for _, nw := range im.Networks {
		for i, l := range nw.Links {
			if l.Source == src && l.Target == dst {
				return i
			}
		}
	}
	return -1
}

func (r *Resource) Use(x int64) {
	r.Used += int64(x)
	if r.Used > r.Capacity {
		r.Used = r.Capacity
	}
}

func (r *Resource) Available() int64 {
	if x := r.Capacity - r.Used; x >= 0 {
		return x
	}
	return 0
}

func (c *Cost) Empty() bool {
	switch {
	case c == nil:
		return true
	case len(c.Nodes) > 0:
		return false
	case len(c.Workloads) == 0:
		return true
	default:
		for _, w := range c.Workloads {
			if len(w.Nodes) > 0 {
				return false
			}
		}
		return true
	}
}

func (r *Recommendation) Empty() bool {
	switch {
	case r == nil:
		return true
	case len(r.Workloads) == 0:
		return true
	default:
		for _, w := range r.Workloads {
			if len(w.Nodes) > 0 {
				return false
			}
		}
		return true
	}
}
