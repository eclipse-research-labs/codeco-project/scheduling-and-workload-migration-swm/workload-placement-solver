# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

.DEFAULT_GOAL:=help
.RECIPEPREFIX=>
ifeq ($(filter undefine,$(value .FEATURES)),)
$(error Make v$(MAKE_VERSION) is not supported. $\
        Use at least v3.82)
endif

VERSION_ALPINE?=3.18.6

define mkdir # path(s)
$(eval $(1): ; mkdir -p $$@)
endef
define copy-rule # dst,src,pat
$(1)/: ; mkdir -p $$@
$(1)/$(3): $(2)/$(3) | $(1)/ ; cp $$< $$@
endef
define cleaner # name,paths,dep
.PHONY: clean-$(1)
.IGNORE: clean-$(1)
clean: clean-$(1)
clean-$(1): $(3)
>rm -rf $$(to-remove) $(2)
endef
clean-up=$(eval $(call cleaner,$(1),$(2)))

ifndef SOLVER_IGNORE_SETTINGS
ifneq "$(USER)" ""
-include $(USER).mk
endif
endif
SOLVER_BUILD_MODE?=docker
ifndef CI
ifeq ($(SOLVER_BUILD_MODE),docker)
useDocker:=yes
endif
else
ifeq ($(CI),local)
useDocker:=yes
endif
export CI_REGISTRY_IMAGE?=cr.siemens.com/itp_cloud_research/qos-solver
regis:=$(CI_REGISTRY_IMAGE)/
endif
export DOCKER_CLI_HINTS=false
define run # tool,image,dir,var(s)
$(if $(useDocker),docker run$(if $(DRFLAGS), $(DRFLAGS)) --rm $\
--volume $${PWD}:/project --workdir=/project$(if $(3),/$(3)) $\
$(if $(4),$(addprefix --env ,$(4)) )$($(2)IN):$($(2)V) $(1),$\
$(if $(3),cd $(3) ; )$(if $(4),$(strip $(4)) )$(1))
endef

lparen:=(
rparen:=)
comma:=,
space:=$(subst X, ,X)

make-list=$(subst $(space),$(comma),$(1))
arch=$(strip $(1)$(lastword $(subst /, ,$(2))))
pkg-name=$(subst /,-,$(1))

local-build=$(if $(if $(strip $(CI)),$(strip $(filter local,$(CI))),t),t,)
local-arch:=$(if $(filter-out x86_64,$(shell uname -m)),arm64,amd64)
on-darwin:=$(if $(strip $(subst Darwin,,$(shell uname))),,yes)
curr-branch:=$(if $(local-build),$\
    $(shell git symbolic-ref --short HEAD | sed -Ee "/^[0-9]+-/s/^([0-9]+)-.*$$/0.\\1/"),$\
    $(or $(CI_COMMIT_REF_SLUG),$(shell git symbolic-ref --short HEAD)))
main-branch:=$(if $(strip $(filter-out $(or $(CI_DEFAULT_BRANCH),main master),$(curr-branch))),,yes)

moduleV:=$(strip $(if $(main-branch),$(shell cat VERSION),$(curr-branch)))
moduleN:=$(strip $(shell \
    sed -Ene "/^module/{s/^module[ \t]+([^ \t]+).*/\1/;p;}" go.mod))
moduleNN:=$(lastword $(subst -, ,$(lastword $(subst /, ,$(moduleN)))))

archs-ci:=$(if $(local-build),$(local-arch),$\
    $(or $(strip $(subst $(comma),$(space),$(runner_archs))),amd64 arm64))
archs-pi:=$(or $(strip $(subst $(comma),$(space),$(target_archs))),amd64 arm64)

#
#   #    # ###### #      #####
#   #    # #      #      #    #
#   ###### #####  #      #    #
#   #    # #      #      #####
#   #    # #      #      #
#   #    # ###### ###### #
#

define awk-gen-help:=
function line(t,d) {printf "%3s%-10s %s\n"," ",t,d}
/^## [a-zA-Z-]+ ::/ {
    n=split(substr($$0,4),a," :: ");
    line(a[1],a[2]);
    next
}
/^##\+ / { line("",substr($$0,5)); next }
/^##/ { printf "%s\n",substr($$0,4) }
END { if (n>0) { printf "\n" } }
endef
userFiles:=$(wordlist 2,$(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))

.PHONY: help
##
## Use one of the following goals:
##
## help :: Show this help text
help:
>@awk '$(strip $(awk-gen-help))' $(firstword $(MAKEFILE_LIST))
>$(if $(userFiles),@awk '$(strip $(awk-gen-help))' $(userFiles))

#
#    ####   ####  #      #    # ###### #####
#   #      #    # #      #    # #      #    #
#    ####  #    # #      #    # #####  #    #
#        # #    # #      #    # #      #####
#   #    # #    # #       #  #  #      #   #
#    ####   ####  ######   ##   ###### #    #
#

gitlab-ci:=.gitlab-ci.yml

.PHONY: solver all
## solver :: Create all deliverables and documentation
solver all: c-images lint reuse $(gitlab-ci) check p-images epub \
    windows-tools

versions:=SOLVER:VERSION\
    SCRIPT:build/ci/script/VERSION \
    GOENV:build/ci/goenv/VERSION \
    REUSE:build/ci/reuse/VERSION \
    MANIFEST:build/ci/manifest/VERSION \
    DOCENV:build/ci/docenv/VERSION \
    SHELLCHECK:build/ci/shellcheck/VERSION

$(foreach v,$(versions),$(eval \
VERSION_$(firstword $(subst :, ,$(v)))=$$(strip \
    $(if $(main-branch),$\
        $$(shell cat $(lastword $(subst :, ,$(v)))),$\
        $(curr-branch)))))

define sed-set-versions
$(foreach v,$(1),-e "/^ *VERSION_$(firstword $(subst :, ,$(v))):/s/[0-9.]+/$\
    $(shell cat $(lastword $(subst :, ,$(v))))/")
endef

$(gitlab-ci): $(patsubst %:,,$(subst :,: ,$(versions)))
>sed -Ei "" $(call sed-set-versions,$(versions)) $@

.PHONY: defaults
## defaults :: Update user’s settings file using
##+    mode=(local|docker)
defaults: mode?=docker
defaults: $(USER).mk
>grep "SOLVER_BUILD_MODE?=" $< >/dev/null \
    && sed -Ei "" -e "/^(SOLVER_BUILD_MODE\\?=).+$$/s//\\1$(mode)/" $<\
    || printf "SOLVER_BUILD_MODE?=$(mode)\n" >>$<

define user-defaults
# Variable SOLVER_BUILD_MODE defines which tools the Makefile uses:
#   - local: Use tools installed locally
#   - docker: Use tools in containers
SOLVER_BUILD_MODE=local
endef

$(USER).mk: export defaults=$(user-defaults)
$(USER).mk:
>@if [ ! -e $@ ] ; then printf "$${defaults}\n" >$@ ; else touch $@ ; fi

#
#    ####        # #    #   ##    ####  ######  ####
#   #    #       # ##  ##  #  #  #    # #      #
#   #      ##### # # ## # #    # #      #####   ####
#   #            # #    # ###### #  ### #           #
#   #    #       # #    # #    # #    # #      #    #
#    ####        # #    # #    #  ####  ######  ####
#

.PHONY: c-images clean-c-images
## c-images :: Create images for pipeline

c-image-names:=script manifest goenv reuse docenv shellcheck

include build/images.mk
include $(foreach i,$(c-image-names),build/ci/$(i)/Makefile)

.PHONY: clean-c-images
clean-c-images: $(foreach i,$(c-image-names),clean-ci-$(i))

$(foreach n,$(c-image-names),$(eval $\
    need-$n=$(if $(useDocker),$($(n)-$(local-arch)F))))

#
#   #####  ####   ####  #       ####
#     #   #    # #    # #      #
#     #   #    # #    # #       ####
#     #   #    # #    # #           #
#     #   #    # #    # #      #    #
#     #    ####   ####  ######  ####
#

export GOPROXY?=https://proxy.golang.org,direct
PROTOC=$(call run,protoc,goenv-amd64,$(tmpProtoD))
GOENVVARS:=CGO_ENABLED=0 CGO_LDFLAGS="-static"
GOFLAGS:=-tags=osusergo
sGOFLAGS+=-ldflags "-s -w$(if $(CI_PIPELINE_ID),\
 -X 'main.toolVersion=$(moduleV)+$(CI_PIPELINE_ID)')"

$(call mkdir,deliv/bin/)
$(call mkdir,deliv/bin/arm64/)
$(call mkdir,deliv/bin/amd64/)
$(call mkdir,deliv/bin/amd64-windows/)
.PHONY: tools
## tools :: Build all executables

.PHONY: windows-tools
windows-tools: $(addprefix deliv/bin/amd64-windows/,\
    $(addsuffix .exe,cmd-solve cmd-solved))

pkgGrpcN=grpc
pkgGrpcD=internal/port/$(pkgGrpcN)
grpcProtos=$(pkgGrpcD)/optimizer-service.proto \
    $(pkgGrpcD)/optimizer.proto
grpcSources=$(addprefix $(pkgGrpcD)/,\
    optimizer.pb.go optimizer_deepcopy.pb.go\
    optimizer-service.pb.go optimizer-service_grpc.pb.go)
tmpProtoD=tmp/proto/$(pkgGrpcN)

changeGoPackage=sed -Ee '/go_package/s|"[^"]+"|"../'$(pkgGrpcN)'"|' $\
    $(pkgGrpcD)/$(1) >$(tmpProtoD)/$(1)

$(call mkdir,$(tmpProtoD))
$(grpcSources)&: $(grpcProtos) $(need-goenv) | $(tmpProtoD)
>$(call changeGoPackage,optimizer.proto)
>$(PROTOC) --go_out=. --deepcopy_out=. optimizer.proto
>$(call changeGoPackage,optimizer-service.proto)
>$(PROTOC) -I. --go_out=. --go-grpc_out=. optimizer-service.proto
>cp $(addprefix $(tmpProtoD)/,$(notdir $(grpcSources))) $(@D)

define tools-for/vars # pkgN,pkgD,srcs
$(1)GF:=$$(wildcard $(2)/*.go) $(3)
$(1)S:=$$(filter-out %_test.go,$$($(1)GF))

endef
define tools-for/arch # main?,pkgN,pkgD,prereqs,binD,goVars,ext
$(2)B_$(notdir $(5)):=$(5)/$(2)$(7)
$$(patsubst deliv/%,%,$(5)/$(2)): $(5)/$(2)$(7)
$$(patsubst deliv/%,%,$(5)/$(2).test): $(5)/$(2).test$(7)
$(if $(1),tools: $(5)/$(2)$(7)
clean-tools: to-remove+=$(5)/$(2)$(7)
fake-tools: tool-names+=$(5)/$(2)$(7)
$(5)/$(2)$(7): $$($(2)S) $(4) $$(need-goenv) | $(5)/
>$(call run,go,goenv-$(local-arch),,$(GOENVVARS) $(6)) $\
 build $$(GOFLAGS) -o $$@ $(moduleN)/$(3)
)tools: $(5)/$(2).test$(7)
clean-tools: to-remove+=$(5)/$(2).test$(7)
fake-tools: tool-names+=$(5)/$(2).test$(7)
$(5)/$(2).test$(7): $$($(2)GF) $(4) $$(need-goenv) | $(5)/
>$(call run,go,goenv-$(local-arch),,$(GOENVVARS) $(6)) $\
 test -c -cover $$(GOFLAGS) -o $$@ $(moduleN)/$(3)
>chmod +x $$@
endef
define tools-for/pkg # pkgN,pkgD,main?,prereqs,srcs
$(call tools-for/vars,$(1),$(2),$(5))\
$(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin)\
$(if $(3),
$(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/arm64,GOARCH=arm64 GOOS=linux)
$(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/amd64,GOARCH=amd64 GOOS=linux)
$(call tools-for/arch,$(3),$(1),$(2),$(4),deliv/bin/amd64-windows,GOARCH=amd64 GOOS=windows,.exe))
endef
define tools-for/ctx  # pkgN,archs,image
$(if $(and $(3),$(2)),$(foreach a,$(2),
$$(subst deliv/,,$$($(1)B_$(a))): pc-$(3)-$(a)
>rm -f $$(imageDir)/$(3)-$(a)/$$(notdir $$($(1)B_$(a)))))
endef
define tools-for # pkg,main?,prereqs,srcs,image
$(eval $\
$(call tools-for/pkg,$(call pkg-name,$(1)),$(1),$(2),$(3),$(4))
$(call tools-for/ctx,$(call pkg-name,$(1)),$(archs-pi),$(5)))
endef

$(call tools-for,internal/port/grpc,,,$$(grpcSources))
$(call tools-for,internal/types,,,)
$(call tools-for,pkg/data/new,,$$(grpcSources))
$(call tools-for,pkg/textformat,,$$(grpcSources))
$(call tools-for,pkg/data,,$$(grpcSources))
$(call tools-for,pkg/solve,,$$(grpcSources))
$(call tools-for,cmd/solve,main,$$(grpcSources),,solve)
$(call tools-for,cmd/solved,main,$$(grpcSources),,solved)
$(call clean-up,tools,$(tmpProtoD) $(grpcSources))

.PHONY: fake-tools
fake-tools: | $(addprefix deliv/bin/,amd64/ arm64/ amd64-windows/)
>for t in $(tool-names) ; do\
     if [ ! -e $${t} ] ; then\
          echo "#!/bin/sh\necho This is fake $${t}." >$${t} ;\
          chmod +x $${t} ;\
     fi ;\
 done ;\
 touch -r README.md $$(find deliv/bin -type f)

.PHONY: sources
sources: $(grpcSources)
## sources :: Create generated sources

$(call clean-up,sources,$(grpcSources))

#
#   #      # #    # #####
#   #      # ##   #   #
#   #      # # #  #   #
#   #      # #  # #   #
#   #      # #   ##   #
#   ###### # #    #   #
#

.PHONY: lint
## lint :: Perform static checks on source code
lint: go-fmt go-vet staticcheck shellcheck

$(call mkdir,deliv/lint/)

.PHONY: reuse
## reuse :: Check REUSE compliance

REUSE=$(call run,reuse,reuse-amd64)
reuse-report:=deliv/lint/reuse-report.txt

reuse: $(need-reuse) | deliv/lint/
>LANGUAGE=C $(REUSE) lint >$(reuse-report) ; status=$$? ;\
 [ "$${status}" == 0 ] && echo "REUSE compliant" || cat $(reuse-report) ;\
 exit $${status}
$(call clean-up,reuse,$(reuse-report))

fmt-report:=deliv/lint/gofmt-report.txt
go-pkg-paths:=$(shell find . -type f -name "*.go" \
    | sed -E -e "/\\/(tmp|private)\\//d" \
          -e '/_test.go$$/d' -e 's/\/[^/]+$$//' -e '/\/test\//d' \
    | sort -u)

.PHONY: go-fmt
go-fmt: sources | deliv/lint/
>printf "Files changed by “gofmt”\n\n" >$(fmt-report) ;\
 gofmt -l $(go-pkg-paths) >>$(fmt-report) ;\
 fn=`sed -e "1,2d" $(fmt-report)` ;\
 if [ -n "$${fn}" ] ; then cat $(fmt-report) ; exit 1 ;\
    else printf "\033[1m%s\033[0m\n" "go fmt changes no files" ; fi
$(call clean-up,fmt,$(fmt-report))

vet-report:=deliv/lint/govet-report.txt
go-check-paths:=$(filter-out ,$(go-pkg-paths))

.PHONY: go-vet
go-vet: sources | deliv/lint/
>printf "Issues found by “go vet”\n\n" >$(vet-report) ;\
 go vet $(go-check-paths) 2>&1 >>$(vet-report) ;\
 txt=`sed -e "1,2d" $(vet-report)` ;\
 if [ -n "$${txt}" ] ; then cat $(vet-report) ; exit 1 ;\
    else printf "\033[1m%s\033[0m\n" "go vet found nothing" ; fi
$(call clean-up,vet,$(vet-report))

STATICCHECK=$(call run,staticcheck,goenv-amd64)
STATICCHECK_OPTIONS=-f stylish

.PHONY: staticcheck
staticcheck: sources | deliv/lint/
>@rs=`awk -f scripts/reports-with-problems.awk deliv/lint/staticcheck-*.txt` ;\
 if [ -n "$${rs}" ] ; then\
 printf "\033[1m%s\033[0m\n" "Problems Reported by Staticcheck\n" ;\
 for f in $${rs} ; do printf "\n" ; cat $${f} ; done ; exit 1 ;\
 else printf "\033[1m%s\033[0m\n" "staticcheck found nothing" ;\
 fi

staticcheck-of/file=deliv/lint/staticcheck-$(call pkg-name,$(1)).txt
define staticcheck-of # pkgD
clean-staticcheck: to-remove+=$(call staticcheck-of/file,$(1))
staticcheck: $(call staticcheck-of/file,$(1))
$(call staticcheck-of/file,$(1)): $\
    $$($(call pkg-name,$(1))GF) $$(grpcSources) $$(need-goenv) | deliv/lint/
>printf "Lint Report for “$(1)”\n\n" >$$@ ;\
 $$(STATICCHECK) $$(STATICCHECK_OPTIONS) $(moduleN)/$(1) >>$$@ ;\
 exit 0
endef
$(foreach d,$(go-check-paths),\
  $(eval $(call staticcheck-of,$(subst ./,,$(d)))))
$(call clean-up,staticcheck)

SHELLCHECK=$(call run,shellcheck,shellcheck-amd64)
SHELLCHECK_OPTIONS=--format=tty --color=never -x
shellcheck-report:=deliv/lint/shellcheck-report.txt
script-files:=$(filter-out scripts/gitlab-infos.sh,$(wildcard scripts/*.sh))

.PHONY: shellcheck
shellcheck: report:=$(shellcheck-report)
shellcheck: | deliv/lint/
>printf "Report from “shellcheck”\n\n" >$(report) ;\
 $(SHELLCHECK) $(SHELLCHECK_OPTIONS) $(script-files) >>$(report) ;\
 txt="`sed -ne "3,+10p" $(report)`" ;\
 if [ -n "$${txt}" ] ; then cat $(report) ; exit 1 ;\
    else printf "\033[1m%s\033[0m\n" "shellcheck found nothing" ; fi
$(call clean-up,shellcheck,$(shellcheck-report))

#
#    ####  #    # ######  ####  #    #
#   #    # #    # #      #    # #   #
#   #      ###### #####  #      ####
#   #      #    # #      #      #  #
#   #    # #    # #      #    # #   #
#    ####  #    # ######  ####  #    #
#

GO=$(call run,go,goenv-amd64)
GO-JUNIT-REPORT=$(call run,go-junit-report,goenv-amd64)
GO-JUNIT-OPTIONS=-set-exit-code -p go.version=$(VERSION_GOLANG)

$(call mkdir,deliv/report/)
check-to-remove:=tmp/coverage
.PHONY: check
## check :: Run tests and get coverage
check:

define run-tool-cmd # pkgN
$(1)GCD:=tmp/coverage/$(1)
$(1)Cmd=$$(call run,deliv/bin/$(1).test,script-amd64,,$\
    GOCOVERDIR=$$(if $$(useDocker),/project,$$$${PWD})/$$($(1)GCD))
endef
define test-report-for # main?,pkgN,pkgD
$(2)TR:=deliv/report/$(2)-tests.xml
.PHONY: report/$(2)
report/$(2): $$($(2)TR) | deliv/report/
check: report/$(2)
test-reports+=$$($(2)TR)
clean-check: to-remove+=$$($(2)TR)
$$($(2)TR): deliv/bin/$(2).test $$(need-script) $$(need-goenv) \
    | $$(dir $$($(2)TR)) $$($(2)GCD)/
>$$($(2)Cmd) -test.v 2>&1 \
 | $$(GO-JUNIT-REPORT) $$(GO-JUNIT-OPTIONS) \
       -package-name $(moduleN)/$(3) >$$@
endef
define coverage-report-for # main?,pkgN,pkgD
$(2)CR:=deliv/report/$(2)-coverage.html
report/$(2): $$($(2)CR)
clean-check: to-remove+=$$($(2)CR)
$(2)CVI:=$$($(2)GCD)/coverage.out
$$(call mkdir,$$(dir $$($(2)CVI)))
$$($(2)CR): deliv/bin/$(2).test $$(need-script) $$(need-goenv) \
    | $$(dir $$($(2)CR)) $$($(2)GCD)/
$(if $(1),>$$($(2)Cmd)
>$$(GO) tool covdata textfmt -i=$$($(2)GCD) -o $$($(2)CVI)
>$$(GO) tool cover -html=$$($(2)CVI) -o $$@,>$$($(2)Cmd) -test.coverprofile=$$($(2)CVI)
>$$(GO) tool cover -html=$$($(2)CVI) -o $$@)
endef
define tests-for # main?,pkg
$(eval $(call run-tool-cmd,$(call pkg-name,$(2))))
$(eval $(call test-report-for,$(1),$(call pkg-name,$(2)),$(2)))
$(eval $(call coverage-report-for,$(1),$(call pkg-name,$(2)),$(2)))
endef

$(call tests-for,main,cmd/solve)
$(call tests-for,main,cmd/solved)
$(call tests-for,,internal/port/grpc)
$(call tests-for,,internal/types)
$(call tests-for,,pkg/data/new)
$(call tests-for,,pkg/textformat)
$(call tests-for,,pkg/data)
$(call tests-for,,pkg/solve)
$(call clean-up,check)

.PHONY: coverage
## coverage :: Get total code coverage
coverage: $(filter-out %port-grpc-tests.xml,$(test-reports))
>awk -f scripts/total-coverage.awk $^ | tr , .

stat-tools:=$(foreach a,$(archs-pi),\
    $(addprefix deliv/bin/$(a)/,cmd-solve cmd-solved))
stat-report:=deliv/report/linkage-report.xml

.PHONY: check-linkage
check-linkage: $(stat-report)
$(stat-report): $(stat-tools) $(need-script) | $(dir $(stat-report))
>file $(stat-tools) |\
 awk -v timestamp=`date +"%FT%T"` -f scripts/check-tool-props.awk >$@

#
#   #####        # #    #   ##    ####  ######  ####
#   #    #       # ##  ##  #  #  #    # #      #
#   #    # ##### # # ## # #    # #      #####   ####
#   #####        # #    # ###### #  ### #           #
#   #            # #    # #    # #    # #      #    #
#   #            # #    # #    #  ####  ######  ####
#

.PHONY: p-images clean-p-images
## p-images :: Build images with executables

p-image-names:=solve solved

clean-p-images: $(foreach i,$(p-image-names),clean-pi-$(i))

$(call tool-images,solve,$(archs-pi),cmd-solve)
$(call tool-images,solved,$(archs-pi),cmd-solved)

$(call multi-arch-image,pi,solve,$(moduleV),$(archs-pi))
$(call multi-arch-image,pi,solved,$(moduleV),$(archs-pi))

#
#   ###### #####  #    # #####
#   #      #    # #    # #    #
#   #####  #    # #    # #####
#   #      #####  #    # #    #
#   #      #      #    # #    #
#   ###### #       ####  #####
#

PANDOC=$(call run,pandoc,docenv-amd64)

epubSrcs=$(addprefix tmp/docs/,$(addsuffix .md,\
    overview algorithm textformat annotated-example makefile pipeline \
    man-solve man-solved))
epubImgs:=$(addprefix tmp/,$(wildcard docs/images/*.svg))
epubMeta:=tmp/docs/epub-metadata.yaml
epub=deliv/docs/solver.epub

.PHONY: epub
## epub :: Produce EPUB documentation
epub: $(epub)

$(call mkdir,$(dir $(epub)))
$(epub): $(epubMeta) $(epubSrcs) $(epubImgs) \
    $(need-docenv) | $(dir $(epub))
>(cd tmp/docs ;\
 $(PANDOC) -f markdown -t epub --toc --metadata-file=$(notdir $(epubMeta) $(epubSrcs)))>$@

$(eval $(call copy-rule,tmp/docs,docs,%))
$(eval $(call copy-rule,tmp/docs/images,docs/images,%))

tmp/docs/man-%.md: docs/%.1 | tmp/docs/
>$(PANDOC) -f man -t markdown $< \
 | sed -E -e "/^# /s/^/#/" -e "1{s/^/# Man Page $(basename $(<F))\n\n/;}" >$@

$(call clean-up,epub,$(epub) $(epubSrcs) $(epubImgs))

#
#   #    # ###### #####   ####  #  ####  #    #
#   #    # #      #    # #      # #    # ##   #
#   #    # #####  #    #  ####  # #    # # #  #
#   #    # #      #####       # # #    # #  # #
#    #  #  #      #   #  #    # # #    # #   ##
#     ##   ###### #    #  ####  #  ####  #    #
#

kind?=revision

version-file=$(if $(image),build/ci/$(image)/VERSION,VERSION).inc

.PHONY: version
## version :: Increment version number (default: revision)
##+    kind=(major|minor|revision)
##+    image=(script|goenv|reuse|manifest|docenv|shellcheck)
version: $(version-file) $(gitlab-ci)

$(call mkdir,tmp/)
.PHONY: $(version-file)
$(version-file): | tmp/
>awk -v FS=. -v kind=$(kind) -f scripts/inc-version.awk $(@:.inc=) >tmp/VERSION
>mv tmp/VERSION $(@:.inc=)

.PHONY: release
## release :: Create release from current version
release: | tmp/
>scripts/release.sh

.PHONY: export
## export :: Push current or given release to target repository
##+    target=(codeco|eclipse)
##+    version=version-number (default: see VERSION)
export: version:=$(if $(version),$(version),$(moduleV))
export: | tmp/
>scripts/export.sh "$(target)" "$(version)"

#
#    ####  #      ######   ##   #    #
#   #    # #      #       #  #  ##   #
#   #      #      #####  #    # # #  #
#   #      #      #      ###### #  # #
#   #    # #      #      #    # #   ##
#    ####  ###### ###### #    # #    #
#

.PHONY: clean
.IGNORE: clean
## clean :: Remove images and generated files
clean: clean-c-images clean-p-images
>rm -rf deliv/ tmp/
