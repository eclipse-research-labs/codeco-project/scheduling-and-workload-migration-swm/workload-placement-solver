// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func parseModel(input ...string) *data {
	p := makeParser(input...)
	p.model()
	return &p.data
}

func TestPModel(t *testing.T) {
	data := parseModel("model name")
	assert.Equal(t, "name", data.model)
	assert.Equal(t, DefaultModelOptions(), data.options)

	data = parseModel("model name applications")
	assert.Equal(t, "name", data.model)
	assert.Equal(t, DefaultModelOptions(), data.options)

	data = parseModel(
		"model name options compound-names:no implicit-reverse-path:no",
		"shortest-path-only:no")
	assert.Equal(t, "name", data.model)
	assert.Equal(t, ModelOptions{false, false, false}, data.options)

	data = parseModel(
		"model name options compound-names:no implicit-reverse-path:no",
		"shortest-path-only:no application")
	assert.Equal(t, "name", data.model)
	assert.Equal(t, ModelOptions{false, false, false}, data.options)

	data = parseModel(
		"model name options compound-names :yes implicit-reverse-path :yes",
		"shortest-path-only :yes")
	assert.Equal(t, "name", data.model)
	assert.Equal(t, ModelOptions{true, true, true}, data.options)

	data = parseModel(
		"model name with compound-names:yes implicit-reverse-path:yes",
		"shortest-path-only:yes")
	assert.Equal(t, "name", data.model)
	assert.Equal(t, ModelOptions{true, true, true}, data.options)

	checkParsingErrors(t, (*parser).model, []errorCase{
		{"model 17", `<input>:1:7: expected: identifier`},
		{"model name options 17", `<input>:1:20: expected: identifier`},
		{"model name options unknown",
			`<input>:1:20: unknown option: "unknown"`},
		{"model name options compound-names:unknown",
			`<input>:1:35: illegal Boolean: "unknown"`}})
}

func TestPHeader(t *testing.T) {
	p := makeParser(
		`workload assignment v1
		 model M options compound-names:no
		 	implicit-reverse-path:no
		 	shortest-path-only:no`)
	p.header()
	assert.Equal(t, EOF, p.token)
	assert.Equal(t, "M", p.data.model)
	assert.Equal(t, 1, p.data.version)
	assert.Equal(t, ModelOptions{false, false, false}, p.data.options)
}

func TestPHeader_errors(t *testing.T) {
	checkParsingErrors(t, (*parser).header, []errorCase{
		{"something",
			`<input>:1:1: expected: "workload"`},
		{"workload 17",
			`<input>:1:10: expected: "assignment"`},
		{"workload assignment 1",
			`<input>:1:21: expected: version`},
		{"workload assignment v1.1",
			`<input>:1:21: illegal version: "v1.1"`},
		{"workload assignment v10",
			`<input>:1:21: unsupported version: "v10"`},
		{"workload assignment v1 error",
			`<input>:1:24: expected: "model"`}})
}
