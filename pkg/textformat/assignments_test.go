// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPWorkloadAsgmt(t *testing.T) {
	p := makeParser(
		`a1•w1 on n1`,
		`w2 on n1`)
	p.data.asgmts = make([]assignment, 1)
	p.workloadAsgmt()
	require.Len(t, p.data.asgmts, 1)
	asgmt := p.data.asgmts[0]
	require.Len(t, asgmt.Workloads, 1)
	wa := asgmt.Workloads[0]
	assert.Equal(t, "a1", wa.app)
	assert.Equal(t, "w1", wa.name)
	assert.Equal(t, "n1", wa.node)

	assert.PanicsWithValue(t,
		"<input>:2:1: expected: qualified name",
		func() { p.workloadAsgmt() })
}

func parseChannelAsgmt(input string) []*channelAsgmt {
	p := makeParser(input)
	p.data.asgmts = make([]assignment, 1)
	p.channelAsgmt()
	if len(p.data.asgmts) == 0 {
		panic("cannot parse channels")
	}
	return p.data.asgmts[0].Channels
}

func TestPChannelAsgmt(t *testing.T) {
	testCases1 := []struct{ input, cr, nr, pr string }{
		{`a•w->a•x on n•p`, "a•w→a•x", "n", "p"},
		{`a•w->a•x on n•1`, "a•w→a•x", "n", "1"},
		{`c1 on n•17`, "c1", "n", "17"},
		{`19 on n•17`, "19", "n", "17"}}
	for _, tc := range testCases1 {
		as := parseChannelAsgmt(tc.input)
		require.GreaterOrEqual(t, len(as), 1)
		assert.Equal(t, tc.cr, as[0].channel)
		assert.Equal(t, tc.nr, as[0].network)
		assert.Equal(t, tc.pr, as[0].path)
	}
	testCases2 := []struct{ input, cr1, nr1, pr1, cr2, nr2, pr2 string }{
		{`a•w--a•x on n•p,m•q`, "a•w→a•x", "n", "p", "a•x→a•w", "m", "q"},
		{`a•w--a•x on n•1,m•2`, "a•w→a•x", "n", "1", "a•x→a•w", "m", "2"}}
	for _, tc := range testCases2 {
		as := parseChannelAsgmt(tc.input)
		require.GreaterOrEqual(t, len(as), 2)
		assert.Equal(t, tc.cr1, as[0].channel)
		assert.Equal(t, tc.nr1, as[0].network)
		assert.Equal(t, tc.pr1, as[0].path)
		assert.Equal(t, tc.cr2, as[1].channel)
		assert.Equal(t, tc.nr2, as[1].network)
		assert.Equal(t, tc.pr2, as[1].path)
	}
	checkParsingErrors(t, (*parser).channelAsgmt, []errorCase{
		{`cpu:`, `<input>:1:1: expected: identifier`},
		{`17 19`, `<input>:1:4: expected: "on"`},
		{`a1•w1 on p1`, `<input>:1:7: expected: directional operator`},
		{`w1->a1•w2 on p1`, `<input>:1:1: expected: qualified name`},
		{`a1•w1->w2 on p1`, `<input>:1:8: expected: qualified name`},
		{`a1•w1->a1•w2 p1`, `<input>:1:14: expected: "on"`},
		{`a•w->b•x on p,q`, `<input>:1:13: expected: qualified name`},
		{`a1•w1->a1•w2 on n•p,n•q`,
			`<input>:1:20: second path ref not allowed`},
		{`a1•w1--a1•w2 on n•p`,
			`<input>:1:20: expected: second path ref`}})
}

func TestPAssignments(t *testing.T) {
	p := makeParser(
		`assignments`,
		`assignment`,
		`workload a•w on n1`,
		`workload a•v on n2`,
		`channel a•w->a•x on n•p`,
		`assignment`,
		`workload a•w on n2`,
		`channel a•w->a•x on n•q`)
	p.assignments()
	require.Len(t, p.data.asgmts, 2)
	asgmt := p.data.asgmts[0]
	assert.Equal(t,
		[]*workloadAsgmt{
			{app: "a", name: "w", node: "n1"},
			{app: "a", name: "v", node: "n2"}},
		asgmt.Workloads)
	assert.Equal(t,
		[]*channelAsgmt{{channel: "a•w→a•x", network: "n", path: "p"}},
		asgmt.Channels)
	asgmt = p.data.asgmts[1]
	assert.Equal(t,
		[]*workloadAsgmt{{app: "a", name: "w", node: "n2"}},
		asgmt.Workloads)
	assert.Equal(t,
		[]*channelAsgmt{{channel: "a•w→a•x", network: "n", path: "q"}},
		asgmt.Channels)

	p = makeParser(`assignment channel`)
	assert.PanicsWithValue(t,
		`<input>:1:12: expected: "workload"`,
		func() { p.assignments() })
}
