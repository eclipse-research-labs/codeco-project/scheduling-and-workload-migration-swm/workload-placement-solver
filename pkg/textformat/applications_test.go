// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/port/grpc"
)

func makeParser(input ...string) *parser {
	p := &parser{}
	p.initWithString(input...)
	p.next()
	return p
}

func TestPWorkloadRef(t *testing.T) {
	p := makeParser(`w1 a1•w2 w3`)
	sp := p.workloadRef("app")
	assert.Equal(t, "app", sp.app)
	assert.Equal(t, "w1", sp.name)
	sp = p.workloadRef("app")
	assert.Equal(t, "a1", sp.app)
	assert.Equal(t, "w2", sp.name)
	assert.PanicsWithValue(t, "<input>:1:10: expected: qualified name",
		func() { p.workloadRef("") })
}

func parseWorkload(input string) (p *parser, wl *grpc.Workload) {
	p = makeParser(input)
	var ag appGroup
	app := &application{name: "x"}
	ag.apps = append(ag.apps, app)
	p.workload(app)
	ws := app.asGrpc().Workloads
	wl = ws[len(ws)-1]
	return
}

func TestPWorkload(t *testing.T) {
	check := func(input string) {
		p, wl := parseWorkload(input)
		assert.Equal(t, EOF, p.token)

		require.NotNil(t, wl)
		assert.Equal(t, "W", wl.Id)
		assert.Equal(t, int32(17), wl.CPU)
		assert.Equal(t, int64(19), wl.Memory)
		assert.Equal(t, []string{"A"}, wl.RequiredLabels)
		assert.Equal(t, []string{"B"}, wl.ForbiddenLabels)
	}
	check(`/*workload*/ W on N needs cpu:17 ram:19 labels:(A ~B)`)
	check(`/*workload*/ W on N needs cpu:17 labels:(~B A) memory:19`)
	check(`/*workload*/ W on N needs ram:19 labels:(A ~B) cpu:17`)
}

func TestPWorkload_multiple(t *testing.T) {
	p := makeParser(
		`/*application*/ A workload W1,W2 needs cpu:17 ram:19 labels:(A !B)`)
	p.application()
	assert.Equal(t, EOF, p.token)

	require.Len(t, p.data.apps.apps, 1)
	workloads := p.data.apps.apps[0].workloads
	require.Len(t, workloads, 2)
	checkNode := func(w *workload, name string) {
		assert.Equal(t, name, w.name)
		assert.Equal(t, milliCPU(17), w.cpu)
		assert.Equal(t, ramMB(19), w.ram)
		assert.Equal(t, []string{"A", "!B"}, w.labels)
	}
	checkNode(workloads[0], "W1")
	checkNode(workloads[1], "W2")
}

func TestPWorkload_defaults(t *testing.T) {
	p, wl := parseWorkload(`/*workload*/ W on N`)
	assert.Equal(t, EOF, p.token)

	require.NotNil(t, wl)
	assert.Equal(t, "W", wl.Id)
	assert.Equal(t, defaults.CPU, wl.CPU)
	assert.Equal(t, defaults.RAM, wl.Memory)
	assert.Len(t, wl.RequiredLabels, 0)
	assert.Len(t, wl.ForbiddenLabels, 0)
}

func TestPWorkload_errors(t *testing.T) {
	parseWl := func(p *parser) { p.workload(nil) }
	checkParsingErrors(t, parseWl, []errorCase{
		// Starting symbol “workload” already recognized
		{"1", `<input>:1:1: expected: identifier`},
		{"W on 1", `<input>:1:6: expected: identifier`},
		{"W on N needs 1", `<input>:1:14: expected: property`},
		{"W on N needs mtu:", `<input>:1:14: unknown property: "mtu:"`},
		{"W1,W2 on N", `<input>:1:10: expected: list with 2 names`}})
}

func TestPApplication(t *testing.T) {
	p := makeParser(`/*application*/ A workload W1 workload W2 workload W3`)
	name := p.application()
	assert.Equal(t, "A", name)
	assert.Equal(t, EOF, p.token)

	require.Len(t, p.data.apps.apps, 1)
	a := p.data.apps.apps[0]
	assert.Equal(t, "A", a.name)
	assert.Len(t, a.workloads, 3)
}

func TestPApplication_errors(t *testing.T) {
	parseApp := func(p *parser) { p.application() }
	checkParsingErrors(t, parseApp, []errorCase{
		// Starting symbol “application” already recognized
		{"1", `<input>:1:1: expected: identifier`},
		{"A channel", `<input>:1:3: expected: "workload"`}})
}

func TestPPathRef(t *testing.T) {
	p := makeParser(`n1•p1 n2•17`)
	ps := p.pathRef()
	assert.Equal(t, pathSpec{"n1", "p1"}, ps)
	ps = p.pathRef()
	assert.Equal(t, pathSpec{"n2", "17"}, ps)

	parsePathRef := func(p *parser) { p.pathRef() }
	checkParsingErrors(t, parsePathRef, []errorCase{
		{"-", `<input>:1:1: expected: identifier`},
		{"1", `<input>:1:1: expected: identifier`},
		{"x", `<input>:1:1: expected: qualified name`}})
}

func TestPPathRefs(t *testing.T) {
	p := makeParser(`n•p n•1,n•2,n•3 •`)
	cs := []channelSpec{{}}
	p.pathRefs(cs)
	assert.Equal(t,
		[]channelSpec{{to: channelData{path: pathSpec{"n", "p"}}}},
		cs)
	cs = []channelSpec{{twoWay: true}, {}}
	p.pathRefs(cs)
	assert.Equal(t, pathSpec{"n", "1"}, cs[0].to.path)
	assert.Equal(t, pathSpec{"n", "2"}, cs[0].fro.path)
	assert.Equal(t, pathSpec{"n", "3"}, cs[1].to.path)
	assert.Equal(t, pathSpec{}, cs[1].fro.path)
	cs = []channelSpec{{}}
	assert.PanicsWithValue(t, "<input>:1:17: expected: identifier",
		func() { p.pathRefs(cs) })
	p = makeParser(`n•1,n•2`)
	assert.PanicsWithValue(t,
		"<input>:1:4: expected: at most 1 path references",
		func() { p.pathRefs(cs) })
	cs = []channelSpec{{twoWay: true}}
	p = makeParser(`n•1`)
	assert.PanicsWithValue(t, "<input>:1:4: not enough path references",
		func() { p.pathRefs(cs) })
}

func parseChannel(t *testing.T, input string) []*channel {
	p := makeParser(input)
	p.channel("X")
	t.Helper()
	assert.Equal(t, EOF, p.token)
	return p.data.apps.channels
}

func TestPChannel(t *testing.T) {
	check := func(input, net, path string) {
		cs := parseChannel(t, input)
		require.Len(t, cs, 1)
		ch := cs[0]
		assert.Equal(t, workloadSpec{"A", "W1"}, ch.src)
		assert.Equal(t, workloadSpec{"A", "W2"}, ch.dst)
		assert.Equal(t, pathSpec{net, path}, ch.path)
		assert.Equal(t, latency(5000), ch.lat)
		assert.Equal(t, bandwidth(1000), ch.bw)
	}
	check(`/*channel*/ A•W1 -> A•W2 needs lat:5µs bw:1Kb`, "", "")
	check(`/*channel*/ A•W1 -> A•W2 needs bw:1Kb lat:5µs`, "", "")
	check(`/*channel*/ A•W1 -> A•W2 on N•P needs lat:5µs bw:1Kb`, "N", "P")

	parseChannel := func(p *parser) { p.channel("X") }
	checkParsingErrors(t, parseChannel, []errorCase{
		{"A•W1->A•W2 needs bad:17", `<input>:1:18: unknown property: "bad:"`},
		{"A•W1->A•W2 needs lat : 5µs", `<input>:1:18: expected: property`},
		{"A•W1->A•W2 needs lat:1,2", `<input>:1:23: unexpected latency`},
		{"A•W1->A•W2 needs bw:1,2", `<input>:1:22: unexpected bandwidth`},
		{"A•W1->A•W2 needs qos:assured,assured",
			`<input>:1:29: unexpected service class`}})
}

func nameSrcDst(cs *channelData) string {
	return fmt.Sprintf("%s:%s•%s->%s•%s",
		cs.name, cs.src.app, cs.src.name, cs.dst.app, cs.dst.name)
}

func TestPChannel_nameSrcDst(t *testing.T) {
	testCases := []struct {
		inputs        []string
		twoWay        bool
		toStr, froStr string
	}{ // all inputs of a case produce the same channel information
		{[]string{`n:w1->w2`, `n :w1->w2`},
			false, "n:a•w1->a•w2", ""},
		{[]string{`n1,n2:w1--w2`, `n1,n2 :w1--w2`},
			true, "n1:a•w1->a•w2", "n2:a•w2->a•w1"},
		{[]string{`17:w1->w2`},
			false, "17:a•w1->a•w2", ""},
		{[]string{`17,19:w1--w2`},
			true, "17:a•w1->a•w2", "19:a•w2->a•w1"},
		{[]string{`n:w1--w2`, `n :w1--w2`},
			true, "n:a•w1->a•w2", "a•w2→a•w1:a•w2->a•w1"},
		{[]string{`w1->w2`, `a•w1->w2`},
			false, "a•w1→a•w2:a•w1->a•w2", ""},
		{[]string{`w1--w2`, `a•w1--w2`},
			true, "a•w1→a•w2:a•w1->a•w2", "a•w2→a•w1:a•w2->a•w1"}}
	for _, tc := range testCases {
		for _, input := range tc.inputs {
			cs := makeParser(input).chSpec("a")
			assert.Equal(t, cs.twoWay, tc.twoWay, input)
			assert.Equal(t, tc.toStr, nameSrcDst(&cs.to), input)
			if tc.twoWay {
				assert.Equal(t, tc.froStr, nameSrcDst(&cs.fro), input)
			}
		}
	}

	parseChSpec := func(p *parser) { p.chSpec("app") }
	checkParsingErrors(t, parseChSpec, []errorCase{
		{"1.0", "<input>:1:1: expected: identifier"},
		{"1,2.0", "<input>:1:3: expected: identifier"},
		{"n1,n2:w1 w2", "<input>:1:10: expected: direction operator"},
		{"n1,n2:w1->w2", "<input>:1:9: expected: two-way operator"},
		{"n1 n2", `<input>:1:4: unexpected: "n2"`}})
}

func TestPChannel_defaults(t *testing.T) {
	check := func(input string) *channel {
		cs := parseChannel(t, input)
		require.GreaterOrEqual(t, len(cs), 1)
		ch := cs[0]
		assert.Equal(t, workloadSpec{"X", "W1"}, ch.src)
		assert.Equal(t, workloadSpec{"X", "W2"}, ch.dst)
		assert.Equal(t, pathSpec{}, ch.path)
		assert.Equal(t, latency(defaults.Latency), ch.lat)
		assert.Equal(t, bandwidth(defaults.Bandwidth), ch.bw)
		return cs[len(cs)-1]
	}
	check(`/*channel*/ W1 -> W2`)
	ch := check(`/*channel*/ W1 -- W2`)
	assert.Equal(t, workloadSpec{"X", "W2"}, ch.src)
	assert.Equal(t, workloadSpec{"X", "W1"}, ch.dst)
	assert.Equal(t, pathSpec{}, ch.path)
	assert.Equal(t, latency(defaults.Latency), ch.lat)
	assert.Equal(t, bandwidth(defaults.Bandwidth), ch.bw)
}

func TestPChannel_onPath(t *testing.T) {
	check := func(input string) *channel {
		cs := parseChannel(t, input)
		require.GreaterOrEqual(t, len(cs), 1)
		ch := cs[0]
		assert.Equal(t, pathSpec{"N", "1"}, ch.path)
		return cs[len(cs)-1]
	}
	check(`/*channel*/ A•W1 -> A•W2 on N•1`)
	ch := check(`/*channel*/ W1 -- W2 on N•1,N•2`)
	assert.Equal(t, pathSpec{"N", "2"}, ch.path)

	assert.PanicsWithValue(t,
		"<input>:1:32: expected: at most 1 path references",
		func() { check(`/*channel*/ A•W1 -> A•W2 on N•1,N•2`) })
	assert.PanicsWithValue(t,
		"<input>:1:28: not enough path references",
		func() { check(`/*channel*/ W1 -- W2 on N•1`) })
}

func TestPChannel_twoWay(t *testing.T) {
	cs := parseChannel(t, `a•w—b•x needs lat:1,2 bw:3,4`)
	require.Len(t, cs, 2)
	assert.Equal(t, latency(1), cs[0].lat)
	assert.Equal(t, bandwidth(3), cs[0].bw)
	assert.Equal(t, latency(2), cs[1].lat)
	assert.Equal(t, bandwidth(4), cs[1].bw)

	cs = parseChannel(t, `a•w—b•x needs qos:assured`)
	require.Len(t, cs, 2)
	assert.Equal(t, "assured", cs[0].qos)
	assert.Equal(t, "assured", cs[1].qos)

	cs = parseChannel(t, `a•w—b•x needs qos:assured,best-effort`)
	require.Len(t, cs, 2)
	assert.Equal(t, "assured", cs[0].qos)
	assert.Equal(t, "best-effort", cs[1].qos)
}

func TestPChannel_multiple(t *testing.T) {
	cs := parseChannel(t,
		`/*channel*/ w1->w2,w2->w3,w3->w4 needs lat:17 bw:19`)
	require.Len(t, cs, 3)
	check := func(idx int) {
		c := cs[idx]
		assert.Equal(t, "w"+strconv.Itoa(idx+1), c.src.name)
		assert.Equal(t, "w"+strconv.Itoa(idx+2), c.dst.name)
		assert.Equal(t, latency(17), c.lat)
		assert.Equal(t, bandwidth(19), c.bw)
	}
	check(0)
	check(1)
	check(2)
}

func TestPApplications(t *testing.T) {
	p := makeParser(
		`applications`,
		`application A1 workload W needs cpu:1`,
		`application A2 workload W needs cpu:2`,
		`channel A1•W -- A2•W needs lat:3`)
	p.applications()
	assert.Equal(t, EOF, p.token)
	assert.Len(t, p.data.apps.apps, 2)
	assert.Len(t, p.data.apps.channels, 2)
}

func TestPApplications_errors(t *testing.T) {
	checkParsingErrors(t, (*parser).applications, []errorCase{
		{"error", `<input>:1:1: expected: "application"`},
		{"applications error", `<input>:1:14: expected: "application"`}})
}
