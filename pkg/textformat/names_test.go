// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLinkName(t *testing.T) {
	assert.Equal(t, "a→b", LinkName("a", "b"))
}

func TestPathName(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(
		"\u00B9\u00B2\u00B3\u2074\u2075"+ // superscript "12345"
			"\u2076\u2077\u2078\u2079\u2070", // superscript "67890"
		itoaSup(1234567890))
	assert.Equal("a→b", PathName("a", "b", 1))
	assert.Equal("a→²b", PathName("a", "b", 2))
}
