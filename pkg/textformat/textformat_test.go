// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
)

func TestParse(t *testing.T) {
	_, err := Parse("name", ``)
	assert.ErrorContains(t, err, `expected: "workload"`)

	asgmt, err := Parse("name", `
		workload assignment v1
		model no-asgmt
		application A workload W
		node C`)
	assert.NoError(t, err)
	assert.Equal(t, "no-asgmt", asgmt.Name)
	assert.NotNil(t, asgmt.AppGroup)
	assert.NotNil(t, asgmt.Infrastructure)
	assert.Nil(t, asgmt.Costs)
	assert.Nil(t, asgmt.Recommendations)
	assert.Nil(t, asgmt.Assignment)

	asgmt, err = Parse("name", `
		workload assignment v1
		model with-asgmt
		application A workload W
		node C
		assignment
		workload A•W on C`)
	assert.NoError(t, err)
	assert.Equal(t, "with-asgmt", asgmt.Name)
	assert.NotNil(t, asgmt.AppGroup)
	assert.NotNil(t, asgmt.Infrastructure)
	assert.Nil(t, asgmt.Costs)
	assert.Nil(t, asgmt.Recommendations)
	assert.NotNil(t, asgmt.Assignment)
}

func TestParse_costs(t *testing.T) {
	asgmt, err := Parse("name", `
		workload assignment v1
		model with-recs
		application A workload W,V
		node C,D
		costs
		workload A•W (C:17)
		workload A•V (C:19 D:23)`)
	assert.NoError(t, err)
	assert.Equal(t, "with-recs", asgmt.Name)
	assert.NotNil(t, asgmt.AppGroup)
	assert.NotNil(t, asgmt.Infrastructure)
	assert.NotNil(t, asgmt.Costs)
	assert.Nil(t, asgmt.Recommendations)
	ws := asgmt.Costs.Workloads
	require.Len(t, ws, 2)
	assert.Equal(t,
		&grpc.Cost_Workload{
			Application: "A", Workload: "W",
			Nodes: []*grpc.NodeValue{
				{Node: "C", Value: 17}}},
		ws[0])
	assert.Equal(t,
		&grpc.Cost_Workload{
			Application: "A", Workload: "V",
			Nodes: []*grpc.NodeValue{
				{Node: "C", Value: 19},
				{Node: "D", Value: 23}}},
		ws[1])
	assert.Nil(t, asgmt.Assignment)
}

func TestParse_recommendations(t *testing.T) {
	asgmt, err := Parse("name", `
		workload assignment v1
		model with-recs
		application A workload W,V
		node C,D
		recommendations
		workload A•W (C:17)
		workload A•V (C:19 D:23)`)
	assert.NoError(t, err)
	assert.Equal(t, "with-recs", asgmt.Name)
	assert.NotNil(t, asgmt.AppGroup)
	assert.NotNil(t, asgmt.Infrastructure)
	assert.Nil(t, asgmt.Costs)
	assert.NotNil(t, asgmt.Recommendations)
	ws := asgmt.Recommendations.Workloads
	require.Len(t, ws, 2)
	assert.Equal(t,
		&grpc.Recommendation_Workload{
			Application: "A", Workload: "W",
			Nodes: []*grpc.NodeValue{
				{Node: "C", Value: 17}}},
		ws[0])
	assert.Equal(t,
		&grpc.Recommendation_Workload{
			Application: "A", Workload: "V",
			Nodes: []*grpc.NodeValue{
				{Node: "C", Value: 19},
				{Node: "D", Value: 23}}},
		ws[1])
	assert.Nil(t, asgmt.Assignment)
}
