// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
)

func TestSplitLinkSpec(t *testing.T) {
	check := func(spec string, ps ...string) {
		ok, m1, m2, m3, m4 := splitLinkSpec(spec)
		if len(ps) == 0 {
			assert.False(t, ok)
		} else {
			assert.Equal(t, ps, []string{m1, m2, m3, m4})
		}
	}
	check("illegal")
	check("N1•N2", "", "", "N1", "N2")
	check("L:N1•N2", "L", "", "N1", "N2")
	check("1:N1•N2", "1", "", "N1", "N2")
	check("L1,L2:N1•N2", "L1", "L2", "N1", "N2")
	check("1,2:N1•N2", "1", "2", "N1", "N2")
}

func TestReverse(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{}, reverse([]string{}))
	assert.Equal([]string{"A"}, reverse([]string{"A"}))
	assert.Equal([]string{"B", "A"}, reverse([]string{"A", "B"}))
	assert.Equal([]string{"C", "B", "A"}, reverse([]string{"A", "B", "C"}))
}

func TestReverseLink(t *testing.T) {
	assert.Equal(t, "b->a", reverseLink("a->b"))
	assert.Equal(t, "b→a", reverseLink("a→b"))
	assert.Equal(t, "a--b", reverseLink("a--b"))
	assert.Equal(t, "a—b", reverseLink("a—b"))
}

func TestReverseLinks(t *testing.T) {
	assert.Equal(t,
		[]string{"d->c", "c->b", "b->a"},
		reverseLinks([]string{"a->b", "b->c", "c->d"}))
}

func TestQosToServiceClass(t *testing.T) {
	assert.Equal(t,
		grpc.ServiceClass_BEST_EFFORT,
		qosToServiceClass("best-effort"))
	assert.Equal(t,
		grpc.ServiceClass_ASSURED,
		qosToServiceClass("assured"))
	assert.Equal(t,
		grpc.ServiceClass_BEST_EFFORT,
		qosToServiceClass("unknown"))
}

func TestNodeCopy(t *testing.T) {
	n := &node{name: "node", kind: kindCompute}
	c := n.copy()
	assert.Equal(t, n, c)
	assert.NotSame(t, n, c)
}

func TestNodeComputeAsGrpc(t *testing.T) {
	assert := assert.New(t)
	cpu := types.NewResource[milliCPU](10)
	cpu.Use(5)
	ram := types.NewResource[ramMB](16)
	ram.Use(8)
	n := node{name: "n", kind: kindCompute, cpu: *cpu, ram: *ram}
	check := func(exp []string) {
		g := n.asGrpc()
		assert.Equal("n", g.Id)
		assert.Equal(grpc.Node_COMPUTE, g.NodeType)
		assert.Equal(exp, g.Labels)
		assert.Equal(&grpc.Resource{Capacity: 10, Used: 5}, g.CPU)
		assert.Equal(&grpc.Resource{Capacity: 16, Used: 8}, g.Memory)
		assert.Zero(g.Latency)
		assert.Nil(g.Bandwidth)
	}
	check(nil)
	n.labels = []string{"a", "b", "c"}
	check([]string{"a", "b", "c"})
}

func TestNodeNetworkAsGrpc(t *testing.T) {
	assert := assert.New(t)
	bw := types.NewResource[bandwidth](23)
	bw.Use(19)
	n := node{name: "n", kind: kindNetwork, lat: 17, bw: *bw}
	check := func(exp []string) {
		g := n.asGrpc()
		assert.Equal("n", g.Id)
		assert.Equal(grpc.Node_NETWORK, g.NodeType)
		assert.Equal(exp, g.Labels)
		assert.Nil(g.CPU)
		assert.Nil(g.Memory)
		assert.Equal(int64(17), g.Latency)
		assert.Equal(&grpc.Resource{Capacity: 23, Used: 19}, g.Bandwidth)
	}
	check(nil)
	n.labels = []string{"a", "b", "c"}
	check(nil)
}

func TestLinkAsGrpc(t *testing.T) {
	assert := assert.New(t)
	l := link{"name", "src", "dst", "medium", 17}
	g := l.asGrpc()
	assert.Equal("name", g.Id)
	assert.Equal("src", g.Source)
	assert.Equal("dst", g.Target)
	assert.Equal("medium", g.Medium)
	assert.Equal(int64(17), g.Latency)
}

func TestPathAsGrpc(t *testing.T) {
	assert := assert.New(t)
	p := path{name: "name"}
	g := p.asGrpc()
	assert.Equal("name", g.Id)
	assert.Len(g.Links, 0)

	p = path{"name", []string{}}
	g = p.asGrpc()
	assert.Equal("name", g.Id)
	assert.Len(g.Links, 0)

	p = path{"name", []string{"l1", "l2"}}
	g = p.asGrpc()
	assert.Equal("name", g.Id)
	assert.Equal([]string{"l1", "l2"}, g.Links)
}

func TestMediumCopy(t *testing.T) {
	m := &medium{name: "node", bw: *types.NewResource[bandwidth](17)}
	c := m.copy()
	assert.Equal(t, m, c)
	assert.NotSame(t, m, c)
}

func TestMediumAsGrpc(t *testing.T) {
	assert := assert.New(t)
	bw := types.NewResource[bandwidth](17)
	bw.Use(13)
	m := medium{name: "name", bw: *bw}
	g := m.asGrpc()
	assert.Equal(g.Id, "name")
	assert.Equal(&grpc.Resource{Capacity: 17, Used: 13}, g.Bandwidth)
}

func TestNet(t *testing.T) {
	assert.Equal(t,
		&network{
			name:    "net",
			options: defaultNetworkOptions()},
		newNet("net"))
}

func TestNetworkMedium(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.medium("", 17))
	require.Len(t, n.media, 1)
	assert.Equal(t, "M1", n.media[0].name)
	assert.Equal(t, bandwidth(17), n.media[0].bw.Cap)
}

func TestNetworkUniqueName(t *testing.T) {
	assert := assert.New(t)
	n := network{}
	assert.Equal("L1", n.uniqueName("L"))
	assert.Equal("L2", n.uniqueName("L"))
	assert.Equal("L3", n.uniqueName("L"))
	assert.Equal("M1", n.uniqueName("M"))
	assert.Equal("P1", n.uniqueName("P"))
	assert.Equal("M2", n.uniqueName("M"))
	assert.Equal("L4", n.uniqueName("L"))
	assert.Equal("X1", n.uniqueName("X"))
}

func TestNetworkNodesToLinks(t *testing.T) {
	assert := assert.New(t)
	net := network{}
	assert.Nil(net.nodesToLinks(""))
	assert.Nil(net.nodesToLinks("N1"))
	assert.Equal([]string{"N1→N2"}, net.nodesToLinks("N1•N2"))
	assert.Equal([]string{"N1→N2", "N2→N3"}, net.nodesToLinks("N1•N2•N3"))

	net.links = []*link{
		{name: "1", src: "N1", dst: "N2"},
		{name: "2", src: "N2", dst: "N3"}}
	assert.Equal([]string{"1", "2"}, net.nodesToLinks("N1•N2•N3"))
}

func TestNetworkLinkWithProps(t *testing.T) {
	n := network{}
	p := linkProps{
		src: "a", dst: "b", wire: true,
		lat: 17, bw: types.Resource[bandwidth]{Cap: 19}}
	n.linkWithProps(&p)
	require.Len(t, n.media, 1)
	assert.Equal(t, "a→bₘ", n.media[0].name)
	require.Len(t, n.links, 1)
	assert.Equal(t, "a→b", n.links[0].name)

	p.src, p.dst, p.path = "c", "d", true
	n.linkWithProps(&p)
	require.Len(t, n.media, 2)
	assert.Equal(t, "c→dₘ", n.media[1].name)
	require.Len(t, n.links, 2)
	assert.Equal(t, "c→d", n.links[1].name)
	require.Len(t, n.paths, 1)
	assert.Equal(t, "c→d", n.paths[0].name)

	n.bearer = "x"
	p.src, p.dst, p.lat = "e", "f", 23
	assert.PanicsWithError(t,
		"link e→f not found",
		func() { n.linkWithProps(&p) })

	p.src, p.dst, p.lat, p.bw.Cap = "a", "b", 23, 29
	n.linkWithProps(&p)
	require.Len(t, n.links, 2)
	assert.Equal(t, latency(23), n.links[0].lat)
	assert.Equal(t, bandwidth(29), n.media[0].bw.Cap)

	n.media[1].name = "?"
	p.src, p.dst, p.lat = "c", "d", 23
	assert.PanicsWithError(t,
		"medium c→dₘ not found",
		func() { n.linkWithProps(&p) })
}

func TestNetworkLinkProps(t *testing.T) {
	net := network{options: defaultNetworkOptions()}
	p1, p2 := net.defaultLinkProps()
	ps := linkProps{wire: true, lat: latency(defaults.LatencyLink),
		bw: types.Resource[bandwidth]{Cap: bandwidth(defaults.BandwidthLink)}}
	assert.Equal(t, ps, p1)
	assert.Equal(t, ps, p2)

	net.options.latency = types.NewOption(latency(17))
	net.options.bandwidth = types.NewOption(bandwidth(19))
	p1, p2 = net.defaultLinkProps()
	ps = linkProps{wire: true, lat: 17,
		bw: types.Resource[bandwidth]{Cap: 19}}
	assert.Equal(t, ps, p1)
	assert.Equal(t, ps, p2)
}

func TestReLinkName(t *testing.T) {
	check := func(link, n1, n2, src, op, dst string) {
		t.Helper()
		assert.Equal(t,
			[]string{link, n1, n2, src, op, dst},
			reLinkName.FindStringSubmatch(link))
	}
	check("a->b", "", "", "a", "->", "b")
	check("a→b", "", "", "a", "→", "b")
	check("a-x-y→b", "", "", "a-x-y", "→", "b")
	check("a-x-y->b", "", "", "a-x-y", "->", "b")
	check("a→b-x-y", "", "", "a", "→", "b-x-y")

	check("n1:a→b", "n1", "", "a", "→", "b")
	check("n1,n2:a→b", "n1", "n2", "a", "→", "b")
}

func TestPathName_internal(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("path", pathNameFromLinks([]string{}))
	assert.Equal("a→b", pathNameFromLinks([]string{"a→b"}))
	assert.Equal("a→²c", pathNameFromLinks([]string{"a→b", "b→c"}))
	assert.Equal("a→³d", pathNameFromLinks([]string{"a→b", "b→c", "c→d"}))

	assert.Equal("a", pathNameFromLinks([]string{"a"}))
	assert.Equal("a∘b", pathNameFromLinks([]string{"a", "b"}))
	assert.Equal("a∘²c", pathNameFromLinks([]string{"a", "b", "c"}))
	assert.Equal("a∘³d", pathNameFromLinks([]string{"a", "b", "c", "d"}))
}

func TestLinksToPath(t *testing.T) {
	assert.Nil(t, linksToPath([]string{}))
	assert.Equal(t,
		&path{name: "N1→N2", links: []string{"N1→N2"}},
		linksToPath([]string{"N1→N2"}))
	assert.Equal(t,
		&path{name: "N1→²N3", links: []string{"N1→N2", "N2→N3"}},
		linksToPath([]string{"N1→N2", "N2→N3"}))
}

func TestNetworkPath_private(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.path(""))
	assert.Len(t, n.paths, 0)

	assert.Same(t, n, n.path("", "a->b"))
	require.Len(t, n.paths, 1)
	assert.Equal(t, "a→b", n.paths[0].name)
	assert.Equal(t, []string{"a->b"}, n.paths[0].links)

	assert.Same(t, n, n.path("", "a→b"))
	require.Len(t, n.paths, 2)
	assert.Equal(t, "a→b", n.paths[1].name)
	assert.Equal(t, []string{"a→b"}, n.paths[1].links)

	assert.Same(t, n, n.path("", "a->b", "b->c"))
	require.Len(t, n.paths, 3)
	assert.Equal(t, "a→²c", n.paths[2].name)
	assert.Equal(t, []string{"a->b", "b->c"}, n.paths[2].links)
}

func TestNetworkPathFromString(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.pathFromString(""))
	assert.Len(t, n.paths, 0)

	assert.Same(t, n, n.pathFromString("N1•N2•N3"))
	require.Len(t, n.paths, 1)
	assert.Equal(t,
		&path{name: "N1→²N3", links: []string{"N1→N2", "N2→N3"}},
		n.paths[0])
}

func TestNetworkBidiPathFromString(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.bidiPathFromString(""))
	assert.Len(t, n.paths, 0)

	assert.Same(t, n, n.bidiPathFromString("N"))
	assert.Len(t, n.paths, 0)

	assert.Same(t, n, n.bidiPathFromString("N1•N2•N3"))
	require.Len(t, n.paths, 2)
	assert.Equal(t,
		&path{name: "N1→²N3", links: []string{"N1→N2", "N2→N3"}},
		n.paths[0])
	assert.Equal(t,
		&path{name: "N3→²N1", links: []string{"N3→N2", "N2→N1"}},
		n.paths[1])
}

func TestNetworkLinkPathFromString(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.linkPathFromString(""))
	assert.Len(t, n.paths, 0)

	assert.Same(t, n, n.linkPathFromString("L1•L2•L3"))
	require.Len(t, n.paths, 1)
	assert.Equal(t,
		&path{name: "L1∘²L3", links: []string{"L1", "L2", "L3"}},
		n.paths[0])
}

func TestNetworkPath(t *testing.T) {
	infra := parseInfrastructure(`
		node N1,N2,N3
		network net
		link N1→N2,N2→N3 with lat:17 bw:19`)
	check := func(input, name string, links ...string) {
		n := infra.networks[0]
		expLen := len(n.paths) + 1
		n.pathFromString(input)
		require.Len(t, n.paths, expLen)
		assert.Equal(t,
			&path{name: name, links: links},
			n.paths[expLen-1])
	}
	check("N1•N2", "N1→N2", "N1→N2")
	check("N1•N2•N3", "N1→²N3", "N1→N2", "N2→N3")
	check("p:N2•N3", "p", "N2→N3")

	infra = parseInfrastructure(`
		node N1,N2,N3,N4
		network net
		link 1:N1→N2,2:N2→N3,3:N3→N4 with lat:17 bw:19`)
	check("N1•N2", "1", "1")
	check("N1•N2•N3", "1∘2", "1", "2")
	check("N1•N2•N3•N4", "1∘²3", "1", "2", "3")
}

func TestNetworkBidiPath(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.bidiPathFromString("N1•N2"))
	assert.Equal(t,
		[]*path{
			{name: "N1→N2", links: []string{"N1→N2"}},
			{name: "N2→N1", links: []string{"N2→N1"}}},
		n.paths)

	n = &network{}
	assert.Same(t, n, n.bidiPathFromString("a:N1•N2"))
	assert.Equal(t,
		[]*path{
			{name: "a", links: []string{"N1→N2"}},
			{name: "N2→N1", links: []string{"N2→N1"}}},
		n.paths)

	n = &network{}
	assert.Same(t, n, n.bidiPathFromString("a,b:N1•N2"))
	assert.Equal(t,
		[]*path{
			{name: "a", links: []string{"N1→N2"}},
			{name: "b", links: []string{"N2→N1"}}},
		n.paths)
}

func TestNetworkLinkPath(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.linkPathFromString("L1"))
	assert.Equal(t,
		[]*path{{name: "L1", links: []string{"L1"}}},
		n.paths)

	n = &network{}
	assert.Same(t, n, n.linkPathFromString("1•2•3"))
	assert.Equal(t,
		[]*path{{name: "1∘²3", links: []string{"1", "2", "3"}}},
		n.paths)

	n = &network{}
	assert.Same(t, n, n.linkPathFromString("a:L1•L2"))
	assert.Equal(t,
		[]*path{{name: "a", links: []string{"L1", "L2"}}},
		n.paths)
}

func TestNetworkAsGrpc(t *testing.T) {
	assert := assert.New(t)
	n := network{name: "name", bearer: "bearer"}
	g := n.asGrpc()
	assert.Equal("name", g.Id)
	assert.Nil(g.Links)
	assert.Nil(g.Media)
	assert.Nil(g.Paths)
	assert.Equal("bearer", g.Bearer)

	n.links = []*link{{name: "name"}}
	n.paths = []*path{{name: "name"}}
	n.media = []*medium{{name: "name"}}
	g = n.asGrpc()
	assert.Equal("name", g.Id)
	assert.Len(g.Links, 1)
	assert.Len(g.Media, 1)
	assert.Len(g.Paths, 1)
	assert.Equal("bearer", g.Bearer)
}

func TestPathIndex(t *testing.T) {
	assert.Equal(t,
		"\u207D\u00B9\u00B2\u00B3\u2074\u2075"+ // superscript "(12345"
			"\u2076\u2077\u2078\u2079\u2070\u207E", // superscript "67890)"
		pathIndex(1234567890))
}

func TestNetworkMakePathsNamesUnique(t *testing.T) {
	assert := assert.New(t)
	net := network{paths: []*path{
		{name: "a→b"},
		{name: "c→\u2074d"},
		{name: "c→\u2074d"},
		{name: "c→\u2074d"},
		{name: "e→\u00B2f"}}}
	net.makePathNamesUnique()
	assert.Equal("a→b", net.paths[0].name)
	assert.Equal("c→\u2074d\u207D\u00B9\u207E", net.paths[1].name)
	assert.Equal("c→\u2074d\u207D\u00B2\u207E", net.paths[2].name)
	assert.Equal("c→\u2074d\u207D\u00B3\u207E", net.paths[3].name)
	assert.Equal("e→\u00B2f", net.paths[4].name)
}

func TestINfrastructureFindLink(t *testing.T) {
	infra := parseInfrastructure(`
		node N1,N2,N3,N4
		network net
		link 1:N1→N2,2:N2→N3,3:N3→N4 with lat:17 bw:19`)
	assert.Equal(t, 0, infra.findLink("N1", "N2"))
	assert.Equal(t, 1, infra.findLink("N2", "N3"))
	assert.Equal(t, -1, infra.findLink("Nx", "N3"))
	assert.Equal(t, -1, infra.findLink("N1", "Nx"))
}

func TestInfrastructureAsGrpc(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	i := infrastructure{}
	g := i.asGrpc()
	assert.Nil(g.Nodes)
	assert.Nil(g.Networks)

	i = infrastructure{
		workers:  []*node{{name: "w1"}},
		networks: []*network{{name: "eth"}}}
	check := func(names ...string) int {
		g := i.asGrpc()
		require.Len(g.Nodes, len(names))
		for i, name := range names {
			assert.Equal(name, g.Nodes[i].Id)
		}
		return len(g.Networks)
	}
	assert.Equal(1, check("w1"))

	i.networks = []*network{
		{name: "n1"},
		{name: "n2", switches: []*node{{name: "s1"}, {name: "s2"}}}}
	assert.Equal(2, check("w1", "s1", "s2"))

	i.networks = append(i.networks, &network{
		name:     "n3",
		switches: []*node{{name: "s1"}, {name: "s2"}},
		bearer:   "n2"})
	assert.Equal(3, check("w1", "s1", "s2"))
}

func TestInfrastructureNode(t *testing.T) {
	i := &infrastructure{}
	assert.Same(t, i, i.node("N", 1000, 8000))
	require.Len(t, i.workers, 1)
	assert.Equal(t,
		&node{
			name: "N",
			kind: kindCompute,
			cpu:  *types.NewResource[milliCPU](1000),
			ram:  *types.NewResource[ramMB](8000)},
		i.workers[0])

	assert.Same(t, i, i.node("M", 1000, 8000, "A", "B"))
	require.Len(t, i.workers, 2)
	assert.Equal(t,
		&node{
			name:   "M",
			kind:   kindCompute,
			cpu:    *types.NewResource[milliCPU](1000),
			ram:    *types.NewResource[ramMB](8000),
			labels: []string{"A", "B"}},
		i.workers[1])
}

func TestInfrastructureNetworkNode(t *testing.T) {
	i := &infrastructure{}
	assert.Same(t, i, i.networkNode("N1", 0, 0))
	require.Len(t, i.workers, 1)
	assert.Equal(t,
		&node{name: "N1", kind: kindNetwork},
		i.workers[0])

	assert.Same(t, i, i.networkNode("N2", 17, 19))
	require.Len(t, i.workers, 2)
	assert.Equal(t,
		&node{
			name: "N2",
			kind: kindNetwork,
			lat:  17,
			bw:   *types.NewResource[bandwidth](19)},
		i.workers[1])
}
