// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
)

type workloadSpec struct {
	app, name string
}

type workloadProps struct {
	cpu    milliCPU
	ram    ramMB
	labels []string
	node   string
}

type workload struct {
	name   string
	cpu    milliCPU
	ram    ramMB
	labels []string
	node   string
}

func workloadWithProps(
	a *application, name string, props *workloadProps,
) *application {
	a.workloads = append(a.workloads, &workload{
		name:   name,
		cpu:    props.cpu,
		ram:    props.ram,
		labels: props.labels,
		node:   props.node})
	return a
}

func (w *workload) asGrpc() *grpc.Workload {
	g := &grpc.Workload{
		Id:     w.name,
		Node:   w.node,
		CPU:    int32(w.cpu),
		Memory: int64(w.ram)}
	if len(w.labels) > 0 {
		g.RequiredLabels = make([]string, 0, len(w.labels))
		g.ForbiddenLabels = make([]string, 0, len(w.labels))
		for _, l := range w.labels {
			if l[0] == '-' || l[0] == '!' || l[0] == '~' || l[0] == '¬' {
				g.ForbiddenLabels = append(g.ForbiddenLabels, l[1:])
			} else {
				g.RequiredLabels = append(g.RequiredLabels, l)
			}
		}
	}
	return g
}

type application struct {
	name      string
	workloads []*workload
}

func (a *application) asGrpc() *grpc.Application {
	g := &grpc.Application{Id: a.name}
	if len(a.workloads) > 0 {
		g.Workloads = make([]*grpc.Workload, len(a.workloads))
		for i, w := range a.workloads {
			g.Workloads[i] = w.asGrpc()
		}
	}
	return g
}

type channelData struct {
	name string
	src  workloadSpec
	dst  workloadSpec
	path pathSpec
}

type channelProps struct {
	qos string
	lat latency
	bw  bandwidth
}

type channelSpec struct {
	twoWay  bool
	to, fro channelData
}

type channel struct {
	name string
	src  workloadSpec
	dst  workloadSpec
	path pathSpec
	qos  string
	lat  latency
	bw   bandwidth
}

func (c *channel) asGrpc() *grpc.Channel {
	v := strings.ReplaceAll(strings.ToUpper(c.qos), "-", "_")
	sc := grpc.ServiceClass(grpc.ServiceClass_value[v])
	return &grpc.Channel{Id: c.name,
		SourceApplication: c.src.app, SourceWorkload: c.src.name,
		TargetApplication: c.dst.app, TargetWorkload: c.dst.name,
		Network: c.path.network, Path: c.path.name,
		Qos: sc, Latency: int64(c.lat), Bandwidth: int64(c.bw)}
}

func channelWithProps(ag *appGroup, cs *channelData, cp *channelProps) {
	c := &channel{name: cs.name,
		src:  cs.src,
		dst:  cs.dst,
		path: cs.path,
		qos:  cp.qos, lat: cp.lat, bw: cp.bw}
	ag.channels = append(ag.channels, c)
}

type appGroup struct {
	apps     []*application
	channels []*channel
}

func (ag *appGroup) asGrpc() *grpc.ApplicationGroup {
	if ag == nil || len(ag.apps) == 0 {
		return nil
	}
	g := &grpc.ApplicationGroup{
		Applications: make([]*grpc.Application, len(ag.apps))}
	for i, a := range ag.apps {
		g.Applications[i] = a.asGrpc()
	}
	if len(ag.channels) > 0 {
		g.Channels = make([]*grpc.Channel, len(ag.channels))
		for i, c := range ag.channels {
			g.Channels[i] = c.asGrpc()
		}
	}
	return g
}

func compound(app, wl string) string {
	return app + "_" + wl
}
func (ag *appGroup) makeCompoundNames() {
	for _, app := range ag.apps {
		for _, w := range app.workloads {
			w.name = compound(app.name, w.name)
		}
	}
	for _, ch := range ag.channels {
		ch.src.name = compound(ch.src.app, ch.src.name)
		ch.dst.name = compound(ch.dst.app, ch.dst.name)
	}
}
