// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
)

func TestAssignmentWorkload(t *testing.T) {
	var a assignment
	assert.Same(t, &a, a.Workload("app•workload:node"))
	require.Len(t, a.Workloads, 1)
	assert.Equal(t,
		&workloadAsgmt{app: "app", name: "workload", node: "node"},
		a.Workloads[0])
	a.Workload("illegal")
	assert.Len(t, a.Workloads, 1)
}

func TestAWorkloadAsgmtAsGrpc(t *testing.T) {
	assert := assert.New(t)
	wa := workloadAsgmt{}
	assert.Nil(wa.asGrpc())

	wa = workloadAsgmt{app: "app", name: "workload", node: "node"}
	assert.Equal(
		&grpc.Assignment_Workload{
			Application: "app", Workload: "workload", Node: "node"},
		wa.asGrpc())
}

func TestAssignmentChannel(t *testing.T) {
	var a assignment
	assert.Same(t, &a, a.Channel("channel:net:path"))
	require.Len(t, a.Channels, 1)
	assert.Equal(t,
		&channelAsgmt{channel: "channel", network: "net", path: "path"},
		a.Channels[0])

	a.Channel("c:nw:p:n:m")
	require.Len(t, a.Channels, 2)
	assert.Equal(t,
		&channelAsgmt{
			channel: "c", network: "nw", path: "p", source: "n", target: "m"},
		a.Channels[1])

	a.Channel("illegal")
	assert.Len(t, a.Channels, 2)
}

func TestChannelAsgmtAsGrpc(t *testing.T) {
	assert := assert.New(t)
	ca := channelAsgmt{}
	assert.Nil(ca.asGrpc())

	ca = channelAsgmt{channel: "chan", network: "net", path: "path"}
	assert.Equal(
		&grpc.Assignment_Channel{
			Channel: ca.channel, Network: ca.network, Path: ca.path},
		ca.asGrpc())

	ca.source, ca.target = "src", "dst"
	assert.Equal(
		&grpc.Assignment_Channel{
			Channel: ca.channel, Network: ca.network, Path: ca.path,
			Source: ca.source, Target: ca.target},
		ca.asGrpc())
}

func TestAssignmentMakeCompoundName(t *testing.T) {
	asgmt := assignment{}
	asgmt.Workload("a•w:n").Workload("b•v:m")
	asgmt.MakeCompoundNames()
	assert.Equal(t, "a_w", asgmt.Workloads[0].name)
	assert.Equal(t, "b_v", asgmt.Workloads[1].name)
}

func TestAssignmentAsGrpc(t *testing.T) {
	assert := assert.New(t)
	a := (*assignment)(nil)
	assert.Nil(a.asGrpc())

	a = &assignment{}
	assert.Nil(a.asGrpc())

	a = &assignment{
		Workloads: []*workloadAsgmt{
			{"app", "workload", "node"}}}
	assert.Equal(
		&grpc.Assignment{
			Workloads: []*grpc.Assignment_Workload{
				{Application: "app", Workload: "workload", Node: "node"}}},
		a.asGrpc())

	a = &assignment{
		Workloads: []*workloadAsgmt{
			{"app", "workload", "node"}},
		Channels: []*channelAsgmt{
			{channel: "chan", network: "net", path: "path"}}}
	assert.Equal(
		&grpc.Assignment{
			Workloads: []*grpc.Assignment_Workload{
				{Application: "app", Workload: "workload", Node: "node"}},
			Channels: []*grpc.Assignment_Channel{
				{Channel: "chan", Network: "net", Path: "path"}}},
		a.asGrpc())
}
