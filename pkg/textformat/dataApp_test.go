// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
)

func TestWorkloadAsGrpc(t *testing.T) {
	w := workload{name: "w", cpu: 17, ram: 19, node: "n"}
	assert.Equal(t,
		&grpc.Workload{Id: "w", CPU: 17, Memory: 19, Node: "n"},
		w.asGrpc())

	w.labels = []string{"a", "b", "!c", "~d"}
	assert.Equal(t,
		&grpc.Workload{
			Id: "w", CPU: 17, Memory: 19, Node: "n",
			RequiredLabels:  []string{"a", "b"},
			ForbiddenLabels: []string{"c", "d"}},
		w.asGrpc())
}

func TestChannelAsGrpc(t *testing.T) {
	c := channel{
		name: "c", lat: 1, bw: 2,
		src:  workloadSpec{"a", "s"},
		dst:  workloadSpec{"a", "d"},
		path: pathSpec{"n", "p"},
	}
	assert.Equal(t,
		&grpc.Channel{
			Id:                "c",
			SourceApplication: "a", SourceWorkload: "s",
			TargetApplication: "a", TargetWorkload: "d",
			Network: "n", Path: "p",
			Latency:   1,
			Bandwidth: 2},
		c.asGrpc())
}

func TestApplicationAsGrpc(t *testing.T) {
	a := application{name: "a"}
	assert.Equal(t,
		&grpc.Application{Id: "a"},
		a.asGrpc())

	a.workloads = []*workload{{name: "w1"}, {name: "w2"}}
	assert.Equal(t,
		&grpc.Application{Id: "a",
			Workloads: []*grpc.Workload{
				{Id: "w1"},
				{Id: "w2"}}},
		a.asGrpc())
}

func TestAppGroupAsGrpc(t *testing.T) {
	ag := (*appGroup)(nil)
	assert.Nil(t, ag.asGrpc())

	ag = &appGroup{}
	assert.Nil(t, ag.asGrpc())

	ag.apps = []*application{
		{name: "a",
			workloads: []*workload{{name: "w1"}}}}
	assert.Equal(t,
		&grpc.ApplicationGroup{
			Applications: []*grpc.Application{
				{Id: "a",
					Workloads: []*grpc.Workload{{Id: "w1"}}}}},
		ag.asGrpc())

	ag.channels = []*channel{{name: "c"}}
	assert.Equal(t,
		&grpc.ApplicationGroup{
			Applications: []*grpc.Application{
				{Id: "a",
					Workloads: []*grpc.Workload{{Id: "w1"}}}},
			Channels: []*grpc.Channel{{Id: "c"}}},
		ag.asGrpc())
}

func TestAppGroupMakeCompoundNames(t *testing.T) {
	ag := appGroup{
		apps: []*application{
			{name: "A1",
				workloads: []*workload{
					{name: "W1"},
					{name: "W2"}}}},
		channels: []*channel{
			{src: workloadSpec{"A1", "W1"},
				dst: workloadSpec{"A1", "W2"}}}}
	ag.makeCompoundNames()
	assert.Equal(t,
		appGroup{
			apps: []*application{
				{name: "A1",
					workloads: []*workload{
						{name: "A1_W1"},
						{name: "A1_W2"}}}},
			channels: []*channel{
				{src: workloadSpec{"A1", "A1_W1"},
					dst: workloadSpec{"A1", "A1_W2"}}}},
		ag)
}

func TestChannelWithProps(t *testing.T) {
	var ag appGroup
	sp := channelData{name: "name",
		src:  workloadSpec{app: "a1", name: "w1"},
		dst:  workloadSpec{app: "a2", name: "w2"},
		path: pathSpec{"net", "p1"}}
	ps := channelProps{qos: "assured", lat: 1, bw: 2}
	channelWithProps(&ag, &sp, &ps)
	cs := ag.channels
	require.Len(t, cs, 1)
	c := cs[0]
	assert.Equal(t, workloadSpec{"a1", "w1"}, c.src)
	assert.Equal(t, workloadSpec{"a2", "w2"}, c.dst)
	assert.Equal(t, "assured", c.qos)
	assert.Equal(t, pathSpec{"net", "p1"}, c.path)
	assert.Equal(t, latency(1), c.lat)
	assert.Equal(t, bandwidth(2), c.bw)

	ps.qos = "best-effort"
	channelWithProps(&ag, &sp, &ps)
	cs = ag.channels
	require.Len(t, cs, 2)
	c = cs[1]
	assert.Equal(t, "best-effort", c.qos)
}
