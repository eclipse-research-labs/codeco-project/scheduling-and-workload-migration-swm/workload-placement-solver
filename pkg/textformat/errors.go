// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import "errors"

var (
	ErrIllegalAsgmt = errors.New("illegal assignment file")
	ErrPrematureEOF = errors.New("premature EOF")
)
