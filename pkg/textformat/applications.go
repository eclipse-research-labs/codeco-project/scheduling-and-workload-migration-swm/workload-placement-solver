// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"fmt"
	"strings"

	"siemens.com/qos-solver/internal/defaults"
)

func (p *parser) workload(app *application) {
	names, nodes := p.nameList(), []string{}
	props := workloadProps{
		cpu: milliCPU(defaults.CPU), ram: ramMB(defaults.RAM)}
	if p.hasWord("on") {
		nodes = p.nameList(len(names))
	}
	if p.hasWord("needs") {
		if p.token != Property {
			p.syntaxError("expected: property")
		}
		for p.token == Property {
			switch strings.ToLower(p.scanner.token) {
			case _cpu:
				p.next()
				props.cpu = p.getCPU()
			case _ram, _memory:
				p.next()
				props.ram = p.getRAM()
			case _labels:
				p.next()
				props.labels = p.getLabels(true)
			default:
				p.syntaxError("unknown property: %q", p.scanner.token)
			}
		}
	}
	for i, name := range names {
		if i < len(nodes) {
			props.node = nodes[i]
		}
		workloadWithProps(app, name, &props)
	}
}

func (p *parser) application() string {
	app := &application{name: p.expectId()}
	p.data.apps.apps = append(p.data.apps.apps, app)
	if p.token != Identifier || !p.identMatch("workload") {
		p.syntaxError("expected: %q", "workload")
	}
	for p.hasWord("workload") {
		p.workload(app)
	}
	return app.name
}

func (p *parser) workloadRef(app string) workloadSpec {
	pos := p.scanner.Position
	id := p.expectId()
	if app == "" && p.token != CompoundOp {
		p.scanner.Position = pos
		p.syntaxError("expected: qualified name")
	}
	if p.hasToken(CompoundOp) {
		return workloadSpec{app: id, name: p.expectId()}
	}
	return workloadSpec{app: app, name: id}
}

func (p *parser) channelName(src, dst workloadSpec) string {
	return fmt.Sprintf("%s•%s→%s•%s", src.app, src.name, dst.app, dst.name)
}

func (p *parser) propIntOrID() string {
	if p.token == Property {
		return p.scanner.token[:len(p.scanner.token)-1]
	} else if p.token == Integer {
		id := p.scanner.token
		p.next()
		return id
	}
	return p.expectId()
}

func (p *parser) chSpec(app string) (cs channelSpec) {
	name := p.propIntOrID()
	switch p.token {
	case Colon, Property: // single name
		cs.to.name = name
		p.next()
		cs.to.src = p.workloadRef(app)
	case Comma: // two names
		p.next()
		cs.to.name, cs.fro.name = name, p.propIntOrID()
		cs.twoWay = true
		if p.token == Property {
			p.next()
		} else {
			p.expect(":", Colon)
		}
		cs.to.src = p.workloadRef(app)
	case CompoundOp: // application name
		p.next()
		cs.to.src.app, cs.to.src.name = name, p.expectId()
	case TwoWayOp, OneWayOp: // workload name
		cs.to.src.app, cs.to.src.name = app, name
	default:
		p.syntaxError("unexpected: %q", p.scanner.token)
	}
	op := p.token
	if op != TwoWayOp && op != OneWayOp {
		p.syntaxError("expected: direction operator")
	} else if cs.twoWay && op == OneWayOp {
		p.syntaxError("expected: two-way operator")
	} else if op == TwoWayOp {
		cs.twoWay = true
	}
	p.next()
	cs.to.dst = p.workloadRef(app)
	if cs.to.name == "" {
		cs.to.name = p.channelName(cs.to.src, cs.to.dst)
	}
	if cs.twoWay {
		cs.fro.src, cs.fro.dst = cs.to.dst, cs.to.src
		if cs.fro.name == "" {
			cs.fro.name = p.channelName(cs.to.dst, cs.to.src)
		}
	}
	return
}

func (p *parser) pathRef() pathSpec {
	pos := p.scanner.Position
	net := p.expectId()
	if p.token != CompoundOp {
		p.scanner.Position = pos
		p.syntaxError("expected: qualified name")
	}
	p.next()
	return pathSpec{network: net, name: p.expectIdOrInt()}
}

func (p *parser) pathRefs(cs []channelSpec) {
	i, to := 0, true
	setPath := func() {
		if pr := p.pathRef(); to {
			cs[i].to.path = pr
			if cs[i].twoWay {
				to = false
			} else {
				i++
			}
		} else {
			cs[i].fro.path = pr
			i, to = i+1, true
		}
	}
	setPath()
	for p.token == Comma {
		if i >= len(cs) {
			p.syntaxError("expected: at most %d path references", len(cs))
		}
		p.next()
		setPath()
	}
	if !to {
		p.syntaxError("not enough path references")
	}
}

func (p *parser) channel(app string) {
	twoWay, specs := false, []channelSpec{}
	nextSpec := func() {
		spec := p.chSpec(app)
		twoWay = twoWay || spec.twoWay
		specs = append(specs, spec)
	}
	for nextSpec(); p.token == Comma; {
		p.next()
		nextSpec()
	}
	if p.hasWord("on") {
		p.pathRefs(specs)
	}
	to := channelProps{
		qos: defaults.NetworkQoS,
		lat: latency(defaults.Latency),
		bw:  bandwidth(defaults.Bandwidth)}
	fro := to
	if p.hasWord("needs") {
		if p.token != Property {
			p.syntaxError("expected: property")
		}
		for p.token == Property {
			switch strings.ToLower(p.scanner.token) {
			case _lat, _latency:
				to.lat = p.getLatency()
				if p.token == Comma {
					if !twoWay {
						p.syntaxError("unexpected latency")
					}
					fro.lat = p.getLatency()
				} else {
					fro.lat = to.lat
				}
			case _bw, _bandwidth:
				to.bw = p.getBandwidth()
				if p.token == Comma {
					if !twoWay {
						p.syntaxError("unexpected bandwidth")
					}
					fro.bw = p.getBandwidth()
				} else {
					fro.bw = to.bw
				}
			case _qos:
				to.qos = p.getQoS()
				if p.token == Comma {
					if !twoWay {
						p.syntaxError("unexpected service class")
					}
					fro.qos = p.getQoS()
				} else {
					fro.qos = to.qos
				}
			default:
				p.syntaxError("unknown property: %q", p.scanner.token)
			}
		}
	}
	for i := range specs {
		s := &specs[i]
		channelWithProps(&p.data.apps, &s.to, &to)
		if s.twoWay {
			channelWithProps(&p.data.apps, &s.fro, &fro)
		}
	}
}

func (p *parser) applications() {
	p.hasWord("applications")
	p.expect("application")
	app := p.application()
	for stop := false; !stop; {
		switch {
		case p.hasWord("application"):
			app = p.application()
		case p.hasWord("channel"):
			p.channel(app)
		default:
			stop = true
		}
	}
}
