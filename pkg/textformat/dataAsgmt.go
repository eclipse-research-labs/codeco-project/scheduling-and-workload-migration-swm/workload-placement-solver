// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"regexp"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
)

type workloadAsgmt struct {
	app  string
	name string
	node string
	// oldNode string
}

func (a *workloadAsgmt) asGrpc() *grpc.Assignment_Workload {
	if a == nil || a.name == "" {
		return nil
	}
	return &grpc.Assignment_Workload{
		Application: a.app, Workload: a.name, Node: a.node}
}

type channelAsgmtProps struct {
	name string
	path pathSpec
}

type channelAsgmt struct {
	channel string
	network string
	path    string
	// oldNetwork string
	// oldPath    string
	// fields below needed for implicit reverse path
	source string
	target string
}

func (a *channelAsgmt) asGrpc() *grpc.Assignment_Channel {
	if a == nil || a.channel == "" {
		return nil
	}
	g := &grpc.Assignment_Channel{
		Channel: a.channel, Network: a.network, Path: a.path}
	if a.source != "" && a.target != "" {
		g.Source, g.Target = a.source, a.target
	}
	return g
}

type assignment struct {
	Workloads []*workloadAsgmt
	Channels  []*channelAsgmt
}

var wlSpec = regexp.MustCompile(`^([\w.-]+)•([\w.-]+):([\w.-]+)$`)

func (a *assignment) Workload(asgmt string) *assignment {
	if m := wlSpec.FindStringSubmatch(asgmt); m != nil {
		a.Workloads = append(a.Workloads, &workloadAsgmt{
			app: m[1], name: m[2], node: m[3]})
	}
	return a
}

func (a *assignment) ChannelWithProps(props *channelAsgmtProps) *assignment {
	a.Channels = append(a.Channels, &channelAsgmt{
		channel: props.name,
		network: props.path.network,
		path:    props.path.name})
	return a
}

func (a *assignment) Channel(asgmt string) *assignment {
	m := strings.Split(asgmt, ":")
	var ca *channelAsgmt
	switch {
	case len(m) == 3:
		ca = &channelAsgmt{
			channel: strings.TrimSpace(m[0]),
			network: strings.TrimSpace(m[1]),
			path:    strings.TrimSpace(m[2])}
	case len(m) == 5:
		ca = &channelAsgmt{
			channel: strings.TrimSpace(m[0]),
			network: strings.TrimSpace(m[1]),
			path:    strings.TrimSpace(m[2]),
			source:  strings.TrimSpace(m[3]),
			target:  strings.TrimSpace(m[4])}
	}
	if ca != nil {
		a.Channels = append(a.Channels, ca)
	}
	return a
}

func (a *assignment) MakeCompoundNames() {
	for _, w := range a.Workloads {
		w.name = compound(w.app, w.name)
	}
}

func (a *assignment) asGrpc() *grpc.Assignment {
	if a == nil || len(a.Workloads) == 0 {
		return nil
	}
	g := grpc.Assignment{
		Workloads: make([]*grpc.Assignment_Workload, len(a.Workloads))}
	for i, w := range a.Workloads {
		g.Workloads[i] = w.asGrpc()
	}
	if len(a.Channels) > 0 {
		g.Channels = make([]*grpc.Assignment_Channel, len(a.Channels))
		for i, c := range a.Channels {
			g.Channels[i] = c.asGrpc()
		}
	}
	return &g
}
