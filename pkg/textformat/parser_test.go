// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParserNext(t *testing.T) {
	p := new(parser)
	p.initWithString("/* \u03B1 */ // \u03B2\ncrash \u03B3")
	assert.Equal(t, Identifier, p.next())
	assert.PanicsWithValue(t,
		"<input>:2:7: illegal char: U+03B3", func() { p.next() })
}

func TestParserExpect_word(t *testing.T) {
	p := makeParser(`id Id ID 17 unknown`)
	expect := func() {
		p.expect("id")
	}
	assert.NotPanics(t, expect)
	assert.NotPanics(t, expect)
	assert.NotPanics(t, expect)
	assert.PanicsWithValue(t, `<input>:1:10: expected: "id"`, expect)
	p.next()
	assert.PanicsWithValue(t, `<input>:1:13: expected: "id"`, expect)
}

func TestParserExpect_token(t *testing.T) {
	p := makeParser(`( ) x`)
	expect := func() {
		p.expect("(", LParen)
	}
	assert.NotPanics(t, expect)
	assert.PanicsWithValue(t, `<input>:1:3: expected: "("`, expect)
	p.next()
	assert.PanicsWithValue(t, `<input>:1:5: expected: "("`, expect)
}

func TestParserHasWord(t *testing.T) {
	p := makeParser(`apps Apps APPS error`)
	hasWord := func() bool { return p.hasWord("apps") }
	assert.True(t, hasWord())
	assert.True(t, hasWord())
	assert.True(t, hasWord())
	assert.False(t, hasWord())
}

func TestParserOneOf(t *testing.T) {
	p := makeParser(`EIns zwei DREI`)
	ws := []string{"eins", "zwei"}
	w, ok := p.hasOneOf(ws...)
	assert.True(t, ok)
	assert.Equal(t, ws[0], w)

	w, ok = p.hasOneOf(ws...)
	assert.True(t, ok)
	assert.Equal(t, ws[1], w)

	w, ok = p.hasOneOf(ws...)
	assert.False(t, ok)
	assert.Equal(t, "", w)
}

func TestPSection(t *testing.T) {
	p := makeParser(
		`application app1 workload w1 infrastructure node c1`)
	p.sections()
	assert.Equal(t, EOF, p.token)
	assert.Len(t, p.data.apps.apps, 1)
	assert.Len(t, p.data.infra.workers, 1)

	p = makeParser(
		`applications application a1 workload w1 node c1 provides cpu:1000`,
		`Section X application x workload y node x1 provides cpu:1000`)
	p.sections()
	assert.Equal(t, EOF, p.token)
	assert.Len(t, p.data.apps.apps, 2)
	assert.Len(t, p.data.infra.workers, 2)
}

func TestParserParse_minimal(t *testing.T) {
	var p parser
	p.init("testdata/textformat/minimal.asgmt")
	assert.NotPanics(t, func() { p.parse() })
}

func TestParserParse_makeCompound(t *testing.T) {
	p := &parser{}
	p.initWithString(`
		workload assignment v1
		model not-eof with compound-names:true
		application a workload w
		node c1
		assignment workload a•w on c1`)
	assert.NoError(t, p.parse())

	ag := &p.data.apps
	require.Len(t, ag.apps, 1)
	require.Len(t, ag.apps[0].workloads, 1)
	assert.Equal(t, "a_w", ag.apps[0].workloads[0].name)

	asgmts := &p.data.asgmts
	require.Len(t, *asgmts, 1)
	require.Len(t, (*asgmts)[0].Workloads, 1)
	assert.Equal(t, "a_w", (*asgmts)[0].Workloads[0].name)
}

func TestParserParse_errors(t *testing.T) {
	p := &parser{}
	p.initWithString(`
		workload assignment v1 model not-eof
		application a workload w
		node C1 -`)
	err := p.parse()
	assert.ErrorContains(t, err, `stopper: "-"`)

	p = &parser{}
	p.initWithString(`
		workload assignment v1 /* not closed`)
	err = p.parse()
	assert.ErrorContains(t, err, "comment not terminated")
}
