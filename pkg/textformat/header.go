// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"regexp"
	"strconv"
)

func (p *parser) version() {
	if p.token != Identifier {
		p.syntaxError("expected: version")
	}
	id := p.scanner.token
	if m, err := regexp.MatchString("^v[0-9]+$", id); err != nil || !m {
		p.syntaxError("illegal version: %q", id)
	}
	if id != "v1" {
		p.syntaxError("unsupported version: %q", id)
	}
	p.data.version, _ = strconv.Atoi(id[1:])
	p.next()
}

func (p *parser) model() {
	p.expect("model")
	p.data.model, p.data.options = p.expectId(), DefaultModelOptions()
	if _, ok := p.hasOneOf("options", "with"); !ok {
		return
	}
	for stop, first := false, true; !stop; first = false {
		name := p.scanner.token
		switch {
		case p.token == Property:
			name = name[:len(name)-1]
		case p.token == Identifier:
		case first:
			p.syntaxError("expected: identifier")
		}
		var value *bool
		switch name {
		case _compoundNames:
			value = &p.data.options.CompoundNames
		case _implicitReversePath:
			value = &p.data.options.ImplicitReversePath
		case _shortestPathOnly:
			value = &p.data.options.ShortestPathsOnly
		default:
			if p.token == Identifier && !first {
				stop = true
				continue
			}
			p.syntaxError("unknown option: %q", name)
		}
		if p.token == Identifier {
			p.next()
			p.expect(":", Colon)
		} else {
			p.next()
		}
		*value = p.getBoolean()
		stop = p.token != Property && p.token != Identifier
	}
}

func (p *parser) header() {
	p.expect("workload")
	p.expect("assignment")
	p.version()
	p.model()
}
