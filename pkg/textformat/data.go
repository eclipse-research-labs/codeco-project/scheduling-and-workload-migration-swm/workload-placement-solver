// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"fmt"

	"siemens.com/qos-solver/internal/defaults"
)

var (
	CompoundNames       bool = defaults.CompoundNames
	ImplicitReversePath bool = defaults.ImplicitReversePath
	ShortestPathsOnly   bool = defaults.ShortestPathsOnly
)

func member(name string, names []string) bool {
	for _, ni := range names {
		if name == ni {
			return true
		}
	}
	return false
}

func addWorkers(n *network, nodes []*node, names []string) {
	for _, node := range nodes {
		found := false
		for _, worker := range n.workers {
			if worker == node {
				found = true
				break
			}
		}
		if !found {
			if found = len(names) == 0; !found {
				for _, name := range names {
					if node.name == name {
						found = true
						break
					}
				}
			}
			if found {
				n.workers = append(n.workers, node)
			}
		}
	}
}

func removeLinksAndPaths(n *network, node string) {
	ls, j := make([]*link, 0, 16), 0
	for _, l := range n.links {
		if l.src != node && l.dst != node {
			n.links[j] = l
			j++
		} else {
			ls = append(ls, l)
		}
	}
	hasOneOf := func(p *path, ls []*link) bool {
		for _, ln := range p.links {
			for _, l := range ls {
				if ln == l.name {
					return true
				}
			}
		}
		return false
	}
	if len(ls) > 0 {
		n.links, j = n.links[:j], 0
		for _, p := range n.paths {
			if !hasOneOf(p, ls) {
				n.paths[j] = p
				j++
			}
		}
		n.paths = n.paths[:j]
	}
}

func filterWorkers(n *network, names []string) {
	removeNodes := func(list []*node) []*node {
		j := 0
		for _, node := range list {
			if !member(node.name, names) {
				removeLinksAndPaths(n, node.name)
			} else {
				list[j] = node
				j++
			}
		}
		return list[:j]
	}
	if len(names) > 0 {
		n.workers = removeNodes(n.workers)
	}
}

func networkFromBearer(
	name string, opts networkOptions, bearer *network,
) *network {
	net := &network{
		name: name, bearer: bearer.name, options: bearer.options}
	copyNodes := func(ns []*node) (cs []*node) {
		if len(ns) > 0 {
			cs = make([]*node, len(ns))
			for i, n := range ns {
				cs[i] = n.copy()
			}
		}
		return
	}
	net.workers = copyNodes(bearer.workers)
	net.switches = copyNodes(bearer.switches)
	if len(bearer.media) > 0 {
		net.media = make([]*medium, len(bearer.media))
		for i, m := range bearer.media {
			net.media[i] = m.copy()
		}
	}
	if len(bearer.links) > 0 {
		net.links = make([]*link, len(bearer.links))
		for i, l := range bearer.links {
			net.links[i] = &link{
				name: l.name, src: l.src, dst: l.dst,
				medium: l.medium, lat: l.lat}
		}
	}
	if len(bearer.paths) > 0 {
		net.paths = make([]*path, len(bearer.paths))
		for i, p := range bearer.paths {
			net.paths[i] = &path{name: p.name}
			if p.links != nil {
				net.paths[i].links = append([]string{}, p.links...)
			}
		}
	}
	if opts.qos.WasSet() {
		net.options.qos = opts.qos
	}
	if opts.latency.WasSet() {
		lat, bearerLat := opts.latency.Value(), bearer.options.latency.Value()
		if lat != bearerLat {
			for _, l := range net.links {
				if l.lat == latency(bearerLat) {
					l.lat = latency(lat)
				}
			}
		}
		net.options.latency = opts.latency
	}
	if opts.bandwidth.WasSet() {
		bw, bearerBw := opts.bandwidth.Value(), bearer.options.bandwidth.Value()
		if bw != bearerBw {
			for _, m := range net.media {
				if m.bw.Cap == bandwidth(bearerBw) {
					m.bw.Cap = bandwidth(bw)
				}
			}
		}
		net.options.bandwidth = opts.bandwidth
	}
	return net
}

type ModelOptions struct {
	CompoundNames       bool
	ImplicitReversePath bool
	ShortestPathsOnly   bool
}

func DefaultModelOptions() ModelOptions {
	return ModelOptions{
		CompoundNames:       CompoundNames,
		ImplicitReversePath: ImplicitReversePath,
		ShortestPathsOnly:   ShortestPathsOnly}
}

func (mo ModelOptions) String() string {
	return fmt.Sprintf(
		"%s:%t %s:%t %s:%t",
		_compoundNames, mo.CompoundNames,
		_implicitReversePath, mo.ImplicitReversePath,
		_shortestPathOnly, mo.ShortestPathsOnly)
}

type pathSpec struct {
	network, name string
}

type data struct {
	model   string
	version int
	options ModelOptions
	apps    appGroup
	infra   infrastructure
	costs   costData
	recs    recsData
	asgmts  []assignment
}
