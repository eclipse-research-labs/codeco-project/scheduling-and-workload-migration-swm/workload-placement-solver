// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"bytes"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/types"
)

type errorCase struct{ input, error string }

func checkParsingErrors(t *testing.T, run func(*parser), cases []errorCase) {
	t.Helper()
	for _, ec := range cases {
		p := makeParser(ec.input)
		require.PanicsWithValue(t, ec.error, func() { run(p) }, ec.input)
	}
}

func TestPNameList(t *testing.T) {
	testCases := []struct {
		input string
		names []string
	}{{"a", []string{"a"}},
		{"a,b", []string{"a", "b"}},
		{`"a","b"`, []string{"a", "b"}},
		{"a,b,c", []string{"a", "b", "c"}}}
	for _, tc := range testCases {
		assert.Equal(t, tc.names, makeParser(tc.input).nameList())
	}

	parseNameList := func(p *parser) { p.nameList() }
	checkParsingErrors(t, parseNameList, []errorCase{
		{"", `<input>:1:0: expected: identifier`},
		{"a,17", `<input>:1:3: expected: identifier`}})

	assert.PanicsWithValue(t,
		`<input>:1:1: expected: list with 4 names`,
		func() { makeParser(`a,b,c`).nameList(4) })
	assert.PanicsWithValue(t,
		`<input>:1:1: expected: list with one name`,
		func() { makeParser(`a,b,c`).nameList(1) })
}

func TestPNode(t *testing.T) {
	check := func(input string, used int) {
		p := makeParser(input)
		p.node()
		assert.Equal(t, EOF, p.token)

		require.Len(t, p.data.infra.workers, 1)
		n := p.data.infra.workers[0]
		assert.Equal(t, "N", n.name)
		assert.Equal(t, kindCompute, n.kind)
		require.NotNil(t, n.cpu)
		assert.Equal(t, milliCPU(17), n.cpu.Cap)
		assert.Equal(t, milliCPU(used), n.cpu.Used())
		require.NotNil(t, n.ram)
		assert.Equal(t, ramMB(19), n.ram.Cap)
		assert.Equal(t, ramMB(used), n.ram.Used())
		assert.Equal(t, []string{"A"}, n.labels)
	}
	check(`/*node*/ N provides cpu:17 ram:19 labels:(A)`, 0)
	check(`/*node*/ N provides cpu:17 labels:(A) memory:19`, 0)
	check(`/*node*/ N provides ram:19 labels:(A) cpu:17`, 0)

	check(`/*node*/ N provides cpu:17-2 ram:19-2 labels:(A)`, 2)
}

func TestPNode_multiple(t *testing.T) {
	p := makeParser(`/*node*/ N,M provides cpu:17 ram:19 labels:(A)`)
	p.node()
	assert.Equal(t, EOF, p.token)

	workers := p.data.infra.workers
	require.Len(t, workers, 2)
	checkNode := func(n *node, name string) {
		assert.Equal(t, name, n.name)
		assert.Equal(t, kindCompute, n.kind)
		require.NotNil(t, n.cpu)
		assert.Equal(t, milliCPU(17), n.cpu.Cap)
		assert.Zero(t, n.cpu.Used())
		require.NotNil(t, n.ram)
		assert.Equal(t, ramMB(19), n.ram.Cap)
		assert.Zero(t, n.ram.Used())
		assert.Equal(t, []string{"A"}, n.labels)
	}
	checkNode(workers[0], "N")
	checkNode(workers[1], "M")
}

func TestPNode_defaults(t *testing.T) {
	node := func(input string) *node {
		p := makeParser(input)
		p.node()
		assert.Equal(t, EOF, p.token)
		require.Len(t, p.data.infra.workers, 1)
		n := p.data.infra.workers[0]
		assert.Equal(t, "N", n.name)
		assert.Len(t, n.labels, 0)
		return n
	}
	n := node(`/*node*/ N provides cpu:1`)
	assert.Equal(t, kindCompute, n.kind)
	assert.Equal(t, milliCPU(1), n.cpu.Cap)
	assert.Equal(t, ramMB(defaults.RAMNode), n.ram.Cap)

	n = node(`/*node*/ N provides ram:1`)
	assert.Equal(t, kindCompute, n.kind)
	assert.Equal(t, milliCPU(defaults.CPUNode), n.cpu.Cap)
	assert.Equal(t, ramMB(1), n.ram.Cap)

	n = node(`/*node*/ N`)
	assert.Equal(t, kindNetwork, n.kind)
	assert.Equal(t, milliCPU(0), n.cpu.Cap)
	assert.Equal(t, ramMB(0), n.ram.Cap)
}

func TestPNode_errors(t *testing.T) {
	parseNode := func(p *parser) { p.node() }
	checkParsingErrors(t, parseNode, []errorCase{
		{"17", `<input>:1:1: expected: identifier`},
		{"N provides 1", `<input>:1:12: expected: property`},
		{"N provides mtu:", `<input>:1:12: unknown property: "mtu:"`},
		{"N provides labels:(!A)", `<input>:1:20: exclusion not allowed`}})
}

func TestPNetwork(t *testing.T) {
	p := makeParser(
		`/*network*/ N with qos:best-effort nodes(C1) node N1`,
		`/*network*/ N (continued) nodes(C1)`)
	p.data.infra.node("C1", 1000, 8000)
	check := func() {
		p.network()
		require.Len(t, p.data.infra.networks, 1)
		net := p.data.infra.networks[0]
		require.Len(t, net.workers, 1)
		assert.Same(t, p.data.infra.workers[0], net.workers[0])
		require.Len(t, net.switches, 1)
		n := net.switches[0]
		assert.Equal(t, "N1", n.name)
		assert.Equal(t, kindNetwork, n.kind)
		assert.Zero(t, n.cpu.Cap)
		assert.Zero(t, n.ram.Cap)
	}
	check()
	check()
	assert.Equal(t, EOF, p.token)
}

func TestPNetwork_bearer(t *testing.T) {
	p := makeParser( // infrastructure
		`node c1,c2,c3 provides cpu:1000`,
		`network eth node sw`,
		`link c1—sw,c2—sw,c3—sw`,
		`find paths (c1•c2 c1•c3 c2•c3)`,
		`network vlan with bearer:eth`,
		`link c1—sw with lat:17 bw:19`)
	p.infrastructure()
	require.Equal(t, EOF, p.token)
	require.Len(t, p.data.infra.networks, 2)
	vlan := p.data.infra.networks[1]
	require.Len(t, vlan.media, 6)
	require.Len(t, vlan.links, 6)
	assert.Equal(t, latency(17), vlan.links[0].lat)
	assert.Equal(t, bandwidth(19), vlan.media[0].bw.Cap)
	require.Len(t, vlan.paths, 6)

	p = makeParser( // infrastructure
		`node c1,c2,c3 provides cpu:1000`,
		`network eth node sw`,
		`link c1—sw,c2—sw,c3—sw`,
		`find paths (c1•c2 c1•c3 c2•c3)`,
		`network vlan with bearer:eth`,
		`nodes (c2 c3)`,
		`link c2—sw with lat:17 bw:19`)
	p.infrastructure()
	require.Equal(t, EOF, p.token)
	require.Len(t, p.data.infra.networks, 2)
	eth, vlan := p.data.infra.networks[0], p.data.infra.networks[1]
	require.Len(t, vlan.media, 6)
	require.Len(t, vlan.links, 4)
	assert.Equal(t, latency(17), vlan.links[0].lat)
	assert.Equal(t, bandwidth(19), vlan.media[2].bw.Cap)
	require.Len(t, vlan.paths, 2)
	assert.Equal(t, eth.paths[4], vlan.paths[0])
	assert.Equal(t, eth.paths[5], vlan.paths[1])
}

func TestPNetwork_bearer_errors(t *testing.T) {
	parse := func(p *parser) {
		opts := defaultNetworkOptions()
		opts.qos.Set(_assured)
		p.data.infra.addNetwork(newNet("?", opts))
		p.infrastructure()
	}
	checkParsingErrors(t, parse, []errorCase{
		{joinLines( // unknown bearer network
			`node c1,c2 provides cpu:1000`,
			`network eth link c1—c2 with path:yes`,
			`network vlan with bearer:X`),
			`<input>:3:27: unknown network: "X"`},
		{joinLines( // cannot add network links
			`node c1,c2 provides cpu:1000`,
			`network eth link c1->c2 with path:yes`,
			`network vlan with bearer:eth`,
			`link c2->c2`),
			`<input>:4:13: unknown link: c2→c2`},
		{joinLines( // cannot add new network nodes
			`node c1,c2 provides cpu:1000`,
			`network eth link c1—c2 with path:yes`,
			`network vlan with bearer:eth`,
			`node sw`),
			`<input>:4:6: illegal node: network has bearer`},
		{joinLines( // cannot change bandwidth in radio network
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio link c1—c2 with path:yes`,
			`network vlan with bearer:wifi`,
			`link c1—c2 with bw:17`),
			`<input>:4:17: illegal property for radio network: "bw:"`},
		{joinLines( // cannot downgrade QoS
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio qos:assured`,
			`link c1—c2 with path:yes`,
			`network vlan with bearer:wifi qos:best-effort`),
			`<input>:4:56: cannot change "qos:" to "best-effort"`},
		{joinLines( // cannot downgrade QoS
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio qos:assured`,
			`link c1—c2 with path:yes`,
			`network vlan with qos:best-effort bearer:wifi`),
			`<input>:4:49: cannot change "qos:" to "best-effort"`},
		{joinLines( // Latency too small
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio lat:1ms`,
			`link c1—c2 with path:yes`,
			`network vlan with bearer:wifi lat:500µs`),
			`<input>:4:45: minimum latency: 1ms`},
		{joinLines( // Latency too small
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio lat:1ms`,
			`link c1—c2 with path:yes`,
			`network vlan with lat:500µs bearer:wifi`),
			`<input>:4:43: minimum latency: 1ms`},
		{joinLines( // Bandwidth too big
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio bw:1Gb`,
			`link c1—c2 with path:yes`,
			`network vlan with bearer:wifi bw:2Gb`),
			`<input>:4:40: maximum bandwidth: 1Gb`},
		{joinLines( // Bandwidth too big
			`node c1,c2 provides cpu:1000`,
			`network wifi with type:radio bw:1Gb`,
			`link c1—c2 with path:yes`,
			`network vlan with bw:2Gb bearer:wifi`),
			`<input>:4:40: maximum bandwidth: 1Gb`}})
}

func TestPNetwork_options(t *testing.T) {
	assert := assert.New(t)
	p := makeParser(
		`/*with*/ mtu:1 qos:assured lat:200µs bw:3`,
		`type:wire links:duplex`)

	mtu := types.NewOption(0)
	mtu.Set(1)
	bearer := types.NewOption("")
	qos := types.NewOption("")
	qos.Set("assured")
	lat := types.NewOption(latency(0))
	two_ms := latency(200 * time.Microsecond)
	lat.Set(two_ms)
	bw := types.NewOption(bandwidth(0))
	bw.Set(3)
	wire := types.NewOption(true)
	wire.Set(true)
	duplex := types.NewOption(true)
	duplex.Set(true)

	assert.Equal(
		networkOptions{
			mtu:       mtu,
			bearer:    bearer,
			qos:       qos,
			latency:   lat,
			bandwidth: bw,
			wire:      wire,
			duplex:    duplex},
		p.netOptions())

	p = makeParser(
		`/*with*/ bearer:X qos:assured latency:200µs bandwidth:3`)
	p.data.infra.addNetwork(newNet("X", defaultNetworkOptions()))
	bearer.Set("X")
	wire = types.NewOption(true)
	duplex = types.NewOption(true)
	assert.Equal(
		networkOptions{
			mtu:       types.NewOption(0),
			bearer:    bearer,
			qos:       qos,
			latency:   lat,
			bandwidth: bw,
			wire:      wire,
			duplex:    duplex},
		p.netOptions())
}

func TestPNetwork_defaults(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	p := makeParser(
		`/*network*/ N node N,M link N->N link M->M with lat:17 bw:19`)
	p.network()
	assert.Equal(EOF, p.token)
	nets := p.data.infra.networks
	require.Len(nets, 1)
	net := nets[0]
	require.Len(net.links, 2)
	link := net.links[0]
	assert.Equal(latency(defaults.LatencyLink), link.lat)
	assert.Equal(bandwidth(defaults.BandwidthLink), net.bandwidthOf(link))
	link = net.links[1]
	assert.Equal(latency(17), link.lat)
	assert.Equal(bandwidth(19), net.bandwidthOf(link))

	p = makeParser(
		`/*network*/ N with lat:23 bw:29`,
		`node N,M link N->N link M->M with lat:17 bw:19`)
	p.network()
	assert.Equal(EOF, p.token)
	nets = p.data.infra.networks
	require.Len(nets, 1)
	net = nets[0]
	require.Len(net.links, 2)
	link = net.links[0]
	assert.Equal(latency(23), link.lat)
	assert.Equal(bandwidth(29), net.bandwidthOf(link))
	link = net.links[1]
	assert.Equal(latency(17), link.lat)
	assert.Equal(bandwidth(19), net.bandwidthOf(link))
}

func TestPNetwork_errors(t *testing.T) {
	parseNetwork := func(p *parser) {
		opts := defaultNetworkOptions()
		opts.qos.Set(_assured)
		p.data.infra.addNetwork(newNet("M", opts))
		p.network()
	}
	checkParsingErrors(t, parseNetwork, []errorCase{
		{"17", `<input>:1:1: expected: identifier`},
		{"N with 1", `<input>:1:8: expected: property`},
		{"N with cpu:", `<input>:1:8: unknown property: "cpu:"`},
		{"N (error", `<input>:1:4: expected: "continued"`},
		{"N (continued,", `<input>:1:13: expected: ")"`},
		// duplicate properties
		{"N with mtu:1 mtu:1",
			`<input>:1:14: duplicate option: "mtu:"`},
		{"N with lat:1 latency:1",
			`<input>:1:14: duplicate option: "latency:"`},
		{"N with bw:1 bandwidth:1",
			`<input>:1:13: duplicate option: "bandwidth:"`},
		{"N with qos:assured qos:assured",
			`<input>:1:20: duplicate option: "qos:"`},
		{"N with type:wire type:wire",
			`<input>:1:18: duplicate option: "type:"`},
		{"N with links:duplex links:duplex",
			`<input>:1:21: duplicate option: "links:"`},
		// exclusive properties (bearer first)
		{"N with bearer:M mtu:1",
			`<input>:1:17: cannot use "mtu:" with "bearer:"`},
		{"N with bearer:M type:wire",
			`<input>:1:17: cannot use "type:" with "bearer:"`},
		{"N with bearer:M links:duplex",
			`<input>:1:17: cannot use "links:" with "bearer:"`},
		// exclusive properties (bearer second)
		{"N with mtu:1 bearer:M",
			`<input>:1:14: cannot use "bearer:" with "mtu:"`},
		{"N with type:wire bearer:M",
			`<input>:1:18: cannot use "bearer:" with "type:"`},
		{"N with links:duplex bearer:M",
			`<input>:1:21: cannot use "bearer:" with "links:"`}})
	p := makeParser(`N M (continued)`)
	require.PanicsWithValue(t,
		`<input>:1:3: unknown network: "M"`,
		func() {
			p.network()
			p.network()
		})
}

func TestPNetwork_errors_findPaths(t *testing.T) {
	parse := func(p *parser) {
		opts := defaultNetworkOptions()
		opts.qos.Set(_assured)
		p.data.infra.addNetwork(newNet("?", opts))
		p.infrastructure()
	}
	checkParsingErrors(t, parse, []errorCase{
		{joinLines( // unknown source node
			`node c1,c2 provides cpu:1000`,
			`network eth link c1—c2 find paths (cx•c2)`),
			`<input>:2:41: unknown node: "cx"`},
		{joinLines( // unknown target node
			`node c1,c2 provides cpu:1000`,
			`network eth link c1->c2 find paths (c1•cx)`),
			`<input>:2:42: unknown node: "cx"`}})
}

func TestNetworkWorkerNodes(t *testing.T) {
	check := func(input string, exp ...string) {
		p := makeParser(input)
		names := p.networkWorkerNodes()
		assert.Equal(t, EOF, p.token)
		assert.Equal(t, exp, names)
	}
	check(`nodes(N)`, "N")
	check(`nodes(N M)`, "N", "M")

	parse := func(p *parser) { p.networkWorkerNodes() }
	checkParsingErrors(t, parse, []errorCase{
		{"nodes n", `<input>:1:7: expected: "("`},
		{"nodes(1", `<input>:1:7: expected: identifier`},
		{"nodes(N", `<input>:1:8: expected: ")"`}})
}

func TestPNetworkNode(t *testing.T) {
	check := func(input string) {
		p := makeParser(input)
		net := &network{}
		p.networkNode(net)
		assert.Equal(t, EOF, p.token)

		require.Len(t, net.switches, 1)
		n := net.switches[0]
		assert.Equal(t, "N", n.name)
		assert.Equal(t, kindNetwork, n.kind)
		assert.Zero(t, n.cpu.Cap)
		assert.Zero(t, n.ram.Cap)
		assert.Equal(t, latency(17), n.lat)
		assert.Equal(t, *types.NewResource[bandwidth](19), n.bw)
		assert.Len(t, n.labels, 0)
	}
	check(`/*node*/ N with lat:17 bw:19`)
	check(`/*node*/ N with bw:19 lat:17`)
}

func TestPNetworkNode_multiple(t *testing.T) {
	p := makeParser(`/*node*/ N,M with lat:17 bw:19`)
	p.node()
	assert.Equal(t, EOF, p.token)

	switches := p.data.infra.workers
	require.Len(t, switches, 2)
	checkNode := func(n *node, name string) {
		assert.Equal(t, name, n.name)
		assert.Equal(t, kindNetwork, n.kind)
		assert.Zero(t, n.cpu.Cap)
		assert.Zero(t, n.ram.Cap)
		assert.Equal(t, latency(17), n.lat)
		assert.Equal(t, *types.NewResource[bandwidth](19), n.bw)
		assert.Len(t, n.labels, 0)
	}
	checkNode(switches[0], "N")
	checkNode(switches[1], "M")
}

func TestPNetworkNode_errors(t *testing.T) {
	parseNode := func(p *parser) { p.networkNode(&network{}) }
	checkParsingErrors(t, parseNode, []errorCase{
		{"17", `<input>:1:1: expected: identifier`},
		{"N with 1", `<input>:1:8: expected: property`},
		{"N with mtu:", `<input>:1:8: unknown property: "mtu:"`}})
}

func TestParserNodePair(t *testing.T) {
	check := func(tok token, ops ...string) {
		for _, op := range ops {
			p := makeParser("N" + op + "M")
			n1, n2, op := p.nodePair(false)
			assert.Equal(t, "N", n1)
			assert.Equal(t, "M", n2)
			assert.Equal(t, tok, op)
		}
	}
	check(CompoundOp, "\u2022")
	check(OneWayOp, "\u2192", "->")
	check(TwoWayOp, "\u2194", "\u2014", "<->", "--")

	parse := func(p *parser) { p.nodePair(false) }
	checkParsingErrors(t, parse, []errorCase{
		{"N 17", `<input>:1:3: expected: direction operator`},
		{"N->1", `<input>:1:4: expected: identifier`}})

	parse = func(p *parser) { p.nodePair(true) }
	checkParsingErrors(t, parse, []errorCase{
		{"N->2", `<input>:1:2: expected: two-way operator`}})
}

func TestLParserLinkSrcDst(t *testing.T) {
	testCases := []struct {
		inputs []string
		srcDst linkSrcDst
	}{ // all inputs of a case produce the same link information
		{[]string{`n:m->o`, `n :m->o`},
			linkSrcDst{"n", "", "m", "o"}},
		{[]string{`n1,n2:m--o`, `n1,n2 :m--o`},
			linkSrcDst{"n1", "n2", "m", "o"}},
		{[]string{`17:m->o`},
			linkSrcDst{"17", "", "m", "o"}},
		{[]string{`17,19:m--o`},
			linkSrcDst{"17", "19", "m", "o"}},
		{[]string{`n:m--o`, `n :m--o`},
			linkSrcDst{"n", "o→m", "m", "o"}},
		{[]string{`m->o`}, linkSrcDst{"m→o", "", "m", "o"}},
		{[]string{`m--o`}, linkSrcDst{"m→o", "o→m", "m", "o"}}}
	net := newNet("net")
	for _, tc := range testCases {
		for _, input := range tc.inputs {
			p := makeParser(input)
			assert.Equal(t, tc.srcDst, p.linkSrcDst(net), input)
		}
	}

	parse := func(p *parser) { p.linkSrcDst(net) }
	checkParsingErrors(t, parse, []errorCase{
		{"1.0", "<input>:1:1: expected: identifier"},
		{"1,2.0", "<input>:1:3: expected: identifier"},
		{"n1,n2:m o", "<input>:1:9: expected: direction operator"},
		{"n1,n2:m->o", "<input>:1:8: expected: two-way operator"}})

	net.bearer, net.links = "eth", []*link{{src: "n1", dst: "n2"}}
	checkParsingErrors(t, parse, []errorCase{
		{"n2->n3", `<input>:1:8: unknown link: n2→n3`},
		{"n1--n2", `<input>:1:8: unknown link: n2→n1`}})
}

func parseLink(t *testing.T, input string) *network {
	p := makeParser(input)
	net := newNet("dummy")
	p.networkLink(net)
	t.Helper()
	assert.Equal(t, EOF, p.token)
	return net
}

func TestPLink_oneWay(t *testing.T) {
	check := func(input string) {
		n := parseLink(t, input)
		ls := n.links
		require.Len(t, ls, 1)
		assert.Equal(t, "N→M", ls[0].name)
		assert.Equal(t, "N", ls[0].src)
		assert.Equal(t, "M", ls[0].dst)
		assert.Equal(t, latency(1), ls[0].lat)
		ma := n.media
		require.Len(t, ma, 1)
		assert.Equal(t, bandwidth(2), ma[0].bw.Cap)
	}
	check(`/*link*/ N->M with lat:1 bw:2`)
	check(`/*link*/ N->M with lat:1 bw:3-1`)
	check(`/*link*/ N->M with bw:2 lat:1`)

	net := parseLink(t, `N->M with lat:1 bw:2 path:yes`)
	assert.Len(t, net.links, 1)
	require.Len(t, net.paths, 1)
	assert.Equal(t, "N→M", net.paths[0].name)
}

func TestPLink_twoWay(t *testing.T) {
	check := func(input string) {
		n := parseLink(t, input)
		ls := n.links
		require.Len(t, ls, 2)
		assert.Equal(t, "N", ls[0].src)
		assert.Equal(t, "M", ls[0].dst)
		assert.Equal(t, latency(1), ls[0].lat)
		ma := n.media
		require.Len(t, ma, 2)
		assert.Equal(t, bandwidth(3), ma[0].bw.Cap)
		assert.Equal(t, "M", ls[1].src)
		assert.Equal(t, "N", ls[1].dst)
		assert.Equal(t, latency(2), ls[1].lat)
		assert.Equal(t, bandwidth(4), ma[1].bw.Cap)
	}
	check(`/*link*/ N--M with lat:1,2 bw:3,4`)
	check(`/*link*/ N--M with bw:4-1,5-1 lat:1,2`)

	net := parseLink(t, `N--M with lat:1 bw:2 path:yes`)
	assert.Len(t, net.links, 2)
	require.Len(t, net.paths, 2)
	assert.Equal(t, "N→M", net.paths[0].name)
	assert.Equal(t, "M→N", net.paths[1].name)
}

func TestPLink_multiple(t *testing.T) {
	n := parseLink(t, `N→M,O→P`)
	require.Len(t, n.links, 2)
	require.Len(t, n.media, 2)
	assert.Equal(t, latency(defaults.LatencyLink), n.links[0].lat)
	assert.Equal(t, bandwidth(defaults.BandwidthLink), n.media[0].bw.Cap)

	n = parseLink(t, `N--M,O--P`)
	assert.Len(t, n.links, 4)
	assert.Len(t, n.media, 4)
}

func TestPLink_errors(t *testing.T) {
	net := &network{options: defaultNetworkOptions()}
	parse := func(p *parser) { p.networkLink(net) }
	checkParsingErrors(t, parse, []errorCase{
		{"17.0", `<input>:1:1: expected: identifier`},
		{"N with", `<input>:1:3: expected: direction operator`},
		{"N--M with 17", `<input>:1:11: expected: property`},
		{"N--M with xxx:", `<input>:1:11: illegal property: "xxx:"`},
		{"N->M with lat:1,2", `<input>:1:16: second latency not allowed`},
		{"N->M with bw:1,2", `<input>:1:15: second bandwidth not allowed`}})

	net.options.wire.Set(false)
	checkParsingErrors(t, parse, []errorCase{
		{"N->M with bw:1",
			`<input>:1:11: illegal property for radio network: "bw:"`}})
}

func TestPLink_bearer(t *testing.T) {
	p := makeParser( // infrastructure
		`node c1,c2 provides cpu:1000`,
		`network eth with lat:1 bw:20 node sw`,
		`link c1->sw link c2->sw with lat:3 bw:4`,
		`network vlan with bearer:eth lat:5 bw:6`,
		`link c2->sw with lat:7 bw:8`)
	p.infrastructure()
	require.Equal(t, EOF, p.token)
	nets := p.data.infra.networks
	require.Len(t, nets, 2)
	eth, vlan := nets[0], nets[1]
	require.Len(t, eth.links, 2)
	assert.Equal(t, latency(1), eth.links[0].lat)
	assert.Equal(t, bandwidth(20), eth.media[0].bw.Cap)
	assert.Equal(t, latency(3), eth.links[1].lat)
	assert.Equal(t, bandwidth(4), eth.media[1].bw.Cap)
	assert.Equal(t, "vlan", vlan.name)
	require.Len(t, vlan.links, 2)
	assert.Equal(t, latency(5), vlan.links[0].lat)
	assert.Equal(t, bandwidth(6), vlan.media[0].bw.Cap)
	assert.Equal(t, latency(7), vlan.links[1].lat)
	assert.Equal(t, bandwidth(8), vlan.media[1].bw.Cap)
}

func TestPNetwork_radio_default(t *testing.T) {
	p := makeParser(`node C network R with type:radio`)
	p.infrastructure()
	assert.Equal(t, EOF, p.token)
	nets := p.data.infra.networks
	require.Len(t, nets, 1)
	assert.Equal(t, types.NewOption(false), nets[0].options.duplex)
	assert.Len(t, nets[0].media, 1)
}

func TestPLink_radio_errors(t *testing.T) {
	checkParsingErrors(t, (*parser).infrastructure, []errorCase{
		{"node C network R with type:radio links:duplex",
			`<input>:1:34: illegal property for radio network: "links:"`},
		{"node C network R with links:duplex type:radio",
			`<input>:1:50: illegal property for radio network: "links:"`},
		{"node C network R with type:radio link C—C with bw:1",
			`<input>:1:48: illegal property for radio network: "bw:"`},
	})
}

func TestParserPathSrcOp(t *testing.T) {
	testCases := []struct {
		inputs []string
		tok    token
		exp    string
	}{ // all inputs of a case produce the same path information
		{[]string{`n:m->`, `n :m->`}, OneWayOp, "n:m"},
		{[]string{`n1,n2:m--`, `n1,n2 :m--`}, TwoWayOp, "n1,n2:m"},
		{[]string{`17:m->`}, OneWayOp, "17:m"},
		{[]string{`17,19:m--`}, TwoWayOp, "17,19:m"},
		{[]string{`n:m--`, `n :m--`}, TwoWayOp, "n:m"},
		{[]string{`m->`}, OneWayOp, "m"},
		{[]string{`m--`}, TwoWayOp, "m"},
		{[]string{"l1,l2"}, Comma, "l1•l2"},
		{[]string{"n:l,"}, Comma, "n:l"},
		{[]string{"1:l,"}, Comma, "1:l"}}
	for _, tc := range testCases {
		for _, input := range tc.inputs {
			var nodes bytes.Buffer
			tok := makeParser(input).pathSrcOp(&nodes)
			assert.Equal(t, tc.tok, tok, input)
			assert.Equal(t, tc.exp, nodes.String(), input)
		}
	}

	parseSrcOp := func(p *parser) {
		var nodes bytes.Buffer
		p.pathSrcOp(&nodes)
	}
	checkParsingErrors(t, parseSrcOp, []errorCase{
		{"n1,n2:m->", "<input>:1:8: expected: two-way operator"},
		{"n1,n2:l,", "<input>:1:8: expected: two-way operator"},
		{"1.0", "<input>:1:1: expected: identifier"},
		{"1,2.0", "<input>:1:3: expected: identifier"},
		{"n1,n2:m ", "<input>:1:8: expected: direction operator or comma"},
		{"n1,n2:m->", "<input>:1:8: expected: two-way operator"},
		{"n1,n2:m,", "<input>:1:8: expected: two-way operator"},
		{"m o", `<input>:1:3: expected: direction operator or comma`}})
}

func TestPPath_oneWay(t *testing.T) {
	check := func(input string, n int) {
		p := makeParser(input)
		net := &network{}
		p.networkPath(net)
		require.Len(t, net.paths, 1)
		require.Len(t, net.paths[0].links, n)
		for i, n := range net.paths[0].links {
			assert.Equal(t, fmt.Sprintf("n%d→n%d", i+1, i+2), n)
		}
	}
	check(`n1->n2`, 1)
	check(`1:n1->n2->n3`, 2)
	check(`p:n1->n2`, 1)
}

func TestPPath_twoWay(t *testing.T) {
	check := func(input string, n int) {
		p := makeParser(input)
		net := &network{}
		p.networkPath(net)
		require.Len(t, net.paths, 2)
		for i, id := range net.paths[0].links {
			assert.Equal(t, fmt.Sprintf("n%d→n%d", i+1, i+2), id)
		}
		for i, id := range net.paths[1].links {
			assert.Equal(t, fmt.Sprintf("n%d→n%d", n-i, n-i-1), id)
		}
	}
	check(`n1--n2`, 2)
	check(`n1--n2--n3`, 3)
}

func TestPPath_links(t *testing.T) {
	check := func(input string, n int) {
		p := makeParser(input)
		net := &network{}
		p.networkPath(net)
		require.Len(t, net.paths, 1)
		require.Len(t, net.paths[0].links, n)
		for i, id := range net.paths[0].links {
			assert.Equal(t, fmt.Sprintf("l%d", i+1), id)
		}
	}
	check(`l1,l2`, 2)
	check(`1:l1,l2,l3`, 3)
	check(`n:l1,l2,l3,l4`, 4)
}

func TestPPath_error(t *testing.T) {
	parsePath := func(p *parser) { p.networkPath(&network{}) }
	checkParsingErrors(t, parsePath, []errorCase{
		{"17.0", `<input>:1:1: expected: identifier`},
		{"N M", `<input>:1:3: expected: direction operator or comma`},
		{"N--17.0", `<input>:1:4: expected: identifier`},
		{"N->M--", `<input>:1:5: two-way operator not allowed`},
		{"N•M--", `<input>:1:4: two-way operator not allowed`},
		{"N--M->", `<input>:1:5: one-way operator not allowed`},
		{"N--M•", `<input>:1:5: one-way operator not allowed`},
		{"l1,l2->", `<input>:1:6: direction operator not allowed`},
		{"l1,l2--", `<input>:1:6: direction operator not allowed`}})
}

func parsePaths(t *testing.T, input string) *network {
	p := makeParser(input)
	net := &network{}
	p.networkPaths(net)
	t.Helper()
	assert.Equal(t, EOF, p.token)
	return net
}

func TestPPaths_nodes(t *testing.T) {
	check := func(input string, n int) {
		ps := parsePaths(t, input).paths
		require.Len(t, ps, n)
		for i, p := range ps {
			for j := 0; j <= i; j++ {
				assert.Equal(t, fmt.Sprintf("n%d→n%d", j+1, j+2), p.links[j])
			}
		}
	}
	check(`/*paths*/ (n1->n2)`, 1)
	check(`/*paths*/ (1:n1->n2)`, 1)
	check(`/*paths*/ (p:n1->n2 n1->n2->n3)`, 2)
}

func TestPPaths_links(t *testing.T) {
	var prefix string
	check := func(input string, n int) {
		ps := parsePaths(t, input).paths
		require.Len(t, ps, n)
		for _, p := range ps {
			for j := 0; j < len(p.links); j++ {
				assert.Equal(t, fmt.Sprintf("%s%d", prefix, j+1), p.links[j])
			}
		}
	}
	prefix = "l"
	check(`/*paths*/ (l1,l2)`, 1)
	check(`/*paths*/ (1:l1,l2)`, 1)
	check(`/*paths*/ (p:l1,l2 l1,l2)`, 2)

	prefix = ""
	check(`/*paths*/ (1,2)`, 1)
	check(`/*paths*/ (1:1,2)`, 1)
	check(`/*paths*/ (p:1,2 1,2)`, 2)
}

func TestPPaths_errors(t *testing.T) {
	parsePaths := func(p *parser) { p.networkPaths(&network{}) }
	checkParsingErrors(t, parsePaths, []errorCase{
		{"17", `<input>:1:1: expected: "("`},
		{"(-", `<input>:1:2: expected: identifier`},
		{"(N->M,", `<input>:1:6: expected: ")"`},
		{"(l1)", `<input>:1:4: expected: direction operator or comma`}})
}

func TestParserFindPaths_defaultOptions(t *testing.T) {
	check := func(shortest bool) {
		p := makeParser(fmt.Sprintf(`
			workload assignment v1
			model set-shortest with shortest-path-only:%t
			application a workload w
			node a,b
			network n
			find paths (a•b)`, shortest))
		p.header()
		p.sections()
		assert.Equal(t, shortest, p.data.options.ShortestPathsOnly)
	}
	check(false)
	check(true)
}

func TestParserFindPaths_errors(t *testing.T) {
	parse := func(p *parser) {
		p.header()
		p.sections()
	}
	model := func(clause string) string {
		return `
			workload assignment v1
			model expected-id
			application a workload w
			node a,b
			network n
			` + clause
	}
	checkParsingErrors(t, parse, []errorCase{
		{model(`find paths (-)`),
			`<input>:7:16: expected: identifier`},
		{model(`find paths (x•b)`),
			`<input>:7:19: unknown node: "x"`},
		{model(`find paths (a•y)`),
			`<input>:7:19: unknown node: "y"`}})
}

func TestPInfrastructure_continuedNetworks(t *testing.T) {
	p := makeParser(
		`node c1,c2 provides cpu:1000`,
		`network N1 link n1--n2 with bw:1 paths (n1•n2)`,
		`network N1 (continued) link n3--n4 with bw:1 paths (n3•n4)`)
	p.infrastructure()
	assert.Len(t, p.data.infra.networks, 1)
}

func TestPInfrastructure_defaults(t *testing.T) {
	p := makeParser(
		`node c1 provides cpu:1000`,
		`node c2 provides cpu:2000`,
		`network N1`)
	p.infrastructure()
	require.Len(t, p.data.infra.networks, 1)
	nodes := p.data.infra.networks[0].workers
	require.Len(t, nodes, 2)
	assert.Equal(t, "c1", nodes[0].name)
	assert.Equal(t, "c2", nodes[1].name)
	assert.Nil(t, p.data.infra.networks[0].switches)
}
