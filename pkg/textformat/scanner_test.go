// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type tokenTestCase struct {
	token        token
	line, column int
	ch           rune
	str          string
}

func TestScannerScan(t *testing.T) {
	scanner := newScannerWithString(
		//        1         2         3
		// 3456789012345678901234567890
		`i1 /* ... */ cpu:`,
		`123 1.2 1.2e3 1.2e-3`,
		`// Single chars and ops`,
		"?+& --<->\u2194\u2013\u2014\u2015 ->\u2192 ",
		"*\u00D7 () ,: !~-\u00AC /\u2022")
	cases := []tokenTestCase{
		{Identifier, 1, 1, ' ', "i1"},
		{Property, 1, 14, '\n', _cpu},
		{Integer, 2, 1, ' ', "123"},
		{Float, 2, 5, ' ', "1.2"},
		{Float, 2, 9, ' ', "1.2e3"},
		{Float, 2, 15, '\n', "1.2e-3"},
		{Illegal, 4, 1, '+', "?"},
		{Illegal, 4, 2, '&', "+"},
		{Illegal, 4, 3, ' ', "&"},
		{TwoWayOp, 4, 5, '<', "--"},
		{TwoWayOp, 4, 7, '\u2194', "<->"},
		{TwoWayOp, 4, 10, '\u2013', "\u2194"},
		{TwoWayOp, 4, 11, '\u2014', "\u2013"},
		{TwoWayOp, 4, 12, '\u2015', "\u2014"},
		{TwoWayOp, 4, 13, ' ', "\u2015"},
		{OneWayOp, 4, 15, '\u2192', "->"},
		{OneWayOp, 4, 17, ' ', "\u2192"},
		{Times, 5, 1, '\u00D7', "*"},
		{Times, 5, 2, ' ', "\u00D7"},
		{LParen, 5, 4, ')', "("},
		{RParen, 5, 5, ' ', ")"},
		{Comma, 5, 7, ':', ","},
		{Colon, 5, 8, ' ', ":"},
		{Not, 5, 10, '~', "!"},
		{Not, 5, 11, '-', "~"},
		{Minus, 5, 12, '\u00AC', "-"},
		{Not, 5, 13, ' ', "\u00AC"},
		{CompoundOp, 5, 15, '\u2022', "/"},
		{CompoundOp, 5, 16, -1, "\u2022"},
		{EOF, 5, 17, -1, ""},
		{EOF, 5, 17, -1, ""}}
	for i, tc := range cases {
		act := tokenTestCase{
			scanner.scan(), scanner.Line, scanner.Column,
			scanner.ch, scanner.token}
		require.Equal(t, tc, act, fmt.Sprintf("%d: %v", i, tc))
	}
}

func TestScannerScan_identifierEndsWithAlnum(t *testing.T) {
	scanner := newScannerWithString("a.-b a-.b- a..b. a-b->c.d a-b--c.d")
	cases := []tokenTestCase{
		{Identifier, 1, 1, ' ', "a.-b"},
		{Identifier, 1, 6, '-', "a-.b"},
		{Minus, 1, 10, ' ', "-"},
		{Identifier, 1, 12, '.', "a..b"},
		{Illegal, 1, 16, ' ', "."},
		{Identifier, 1, 18, '-', "a-b"},
		{OneWayOp, 1, 21, 'c', "->"},
		{Identifier, 1, 23, ' ', "c.d"},
		{Identifier, 1, 27, '-', "a-b"},
		{TwoWayOp, 1, 30, 'c', "--"},
		{Identifier, 1, 32, -1, "c.d"}}
	for i, tc := range cases {
		act := tokenTestCase{
			scanner.scan(), scanner.Line, scanner.Column,
			scanner.ch, scanner.token}
		require.Equal(t, tc, act, fmt.Sprintf("%d: %v", i, tc))
	}
}

func TestScannerScan_infixMinus(t *testing.T) {
	scanner := newScannerWithString("10 - 9 10-9 prop:-10")
	cases := []tokenTestCase{
		{Integer, 1, 1, ' ', "10"},
		{Minus, 1, 4, ' ', "-"},
		{Integer, 1, 6, ' ', "9"},
		{Integer, 1, 8, '-', "10"},
		{Minus, 1, 10, '9', "-"},
		{Integer, 1, 11, ' ', "9"},
		{Property, 1, 13, '-', "prop:"},
		{Minus, 1, 18, '1', "-"},
		{Integer, 1, 19, -1, "10"}}
	for i, tc := range cases {
		act := tokenTestCase{
			scanner.scan(), scanner.Line, scanner.Column,
			scanner.ch, scanner.token}
		require.Equal(t, tc, act, fmt.Sprintf("%d: %v", i, tc))
	}
}

func TestScannerScan_integerWithUnits(t *testing.T) {
	cases := []string{
		"1mCPU",

		"2B", "3b", "4Byte", "5bit", "6KB", "7KiB", "8KiByte", "9Kib",
		"10Kibit", "11MB", "12GB",

		"13s", "14ms", "15us", "16µs", "17ns",
	}
	digits := regexp.MustCompile(`^([0-9]+)`)
	for _, input := range cases {
		scanner := newScannerWithString(input)
		assert.Equal(t, Integer, scanner.scan())
		assert.Equal(t, eof, scanner.ch)
		token := digits.ReplaceAllString(input, "$1 ")
		assert.Equal(t, token, scanner.token)
	}
}

func TestScannerScan_quotedIdentifiers(t *testing.T) {
	testCases := []struct{ input, token string }{
		{`"a"`, "a"},
		{`'a'`, "a"},
		{`“a”`, "a"},
		{`«a»`, "a"},
		{`"a•w—a•v"`, "a•w—a•v"}}
	for _, tc := range testCases {
		scanner := newScannerWithString(tc.input)
		assert.Equal(t, Identifier, scanner.scan(), tc.input)
		assert.Equal(t, eof, scanner.ch, tc.input)
		assert.Equal(t, tc.token, scanner.token, tc.input)
	}
	errorCases := []errorCase{
		{`"ab`, `identifier not terminated`},
		{`"ab'`, `identifier not terminated`},
		{`'ab"`, `identifier not terminated`},
		{`“ab"`, `identifier not terminated`},
		{`«ab"`, `identifier not terminated`},
		{`""`, `empty identifier`}}
	for _, ec := range errorCases {
		assert.PanicsWithValue(t, ec.error,
			func() {
				newScannerWithString(ec.input).scan()
			})
	}
}

func TestJkgBufioScanner(t *testing.T) {
	t.Skip("manual test")
	s, err := newScanner("testdata/textformat/minimal.asgmt")
	require.NoError(t, err)
	n := 100
	for tok := s.scan(); tok != EOF && n >= 0; tok = s.scan() {
		fmt.Printf("%s: %s:%q\n", s.Position, tok, s.token)
		n--
	}
}
