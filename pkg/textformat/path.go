// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import "regexp"

type nodeInfo struct {
	index map[string]int
	links [][]int
}

func nodeInfos(workers []*node, net *network) nodeInfo {
	nodes, links := append(workers, net.switches...), net.links
	ni := nodeInfo{
		index: make(map[string]int, len(nodes)),
		links: make([][]int, len(nodes))}
	for i := range nodes {
		ni.index[nodes[i].name] = i
	}
	for i, l := range links {
		src := l.src
		srcI, ok := ni.index[src]
		if !ok {
			continue
		}
		if _, ok := ni.index[l.dst]; !ok {
			continue
		}
		ni.links[srcI] = append(ni.links[srcI], i)
		ls := ni.links[srcI]
		needsSwap := func(j int) bool {
			x := ni.index[links[ls[j-1]].dst]
			y := ni.index[links[ls[j]].dst]
			return x > y
		}
		for k := len(ls) - 1; k > 0 && needsSwap(k); k-- {
			ls[k-1], ls[k] = ls[k], ls[k-1]
		}
	}
	return ni
}

type searchState struct {
	nodes     []*node
	links     []*link
	allPaths  bool
	nodeInfos nodeInfo
}

func newSearchState(nodes []*node, net *network, allPaths bool) searchState {
	s := searchState{
		nodes:     nodes,
		links:     net.links,
		allPaths:  allPaths,
		nodeInfos: nodeInfos(nodes, net)}
	return s
}

func (s *searchState) findLink(src, dst int) *link {
	for _, li := range s.nodeInfos.links[src] {
		l := s.links[li]
		if n, ok := s.nodeInfos.index[l.dst]; ok && n == dst {
			return l
		}
	}
	panic("link not found")
}

func (s *searchState) makePath(node int, parents []int) *path {
	ls := make([]string, len(parents))
	for i := 0; i < len(parents)-1; i++ {
		ls[i] = s.findLink(parents[i], parents[i+1]).name
	}
	ls[len(ls)-1] = s.findLink(parents[len(parents)-1], node).name
	return linksToPath(ls)
}

func contains(a []int, x int) bool {
	for _, n := range a {
		if n == x {
			return true
		}
	}
	return false
}

func (s *searchState) pathsBetween(src, dst string) (ps []*path) {
	srcIdx, ok := s.nodeInfos.index[src]
	if !ok {
		return
	}
	dstIdx, ok := s.nodeInfos.index[dst]
	if !ok {
		return
	}
	q := make(queue, 0, len(s.nodes))
	q.push(srcIdx, []int{})
	for !q.isEmpty() {
		qeNode, qeParents := q.pop()
		if qeNode == dstIdx {
			ps = append(ps, s.makePath(qeNode, qeParents))
			if s.allPaths {
				continue
			}
			break
		}
		for _, l := range s.nodeInfos.links[qeNode] {
			dst, ok := s.nodeInfos.index[s.links[l].dst]
			if !ok || dst == qeNode {
				continue
			}
			if !contains(qeParents, dst) {
				parents := append(qeParents, qeNode)
				q.push(dst, parents)
			}
		}
	}
	return
}

var fromTo = regexp.MustCompile(`^([\w.-]+)(?:•|->)([\w.-]+)$`)

func findPaths(s searchState, specs []string) []*path {
	ps := make([]*path, 0, len(specs))
	for _, spec := range specs {
		if m := fromTo.FindStringSubmatch(spec); m != nil {
			ps = append(ps, s.pathsBetween(m[1], m[2])...)
		}
	}
	if len(ps) == 0 {
		return nil
	}
	return ps
}

func FindShortestPaths(im *infrastructure, specs ...string) []*path {
	return findPaths(
		newSearchState(im.workers, im.networks[0], false), specs)
}

func FindAllPaths(im *infrastructure, specs ...string) []*path {
	return findPaths(
		newSearchState(im.workers, im.networks[0], true), specs)
}

var linkNodes = regexp.MustCompile(`^([\w.-]+)(?:->|•)([\w.-]+)$`)

func findPathsBidi(s searchState, specs []string) []*path {
	ps := make([]*path, 0, len(specs))
	for _, spec := range specs {
		if m := linkNodes.FindStringSubmatch(spec); m != nil {
			ps = append(ps, s.pathsBetween(m[1], m[2])...)
			ps = append(ps, s.pathsBetween(m[2], m[1])...)
		}
	}
	if len(ps) == 0 {
		return nil
	}
	return ps
}

func FindShortestPathsBidi(im *infrastructure, specs ...string) []*path {
	return findPathsBidi(
		newSearchState(im.workers, im.networks[0], false), specs)
}

func FindAllPathsBidi(im *infrastructure, specs ...string) []*path {
	return findPathsBidi(
		newSearchState(im.workers, im.networks[0], true), specs)
}

func isReverseLink(x, y string) bool {
	if x := reLinkName.FindStringSubmatch(x); x != nil {
		if y := reLinkName.FindStringSubmatch(y); y != nil {
			return x[3] == y[5] && x[5] == y[3]
		}
	}
	return false
}

func isReverse(p, q *path) bool {
	if p == nil {
		return q == nil || len(q.links) == 0
	} else if q == nil {
		return len(p.links) == 0
	}
	if l := len(p.links) - 1; l == len(q.links)-1 {
		for i, n := range p.links {
			if !isReverseLink(n, q.links[l-i]) {
				return false
			}
		}
		return true
	}
	return false
}

func bidiPaths(to, fro []*path) []*path {
	j := 0
	for i := range to {
		p := to[i]
		for k := range fro {
			if isReverse(fro[k], p) {
				to[j], fro[k] = p, nil
				j++
				break
			}
		}
	}
	if j == 0 {
		return nil
	}
	return to[:j]
}

func findBidiPaths(s searchState, specs []string) []*path {
	ps := make([]*path, 0, len(specs))
	for _, spec := range specs {
		if m := linkNodes.FindStringSubmatch(spec); m != nil {
			ps = append(ps, bidiPaths(
				s.pathsBetween(m[1], m[2]),
				s.pathsBetween(m[2], m[1]))...)
		}
	}
	if len(ps) == 0 {
		return nil
	}
	return ps
}

func FindShortestBidiPaths(im *infrastructure, specs ...string) []*path {
	return findBidiPaths(
		newSearchState(im.workers, im.networks[0], false), specs)
}

func FindAllBidiPaths(im *infrastructure, specs ...string) []*path {
	return findBidiPaths(
		newSearchState(im.workers, im.networks[0], true), specs)
}

func FindNetworkPaths(
	nodes []*node, net *network, kind string,
	implicitReversePath bool, specs ...string,
) []*path {
	proc := findPathsBidi
	if implicitReversePath {
		proc = findBidiPaths
	}
	return proc(
		newSearchState(nodes, net, kind == "all"),
		specs)
}
