// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
)

func TestNodeValuesAsGrpc(t *testing.T) {
	data := nodeValues{
		nodeValue{"a", 1},
		nodeValue{"b", 2},
		nodeValue{"c", 3}}
	exp := []*grpc.NodeValue{
		{Node: "a", Value: 1},
		{Node: "b", Value: 2},
		{Node: "c", Value: 3}}
	assert.Equal(t, exp, (nodeValues).asGrpc(data))
}

func parseNodeInfos(t *testing.T, input ...string) []nodeValue {
	p := makeParser(input...)
	is := p.nodeValues()
	t.Helper()
	assert.Equal(t, EOF, p.token)
	return is
}

func TestPNodeInfos(t *testing.T) {
	check := func(input string, exp []nodeValue) {
		assert.Equal(t, exp, parseNodeInfos(t, input))
	}
	check("(N:1)", []nodeValue{{"N", 1}})
	check("(N : 1)", []nodeValue{{"N", 1}})
	check("(N:1 O:2)", []nodeValue{{"N", 1}, {"O", 2}})

	parseInfos := func(p *parser) { p.nodeValues() }
	checkParsingErrors(t, parseInfos, []errorCase{
		{"", `<input>:1:0: expected: "("`},
		{"(17", "<input>:1:2: expected: identifier"},
		{"(N,", `<input>:1:3: expected ":"`},
		{"(N:M", `<input>:1:4: expected: float`},
		{"(N:5,", `<input>:1:5: expected: ")"`}})
}

func parseWlCosts(t *testing.T, input ...string) costData {
	p := makeParser(input...)
	p.costs()
	t.Helper()
	assert.Equal(t, EOF, p.token)
	return p.data.costs
}

func TestPWlInfoCosts(t *testing.T) {
	aw := workloadSpec{app: "A", name: "W"}
	nvN := []nodeValue{{"N", 1}}
	nvNM := []nodeValue{{"N", 1}, {"M", 2}}
	assert.Equal(t, costData{}, parseWlCosts(t, ""))
	assert.Equal(t,
		costData{nodes: nvN, workloads: []wlCosts{}},
		parseWlCosts(t, "costs nodes (N:1)"))
	assert.Equal(t,
		costData{nodes: nvN, workloads: []wlCosts{{wl: aw, nodes: nvN}}},
		parseWlCosts(t, "costs nodes (N:1) workload A•W (N:1)"))
	assert.Equal(t,
		costData{workloads: []wlCosts{{wl: aw, nodes: nvN}}},
		parseWlCosts(t, "costs workload A•W (N:1)"))
	assert.Equal(t,
		costData{workloads: []wlCosts{{wl: aw, nodes: nvNM}}},
		parseWlCosts(t, "costs workload A•W (N:1 M:2)"))

	checkParsingErrors(t, (*parser).costs, []errorCase{
		{"costs dummy", `<input>:1:7: expected: "nodes" or "workload"`},
		{"costs workload 17", `<input>:1:16: expected: identifier`},
		{"costs workload A•17", `<input>:1:18: expected: identifier`},
		{"costs workload A•W N", `<input>:1:20: expected: "("`}})
}

func parseWlRecs(t *testing.T, input ...string) recsData {
	p := makeParser(input...)
	p.recommendations()
	t.Helper()
	assert.Equal(t, EOF, p.token)
	return p.data.recs
}

func TestPWlInfoRecs(t *testing.T) {
	aw := workloadSpec{app: "A", name: "W"}
	nvN := []nodeValue{{"N", 1}}
	assert.Equal(t, recsData{}, parseWlRecs(t, ""))
	assert.Equal(t,
		recsData{workloads: []wlRecs{{wl: aw, nodes: nvN}}},
		parseWlRecs(t, "recommendations workload A•W (N:1)"))

	checkParsingErrors(t, (*parser).recommendations, []errorCase{
		{"recommendations dummy", `<input>:1:17: expected: "workload"`},
		{"recommendations workload 17", "<input>:1:26: expected: identifier"},
		{"recommendations workload A•17", "<input>:1:28: expected: identifier"},
		{"recommendations workload A•W N", `<input>:1:30: expected: "("`}})
}

func TestWlInfoRecs_notAllowed(t *testing.T) {
	parseInfos := func(p *parser) { p.costs(); p.recommendations() }
	checkParsingErrors(t, parseInfos, []errorCase{
		{"costs nodes (N:1)\nrecommendations",
			`<input>:2:1: recommendations not allowed`},
		{"costs workload A•W (N:1)\nrecommendations",
			`<input>:2:1: recommendations not allowed`}})
}

func TestWlRecDataMgmt(t *testing.T) {
	rs := parseWlRecs(t, `
		recommendations
		workload A•W (N:1 M:2)
		workload B•V (M:3 N:4 O:5)`)
	require.Len(t, rs.workloads, 2)
	assert.Equal(t,
		&grpc.Recommendation_Workload{
			Application: "A", Workload: "W",
			Nodes: []*grpc.NodeValue{
				{Node: "N", Value: 1}, {Node: "M", Value: 2}}},
		rs.workloads[0].asGrpc())
	assert.Equal(t,
		&grpc.Recommendation_Workload{
			Application: "B", Workload: "V",
			Nodes: []*grpc.NodeValue{
				{Node: "M", Value: 3}, {Node: "N", Value: 4},
				{Node: "O", Value: 5}}},
		rs.workloads[1].asGrpc())
}

func TestRecDataAsGrpc(t *testing.T) {
	rd := recsData{}
	assert.Nil(t, rd.asGrpc())

	rd = recsData{workloads: []wlRecs{
		{wl: workloadSpec{app: "A", name: "W1"}},
		{wl: workloadSpec{app: "A", name: "W2"}}}}
	assert.Nil(t, rd.asGrpc())

	rd = recsData{workloads: []wlRecs{
		{wl: workloadSpec{app: "A", name: "W1"}},
		{wl: workloadSpec{app: "A", name: "W2"},
			nodes: []nodeValue{{node: "X", value: 1}}}}}
	rg := rd.asGrpc()
	require.Len(t, rg.Workloads, 1)
	w := rg.Workloads[0]
	require.Len(t, w.Nodes, 1)
	assert.Equal(t, "X", w.Nodes[0].Node)
}
