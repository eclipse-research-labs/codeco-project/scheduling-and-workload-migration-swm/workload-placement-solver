// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

func (p *parser) workloadAsgmt() {
	wl := p.workloadRef("")
	p.expect(_on)
	asgmt := &p.data.asgmts[len(p.data.asgmts)-1]
	asgmt.Workload(wl.app + "•" + wl.name + ":" + p.expectId())
}

func (p *parser) channelAsgmt() {
	var twoWay bool
	var to, fro channelAsgmtProps
	if p.token == Integer {
		to.name = p.scanner.token
		p.next()
		p.expect(_on)
		to.path = p.pathRef()
	} else {
		pos := p.scanner.Position
		wl1 := p.expectId()
		if p.token == CompoundOp {
			p.next()
			wl1 += "•" + p.expectId()
			if p.token != TwoWayOp && p.token != OneWayOp {
				p.syntaxError("expected: directional operator")
			}
			twoWay = p.token == TwoWayOp
			p.next()
			wl := p.workloadRef("")
			wl2 := wl.app + "•" + wl.name
			to.name, fro.name = wl1+"→"+wl2, wl2+"→"+wl1
		} else if p.token == TwoWayOp || p.token == OneWayOp {
			p.scanner.Position = pos
			p.syntaxError("expected: qualified name")
		} else {
			to.name = wl1
		}
		p.expect(_on)
		to.path = p.pathRef()
		if p.token == Comma {
			if !twoWay {
				p.syntaxError("second path ref not allowed")
			}
			p.next()
			fro.path = p.pathRef()
		} else if twoWay {
			p.syntaxError("expected: second path ref")
		}
	}
	asgmt := &p.data.asgmts[len(p.data.asgmts)-1]
	asgmt.ChannelWithProps(&to)
	if twoWay {
		asgmt.ChannelWithProps(&fro)
	}
}

func (p *parser) assignment() {
	p.expect("workload")
	p.data.asgmts = append(p.data.asgmts, assignment{})
	p.workloadAsgmt()
	for p.hasWord("workload") {
		p.workloadAsgmt()
	}
	for p.hasWord("channel") {
		p.channelAsgmt()
	}
}

func (p *parser) assignments() {
	p.hasWord("assignments")
	for p.hasWord("assignment") {
		p.assignment()
	}
}
