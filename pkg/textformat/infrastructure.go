// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"bytes"
	"fmt"
	"strings"
	ts "text/scanner"
	"time"

	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/types"
)

func (p *parser) doNameList(expect func(*parser) string, n ...int) []string {
	pos := p.scanner.Position
	names := []string{expect(p)}
	for p.hasToken(Comma) {
		names = append(names, expect(p))
	}
	if len(n) == 1 && len(names) != n[0] {
		p.scanner.Position = pos
		if n[0] == 1 {
			p.syntaxError("expected: list with one name")
		} else {
			p.syntaxError("expected: list with %d names", n[0])
		}
	}
	return names
}

func (p *parser) nameList(n ...int) []string {
	return p.doNameList((*parser).expectId, n...)
}

func (p *parser) node() {
	names, labels := p.nameList(), []string{}
	if !p.hasWord("provides") {
		latency, bandwidth := p.communicationProperties()
		for _, name := range names {
			p.data.infra.networkNode(name, latency, bandwidth.Available())
		}
		return
	}
	var cpuUsed milliCPU
	var ramUsed ramMB
	cpu, ram := milliCPU(defaults.CPUNode), ramMB(defaults.RAMNode)
	if p.token != Property {
		p.syntaxError("expected: property")
	}
	for p.token == Property {
		switch strings.ToLower(p.scanner.token) {
		case _cpu:
			p.next()
			cpu = p.getCPU()
			if p.hasToken(Minus) {
				cpuUsed = p.getCPU()
			}
		case _ram, _memory:
			p.next()
			ram = p.getRAM()
			if p.hasToken(Minus) {
				ramUsed = p.getRAM()
			}
		case _labels:
			p.next()
			labels = p.getLabels(false)
		default:
			p.syntaxError("unknown property: %q", p.scanner.token)
		}
	}
	for _, name := range names {
		p.data.infra.node(name, cpu, ram, labels...)
		n := p.data.infra.workers[len(p.data.infra.workers)-1]
		n.cpu.Use(milliCPU(cpuUsed))
		n.ram.Use(ramMB(ramUsed))
	}
}

func (p *parser) netOptions() networkOptions {
	if p.token != Property {
		p.syntaxError("expected: property")
	}
	opts := defaultNetworkOptions()
	normalize := func(prop string) string {
		prop = strings.ToLower(prop)
		if prop == _lat {
			prop = _latency
		} else if prop == _bw {
			prop = _bandwidth
		}
		return prop
	}
	barfIfDuplicate := func(seen bool, what string) {
		if seen {
			p.syntaxError("duplicate option: %q", what)
		}
	}
	var bearer *network
	barfIfWithBearer := func(propSet bool, prop string, pos ...ts.Position) {
		if propSet && opts.bearer.WasSet() {
			if len(pos) == 1 {
				p.scanner.Position = pos[0]
			}
			if bearer != nil {
				p.syntaxError("cannot use %q with %q", prop, _bearer)
			} else {
				p.syntaxError("cannot use %q with %q", _bearer, prop)
			}
		}
	}
	barfIfQosDowngrade := func() {
		if bearer == nil || !opts.qos.WasSet() {
			return
		}
		if bearer.options.qos.Value() == _assured &&
			opts.qos.Value() == _bestEffort {
			p.syntaxError("cannot change %q to %q", _qos, _bestEffort)
		}
	}
	barfIfLatencyToSmall := func() {
		if bearer == nil || !opts.latency.WasSet() {
			return
		}
		if opts.latency.Value() < bearer.options.latency.Value() {
			p.syntaxError("minimum latency: %s",
				time.Duration(bearer.options.latency.Value()))
		}
	}
	barfIfBandwidthTooBig := func() {
		if bearer == nil || !opts.bandwidth.WasSet() {
			return
		}
		if bearer.options.bandwidth.Value() < opts.bandwidth.Value() {
			p.syntaxError("maximum bandwidth: %s",
				types.Bandwidth(bearer.options.bandwidth.Value()))
		}
	}
	for p.token == Property {
		prop := normalize(p.scanner.token)
		switch prop {
		case _mtu:
			barfIfDuplicate(opts.mtu.WasSet(), prop)
			barfIfWithBearer(true, _mtu)
			p.next()
			opts.mtu.Set(int(p.getRAM()))
		case _bearer:
			barfIfDuplicate(opts.bearer.WasSet(), prop)
			pos := p.scanner.Position
			p.next()
			opts.bearer.Set(p.expectId())
			barfIfWithBearer(opts.mtu.WasSet(), _mtu, pos)
			barfIfWithBearer(opts.wire.WasSet(), _type, pos)
			barfIfWithBearer(opts.duplex.WasSet(), _links, pos)
			bearer = p.data.infra.findNetwork(opts.bearer.Value())
			if bearer == nil {
				p.syntaxError("unknown network: %q", opts.bearer.Value())
			}
			barfIfQosDowngrade()
			barfIfLatencyToSmall()
			barfIfBandwidthTooBig()
		case _qos:
			barfIfDuplicate(opts.qos.WasSet(), prop)
			opts.qos.Set(p.getQoS())
			barfIfQosDowngrade()
		case _latency:
			barfIfDuplicate(opts.latency.WasSet(), prop)
			opts.latency.Set(p.getLatency())
			barfIfLatencyToSmall()
		case _bandwidth:
			barfIfDuplicate(opts.bandwidth.WasSet(), prop)
			opts.bandwidth.Set(p.getBandwidth())
			barfIfBandwidthTooBig()
		case _type:
			barfIfDuplicate(opts.wire.WasSet(), prop)
			barfIfWithBearer(true, _type)
			p.next()
			opts.wire.Set(
				p.expectOneOf("network type", _wire, _radio) == _wire)
			if !opts.wire.Value() {
				if opts.duplex.WasSet() {
					p.notForRadio(opts.wire, _links)
				}
				opts.duplex = types.NewOption(false)
			}
		case _links:
			barfIfDuplicate(opts.duplex.WasSet(), prop)
			barfIfWithBearer(true, _links)
			p.notForRadio(opts.wire, _links)
			p.next()
			opts.duplex.Set(
				p.expectOneOf("link type", _duplex, _halfDuplex) == _duplex)
		default:
			p.syntaxError("unknown property: %q", p.scanner.token)
		}
	}
	return opts
}

func (p *parser) networkWorkerNodes() []string {
	names := []string{}
	if p.hasWord("nodes") {
		p.expect("(", LParen)
		if p.token != Identifier {
			p.syntaxError("expected: identifier")
		}
		for i := 0; p.token == Identifier; i++ {
			names = append(names, p.expectId())
		}
		p.expect(")", RParen)
	}
	return names
}

func (p *parser) getBandwidthTerm() (b types.Resource[bandwidth]) {
	b.Cap = p.getBandwidth()
	if p.token == Minus {
		b.Load(p.getBandwidth())
	}
	return
}

func (p *parser) communicationProperties() (latency, types.Resource[bandwidth]) {
	var lat latency
	var bw types.Resource[bandwidth]
	if p.hasWord("with") {
		if p.token != Property {
			p.syntaxError("expected: property")
		}
		for p.token == Property {
			prop := strings.ToLower(p.scanner.token)
			switch prop {
			case _lat, _latency:
				lat = p.getLatency()
			case _bw, _bandwidth:
				bw = p.getBandwidthTerm()
			default:
				p.syntaxError("unknown property: %q", p.scanner.token)
			}
		}
	}
	return lat, bw
}

func (p *parser) networkNode(net *network) {
	names := p.nameList()
	latency, bandwidth := p.communicationProperties()
	for _, name := range names {
		net.switches = append(net.switches,
			makeNetworkNode(name, latency, bandwidth.Available()))
	}
}

func (p *parser) nodePair(twoWayOnly bool) (from string, to string, op token) {
	from = p.expectId()
	switch p.token {
	case CompoundOp, OneWayOp:
		if twoWayOnly {
			p.syntaxError("expected: two-way operator")
		}
		fallthrough
	case TwoWayOp:
		op = p.token
	default:
		p.syntaxError("expected: direction operator")
	}
	p.next()
	to = p.expectId()
	return
}

type linkSrcDst struct {
	n1, n2   string
	src, dst string
}

func (l *linkSrcDst) twoWay() bool {
	return l.n2 != ""
}

func (p *parser) linkSrcDst(net *network) (sd linkSrcDst) {
	var op token
	name, twoWay := p.propIntOrID(), false
	switch p.token {
	case Colon, Property: // single name
		sd.n1 = name
		p.next()
		sd.src, sd.dst, op = p.nodePair(false)
		twoWay = op == TwoWayOp
	case Comma: // two names
		p.next()
		twoWay, sd.n1, sd.n2 = true, name, p.propIntOrID()
		if p.token == Property {
			p.next()
		} else {
			p.expect(":", Colon)
		}
		sd.src, sd.dst, _ = p.nodePair(twoWay)
	case CompoundOp, TwoWayOp, OneWayOp: // node name
		twoWay = p.token == TwoWayOp
		p.next()
		sd.src, sd.dst = name, p.expectId()
	default:
		p.syntaxError("expected: direction operator")
	}
	if sd.n1 == "" {
		sd.n1 = LinkName(sd.src, sd.dst)
	}
	if twoWay && sd.n2 == "" {
		sd.n2 = LinkName(sd.dst, sd.src)
	}
	if net.bearer != "" {
		if net.findLink(sd.src, sd.dst) == nil {
			p.syntaxError("unknown link: %s", sd.n1)
		}
		if twoWay && net.findLink(sd.dst, sd.src) == nil {
			p.syntaxError("unknown link: %s", sd.n2)
		}
	}
	return
}

func (p *parser) notForRadio(wire types.Option[bool], prop string) {
	if !wire.Value() {
		p.syntaxError(`illegal property for radio network: %q`, prop)
	}
}

func (p *parser) networkLink(net *network) {
	sds := []linkSrcDst{p.linkSrcDst(net)}
	twoWay := sds[0].twoWay()
	for p.hasToken(Comma) {
		sds = append(sds, p.linkSrcDst(net))
		twoWay = twoWay || sds[len(sds)-1].twoWay()
	}
	to, fro := net.defaultLinkProps()
	if p.hasWord("with") {
		if p.token != Property {
			p.syntaxError("expected: property")
		}
		for p.token == Property {
			prop := strings.ToLower(p.scanner.token)
			switch prop {
			case _lat, _latency:
				to.lat = latency(p.getLatency())
				fro.lat = to.lat
				if p.token == Comma {
					if !twoWay {
						p.syntaxError("second latency not allowed")
					}
					fro.lat = latency(p.getLatency())
				}
			case _bw, _bandwidth:
				p.notForRadio(net.options.wire, prop)
				to.bw = p.getBandwidthTerm()
				fro.bw = to.bw
				if p.token == Comma {
					if !twoWay {
						p.syntaxError("second bandwidth not allowed")
					}
					fro.bw = p.getBandwidthTerm()
				}
			case _path:
				p.next()
				p.expectOneOf("path flag", _yes, _true)
				to.path, fro.path = true, true
			default:
				p.syntaxError("illegal property: %q", p.scanner.token)
			}
		}
	}
	for _, sd := range sds {
		to.name, to.src, to.dst = sd.n1, sd.src, sd.dst
		net.linkWithProps(&to)
		if sd.twoWay() {
			fro.name, fro.src, fro.dst = sd.n2, sd.dst, sd.src
			net.linkWithProps(&fro)
		}
	}
}

func (p *parser) findPaths(net *network) {
	what, found := p.hasOneOf(_all, _shortest)
	if !found {
		if p.data.options.ShortestPathsOnly {
			what = _shortest
		} else {
			what = _all
		}
	}
	pairs := []string{}
	p.expect("paths")
	p.expect("(", LParen)
	if p.token != Identifier {
		p.syntaxError("expected: identifier")
	}
	for p.token == Identifier {
		from, to, _ := p.nodePair(false)
		if p.data.infra.findWorker(from) == nil {
			p.syntaxError(`unknown node: %q`, from)
		}
		if p.data.infra.findWorker(to) == nil {
			p.syntaxError(`unknown node: %q`, to)
		}
		pairs = append(pairs, from+"•"+to)
	}
	p.expect(")", RParen)
	nodes := append(net.workers, net.switches...)
	net.paths = append(net.paths,
		FindNetworkPaths(nodes, net, what,
			p.data.options.ImplicitReversePath, pairs...)...)
}

func (p *parser) pathSrcOp(spec *bytes.Buffer) token {
	name, twoWay := p.propIntOrID(), false
	switch p.token {
	case Colon, Property:
		p.next()
		fmt.Fprintf(spec, "%s:%s", name, p.expectIdOrInt())
	case Comma:
		p.next()
		if name2 := p.propIntOrID(); p.token == Property {
			p.next()
			fmt.Fprintf(spec, "%s,%s:%s", name, name2, p.expectIdOrInt())
			twoWay = true
		} else if p.hasToken(Colon) {
			fmt.Fprintf(spec, "%s,%s:%s", name, name2, p.expectIdOrInt())
			twoWay = true
		} else {
			fmt.Fprintf(spec, "%s•%s", name, name2)
			return Comma
		}
	case CompoundOp, TwoWayOp, OneWayOp:
		spec.WriteString(name)
	}
	t := p.token
	if t != CompoundOp && t != TwoWayOp && t != OneWayOp && t != Comma {
		p.syntaxError("expected: direction operator or comma")
	} else if twoWay && (t == CompoundOp || t == OneWayOp || t == Comma) {
		p.syntaxError("expected: two-way operator")
	}
	return t
}

func (p *parser) networkPath(net *network) {
	var spec bytes.Buffer
	tok := p.pathSrcOp(&spec)
	for p.hasToken(tok) || tok == CompoundOp && p.hasToken(OneWayOp) {
		spec.WriteRune('•')
		spec.WriteString(p.expectIdOrInt())
	}
	if tok == Comma {
		if p.token == OneWayOp || p.token == CompoundOp || p.token == TwoWayOp {
			p.syntaxError("direction operator not allowed")
		}
		net.linkPathFromString(spec.String())
	} else if tok == OneWayOp || tok == CompoundOp {
		if p.token == TwoWayOp {
			p.syntaxError("two-way operator not allowed")
		}
		net.pathFromString(spec.String())
	} else {
		if p.token == OneWayOp || p.token == CompoundOp {
			p.syntaxError("one-way operator not allowed")
		}
		net.bidiPathFromString(spec.String())
	}
}

func (p *parser) networkPaths(net *network) {
	p.expect("(", LParen)
	if p.token != Identifier && p.token != Property && p.token != Integer {
		p.syntaxError("expected: identifier")
	}
	for p.token == Identifier || p.token == Property || p.token == Integer {
		p.networkPath(net)
	}
	p.expect(")", RParen)
}

func (p *parser) networkWithBearer(net *network) {
	filterWorkers(net, p.networkWorkerNodes())
	for stop := false; !stop; {
		switch {
		case p.hasWord("node"):
			p.syntaxError(`illegal node: network has bearer`)
		case p.hasWord("link"):
			p.networkLink(net)
		default:
			stop = true
		}
	}
}

func (p *parser) networkWithoutBearer(net *network) {
	addWorkers(net, p.data.infra.workers, p.networkWorkerNodes())
	for stop := false; !stop; {
		switch {
		case p.hasWord("node"):
			p.networkNode(net)
		case p.hasWord("link"):
			p.networkLink(net)
		default:
			stop = true
		}
	}
	for stop := false; !stop; {
		switch {
		case p.hasWord("find"):
			p.findPaths(net)
		case p.hasWord("paths"):
			p.networkPaths(net)
		default:
			stop = true
		}
	}
}

func (p *parser) network() {
	add, pos := true, p.scanner.Position
	var net *network
	name, opts := p.expectId(), defaultNetworkOptions()
	if p.hasWord("with") {
		opts = p.netOptions()
		if b := p.data.infra.findNetwork(opts.bearer.Value()); b != nil {
			net = networkFromBearer(name, opts, b)
		}
	} else if p.hasToken(LParen) {
		p.expect("continued")
		p.expect(")", RParen)
		net, add = p.data.infra.findNetwork(name), false
		if net == nil {
			p.scanner.Position = pos
			p.syntaxError("unknown network: %q", name)
		}
	}
	if net == nil {
		net = newNet(name, opts)
	}
	if opts.bearer.WasSet() {
		p.networkWithBearer(net)
	} else {
		p.networkWithoutBearer(net)
	}
	if add {
		p.data.infra.addNetwork(net)
	}
}

func (p *parser) infrastructure() {
	p.hasWord("infrastructure")
	p.expect("node")
	p.node()
	for stop := false; !stop; {
		switch {
		case p.hasWord("node"):
			p.node()
		case p.hasWord("network"):
			p.network()
		default:
			stop = true
		}
	}
}
