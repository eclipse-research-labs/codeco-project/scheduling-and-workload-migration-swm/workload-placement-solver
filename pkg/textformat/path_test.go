// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"math/rand"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func parseInfrastructure(text string) *infrastructure {
	var p parser
	p.initWithString(`
		workload assignment v1
		model infrastructure
		application dummy workload w
		infrastructure` +
		text)
	p.scanner.Filename = "infra-test"
	if err := p.parse(); err != nil {
		panic(err.Error())
	}
	return &p.data.infra
}

func fourNodesModel(op string) *infrastructure {
	text := `
		node C1,C2,C3,C4 provides cpu:1000 ram:8000
		network net
		node N1,N2,N3,N4
		link C1--N1,C1--C4,N1--N2,N1--N3,N2--C2,
			 N2--N4,N3--N4,N4--C3,C2--C3
			 with lat:1 bw:100`
	return parseInfrastructure(strings.ReplaceAll(text, "--", op))
}

func linkList(model *infrastructure, specs ...string) []int {
	ls := make([]int, len(specs))
	for i, spec := range specs {
		ps := strings.Split(spec, "•")
		ls[i] = model.findLink(ps[0], ps[1])
	}
	return ls
}

func TestNodeInfos(t *testing.T) {
	model := fourNodesModel("→")
	assert.Equal(t,
		nodeInfo{
			index: map[string]int{
				"C1": 0, "C2": 1, "C3": 2, "C4": 3,
				"N1": 4, "N2": 5, "N3": 6, "N4": 7},
			links: [][]int{
				linkList(model, "C1•C4", "C1•N1"),
				linkList(model, "C2•C3"),
				nil,
				nil,
				linkList(model, "N1•N2", "N1•N3"),
				linkList(model, "N2•C2", "N2•N4"),
				linkList(model, "N3•N4"),
				linkList(model, "N4•C3")}},
		nodeInfos(model.workers, model.networks[0]))

	model = parseInfrastructure(`
		node C1,C2 provides cpu:100 ram:800
		network net
		link C1•C2,Cx•C2,C1•Cx with lat:1 bw:100`)
	assert.Equal(t,
		nodeInfo{
			index: map[string]int{"C1": 0, "C2": 1},
			links: [][]int{
				linkList(model, "C1•C2"),
				nil}},
		nodeInfos(model.workers, model.networks[0]))
}

func TestNodeInfos_shuffle(t *testing.T) {
	model := fourNodesModel("—")
	for i := 0; i < 10; i++ {
		ls := model.networks[0].links
		rand.Shuffle(len(ls), func(i, j int) {
			ls[i], ls[j] = ls[j], ls[i]
		})
		assert.Equal(t,
			nodeInfo{
				index: map[string]int{
					"C1": 0, "C2": 1, "C3": 2, "C4": 3,
					"N1": 4, "N2": 5, "N3": 6, "N4": 7},
				links: [][]int{
					linkList(model, "C1•C4", "C1•N1"),
					linkList(model, "C2•C3", "C2•N2"),
					linkList(model, "C3•C2", "C3•N4"),
					linkList(model, "C4•C1"),
					linkList(model, "N1•C1", "N1•N2", "N1•N3"),
					linkList(model, "N2•C2", "N2•N1", "N2•N4"),
					linkList(model, "N3•N1", "N3•N4"),
					linkList(model, "N4•C3", "N4•N2", "N4•N3")}},
			nodeInfos(model.workers, model.networks[0]),
			"round %d", i)
	}
}

func TestNewSearchState(t *testing.T) {
	im := fourNodesModel("→")
	assert.Equal(t,
		searchState{
			nodes:     im.workers,
			links:     im.networks[0].links,
			allPaths:  true,
			nodeInfos: nodeInfos(im.workers, im.networks[0])},
		newSearchState(im.workers, im.networks[0], true))
}

func TestPathsBetween(t *testing.T) {
	assert := assert.New(t)
	model := fourNodesModel("→")
	s := newSearchState(model.workers, model.networks[0], true)
	assert.Equal(
		pathList("C1•N1•N2•C2"),
		s.pathsBetween("C1", "C2"))
	assert.Equal(
		pathList("C1•C4"),
		s.pathsBetween("C1", "C4"))

	assert.Equal(pathList(), s.pathsBetween("Cx", "C4"))
	assert.Equal(pathList(), s.pathsBetween("C1", "Cx"))
	model.networks[0].links[0].dst = "Nx"
	assert.Equal(pathList(), s.pathsBetween("C1", "C2"))
}

func modelC2N4Bidi() *infrastructure {
	return parseInfrastructure(`
		node C1,C2 provides cpu:100 ram:8000
		network net
		node N1,N2,N3,N4
		link C1—N1,C1—N2,N1—N3,N1—N4,N2—N3,N2—N4,N3—C2,N4—C2
			 with lat:1 bw:100`)
}

func TestPathsBetweenForModelC2N4Bidi(t *testing.T) {
	im := modelC2N4Bidi()
	s := newSearchState(im.workers, im.networks[0], true)
	assert.Equal(t,
		pathList(
			"C1•N1•N3•C2",
			"C1•N1•N4•C2",
			"C1•N2•N3•C2",
			"C1•N2•N4•C2",
			"C1•N1•N3•N2•N4•C2",
			"C1•N1•N4•N2•N3•C2",
			"C1•N2•N3•N1•N4•C2",
			"C1•N2•N4•N1•N3•C2"),
		s.pathsBetween("C1", "C2"))
	assert.Equal(t,
		pathList(
			"C2•N3•N1•C1",
			"C2•N3•N2•C1",
			"C2•N4•N1•C1",
			"C2•N4•N2•C1",
			"C2•N3•N1•N4•N2•C1",
			"C2•N3•N2•N4•N1•C1",
			"C2•N4•N1•N3•N2•C1",
			"C2•N4•N2•N3•N1•C1"),
		s.pathsBetween("C2", "C1"))
}

func TestFindShortestPathsBidiForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	pathsC1C2 := pathList(
		"C1•N1•N3•C2",
		"C2•N3•N1•C1")
	assert.Equal(t,
		pathsC1C2,
		FindShortestPaths(model, "C1•C2", "C2•C1"))
	assert.Equal(t,
		pathList(),
		FindShortestPaths(model, "C1::C2"))

	assert.Equal(t,
		pathsC1C2,
		FindShortestPathsBidi(model, "C1•C2"))
	assert.Equal(t,
		pathList(),
		FindShortestPathsBidi(model, "C1::C2"))
}

func TestFindAllPathsBidiForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	pathsC1C2 := pathList(
		"C1•N1•N3•C2",
		"C1•N1•N4•C2",
		"C1•N2•N3•C2",
		"C1•N2•N4•C2",
		"C1•N1•N3•N2•N4•C2",
		"C1•N1•N4•N2•N3•C2",
		"C1•N2•N3•N1•N4•C2",
		"C1•N2•N4•N1•N3•C2",
		"C2•N3•N1•C1",
		"C2•N3•N2•C1",
		"C2•N4•N1•C1",
		"C2•N4•N2•C1",
		"C2•N3•N1•N4•N2•C1",
		"C2•N3•N2•N4•N1•C1",
		"C2•N4•N1•N3•N2•C1",
		"C2•N4•N2•N3•N1•C1")
	assert.Equal(t,
		pathsC1C2,
		FindAllPaths(model, "C1•C2", "C2•C1"))
	assert.Equal(t,
		pathList(),
		FindAllPaths(model, "C1::C2"))

	assert.Equal(t,
		pathsC1C2,
		FindAllPathsBidi(model, "C1•C2"))
	assert.Equal(t,
		pathList(),
		FindAllPathsBidi(model, "C1::C2"))
}

func TestIsReverseLink(t *testing.T) {
	assert.True(t, isReverseLink("n2->n1", "n1->n2"))
	assert.True(t, isReverseLink("n2->n1", "n1→n2"))
	assert.True(t, isReverseLink("n2→n1", "n1->n2"))
	assert.True(t, isReverseLink("n2→n1", "n1→n2"))
	assert.False(t, isReverseLink("x→a", "a→b"))
	assert.False(t, isReverseLink("b→x", "a→b"))
	assert.False(t, isReverseLink("b→a", "a"))
	assert.False(t, isReverseLink("b", "a→b"))
}

func TestIsReverse(t *testing.T) {
	p1 := &path{links: []string{}}
	p2 := &path{links: []string{"A->B", "B->C"}}
	p3 := &path{links: []string{"C->B", "B->A"}}
	p4 := &path{links: []string{"C->B", "X->A"}}
	assert.True(t, isReverse(nil, nil))
	assert.True(t, isReverse(nil, p1))
	assert.True(t, isReverse(p1, nil))
	assert.True(t, isReverse(p1, p1))
	assert.True(t, isReverse(p2, p3))
	assert.False(t, isReverse(nil, p2))
	assert.False(t, isReverse(p2, nil))
	assert.False(t, isReverse(p2, p4))
	assert.False(t, isReverse(p1, p2))
}

func TestBidiPaths(t *testing.T) {
	assert.Equal(t,
		pathList("A•N1•C", "A•N2•C"),
		bidiPaths(
			pathList("A•N1•B", "A•N2•B", "A•N1•C", "A•N2•C"),
			pathList("C•N1•A", "C•N2•A")))
	assert.Equal(t,
		pathList(),
		bidiPaths(
			pathList("A•N1•B", "A•N2•B"),
			pathList()))
}

func TestFindShortestBidiPathsForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	assert.Equal(t,
		pathList("C1•N1•N3•C2"),
		FindShortestBidiPaths(model, "C1•C2"))
	assert.Equal(t,
		pathList(),
		FindShortestBidiPaths(model, "C1::C2"))
}

func TestFindAllBidiPathsForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	assert.Equal(t,
		pathList(
			"C1•N1•N3•C2",
			"C1•N1•N4•C2",
			"C1•N2•N3•C2",
			"C1•N2•N4•C2",
			"C1•N1•N3•N2•N4•C2",
			"C1•N1•N4•N2•N3•C2",
			"C1•N2•N3•N1•N4•C2",
			"C1•N2•N4•N1•N3•C2"),
		FindAllBidiPaths(model, "C1•C2"))
	assert.Equal(t,
		pathList(),
		FindAllBidiPaths(model, "C1::C2"))
}
