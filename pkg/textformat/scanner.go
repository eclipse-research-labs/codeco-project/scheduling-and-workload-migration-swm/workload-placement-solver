// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
	ts "text/scanner"
	"unicode"
	"unicode/utf8"
)

type tokenBuilder struct {
	bytes.Buffer
	runes int
}

func (b *tokenBuilder) WriteRune(r rune) (n int, err error) {
	if n, err = b.Buffer.WriteRune(r); err == nil {
		b.runes++
	}
	return
}

func (b *tokenBuilder) WriteString(s string) (n int, err error) {
	if n, err = b.Buffer.WriteString(s); err == nil {
		b.runes += utf8.RuneCountInString(s)
	}
	return
}

func (b *tokenBuilder) add(r rune) {
	b.WriteRune(r)
}

func (b *tokenBuilder) Reset() {
	b.Buffer.Reset()
	b.runes = 0
}

type scanner struct {
	r       *strings.Reader
	ch      rune // current character
	line    int  // line count
	column  int  // character count
	builder tokenBuilder
	token   string
	ts.Position
}

type token int

const (
	EOF token = -(iota + 1)
	Illegal
	Identifier
	Property
	Integer
	Float
	TwoWayOp
	OneWayOp
	CompoundOp
	Times
	Not
	LParen
	RParen
	Comma
	Minus
	Colon
)

var tokenNames = []string{"EOF", "Illegal", "Char", "Identifier", "Property",
	"Integer", "Float", "TwoWayOp", "OneWayOp", "CompoundOp", "Times",
	"Not", "LParen", "RParen", "Comma", "Minus", "Colon"}

func (t token) String() string {
	x := int(-t) - 1
	if 0 <= x && x < len(tokenNames) {
		return tokenNames[x]
	}
	return fmt.Sprintf("token(%d)", t)
}

const eof rune = rune(-1)

func (s *scanner) init(input string) *scanner {
	s.r = strings.NewReader(input)
	s.line = 1
	s.column = 0
	s.Line = 0
	s.ch = s.read()
	return s
}

func newScanner(name string) (*scanner, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	var s scanner
	s.Filename = name
	return s.init(string(data)), nil
}

func joinLines(lines ...string) string {
	return strings.Join(lines, "\n")
}

func newScannerWithString(input ...string) *scanner {
	var s scanner
	s.Filename = "<input>"
	return s.init(joinLines(input...))
}

func (s *scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		ch = eof
	} else if ch == '\n' {
		s.line++
		s.column = 0
	} else {
		s.column++
	}
	s.ch = ch
	return ch
}

func isLetter(ch rune) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z'
}

func isDigit(ch rune) bool {
	return '0' <= ch && ch <= '9'
}

func (s *scanner) addCh() rune {
	s.builder.add(s.ch)
	return s.read()
}

func (s *scanner) skipWhiteSpace() {
	for unicode.IsSpace(s.ch) {
		s.ch = s.read()
	}
}

func (s *scanner) skipComment() {
	prv := s.ch
	ch := s.read()
	if prv == '/' {
		for ch != '\n' && ch != eof {
			ch = s.read()
		}
		return
	}
	for {
		if ch == eof {
			panic("comment not terminated")
		}
		prv, ch = ch, s.read()
		if prv == '*' && ch == '/' {
			break
		}
	}
	s.ch = s.read()
}

func (s *scanner) scanIdent() token {
	s.addCh()
	for s.ch != eof {
		switch {
		case isLetter(s.ch) || isDigit(s.ch):
			s.addCh()
		case s.ch == '.' || s.ch == '-':
			var cs bytes.Buffer
			cs.WriteRune(s.ch)
			ch, r0, c0 := rune(0), s.ch, s.column
			for ch = s.read(); ch == '.' || ch == '-'; ch = s.read() {
				if r0 == '-' && ch == '-' && cs.Len() == 1 {
					break
				}
				cs.WriteRune(ch)
			}
			if isLetter(ch) || isDigit(ch) {
				s.builder.WriteString(cs.String())
				s.ch = ch
			} else {
				s.r.Seek(int64(-cs.Len()), io.SeekCurrent)
				s.ch, s.column = r0, c0
				return Identifier
			}
		default:
			if s.ch == ':' {
				s.addCh()
				return Property
			}
			return Identifier
		}
	}
	return Identifier
}

func (s *scanner) scanQIdent() token {
	start := s.ch
	if start == '\u201C' {
		start = '\u201D'
	}
	if start == '\u00AB' {
		start = '\u00BB'
	}
	if s.read() == start {
		panic("empty identifier")
	}
	for {
		ch := s.addCh()
		if ch == start || ch == eof {
			break
		}
	}
	if s.ch == eof {
		panic("identifier not terminated")
	}
	s.read()
	return Identifier
}

func (s *scanner) scanNumber() token {
	ch, t := s.addCh(), Integer
	for isDigit(ch) {
		ch = s.addCh()
	}
	if ch == '.' {
		ch, t = s.addCh(), Float
		for isDigit(ch) {
			ch = s.addCh()
		}
	}
	if ch == 'e' || ch == 'E' {
		if ch, t = s.addCh(), Float; ch == '-' {
			ch = s.addCh()
		}
		for isDigit(ch) {
			ch = s.addCh()
		}
	}
	if t == Integer && (isLetter(ch) || ch == '\u00B5') {
		s.builder.add(' ')
		ch = s.addCh()
		for isLetter(ch) || ch == '\u00B5' {
			ch = s.addCh()
		}
	}
	s.ch = ch
	return t
}

func (s *scanner) scan() token {
	defer func() {
		s.token = s.builder.String()
		if s.ch == eof {
			s.column += s.builder.runes
		}
	}()
redo:
	s.builder.Reset()
	s.skipWhiteSpace()
	s.Line, s.Column = s.line, s.column
	switch {
	case s.ch == eof:
		return EOF
	case isLetter(s.ch):
		return s.scanIdent()
	case isDigit(s.ch):
		return s.scanNumber()
	default:
		switch s.ch {
		case '/':
			if ch := s.addCh(); ch == '/' || ch == '*' {
				s.skipComment()
				goto redo
			}
			return CompoundOp
		case 0x2194, 0x2013, 0x2014, 0x2015:
			s.addCh()
			return TwoWayOp
		case 0x2192:
			s.addCh()
			return OneWayOp
		case '-':
			if ch := s.addCh(); ch == '-' {
				s.ch = s.addCh()
				return TwoWayOp
			} else if ch == '>' {
				s.ch = s.addCh()
				return OneWayOp
			} else {
				s.ch = ch
				return Minus
			}
		case '<':
			ch := s.addCh()
			if ch != '-' {
				return Illegal
			}
			ch = s.addCh()
			s.ch = s.addCh()
			if ch == '>' {
				return TwoWayOp
			} else {
				return Illegal
			}
		case '(':
			s.ch = s.addCh()
			return LParen
		case ')':
			s.ch = s.addCh()
			return RParen
		case '*', 0x00D7:
			s.ch = s.addCh()
			return Times
		case 0x00AC, '!', '~':
			s.ch = s.addCh()
			return Not
		case 0x2022:
			s.ch = s.addCh()
			return CompoundOp
		case ',':
			s.ch = s.addCh()
			return Comma
		case ':':
			s.ch = s.addCh()
			return Colon
		case '"', '\'', '\u201C', '\u00AB':
			return s.scanQIdent()
		default:
			s.ch = s.addCh()
			return Illegal
		}
	}
}
