// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"fmt"
	"regexp"
	"strings"

	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
)

type nodeKind uint8

const (
	kindCompute nodeKind = 0
	kindNetwork nodeKind = 1
)

type milliCPU int32
type ramMB int64
type latency int64
type bandwidth int64

type node struct {
	name   string
	kind   nodeKind
	labels []string
	// kindCompute
	cpu types.Resource[milliCPU]
	ram types.Resource[ramMB]
	// kindNetwork
	lat latency
	bw  types.Resource[bandwidth]
}

func (n *node) copy() *node {
	c := *n
	return &c
}

func (n *node) asGrpc() *grpc.Node {
	g := &grpc.Node{Id: n.name}
	switch n.kind {
	case kindCompute:
		g.NodeType = grpc.Node_COMPUTE
		if len(n.labels) > 0 {
			g.Labels = n.labels
		}
		g.CPU = &grpc.Resource{
			Capacity: int64(n.cpu.Cap), Used: int64(n.cpu.Used())}
		g.Memory = &grpc.Resource{
			Capacity: int64(n.ram.Cap), Used: int64(n.ram.Used())}
	case kindNetwork:
		g.NodeType = grpc.Node_NETWORK
		g.Latency = int64(n.lat)
		g.Bandwidth = &grpc.Resource{
			Capacity: int64(n.bw.Cap), Used: int64(n.bw.Used())}
	}
	return g
}

type linkProps struct {
	name     string
	src, dst string
	wire     bool
	path     bool
	lat      latency
	bw       types.Resource[bandwidth]
}

type link struct {
	name   string
	src    string
	dst    string
	medium string
	lat    latency
}

var namedLink = regexp.MustCompile(
	`^(?:([\w.-]+|[0-9]+)(?:,([\w.-]+|[0-9]+))?:)?([\w.-]+)(?:->|•)([\w.-]+)$`)

func splitLinkSpec(spec string) (bool, string, string, string, string) {
	if m := namedLink.FindStringSubmatch(spec); m != nil {
		return true, m[1], m[2], m[3], m[4]
	}
	return false, "", "", "", ""
}

func (l *link) asGrpc() *grpc.Network_Link {
	return &grpc.Network_Link{
		Id: l.name, Source: l.src, Target: l.dst, Medium: l.medium,
		Latency: int64(l.lat)}
}

type path struct {
	name  string
	links []string
}

func (p *path) asGrpc() *grpc.Network_Path {
	return &grpc.Network_Path{Id: p.name, Links: p.links}
}

type medium struct {
	name string
	bw   types.Resource[bandwidth]
}

func (m *medium) copy() *medium {
	c := *m
	return &c
}

func (m *medium) asGrpc() *grpc.Medium {
	return &grpc.Medium{
		Id: m.name,
		Bandwidth: &grpc.Resource{
			Capacity: int64(m.bw.Cap), Used: int64(m.bw.Used())}}
}

type networkOptions struct {
	mtu       types.Option[int]
	bearer    types.Option[string]
	qos       types.Option[string]
	latency   types.Option[latency]
	bandwidth types.Option[bandwidth]
	wire      types.Option[bool]
	duplex    types.Option[bool]
}

func defaultNetworkOptions() networkOptions {
	return networkOptions{
		qos:       types.NewOption(defaults.NetworkQoS),
		latency:   types.NewOption(latency(defaults.LatencyLink)),
		bandwidth: types.NewOption(bandwidth(defaults.BandwidthLink)),
		wire:      types.NewOption(defaults.NetworkType == "wire"),
		duplex:    types.NewOption(defaults.NetworkLinks == "duplex")}
}

type network struct {
	name     string
	options  networkOptions
	serials  map[string]int
	workers  []*node
	switches []*node
	links    []*link
	paths    []*path
	media    []*medium
	bearer   string
}

func newNet(name string, opts ...networkOptions) *network {
	n := &network{name: name, options: defaultNetworkOptions()}
	if len(opts) > 0 {
		n.options = opts[0]
	}
	if !n.options.wire.Value() {
		bw := bandwidth(defaults.BandwidthLink)
		if n.options.bandwidth.WasSet() {
			bw = n.options.bandwidth.Value()
		}
		n.medium(name, bw)
	}
	return n
}

func makeNetworkNode(name string, lat latency, bw bandwidth) *node {
	node := &node{name: name, kind: kindNetwork, lat: lat}
	if bw > 0 {
		node.bw = *types.NewResource[bandwidth](bw)
	}
	return node
}

func (n *network) medium(owner string, cap bandwidth) *network {
	var mn string
	if owner != "" {
		mn = owner + "\u2098"
	} else {
		mn = n.uniqueName("M")
	}
	n.media = append(n.media, &medium{
		name: mn,
		bw:   *types.NewResource[bandwidth](cap)})
	return n
}

func (n *network) defaultLinkProps() (linkProps, linkProps) {
	to := linkProps{lat: latency(n.options.latency.Value()),
		bw: types.Resource[bandwidth]{Cap: n.options.bandwidth.Value()}}
	fro := to
	if n.options.wire.Value() {
		to.wire, fro.wire = true, n.options.duplex.Value()
	}
	return to, fro
}

func (n *network) uniqueName(pre string) string {
	if n.serials == nil {
		n.serials = map[string]int{"L": 0, "M": 0, "P": 0}
	}
	s := n.serials[pre]
	s++
	n.serials[pre] = s
	return fmt.Sprintf("%s%d", pre, s)
}

func (n *network) linkName(src, dst string) string {
	for _, l := range n.links {
		if l.src == src && l.dst == dst {
			return l.name
		}
	}
	return LinkName(src, dst)
}

func (n *network) nodesToLinks(nodes string) (ls []string) {
	if ns := strings.Split(nodes, "•"); len(ns) > 1 {
		ls = make([]string, len(ns)-1)
		for i := 1; i < len(ns); i++ {
			ls[i-1] = n.linkName(ns[i-1], ns[i])
		}
	}
	return
}

func (n *network) linkWithProps(props *linkProps) *network {
	var l *link
	if n.bearer != "" {
		l = n.findLink(props.src, props.dst)
		if l == nil {
			panic(fmt.Errorf("link %s→%s not found", props.src, props.dst))
		}
		l.lat = props.lat
		if props.wire {
			m := n.findMedium(l.medium)
			if m == nil {
				panic(fmt.Errorf("medium %s not found", l.medium))
			}
			m.bw.Cap = bandwidth(props.bw.Cap)
		}
	} else {
		name := props.name
		if name == "" {
			name = LinkName(props.src, props.dst)
		}
		l = &link{
			name: name,
			src:  props.src, dst: props.dst,
			lat: props.lat}
		if props.wire {
			n.medium(l.name, props.bw.Available())
		}
		l.medium = n.media[len(n.media)-1].name
		n.links = append(n.links, l)
	}
	if props.path && n.bearer == "" {
		n.path("", l.name)
	}
	return n
}

var reLinkName *regexp.Regexp = regexp.MustCompile(
	`^(?:([[:alnum:].-]+)(?:,([[:alnum:].-]+))?:)?` +
		`([[:alnum:]](?:[[:alnum:].-]*[[:alnum:]])?)` +
		`(->|→)` +
		`([[:alnum:]](?:[[:alnum:].-]*[[:alnum:]])?)$`)

func pathNameFromLinks(links []string) string {
	if len(links) == 0 {
		return "path"
	}
	var src, dst string
	if m := reLinkName.FindStringSubmatch(links[0]); m != nil {
		src = m[3]
	}
	if m := reLinkName.FindStringSubmatch(links[len(links)-1]); m != nil {
		dst = m[5]
	}
	if src != "" && dst != "" {
		return PathName(src, dst, len(links))
	}
	if len(links) == 1 {
		return links[0]
	}
	var lc string
	if len(links) > 2 {
		lc = itoaSup(len(links) - 1)
	}
	return fmt.Sprintf("%s∘%s%s", links[0], lc, links[len(links)-1])
}

func linksToPath(links []string) (p *path) {
	if len(links) > 0 {
		p = &path{name: pathNameFromLinks(links), links: links}
	}
	return
}

func (n *network) path(name string, links ...string) *network {
	if p := linksToPath(links); p != nil {
		if name != "" {
			p.name = name
		}
		n.paths = append(n.paths, p)
	}
	return n
}

var rePathSpec *regexp.Regexp = regexp.MustCompile(
	`^(?:([^,]+)(?:,([^:]+))?:)?(.+)$`)

func (n *network) pathFromString(spec string) *network {
	if m := rePathSpec.FindStringSubmatch(spec); m != nil {
		if ls := n.nodesToLinks(m[3]); ls != nil {
			return n.path(m[1], ls...)
		}
	}
	return n
}

func reverse(src []string) []string {
	srcLen := len(src)
	dst := make([]string, srcLen)
	for i := range src {
		dst[srcLen-1-i] = src[i]
	}
	return dst
}

func reverseLink(l string) string {
	if m := reLinkName.FindStringSubmatch(l); m != nil {
		return m[5] + m[4] + m[3]
	}
	return l
}

func reverseLinks(links []string) []string {
	links = reverse(links)
	for i := range links {
		links[i] = reverseLink(links[i])
	}
	return links
}

func (n *network) bidiPathFromString(spec string) *network {
	if m := rePathSpec.FindStringSubmatch(spec); m != nil {
		if ls := n.nodesToLinks(m[3]); ls != nil {
			return n.path(m[1], ls...).path(m[2], reverseLinks(ls)...)
		}
	}
	return n
}

func (n *network) linkPathFromString(spec string) *network {
	if m := rePathSpec.FindStringSubmatch(spec); m != nil {
		if ls := strings.Split(m[3], "•"); len(ls) > 0 {
			return n.path(m[1], ls...)
		}
	}
	return n
}

func pathList(specs ...string) []*path {
	if len(specs) == 0 {
		return nil
	}
	ps, net := make([]*path, len(specs)), &network{}
	for i, spec := range specs {
		ps[i] = linksToPath(net.nodesToLinks(spec))
	}
	return ps
}

func qosToServiceClass(qos string) grpc.ServiceClass {
	qos = strings.ReplaceAll(strings.ToUpper(qos), "-", "_")
	if sc, ok := grpc.ServiceClass_value[qos]; ok {
		return grpc.ServiceClass(sc)
	}
	return grpc.ServiceClass_BEST_EFFORT
}

func (n *network) asGrpc() *grpc.Network {
	g := &grpc.Network{
		Id:     n.name,
		Bearer: n.bearer,
		Qos:    qosToServiceClass(n.options.qos.Value())}
	if N := len(n.links); N > 0 {
		g.Links = make([]*grpc.Network_Link, N)
		for i, l := range n.links {
			g.Links[i] = l.asGrpc()
		}
	}
	if N := len(n.paths); N > 0 {
		g.Paths = make([]*grpc.Network_Path, N)
		for i, p := range n.paths {
			g.Paths[i] = p.asGrpc()
		}
	}
	if N := len(n.media); N > 0 {
		g.Media = make([]*grpc.Medium, N)
		for i, p := range n.media {
			g.Media[i] = p.asGrpc()
		}
	}
	return g
}

func (n *network) findLink(src, dst string) *link {
	for _, l := range n.links {
		if l.src == src && l.dst == dst {
			return l
		}
	}
	return nil
}

func (n *network) findMedium(name string) *medium {
	if n != nil {
		for _, medium := range n.media {
			if medium.name == name {
				return medium
			}
		}
	}
	return nil
}

func (n *network) bandwidthOf(link *link) (bw bandwidth) {
	if link != nil {
		if m := n.findMedium(link.medium); m != nil {
			bw = m.bw.Cap
		}
	}
	return
}

func pathIndex(i int) string {
	return "\u207D" + itoaSup(i) + "\u207E"
}

func (n *network) makePathNamesUnique() {
	cache := make(map[string][]int, len(n.paths))
	for i, p := range n.paths {
		ns, ok := cache[p.name]
		if !ok {
			ns = make([]int, 0, 4)
		}
		cache[p.name] = append(ns, i)
	}
	for id, ns := range cache {
		if len(ns) < 2 {
			continue
		}
		for i := range ns {
			n.paths[ns[i]].name = id + pathIndex(i+1)
		}
	}
}

type infrastructure struct {
	workers  []*node
	networks []*network
}

func (i *infrastructure) findWorker(name string) *node {
	for _, n := range i.workers {
		if n.name == name {
			return n
		}
	}
	return nil
}

func (im *infrastructure) findLink(src, dst string) int {
	for _, nw := range im.networks {
		for i, l := range nw.links {
			if l.src == src && l.dst == dst {
				return i
			}
		}
	}
	return -1
}

func (i *infrastructure) findNetwork(name string) *network {
	for _, n := range i.networks {
		if n.name == name {
			return n
		}
	}
	return nil
}

func (i *infrastructure) asGrpc() *grpc.Infrastructure {
	g := &grpc.Infrastructure{}
	N := len(i.workers)
	for _, n := range i.networks {
		if n.bearer == "" {
			N += len(n.switches)
		}
	}
	if N > 0 {
		g.Nodes = make([]*grpc.Node, N)
		p := 0
		add := func(ns []*node) {
			for _, n := range ns {
				g.Nodes[p], p = n.asGrpc(), p+1
			}
		}
		add(i.workers)
		for _, n := range i.networks {
			if n.bearer == "" {
				add(n.switches)
			}
		}
	}
	if N = len(i.networks); N > 0 {
		g.Networks = make([]*grpc.Network, N)
		for j, n := range i.networks {
			g.Networks[j] = n.asGrpc()
		}
	}
	return g
}

func (i *infrastructure) node(
	name string, cpu milliCPU, ram ramMB, labels ...string,
) *infrastructure {
	n := &node{
		name: name,
		kind: kindCompute,
		cpu:  *types.NewResource[milliCPU](cpu),
		ram:  *types.NewResource[ramMB](ram)}
	if len(labels) > 0 {
		n.labels = labels
	}
	i.addNode(n)
	return i
}

func (i *infrastructure) networkNode(
	name string, lat latency, bw bandwidth,
) *infrastructure {
	i.workers = append(i.workers, makeNetworkNode(name, lat, bw))
	return i
}

func (i *infrastructure) addNode(n *node) {
	i.workers = append(i.workers, n)
}

func (i *infrastructure) addNetwork(n *network) {
	i.networks = append(i.networks, n)
}
