// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/types"
)

func TestModelOptionsString(t *testing.T) {
	assert.Equal(t,
		"compound-names:false implicit-reverse-path:false shortest-path-only:false",
		ModelOptions{false, false, false}.String())
	assert.Equal(t,
		"compound-names:true implicit-reverse-path:true shortest-path-only:true",
		ModelOptions{true, true, true}.String())
}

func TestMember(t *testing.T) {
	assert := assert.New(t)
	assert.True(member("eins", []string{"eins", "zwei", "drei"}))
	assert.False(member("eins", []string{}))
	assert.False(member("vier", []string{"eins", "zwei", "drei"}))
}

func TestFilterWorkers(t *testing.T) {
	p := makeParser( // infrastructure
		`node c1,c2,c3 provides cpu:1000`,
		`network eth node sw`,
		`link c1—sw,c2—sw,c3—sw`,
		`find paths (c1•c2 c1•c3 c2•c3)`)
	p.infrastructure()
	require.Equal(t, EOF, p.token)
	nets := p.data.infra.networks
	require.Len(t, nets, 1)
	n0 := nets[0]
	opts := defaultNetworkOptions()
	nc := networkFromBearer("vlan", opts, n0)
	filterWorkers(nc, []string{})
	assert.Equal(t,
		&network{name: "vlan",
			bearer:   n0.name,
			options:  n0.options,
			workers:  n0.workers,
			switches: n0.switches,
			media:    n0.media,
			links:    n0.links,
			paths:    n0.paths},
		nc)

	nc = networkFromBearer("vlan", opts, n0)
	filterWorkers(nc, []string{"c1", "c2"})
	assert.Equal(t,
		&network{name: "vlan",
			bearer:   n0.name,
			options:  n0.options,
			workers:  n0.workers[0:2],
			switches: n0.switches,
			media:    n0.media,
			links:    n0.links[0:4],
			paths:    n0.paths[0:2]},
		nc)

	nc = networkFromBearer("vlan", opts, n0)
	filterWorkers(nc, []string{"c2", "c3"})
	assert.Equal(t,
		&network{name: "vlan",
			bearer:   n0.name,
			options:  n0.options,
			workers:  n0.workers[1:3],
			switches: n0.switches,
			media:    n0.media,
			links:    n0.links[2:6],
			paths:    n0.paths[4:6]},
		nc)
}

func TestNetworkFromBearer(t *testing.T) {
	p := makeParser( // infrastructure
		`node c1,c2 provides cpu:1000`,
		`network eth with lat:1 bw:2 node sw`,
		`link c1—sw`,
		`link c2—sw with lat:3 bw:4`,
		`find paths (c1•c2)`)
	p.infrastructure()
	require.Equal(t, EOF, p.token)
	require.Len(t, p.data.infra.networks, 1)
	bearer := p.data.infra.networks[0]

	// options := new.MakeNetworkOptions(0, "", "assured", 5, 6, true, true)
	qos := types.Option[string]{}
	qos.Set("assured")
	lat := types.Option[latency]{}
	lat.Set(5)
	bw := types.Option[bandwidth]{}
	bw.Set(6)
	options := networkOptions{qos: qos, latency: lat, bandwidth: bw,
		wire: types.NewOption(true), duplex: types.NewOption(true)}
	net := networkFromBearer("vlan", options, bearer)
	assert.Equal(t, "vlan", net.name)
	assert.Equal(t, options, net.options)
	checkLink := func(idx int, lat latency) {
		bl, nl := bearer.links[idx], net.links[idx]
		assert.NotSame(t, bl, nl)
		assert.Equal(t, bl.name, nl.name)
		assert.Equal(t, bl.src, nl.src)
		assert.Equal(t, bl.dst, nl.dst)
		assert.Equal(t, bl.medium, nl.medium)
		assert.Equal(t, lat, nl.lat)
	}
	checkLink(0, net.options.latency.Value())
	checkLink(1, net.options.latency.Value())
	checkLink(2, 3)
	checkLink(3, 3)

	checkMedium := func(idx int, bw bandwidth) {
		bm, nm := bearer.media[idx], net.media[idx]
		assert.NotSame(t, bm, nm)
		assert.Equal(t, bm.name, nm.name)
		assert.Equal(t, bw, nm.bw.Cap)
	}
	checkMedium(0, net.options.bandwidth.Value())
	checkMedium(1, net.options.bandwidth.Value())
	checkMedium(2, 4)
	checkMedium(3, 4)
}
