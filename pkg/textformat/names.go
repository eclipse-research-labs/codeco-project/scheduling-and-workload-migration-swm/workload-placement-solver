// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"fmt"
	"strings"
)

var digitsSup []rune = []rune{
	'\u2070', '\u00B9', '\u00B2', '\u00B3', '\u2074',
	'\u2075', '\u2076', '\u2077', '\u2078', '\u2079'}

func itoaSup(n int) string {
	return strings.Map(
		func(d rune) rune {
			return digitsSup[d-'0']
		},
		fmt.Sprintf("%d", n))
}

func LinkName(src, dst string) string {
	return src + "→" + dst
}

func PathName(src, dst string, links int) string {
	var lc string
	if links > 1 {
		lc = itoaSup(links)
	}
	return fmt.Sprintf("%s→%s%s", src, lc, dst)

}
