// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"strings"

	pb "siemens.com/qos-solver/internal/port/grpc"
)

type Assignment struct {
	Name            string
	Options         ModelOptions
	AppGroup        *pb.ApplicationGroup
	Infrastructure  *pb.Infrastructure
	Costs           *pb.Cost
	Recommendations *pb.Recommendation
	Assignment      []*pb.Assignment
}

func Parse(name, data string) (Assignment, error) {
	var p parser
	p.initWithString(data)
	p.scanner.Filename = name
	err := p.parse()
	if err != nil && !strings.Contains(err.Error(), "stopper:") {
		return Assignment{}, err
	}
	asgmt := Assignment{
		Name:            p.data.model,
		Options:         p.data.options,
		AppGroup:        p.data.apps.asGrpc(),
		Infrastructure:  p.data.infra.asGrpc(),
		Costs:           p.data.costs.asGrpc(),
		Recommendations: p.data.recs.asGrpc()}
	if n := len(p.data.asgmts); n > 0 {
		asgmt.Assignment = make([]*pb.Assignment, n)
		for i, a := range p.data.asgmts {
			asgmt.Assignment[i] = a.asGrpc()
		}
	}
	return asgmt, nil
}

func Convert(name, data string, model *pb.Model) error {
	asgmt, err := Parse(name, data)
	if err != nil {
		return err
	}
	model.AppGroup = asgmt.AppGroup
	model.Infrastructure = asgmt.Infrastructure
	model.Recommendations = asgmt.Recommendations
	return err
}
