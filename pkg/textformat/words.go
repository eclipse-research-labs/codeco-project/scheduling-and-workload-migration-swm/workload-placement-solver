// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

const (
	_all                 string = "all"
	_assured             string = "assured"
	_bestEffort          string = "best-effort"
	_compoundNames       string = "compound-names"
	_duplex              string = "duplex"
	_false               string = "false"
	_halfDuplex          string = "half-duplex"
	_implicitReversePath string = "implicit-reverse-path"
	_no                  string = "no"
	_off                 string = "off"
	_on                  string = "on"
	_radio               string = "radio"
	_shortest            string = "shortest"
	_shortestPathOnly    string = "shortest-path-only"
	_true                string = "true"
	_wire                string = "wire"
	_yes                 string = "yes"
	// Property Names
	_bandwidth string = "bandwidth:"
	_bearer    string = "bearer:"
	_bw        string = "bw:"
	_cpu       string = "cpu:"
	_labels    string = "labels:"
	_lat       string = "lat:"
	_latency   string = "latency:"
	_links     string = "links:"
	_memory    string = "memory:"
	_mtu       string = "mtu:"
	_path      string = "path:"
	_qos       string = "qos:"
	_ram       string = "ram:"
	_type      string = "type:"
)
