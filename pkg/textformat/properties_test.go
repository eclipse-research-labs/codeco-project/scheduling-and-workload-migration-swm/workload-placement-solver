// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParserGetCPU(t *testing.T) {
	cases := []struct {
		input string
		cpu   milliCPU
	}{{"17", 17},
		{"19mCPU", 19},
		{"23CPU", 23000}}
	for _, tc := range cases {
		p := makeParser(tc.input)
		assert.Equal(t, tc.cpu, p.getCPU(), tc.input)
	}

	parse := func(p *parser) { p.getCPU() }
	checkParsingErrors(t, parse, []errorCase{
		{"1000000000CPU", `<input>:1:1: illegal CPU value: "1000000000 CPU"`},
		{"x", `<input>:1:1: expected: integer`},
		{"x y", `<input>:1:1: expected: integer`}})
}

func TestParserGetCPU_error(t *testing.T) {
	p := makeParser("error")
	assert.PanicsWithValue(t,
		`<input>:1:1: expected: integer`,
		func() { p.getCPU() })

	p = makeParser("1000unknown")
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal CPU value: "1000 unknown"`,
		func() { p.getCPU() })
}

func TestDataFactor(t *testing.T) {
	cases := []struct {
		input       string
		bytes, bits int64
	}{{"KiB", 1024, 1024 * 8},
		{"KB", 1000, 1000 * 8},
		{"Mb", 1000 * 1000 / 8, 1000 * 1000},
		{"B", 1, 8},
		{"b", 1, 1}}
	assert := assert.New(t)
	for _, tc := range cases {
		f, err := dataFactor(tc.input, false)
		assert.Equal(tc.bytes, f, tc.input)
		assert.NoError(err, tc.input)

		f, _ = dataFactor(tc.input, true)
		assert.Equal(tc.bits, f, tc.input)
	}
	f, err := dataFactor("unknown", false)
	assert.Equal(int64(0), f)
	assert.ErrorIs(err, errIllegalUnit)
}

func TestParserGetRAM(t *testing.T) {
	cases := []struct {
		input string
		ram   ramMB
	}{{"17", 17},
		{"19Kib", 19 * 1024 / 8},
		{"23GB", 23_000_000_000}}
	for _, tc := range cases {
		p := makeParser(tc.input)
		assert.Equal(t, tc.ram, p.getRAM(), tc.input)
	}
}

func TestParserGetRAM_error(t *testing.T) {
	p := makeParser("error")
	assert.PanicsWithValue(t,
		`<input>:1:1: expected: integer`,
		func() { p.getRAM() })

	p = makeParser("1000unknown")
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal RAM value: "1000 unknown"`,
		func() { p.getRAM() })
}

func TestParserGetLabels(t *testing.T) {
	p := makeParser("(A ~B)")
	assert.Equal(t, []string{"A", "~B"}, p.getLabels(true))
}

func TestParserGetLabels_errors(t *testing.T) {
	parseLabels := func(p *parser) { p.getLabels(true) }
	checkParsingErrors(t, parseLabels, []errorCase{
		{"error", `<input>:1:1: expected: "("`},
		{"()", `<input>:1:2: expected: identifier`},
		{"(-", `<input>:1:3: expected: identifier`},
		{"(!)", `<input>:1:3: expected: identifier`},
		{"(A B 17", `<input>:1:6: expected: ")"`}})
}

func TestParserGetLatency(t *testing.T) {
	cases := []struct {
		input string
		nanos latency
	}{{"17", 17},
		{"23ns", 23},
		{"19ms", latency(19 * time.Millisecond)},
		{"29us", latency(29 * time.Microsecond)},
		{"29µs", latency(29 * time.Microsecond)},
		{"23s", latency(23 * time.Second)}}
	for _, tc := range cases {
		var p parser
		p.initWithString(tc.input)
		assert.Equal(t, tc.nanos, p.getLatency(), tc.input)
	}
}

func TestParserGetLatency_error(t *testing.T) {
	var p parser
	p.initWithString("error")
	assert.PanicsWithValue(t,
		`<input>:1:1: expected: integer`,
		func() { p.getLatency() })

	p.initWithString("1000unknown")
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal duration: "1000 unknown"`,
		func() { p.getLatency() })
}

func TestParserGetBandwidth(t *testing.T) {
	cases := []struct {
		input string
		bw    bandwidth
	}{{"17", 17},
		{"19Kb", bandwidth(19 * 1000)},
		{"19Kib", bandwidth(19 * 1024)},
		{"23KB", bandwidth(23 * 1000 * 8)},
		{"30*64KiB", bandwidth(30 * 65536 * 8)}}
	for _, tc := range cases {
		var p parser
		p.initWithString(tc.input)
		assert.Equal(t, tc.bw, p.getBandwidth(), tc.input)
	}
}

func TestParserGetBandwidth_error(t *testing.T) {
	var p parser
	p.initWithString("error")
	assert.PanicsWithValue(t,
		`<input>:1:1: expected: integer`,
		func() { p.getBandwidth() })

	p.initWithString("1000unknown")
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal bandwidth: "1000 unknown"`,
		func() { p.getBandwidth() })
}

func TestParserGetOneOf(t *testing.T) {
	p := makeParser(`Best-Effort`)
	expect := func() string {
		return p.expectOneOf("service class", _bestEffort, _assured)
	}
	assert.Equal(t, _bestEffort, expect())

	p = makeParser(`17`)
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal service class: "17"`,
		func() { expect() })
	p = makeParser(`unknown`)
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal service class: "unknown"`,
		func() { expect() })
}

func TestParserGetQoS(t *testing.T) {
	p := makeParser("qos:best-effort")
	assert.Equal(t, "best-effort", p.getQoS())
	p = makeParser("qos:assured")
	assert.Equal(t, "assured", p.getQoS())
}

func TestParserGetBoolean(t *testing.T) {
	for _, w := range []string{"TRUE", "YES", "ON"} {
		p := makeParser(w)
		assert.True(t, p.getBoolean(), w)
		assert.Equal(t, EOF, p.token)
	}
	for _, w := range []string{"FALSE", "NO", "OFF"} {
		p := makeParser(w)
		assert.False(t, p.getBoolean(), w)
		assert.Equal(t, EOF, p.token)
	}
	assert.PanicsWithValue(t,
		`<input>:1:1: illegal Boolean: "unknown"`,
		func() { makeParser("unknown").getBoolean() })
}
