// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"strconv"

	"siemens.com/qos-solver/internal/port/grpc"
)

type nodeValue struct {
	node  string
	value float64
}

func (ni nodeValue) asGrpc() *grpc.NodeValue {
	return &grpc.NodeValue{Node: ni.node, Value: ni.value}
}

type nodeValues []nodeValue

func (vs nodeValues) asGrpc() []*grpc.NodeValue {
	var ns []*grpc.NodeValue
	if n := len(vs); n > 0 {
		ns = make([]*grpc.NodeValue, n)
		for i := range vs {
			ns[i] = vs[i].asGrpc()
		}
	}
	return ns
}

func (p *parser) propOrID() string {
	if p.token == Property {
		return p.scanner.token[:len(p.scanner.token)-1]
	}
	return p.expectId()
}

func (p *parser) nodeValues() []nodeValue {
	parse := func() nodeValue {
		name := p.propOrID()
		if p.token != Colon && p.token != Property {
			p.syntaxError(`expected ":"`)
		}
		p.next()
		if p.token != Float && p.token != Integer {
			p.syntaxError("expected: float")
		}
		value, err := strconv.ParseFloat(p.scanner.token, 64)
		if err != nil {
			p.syntaxError("illegal float: %q", p.scanner.token)
		}
		p.next()
		return nodeValue{node: name, value: value}
	}
	p.expect("(", LParen)
	is := make([]nodeValue, 1, 8)
	is[0] = parse()
	for p.token == Identifier || p.token == Property {
		is = append(is, parse())
	}
	p.expect(")", RParen)
	return is
}

type wlCosts struct {
	wl    workloadSpec
	nodes []nodeValue
}

func (wc wlCosts) asGrpc() *grpc.Cost_Workload {
	return &grpc.Cost_Workload{
		Application: wc.wl.app,
		Workload:    wc.wl.name,
		Nodes:       (nodeValues).asGrpc(wc.nodes),
	}
}

type costData struct {
	nodes     []nodeValue
	workloads []wlCosts
}

func (data *costData) empty() bool {
	return data == nil || len(data.nodes) == 0 && len(data.workloads) == 0
}

func (data costData) asGrpc() *grpc.Cost {
	if data.empty() {
		return nil
	}
	var c grpc.Cost
	c.Nodes = (nodeValues).asGrpc(data.nodes)
	if len(data.workloads) > 0 {
		c.Workloads = make([]*grpc.Cost_Workload, len(data.workloads))
		for i := range data.workloads {
			c.Workloads[i] = data.workloads[i].asGrpc()
		}
	}
	return &c
}

func (p *parser) costs() {
	if !p.hasWord("costs") {
		return
	}
	parseWl := func() {
		var wi wlCosts
		wi.wl = p.workloadRef("")
		wi.nodes = p.nodeValues()
		p.data.costs.workloads = append(p.data.costs.workloads, wi)
	}
	p.data.costs.workloads = make([]wlCosts, 0, 8)
	switch {
	case p.hasWord("nodes"):
		p.data.costs.nodes = p.nodeValues()
	case p.hasWord("workload"):
		parseWl()
	default:
		p.syntaxError(`expected: "nodes" or "workload"`)
	}
	for p.hasWord("workload") {
		parseWl()
	}
}

type wlRecs struct {
	wl    workloadSpec
	nodes []nodeValue
}

func (data *wlRecs) empty() bool {
	return data == nil || len(data.nodes) == 0
}

func (rec wlRecs) asGrpc() *grpc.Recommendation_Workload {
	if rec.empty() {
		return nil
	}
	return &grpc.Recommendation_Workload{
		Application: rec.wl.app,
		Workload:    rec.wl.name,
		Nodes:       (nodeValues).asGrpc(rec.nodes)}
}

type recsData struct {
	workloads []wlRecs
}

func (data recsData) asGrpc() *grpc.Recommendation {
	var r *grpc.Recommendation
	if n := len(data.workloads); n > 0 {
		ws := make([]*grpc.Recommendation_Workload, 0, n)
		for _, w := range data.workloads {
			if g := w.asGrpc(); g != nil {
				ws = append(ws, g)
			}
		}
		if len(ws) > 0 {
			r = &grpc.Recommendation{Workloads: ws}
		}
	}
	return r
}

func (p *parser) recommendations() {
	if !p.identMatch("recommendations") {
		return
	}
	if len(p.data.costs.nodes) != 0 || len(p.data.costs.workloads) != 0 {
		p.syntaxError(`recommendations not allowed`)
	}
	p.next()
	parseWl := func() {
		var wl wlRecs
		wl.wl = p.workloadRef("")
		wl.nodes = p.nodeValues()
		p.data.recs.workloads = append(p.data.recs.workloads, wl)
	}
	p.data.recs.workloads = make([]wlRecs, 0, 8)
	if p.hasWord("workload") {
		parseWl()
	} else {
		p.syntaxError(`expected: "workload"`)
	}
	for p.hasWord("workload") {
		parseWl()
	}
}
