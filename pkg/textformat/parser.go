// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"errors"
	"fmt"
	"strings"
	"unicode/utf8"
)

type parser struct {
	scanner *scanner
	token   token
	data    data
	err     string
}

func (p *parser) init(file string) (err error) {
	s, err := newScanner(file)
	if err == nil {
		p.scanner = s
	}
	return
}

func (p *parser) initWithString(input ...string) {
	p.scanner = newScannerWithString(joinLines(input...))
}

func (p *parser) formatError(format string, args ...interface{}) {
	msg := fmt.Sprintf(format, args...)
	p.err = fmt.Sprintf("%s: %s", p.scanner.Position, msg)
}

func (p *parser) syntaxError(format string, args ...interface{}) {
	p.formatError(format, args...)
	panic(p.err)
}

func (p *parser) identMatch(w string) bool {
	return p.token == Identifier && strings.EqualFold(p.scanner.token, w)
}

func (p *parser) next() token {
	p.token = p.scanner.scan()
	if p.token == Illegal {
		r, _ := utf8.DecodeLastRuneInString(p.scanner.token)
		p.syntaxError("illegal char: U+%04X", r)
	}
	return p.token
}

func (p *parser) expect(w string, ts ...token) {
	switch {
	case len(ts) == 1 && p.token == ts[0]:
	case p.identMatch(w):
	default:
		p.syntaxError("expected: %q", w)
	}
	p.next()
}

func (p *parser) expectId() string {
	if p.token != Identifier {
		p.syntaxError("expected: identifier")
	}
	id := p.scanner.token
	p.next()
	return id
}

func (p *parser) expectIdOrInt() string {
	if p.token != Identifier && p.token != Integer {
		p.syntaxError("expected: identifier")
	}
	id := p.scanner.token
	p.next()
	return id
}

func (p *parser) hasWord(w string) bool {
	if p.identMatch(w) {
		p.next()
		return true
	}
	return false
}

func (p *parser) hasToken(tok token) bool {
	if p.token == tok {
		p.next()
		return true
	}
	return false
}

func (p *parser) isOneOf(ws ...string) (string, bool) {
	for _, w := range ws {
		if p.identMatch(w) {
			return w, true
		}
	}
	return "", false
}

func (p *parser) hasOneOf(ws ...string) (w string, ok bool) {
	if w, ok = p.isOneOf(ws...); ok {
		p.next()
	}
	return
}

func (p *parser) expectOneOf(name string, options ...string) (w string) {
	ok := p.token == Identifier
	if ok {
		w, ok = p.hasOneOf(options...)
	}
	if !ok {
		p.syntaxError("illegal %s: %q", name, p.scanner.token)
	}
	return
}

func (p *parser) sections() {
	for p.token == Identifier {
		if _, ok := p.isOneOf("assignments", "assignment"); ok {
			break
		} else if p.hasWord("section") {
			p.expectId()
		}
		p.applications()
		p.infrastructure()
		p.costs()
		p.recommendations()
	}
	p.assignments()
}

func (p *parser) parse() (err error) {
	defer func() {
		if r := recover(); r != nil {
			if p.err == "" {
				p.formatError("%v", r)
			}
			err = errors.New(p.err)
		}
	}()
	p.next()
	p.header()
	p.sections()
	if p.token != EOF {
		return fmt.Errorf("%s: stopper: %q",
			p.scanner.Position, p.scanner.token)
	}
	for _, n := range p.data.infra.networks {
		n.makePathNamesUnique()
	}
	if p.data.options.CompoundNames {
		p.data.apps.makeCompoundNames()
		for i := range p.data.asgmts {
			p.data.asgmts[i].MakeCompoundNames()
		}
	}
	return nil
}
