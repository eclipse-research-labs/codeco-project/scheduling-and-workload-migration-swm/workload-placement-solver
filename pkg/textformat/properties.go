// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package textformat

import (
	"errors"
	"math"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

var errIllegalUnit = errors.New("illegal unit")
var errValueTooBig = errors.New("value too big")

func cpuFactor(unit string) (int64, error) {
	switch unit {
	case "mCPU":
		return 1, nil
	case "CPU":
		return 1000, nil
	default:
		return 0, errIllegalUnit
	}
}

func (p *parser) getCPU() milliCPU {
	if p.token != Integer {
		p.syntaxError("expected: integer")
	}
	var n int64
	var err error
	if sp := strings.IndexRune(p.scanner.token, ' '); sp < 0 {
		n, err = strconv.ParseInt(p.scanner.token, 10, 32)
	} else {
		n, err = strconv.ParseInt(p.scanner.token[:sp], 10, 32)
		if err == nil {
			var f int64
			if f, err = cpuFactor(p.scanner.token[sp+1:]); err == nil {
				n *= f
				if n > math.MaxInt32 {
					err = errValueTooBig
				}
			}
		}
	}
	if err != nil {
		p.syntaxError("illegal CPU value: %q", p.scanner.token)
	}
	p.next()
	return milliCPU(n)
}

var dataUnit = regexp.MustCompile(`^([KMG]i?)?(Byte|B|bit|b)$`)

func dataFactor(unit string, bits bool) (int64, error) {
	if m := dataUnit.FindStringSubmatchIndex(unit); m != nil {
		n := int64(1)
		if m[2] == 0 {
			k := int64(1000)
			if m[3] > 1 {
				k = 1024
			}
			for _, c := range []byte("KMG") {
				n *= k
				if c == unit[0] {
					break
				}
			}
		}
		if s := unit[m[4]:]; s == "B" || s == "Byte" {
			if bits {
				n *= 8
			}
		} else if !bits {
			n = (n + 7) / 8
		}
		return n, nil
	}
	return 0, errIllegalUnit
}

func (p *parser) getDataSize(bits bool) (n int64, err error) {
	if sp := strings.IndexRune(p.scanner.token, ' '); sp < 0 {
		n, err = strconv.ParseInt(p.scanner.token, 10, 0)
	} else {
		n, err = strconv.ParseInt(p.scanner.token[:sp], 10, 0)
		if err == nil {
			var f int64
			f, err = dataFactor(p.scanner.token[sp+1:], bits)
			if err == nil {
				n *= f
			}
		}
	}
	return
}

func (p *parser) getRAM() ramMB {
	if p.token != Integer {
		p.syntaxError("expected: integer")
	}
	n, err := p.getDataSize(false)
	if err != nil {
		p.syntaxError("illegal RAM value: %q", p.scanner.token)
	}
	p.next()
	return ramMB(n)
}

func (p *parser) getLabels(allowNot bool) []string {
	p.expect("(", LParen)
	name := func() string {
		var n string
		if p.token == Not || p.token == Minus {
			if !allowNot {
				p.syntaxError("exclusion not allowed")
			}
			n = p.scanner.token
			p.next()
		}
		return n + p.expectId()
	}
	ls := []string{name()}
	for p.token == Identifier || p.token == Not || p.token == Minus {
		ls = append(ls, name())
	}
	p.expect(")", RParen)
	return ls
}

var durationUnit = regexp.MustCompile(`^[mµun]?s$`)

func durationFactor(unit string) (int64, error) {
	if durationUnit.MatchString(unit) {
		r, _ := utf8.DecodeRuneInString(unit)
		switch r {
		case 'm':
			return int64(time.Millisecond), nil
		case 'µ', 'u':
			return int64(time.Microsecond), nil
		case 'n':
			return int64(time.Nanosecond), nil
		default:
			return int64(time.Second), nil
		}
	}
	return 0, errIllegalUnit
}

func (p *parser) getLatency() latency {
	if p.next() != Integer {
		p.syntaxError("expected: integer")
	}
	var n int64
	var err error
	if sp := strings.IndexRune(p.scanner.token, ' '); sp < 0 {
		n, err = strconv.ParseInt(p.scanner.token, 10, 0)
	} else {
		n, err = strconv.ParseInt(p.scanner.token[:sp], 10, 0)
		if err == nil {
			var f int64
			if f, err = durationFactor(p.scanner.token[sp+1:]); err == nil {
				n *= f
			}
		}
	}
	if err != nil {
		p.syntaxError("illegal duration: %q", p.scanner.token)
	}
	p.next()
	return latency(n)
}

func (p *parser) getBandwidth() bandwidth {
	if p.next() != Integer {
		p.syntaxError("expected: integer")
	}
	next := true
	var n int64
	var err error
	if sp := strings.IndexRune(p.scanner.token, ' '); sp < 0 {
		n, err = strconv.ParseInt(p.scanner.token, 10, 0)
		if err == nil {
			if p.next() == Times {
				p.next()
				var m int64
				m, err = p.getDataSize(true)
				n *= m
			} else {
				next = false
			}
		}
	} else {
		n, err = strconv.ParseInt(p.scanner.token[:sp], 10, 0)
		if err == nil {
			var f int64
			f, err = dataFactor(p.scanner.token[sp+1:], true)
			n *= f
		}
	}
	if err != nil {
		p.syntaxError("illegal bandwidth: %q", p.scanner.token)
	}
	if next {
		p.next()
	}
	return bandwidth(n)
}

func (p *parser) getQoS() string {
	p.next()
	return p.expectOneOf("service class", _bestEffort, _assured)
}

func (p *parser) getBoolean() bool {
	word := p.expectOneOf("Boolean", _true, _yes, _on, _false, _no, _off)
	return word == _true || word == _yes || word == _on
}
