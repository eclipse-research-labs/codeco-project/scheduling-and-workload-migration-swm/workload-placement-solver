// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"fmt"
	"regexp"
	"unicode/utf8"
)

var k8sName = regexp.MustCompile("^[[:alnum:]._-]+$")

func isK8sName(name string) bool {
	l := utf8.RuneCountInString(name)
	return 1 <= l &&
		l <= 253 &&
		k8sName.MatchString(name)
}

func (s *state) checkAppName(name string) error {
	if !isK8sName(name) {
		return illegalName(name)
	}
	for i := range s.apps {
		if s.apps[i].name == name {
			return duplicate("application", name)
		}
	}
	return nil
}

func (s *state) checkWlName(app *application, name string) error {
	if !isK8sName(name) {
		return illegalName(name)
	}
	for i := int(app.base); i < len(s.workloads); i++ {
		if s.workloads[i].name == name {
			return duplicate("workload", app.name, ".", name)
		}
	}
	return nil
}

func (s *state) addLabels(req, forb []string) ([]nameIndex, error) {
	if len(req) == 0 && len(forb) == 0 {
		return nil, nil
	}
	ns, ni := make([]nameIndex, len(req)+len(forb)), 0
	add := func(base, n int, idx nameIndex) {
		for i := 0; i < n; i++ {
			if ns[base+i] == idx {
				return
			}
		}
		ns[ni], ni = idx, ni+1
	}
	for i, n := range req {
		if !isK8sName(n) {
			return nil, illegalName(n)
		}
		add(0, i, s.addName(n))
	}
	rl := ni
	for i, n := range forb {
		if !isK8sName(n) {
			return nil, illegalName(n)
		}
		idx := s.addName(n)
		for j := range req {
			if ns[j] == idx {
				return nil, fmt.Errorf(
					`label required and forbidden: %q"`, n)

			}
		}
		add(rl, i, ^idx)
	}
	return ns[:ni], nil
}

func (s *state) checkNodeName(name string) error {
	if !isK8sName(name) {
		return illegalName(name)
	}
	for i := range s.nodes {
		if s.nodes[i].name == name {
			return duplicate("node", name)
		}
	}
	return nil
}

func (n *network) checkMediumName(name string) error {
	if m := n.getMedium(name); m != nil {
		return duplicate("medium", name)
	}
	return nil
}

func (n *network) checkLinkName(name string) error {
	for i := range n.links {
		if n.links[i].name == name {
			return duplicate("link", name)
		}
	}
	return nil
}

func (n *network) checkPathName(name string) error {
	for i := range n.paths {
		if n.paths[i].name == name {
			return duplicate("path", name)
		}
	}
	return nil
}

func (n *network) checkPath(idx int, s *state) error {
	p := &n.paths[idx]
	sd, ds := p.pair, p.pair.swap()
	dupErr := func() error {
		name := p.name
		if name == "" {
			name = s.nodes[sd.src].name + "→" + s.nodes[sd.dst].name
		}
		return duplicate("path", name)
	}
	for i := range n.paths {
		pi := &n.paths[i]
		if p == &n.paths[i] {
			continue
		}
		if s.options.shortestPathsOnly && sd == pi.pair || p.equal(pi) {
			return dupErr()
		}
		if s.options.implicitReversePath && ds == pi.pair {
			return dupErr()
		}
	}
	return nil
}
