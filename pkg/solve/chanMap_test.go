// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChanMapSet(t *testing.T) {
	assert := assert.New(t)
	cm := newChanMap(6)
	assert.Equal(0, cm.set(pair[wlIndex]{1, 1}, 17))
	assert.Equal(1, cm.set(pair[wlIndex]{1, 2}, 17))
	assert.Equal(0, cm.set(pair[wlIndex]{0, 1}, 17))
	assert.Equal(1, cm.set(pair[wlIndex]{1, 0}, 17))
	assert.Equal(1, cm.set(pair[wlIndex]{1, 0}, 18))
	assert.Equal([]chanIndex{17, 18}, cm.find(pair[wlIndex]{1, 0}))
	assert.Equal(4, cm.set(pair[wlIndex]{2, 0}, 17))
	assert.Equal(4, cm.set(pair[wlIndex]{1, 3}, 17))

	assert.Equal(1, cm.set(pair[wlIndex]{1, 0}, 19))
	assert.Equal([]chanIndex{17, 18, 19}, cm.find(pair[wlIndex]{1, 0}))

	assert.Equal(-1, cm.set(pair[wlIndex]{2, 1}, 19))
	assert.Len(cm, 6)
}

func TestChanMapFind(t *testing.T) {
	assert := assert.New(t)
	cm := newChanMap(3)
	assert.Equal(0, cm.set(pair[wlIndex]{0, 1}, 10))
	assert.Equal(1, cm.set(pair[wlIndex]{1, 1}, 20))
	assert.Equal(2, cm.set(pair[wlIndex]{2, 0}, 30))

	assert.Equal([]chanIndex(nil), cm.find(pair[wlIndex]{0, 0}))
	assert.Equal([]chanIndex{10}, cm.find(pair[wlIndex]{0, 1}))
	assert.Equal([]chanIndex(nil), cm.find(pair[wlIndex]{1, 0}))
	assert.Equal([]chanIndex{20}, cm.find(pair[wlIndex]{1, 1}))
	assert.Equal([]chanIndex(nil), cm.find(pair[wlIndex]{1, 2}))
	assert.Equal([]chanIndex{30}, cm.find(pair[wlIndex]{2, 0}))
	assert.Equal([]chanIndex(nil), cm.find(pair[wlIndex]{2, 1}))
}

func BenchmarkChanMapUsage(b *testing.B) {
	for n := 0; n < b.N; n++ {
		cm := newChanMap(3)
		cm.set(pair[wlIndex]{0, 1}, 10)
		cm.set(pair[wlIndex]{1, 1}, 20)
		cm.find(pair[wlIndex]{0, 0})
		cm.find(pair[wlIndex]{0, 1})
		cm.find(pair[wlIndex]{1, 0})
		cm.find(pair[wlIndex]{1, 1})
		cm.find(pair[wlIndex]{1, 2})
	}
}
