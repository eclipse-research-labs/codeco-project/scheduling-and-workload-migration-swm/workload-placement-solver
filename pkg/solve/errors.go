// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"errors"
	"strings"
)

var (
	ErrEmptyAppModel     = errors.New("empty application model")
	ErrNoNodes           = errors.New("no nodes")
	ErrUnknownWorkload   = errors.New("unknown workload")
	ErrNoAssignmentFound = errors.New("no assignment found")
	ErrIllegalPath       = errors.New("illegal path")
	errAssignmentFound   = errors.New("assignment found")
	errUnknownDataSet    = errors.New("unknown data set")
	errRecsNotAllowed    = errors.New("recommendations not allowed")
)

func errorWithName(kind, obj string, name ...string) error {
	var msg strings.Builder
	msg.WriteString(kind)
	msg.WriteRune(' ')
	msg.WriteString(obj)
	msg.WriteString(": ")
	for _, s := range name {
		msg.WriteString(s)
	}
	return errors.New(msg.String())
}

func unknown(obj string, name ...string) error {
	return errorWithName("unknown", obj, name...)
}

func duplicate(obj string, name ...string) error {
	return errorWithName("duplicate", obj, name...)
}

func empty(obj string, name string) error {
	return errorWithName("empty", obj, name)
}

func illegalName(name string) error {
	return errorWithName("illegal", "name", name)
}
