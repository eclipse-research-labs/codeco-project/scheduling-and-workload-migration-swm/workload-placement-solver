// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data"
	"siemens.com/qos-solver/pkg/textformat"
)

func restore[T any](who *T, value T) func() {
	store := *who
	*who = value
	return func() {
		*who = store
	}
}

func solveSemApps(apps ...string) (assignment, error) {
	defer restore(&textformat.CompoundNames, false)()
	state, err := prepStateFromModel(&grpc.Model{
		AppGroup:       data.GetAppModel("semiotics-test", 0, apps),
		Infrastructure: data.GetInfraModel("semiotics", 0)})
	if err != nil {
		return assignment{}, err
	}
	if err := state.solve(); err != nil {
		return assignment{}, err
	}
	return state.assignmentStable(), nil
}

func TestSolve_semiotics_0_singleApp(t *testing.T) {
	tests := []struct {
		app          string
		combinations int
		asgmt        string
	}{
		{app: "SDN", combinations: 13, asgmt: `
			(assignment
			  (workloads SDN.Control:C2 SDN.Agent1:N3 SDN.Agent2:N4
				SDN.Agent3:N5 SDN.Agent4:N6)
			  (channels
				SDN.Control→SDN.Agent1:Ethernet:C2→N3
				SDN.Control→SDN.Agent2:Ethernet:C2→N4
				SDN.Control→SDN.Agent3:Ethernet:C2→N5
				SDN.Control→SDN.Agent4:Ethernet:C2→N6
				SDN.Agent1→SDN.Control:Ethernet:N3→C2
				SDN.Agent2→SDN.Control:Ethernet:N4→C2
				SDN.Agent3→SDN.Control:Ethernet:N5→C2
				SDN.Agent4→SDN.Control:Ethernet:N6→C2))`},
		{app: "SCADA", combinations: 7, asgmt: `
			(assignment
			  (workloads SCADA.SCADA:C2 SCADA.PlcEmerg:C1 SCADA.PlcData:C1)
			  (channels
				SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C2→C1
				SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C2
				SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
				SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1))`},
		{app: "Analysis", combinations: 4, asgmt: `
			(assignment
			  (workloads Analysis.Accel:C2 Analysis.DCorr:C1
				Analysis.Audio:C6 Analysis.Video:C5)
			  (channels))`},
		{app: "MDSP", combinations: 3, asgmt: `
			(assignment
			  (workloads MDSP.Connect:C1 MDSP.CloudIntf:C5)
			  (channels MDSP.Connect→MDSP.CloudIntf:Ethernet:C1→C5))`},
		{app: "AppIntf", combinations: 12, asgmt: `
			(assignment
			  (workloads AppIntf.SemGW:C1 AppIntf.PatOrch:C2
				AppIntf.RecipeFW:C2 AppIntf.KnowRep:C3)
			  (channels
				AppIntf.SemGW→AppIntf.PatOrch:Ethernet:C1→C2
				AppIntf.PatOrch→AppIntf.SemGW:Ethernet:C2→C1
				AppIntf.PatOrch→AppIntf.RecipeFW:Loopback:C2→C2
				AppIntf.PatOrch→AppIntf.KnowRep:Ethernet:C2→C3
				AppIntf.RecipeFW→AppIntf.PatOrch:Loopback:C2→C2
				AppIntf.RecipeFW→AppIntf.KnowRep:Ethernet:C2→C3
				AppIntf.KnowRep→AppIntf.PatOrch:Ethernet:C3→C2
				AppIntf.KnowRep→AppIntf.RecipeFW:Ethernet:C3→C2))`}}
	for _, tc := range tests {
		asgmt, err := solveSemApps(tc.app)
		assert.NoError(t, err)
		equalAsgmt(t, tc.asgmt, asgmt, tc.app)
		assert.Equal(t, tc.combinations, stats.Combinations)
		assert.Equal(t, 0, stats.CheckLinks)
	}
}

func TestSolve_semiotics0_SDN_SCADA(t *testing.T) {
	asgmt, err := solveSemApps("SDN", "SCADA")
	assert.NoError(t, err)
	equalAsgmt(t, `
		(assignment
		  (workloads SDN.Control:C2 SDN.Agent1:N3 SDN.Agent2:N4
			SDN.Agent3:N5 SDN.Agent4:N6 SCADA.SCADA:C3 SCADA.PlcEmerg:C1
			SCADA.PlcData:C1)
		  (channels
			SDN.Control→SDN.Agent1:Ethernet:C2→N3
			SDN.Control→SDN.Agent2:Ethernet:C2→N4
			SDN.Control→SDN.Agent3:Ethernet:C2→N5
			SDN.Control→SDN.Agent4:Ethernet:C2→N6
			SDN.Agent1→SDN.Control:Ethernet:N3→C2
			SDN.Agent2→SDN.Control:Ethernet:N4→C2
			SDN.Agent3→SDN.Control:Ethernet:N5→C2
			SDN.Agent4→SDN.Control:Ethernet:N6→C2
			SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C3→C1
			SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C3
			SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
			SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1))`,
		asgmt)
	assert.Equal(t, 20, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)
}

func TestSolve_semiotics0_SDN_SCADA_Analysis(t *testing.T) {
	asgmt, err := solveSemApps("SDN", "SCADA", "Analysis")
	assert.NoError(t, err)
	equalAsgmt(t, `
		(assignment
		  (workloads SDN.Control:C2 SDN.Agent1:N3 SDN.Agent2:N4
			SDN.Agent3:N5 SDN.Agent4:N6 SCADA.SCADA:C3 SCADA.PlcEmerg:C1
			SCADA.PlcData:C1 Analysis.Accel:C4 Analysis.DCorr:C6
			Analysis.Audio:C6 Analysis.Video:C5)
		  (channels
			SDN.Control→SDN.Agent1:Ethernet:C2→N3
			SDN.Control→SDN.Agent2:Ethernet:C2→N4
			SDN.Control→SDN.Agent3:Ethernet:C2→N5
			SDN.Control→SDN.Agent4:Ethernet:C2→N6
			SDN.Agent1→SDN.Control:Ethernet:N3→C2
			SDN.Agent2→SDN.Control:Ethernet:N4→C2
			SDN.Agent3→SDN.Control:Ethernet:N5→C2
			SDN.Agent4→SDN.Control:Ethernet:N6→C2
			SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C3→C1
			SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C3
			SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
			SCADA.PlcEmerg→Analysis.Accel:Ethernet:C1→C4
			SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1
			Analysis.Accel→SCADA.PlcEmerg:Ethernet:C4→C1))`,
		asgmt)
	assert.Equal(t, 26, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)
}

func TestSolve_semiotics0_SDN_SCADA_Analysis_AppIntf(t *testing.T) {
	asgmt, err := solveSemApps("SDN", "SCADA", "Analysis", "MDSP")
	assert.NoError(t, err)
	equalAsgmt(t, `
		(assignment
		  (workloads SDN.Control:C2 SDN.Agent1:N3 SDN.Agent2:N4
			SDN.Agent3:N5 SDN.Agent4:N6 SCADA.SCADA:C3 SCADA.PlcEmerg:C1
			SCADA.PlcData:C1 Analysis.Accel:C4 Analysis.DCorr:C5
			Analysis.Audio:C5 Analysis.Video:C6 MDSP.Connect:C5
			MDSP.CloudIntf:C5)
		  (channels
			SDN.Control→SDN.Agent1:Ethernet:C2→N3
			SDN.Control→SDN.Agent2:Ethernet:C2→N4
			SDN.Control→SDN.Agent3:Ethernet:C2→N5
			SDN.Control→SDN.Agent4:Ethernet:C2→N6
			SDN.Agent1→SDN.Control:Ethernet:N3→C2
			SDN.Agent2→SDN.Control:Ethernet:N4→C2
			SDN.Agent3→SDN.Control:Ethernet:N5→C2
			SDN.Agent4→SDN.Control:Ethernet:N6→C2
			SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C3→C1
			SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C3
			SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
			SCADA.PlcEmerg→Analysis.Accel:Ethernet:C1→C4
			SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1
			Analysis.Accel→SCADA.PlcEmerg:Ethernet:C4→C1
			Analysis.Accel→MDSP.Connect:Ethernet:C4→C5
			Analysis.DCorr→MDSP.Connect:Loopback:C5→C5
			Analysis.Audio→MDSP.Connect:Loopback:C5→C5
			Analysis.Video→MDSP.Connect:Ethernet:C6→C5
			MDSP.Connect→MDSP.CloudIntf:Loopback:C5→C5))`,
		asgmt)
	assert.Equal(t, 34, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)
}

func TestSolve_semiotics0(t *testing.T) {
	expected := `
	(assignment
	  (workloads SDN.Control:C2 SDN.Agent1:N3 SDN.Agent2:N4
		SDN.Agent3:N5 SDN.Agent4:N6 SCADA.SCADA:C3
		SCADA.PlcEmerg:C1 SCADA.PlcData:C1
		Analysis.Accel:C4 Analysis.DCorr:C5
		Analysis.Audio:C6 Analysis.Video:C6
		MDSP.Connect:C5 MDSP.CloudIntf:C5
		AppIntf.SemGW:C6 AppIntf.PatOrch:C5
		AppIntf.RecipeFW:C2 AppIntf.KnowRep:C4)
	  (channels
		SDN.Control→SDN.Agent1:Ethernet:C2→N3
		SDN.Control→SDN.Agent2:Ethernet:C2→N4
		SDN.Control→SDN.Agent3:Ethernet:C2→N5
		SDN.Control→SDN.Agent4:Ethernet:C2→N6
		SDN.Control→AppIntf.PatOrch:Ethernet:C2→C5
		SDN.Agent1→SDN.Control:Ethernet:N3→C2
		SDN.Agent2→SDN.Control:Ethernet:N4→C2
		SDN.Agent3→SDN.Control:Ethernet:N5→C2
		SDN.Agent4→SDN.Control:Ethernet:N6→C2
		SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C3→C1
		SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C3
		SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
		SCADA.PlcEmerg→Analysis.Accel:Ethernet:C1→C4
		SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1
		Analysis.Accel→SCADA.PlcEmerg:Ethernet:C4→C1
		Analysis.Accel→MDSP.Connect:Ethernet:C4→C5
		Analysis.Accel→AppIntf.SemGW:Ethernet:C4→C6
		Analysis.DCorr→MDSP.Connect:Loopback:C5→C5
		Analysis.DCorr→AppIntf.SemGW:Ethernet:C5→C6
		Analysis.Audio→MDSP.Connect:Ethernet:C6→C5
		Analysis.Audio→AppIntf.SemGW:Loopback:C6→C6
		Analysis.Video→MDSP.Connect:Ethernet:C6→C5
		Analysis.Video→AppIntf.SemGW:Loopback:C6→C6
		MDSP.Connect→MDSP.CloudIntf:Loopback:C5→C5
		AppIntf.SemGW→Analysis.Accel:Ethernet:C6→C4
		AppIntf.SemGW→Analysis.DCorr:Ethernet:C6→C5
		AppIntf.SemGW→Analysis.Audio:Loopback:C6→C6
		AppIntf.SemGW→Analysis.Video:Loopback:C6→C6
		AppIntf.SemGW→AppIntf.PatOrch:Ethernet:C6→C5
		AppIntf.PatOrch→SDN.Control:Ethernet:C5→C2
		AppIntf.PatOrch→AppIntf.SemGW:Ethernet:C5→C6
		AppIntf.PatOrch→AppIntf.RecipeFW:Ethernet:C5→C2
		AppIntf.PatOrch→AppIntf.KnowRep:Ethernet:C5→C4
		AppIntf.RecipeFW→AppIntf.PatOrch:Ethernet:C2→C5
		AppIntf.RecipeFW→AppIntf.KnowRep:Ethernet:C2→C4
		AppIntf.KnowRep→AppIntf.PatOrch:Ethernet:C4→C5
		AppIntf.KnowRep→AppIntf.RecipeFW:Ethernet:C4→C2))`
	asgmt, err := solveSemApps("SDN", "SCADA", "Analysis", "MDSP", "AppIntf")
	require.NoError(t, err)
	equalAsgmt(t, expected, asgmt)
	assert.Equal(t, 57, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)

	defer restore(&textformat.CompoundNames, false)()
	asgmt, err = solveStable("semiotics", "semiotics", "0")
	require.NoError(t, err)
	equalAsgmt(t, expected, asgmt)
	assert.Equal(t, 57, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)
}

func TestSolve_semiotics1(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := solve("semiotics", "semiotics", "1")
	assert.NoError(t, err)
	equalAsgmt(t, `
		(assignment
		  (workloads
			SCADA.PlcData:C1 M1.Accel:M1.C2 M1.TSens:M1.C1
			M1.Audio:M1.C1 M1.Video:M1.C1 MDSP.CloudIntf:C5
			SDN.Agent1:N3 SDN.Agent2:N4 SDN.Agent3:N5
			SDN.Agent4:N6 SCADA.SCADA:C2 AppIntf.RecipeFW:C3
			AppIntf.KnowRep:C4 SDN.Control:C6 Analysis.Accel:C3
			Analysis.Video:C4 AppIntf.PatOrch:C5 SCADA.PlcEmerg:C1
			Analysis.DCorr:M1.C2 Analysis.Audio:M1.C2 MDSP.Connect:C5
			AppIntf.SemGW:C6)
		  (channels
			M1.Accel→SCADA.SCADA:Ethernet:M1.C2→C2
			M1.TSens→SCADA.SCADA:Ethernet:M1.C1→C2
			AppIntf.RecipeFW→AppIntf.KnowRep:Ethernet:C3→C4
			AppIntf.KnowRep→AppIntf.RecipeFW:Ethernet:C4→C3
			SDN.Control→SDN.Agent1:Ethernet:C6→N3
			SDN.Control→SDN.Agent2:Ethernet:C6→N4
			SDN.Control→SDN.Agent3:Ethernet:C6→N5
			SDN.Control→SDN.Agent4:Ethernet:C6→N6
			SDN.Agent1→SDN.Control:Ethernet:N3→C6
			SDN.Agent2→SDN.Control:Ethernet:N4→C6
			SDN.Agent3→SDN.Control:Ethernet:N5→C6
			SDN.Agent4→SDN.Control:Ethernet:N6→C6
			M1.Accel→Analysis.Accel:Ethernet:M1.C2→C3
			M1.Video→Analysis.Video:Ethernet:M1.C1→C4
			AppIntf.PatOrch→AppIntf.RecipeFW:Ethernet:C5→C3
			AppIntf.PatOrch→AppIntf.KnowRep:Ethernet:C5→C4
			AppIntf.RecipeFW→AppIntf.PatOrch:Ethernet:C3→C5
			AppIntf.KnowRep→AppIntf.PatOrch:Ethernet:C4→C5
			SDN.Control→AppIntf.PatOrch:Ethernet:C6→C5
			AppIntf.PatOrch→SDN.Control:Ethernet:C5→C6
			SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
			SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1
			SCADA.PlcEmerg→Analysis.Accel:Ethernet:C1→C3
			Analysis.Accel→SCADA.PlcEmerg:Ethernet:C3→C1
			SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C2→C1
			SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C2
			M1.Accel→Analysis.DCorr:Loopback:M1.C2→M1.C2
			M1.TSens→Analysis.DCorr:Ethernet:M1.C1→M1.C2
			M1.Audio→Analysis.Audio:Ethernet:M1.C1→M1.C2
			MDSP.Connect→MDSP.CloudIntf:Loopback:C5→C5
			Analysis.Accel→MDSP.Connect:Ethernet:C3→C5
			Analysis.Video→MDSP.Connect:Ethernet:C4→C5
			Analysis.DCorr→MDSP.Connect:Ethernet:M1.C2→C5
			Analysis.Audio→MDSP.Connect:Ethernet:M1.C2→C5
			Analysis.Accel→AppIntf.SemGW:Ethernet:C3→C6
			Analysis.Video→AppIntf.SemGW:Ethernet:C4→C6
			AppIntf.SemGW→Analysis.Accel:Ethernet:C6→C3
			AppIntf.SemGW→Analysis.Video:Ethernet:C6→C4
			AppIntf.SemGW→AppIntf.PatOrch:Ethernet:C6→C5
			AppIntf.PatOrch→AppIntf.SemGW:Ethernet:C5→C6
			Analysis.DCorr→AppIntf.SemGW:Ethernet:M1.C2→C6
			Analysis.Audio→AppIntf.SemGW:Ethernet:M1.C2→C6
			AppIntf.SemGW→Analysis.DCorr:Ethernet:C6→M1.C2
			AppIntf.SemGW→Analysis.Audio:Ethernet:C6→M1.C2))`,
		asgmt)
	assert.Equal(t, 78, stats.Combinations)
	assert.Equal(t, 0, stats.NoNodeFound)
	assert.Equal(t, 0, stats.CheckLinks)
	assert.Equal(t, 4, stats.NoPathFound)
	assert.Equal(t, 0, stats.LinkOverload)
	assert.Equal(t, 48, stats.PathSearches)
}

func TestSolve_semiotics2(t *testing.T) {
	_, err := solve("semiotics", "semiotics", "2")
	assert.NoError(t, err)
	assert.Equal(t, 108, stats.Combinations)
	assert.Equal(t, 0, stats.NoNodeFound)
	assert.Equal(t, 0, stats.CheckLinks)
	assert.Equal(t, 12, stats.NoPathFound)
	assert.Equal(t, 0, stats.LinkOverload)
	assert.Equal(t, 67, stats.PathSearches)
}

func TestSolve_semiotics4(t *testing.T) {
	_, err := solve("semiotics", "semiotics", "4")
	assert.NoError(t, err)
	assert.Equal(t, 148, stats.Combinations)
	assert.Equal(t, 0, stats.NoNodeFound)
	assert.Equal(t, 0, stats.CheckLinks)
	assert.Equal(t, 18, stats.NoPathFound)
	assert.Equal(t, 0, stats.LinkOverload)
	assert.Equal(t, 87, stats.PathSearches)
}

func TestSolve_semiotics5(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := solve("semiotics", "semiotics", "5")
	assert.NoError(t, err)
	equalAsgmt(t, `
		(assignment
		  (workloads
			SCADA.PlcData:C1 M1.Accel:M1.C2 M1.TSens:M1.C1
			M1.Audio:M1.C1 M1.Video:M1.C1 M2.Accel:M2.C2
			M2.TSens:M2.C1 M2.Audio:M2.C1 M2.Video:M2.C1
			M3.Accel:M3.C2 M3.TSens:M3.C1 M3.Audio:M3.C1
			M3.Video:M3.C1 M4.Accel:M4.C2 M4.TSens:M4.C1
			M4.Audio:M4.C1 M4.Video:M4.C1 M5.Accel:M5.C2
			M5.TSens:M5.C1 M5.Audio:M5.C1 M5.Video:M5.C1
			MDSP.CloudIntf:C5 SDN.Agent1:N3 SDN.Agent2:N4
			SDN.Agent3:N5 SDN.Agent4:N6 SCADA.SCADA:C2
			AppIntf.RecipeFW:C3 AppIntf.KnowRep:C4 SDN.Control:C6
			Analysis.Accel:C3 Analysis.Video:C4 AppIntf.PatOrch:C5
			SCADA.PlcEmerg:C1 Analysis.DCorr:C5 Analysis.Audio:C3
			MDSP.Connect:M1.C2 AppIntf.SemGW:M2.C2)
		  (channels
			M1.Accel→SCADA.SCADA:Ethernet:M1.C2→C2
			M1.TSens→SCADA.SCADA:Ethernet:M1.C1→C2
			M2.Accel→SCADA.SCADA:Ethernet:M2.C2→C2
			M2.TSens→SCADA.SCADA:Ethernet:M2.C1→C2
			M3.Accel→SCADA.SCADA:Ethernet:M3.C2→C2
			M3.TSens→SCADA.SCADA:Ethernet:M3.C1→C2
			M4.Accel→SCADA.SCADA:Ethernet:M4.C2→C2
			M4.TSens→SCADA.SCADA:Ethernet:M4.C1→C2
			M5.Accel→SCADA.SCADA:Ethernet:M5.C2→C2
			M5.TSens→SCADA.SCADA:Ethernet:M5.C1→C2
			AppIntf.RecipeFW→AppIntf.KnowRep:Ethernet:C3→C4
			AppIntf.KnowRep→AppIntf.RecipeFW:Ethernet:C4→C3
			SDN.Control→SDN.Agent1:Ethernet:C6→N3
			SDN.Control→SDN.Agent2:Ethernet:C6→N4
			SDN.Control→SDN.Agent3:Ethernet:C6→N5
			SDN.Control→SDN.Agent4:Ethernet:C6→N6
			SDN.Agent1→SDN.Control:Ethernet:N3→C6
			SDN.Agent2→SDN.Control:Ethernet:N4→C6
			SDN.Agent3→SDN.Control:Ethernet:N5→C6
			SDN.Agent4→SDN.Control:Ethernet:N6→C6
			M1.Accel→Analysis.Accel:Ethernet:M1.C2→C3
			M2.Accel→Analysis.Accel:Ethernet:M2.C2→C3
			M3.Accel→Analysis.Accel:Ethernet:M3.C2→C3
			M4.Accel→Analysis.Accel:Ethernet:M4.C2→C3
			M5.Accel→Analysis.Accel:Ethernet:M5.C2→C3
			M1.Video→Analysis.Video:Ethernet:M1.C1→C4
			M2.Video→Analysis.Video:Ethernet:M2.C1→C4
			M3.Video→Analysis.Video:Ethernet:M3.C1→C4
			M4.Video→Analysis.Video:Ethernet:M4.C1→C4
			M5.Video→Analysis.Video:Ethernet:M5.C1→C4
			AppIntf.PatOrch→AppIntf.RecipeFW:Ethernet:C5→C3
			AppIntf.PatOrch→AppIntf.KnowRep:Ethernet:C5→C4
			AppIntf.RecipeFW→AppIntf.PatOrch:Ethernet:C3→C5
			AppIntf.KnowRep→AppIntf.PatOrch:Ethernet:C4→C5
			SDN.Control→AppIntf.PatOrch:Ethernet:C6→C5
			AppIntf.PatOrch→SDN.Control:Ethernet:C5→C6
			SCADA.PlcEmerg→SCADA.PlcData:Loopback:C1→C1
			SCADA.PlcData→SCADA.PlcEmerg:Loopback:C1→C1
			SCADA.SCADA→SCADA.PlcEmerg:Ethernet:C2→C1
			SCADA.PlcEmerg→SCADA.SCADA:Ethernet:C1→C2
			SCADA.PlcEmerg→Analysis.Accel:Ethernet:C1→C3
			Analysis.Accel→SCADA.PlcEmerg:Ethernet:C3→C1
			M1.Accel→Analysis.DCorr:Ethernet:M1.C2→C5
			M1.TSens→Analysis.DCorr:Ethernet:M1.C1→C5
			M2.Accel→Analysis.DCorr:Ethernet:M2.C2→C5
			M2.TSens→Analysis.DCorr:Ethernet:M2.C1→C5
			M3.Accel→Analysis.DCorr:Ethernet:M3.C2→C5
			M3.TSens→Analysis.DCorr:Ethernet:M3.C1→C5
			M4.Accel→Analysis.DCorr:Ethernet:M4.C2→C5
			M4.TSens→Analysis.DCorr:Ethernet:M4.C1→C5
			M5.Accel→Analysis.DCorr:Ethernet:M5.C2→C5
			M5.TSens→Analysis.DCorr:Ethernet:M5.C1→C5
			M1.Audio→Analysis.Audio:Ethernet:M1.C1→C3
			M2.Audio→Analysis.Audio:Ethernet:M2.C1→C3
			M3.Audio→Analysis.Audio:Ethernet:M3.C1→C3
			M4.Audio→Analysis.Audio:Ethernet:M4.C1→C3
			M5.Audio→Analysis.Audio:Ethernet:M5.C1→C3
			MDSP.Connect→MDSP.CloudIntf:Ethernet:M1.C2→C5
			Analysis.Accel→MDSP.Connect:Ethernet:C3→M1.C2
			Analysis.Video→MDSP.Connect:Ethernet:C4→M1.C2
			Analysis.DCorr→MDSP.Connect:Ethernet:C5→M1.C2
			Analysis.Audio→MDSP.Connect:Ethernet:C3→M1.C2
			Analysis.Accel→AppIntf.SemGW:Ethernet:C3→M2.C2
			Analysis.Video→AppIntf.SemGW:Ethernet:C4→M2.C2
			AppIntf.SemGW→Analysis.Accel:Ethernet:M2.C2→C3
			AppIntf.SemGW→Analysis.Video:Ethernet:M2.C2→C4
			AppIntf.SemGW→AppIntf.PatOrch:Ethernet:M2.C2→C5
			AppIntf.PatOrch→AppIntf.SemGW:Ethernet:C5→M2.C2
			Analysis.DCorr→AppIntf.SemGW:Ethernet:C5→M2.C2
			Analysis.Audio→AppIntf.SemGW:Ethernet:C3→M2.C2
			AppIntf.SemGW→Analysis.DCorr:Ethernet:M2.C2→C5
			AppIntf.SemGW→Analysis.Audio:Ethernet:M2.C2→C3))`,
		asgmt)
	assert.Equal(t, 168, stats.Combinations)
	assert.Equal(t, 0, stats.NoNodeFound)
	assert.Equal(t, 0, stats.CheckLinks)
	assert.Equal(t, 21, stats.NoPathFound)
	assert.Equal(t, 0, stats.LinkOverload)
	assert.Equal(t, 97, stats.PathSearches)
}

func TestNewVarList_channelVars(t *testing.T) {
	state, err := justSolveM(data.GetModel("semiotics", "1"))
	assert.NoError(t, err)
	assert.Len(t, state.x, len(state.workloads)+len(state.channels))
	for i := range state.channels {
		ch := &state.channels[i]
		require.NotNil(t, ch.dv, "channel %d", i)
		assert.Equal(t, ch, ch.dv.channel)
	}
}

func TestExportedSolve_semiotics1(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := Solve(data.GetModel("semiotics", "1"))
	require.NoError(t, err)
	asgmt2, err := textformat.Parse("solution", `
		workload assignment v1 model solution
		application a workload w
		node c1,c2,c3,c4,c5,c6
		assignment
		workload SDN•Control on C6
		workload SDN•Agent1 on N3
		workload SDN•Agent2 on N4
		workload SDN•Agent3 on N5
		workload SDN•Agent4 on N6
		workload SCADA•SCADA on C2
		workload SCADA•PlcEmerg on C1
		workload SCADA•PlcData on C1
		workload Analysis•Accel on C3
		workload Analysis•DCorr on M1.C2
		workload Analysis•Audio on M1.C2
		workload Analysis•Video on C4
		workload MDSP•Connect on C5
		workload MDSP•CloudIntf on C5
		workload AppIntf•SemGW on C6
		workload AppIntf•PatOrch on C5
		workload AppIntf•RecipeFW on C3
		workload AppIntf•KnowRep on C4
		workload M1•Accel on M1.C2
		workload M1•TSens on M1.C1
		workload M1•Audio on M1.C1
		workload M1•Video on M1.C1

		channel SDN•Control→SDN•Agent1 on Ethernet•"C6→⁴N3"
		channel SDN•Control→SDN•Agent2 on Ethernet•"C6→³N4"
		channel SDN•Control→SDN•Agent3 on Ethernet•"C6→²N5"
		channel SDN•Control→SDN•Agent4 on Ethernet•"C6→³N6"
		channel SDN•Control→AppIntf•PatOrch on Ethernet•"C6→²C5"
		channel SDN•Agent1→SDN•Control on Ethernet•"N3→⁴C6"
		channel SDN•Agent2→SDN•Control on Ethernet•"N4→³C6"
		channel SDN•Agent3→SDN•Control on Ethernet•"N5→²C6"
		channel SDN•Agent4→SDN•Control on Ethernet•"N6→³C6"
		channel SCADA•SCADA→SCADA•PlcEmerg on Ethernet•"C2→³C1"
		channel SCADA•PlcEmerg→SCADA•SCADA on Ethernet•"C1→³C2"
		channel SCADA•PlcEmerg→SCADA•PlcData on Loopback•"C1→C1"
		channel SCADA•PlcEmerg→Analysis•Accel on Ethernet•"C1→⁶C3"
		channel SCADA•PlcData→SCADA•PlcEmerg on Loopback•"C1→C1"
		channel Analysis•Accel→SCADA•PlcEmerg on Ethernet•"C3→⁶C1"
		channel Analysis•Accel→MDSP•Connect on Ethernet•"C3→⁵C5"
		channel Analysis•Accel→AppIntf•SemGW on Ethernet•"C3→⁵C6"
		channel Analysis•DCorr→MDSP•Connect on Ethernet•"M1.C2→⁶C5"
		channel Analysis•DCorr→AppIntf•SemGW on Ethernet•"M1.C2→⁶C6"
		channel Analysis•Audio→MDSP•Connect on Ethernet•"M1.C2→⁶C5"
		channel Analysis•Audio→AppIntf•SemGW on Ethernet•"M1.C2→⁶C6"
		channel Analysis•Video→MDSP•Connect on Ethernet•"C4→⁵C5"
		channel Analysis•Video→AppIntf•SemGW on Ethernet•"C4→⁵C6"
		channel MDSP•Connect→MDSP•CloudIntf on Loopback•"C5→C5"
		channel AppIntf•SemGW→Analysis•Accel on Ethernet•"C6→⁵C3"
		channel AppIntf•SemGW→Analysis•DCorr on Ethernet•"C6→⁶M1.C2"
		channel AppIntf•SemGW→Analysis•Audio on Ethernet•"C6→⁶M1.C2"
		channel AppIntf•SemGW→Analysis•Video on Ethernet•"C6→⁵C4"
		channel AppIntf•SemGW→AppIntf•PatOrch on Ethernet•"C6→²C5"
		channel AppIntf•PatOrch→SDN•Control on Ethernet•"C5→²C6"
		channel AppIntf•PatOrch→AppIntf•SemGW on Ethernet•"C5→²C6"
		channel AppIntf•PatOrch→AppIntf•RecipeFW on Ethernet•"C5→⁵C3"
		channel AppIntf•PatOrch→AppIntf•KnowRep on Ethernet•"C5→⁵C4"
		channel AppIntf•RecipeFW→AppIntf•PatOrch on Ethernet•"C3→⁵C5"
		channel AppIntf•RecipeFW→AppIntf•KnowRep on Ethernet•"C3→²C4"
		channel AppIntf•KnowRep→AppIntf•PatOrch on Ethernet•"C4→⁵C5"
		channel AppIntf•KnowRep→AppIntf•RecipeFW on Ethernet•"C4→²C3"
		channel M1•Accel→SCADA•SCADA on Ethernet•"M1.C2→²C2"
		channel M1•Accel→Analysis•Accel on Ethernet•"M1.C2→⁵C3"
		channel M1•Accel→Analysis•DCorr on Loopback•"M1.C2→M1.C2"
		channel M1•TSens→SCADA•SCADA on Ethernet•"M1.C1→³C2"
		channel M1•TSens→Analysis•DCorr on Ethernet•"M1.C1→³M1.C2"
		channel M1•Audio→Analysis•Audio on Ethernet•"M1.C1→³M1.C2"
		channel M1•Video→Analysis•Video on Ethernet•"M1.C1→⁶C4"`)
	require.NoError(t, err)
	assert.Equal(t, asgmt2.Assignment[0], asgmt)
}

func benchmarkSemiotics(mills int, b *testing.B) {
	state, _ := newStateFromData("semiotics", strconv.Itoa(mills))
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		state.reset()
		state.solve()
	}
}

func BenchmarkSemiotics0(b *testing.B) { benchmarkSemiotics(0, b) }
func BenchmarkSemiotics1(b *testing.B) { benchmarkSemiotics(1, b) }
func BenchmarkSemiotics2(b *testing.B) { benchmarkSemiotics(2, b) }
func BenchmarkSemiotics3(b *testing.B) { benchmarkSemiotics(3, b) }
func BenchmarkSemiotics4(b *testing.B) { benchmarkSemiotics(4, b) }
func BenchmarkSemiotics5(b *testing.B) { benchmarkSemiotics(5, b) }

func TestSemiotics5_find1e7(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	defer restore(&textformat.CompoundNames, false)()
	state, err := prepStateFromData("semiotics", "5")
	require.NoError(t, err)
	// state.options.findMax = 10_000_000
	// state.solve()
	// assert.Equal(t, 117_850_038, state.stats.Combinations)
	// assert.Equal(t, 10_000_000, state.stats.Assignments)
	state.options.findMax = 1_000_000
	require.NoError(t, state.solve())
	assert.Equal(t, 11_830_672, state.stats.Combinations)
	assert.Equal(t, 1_000_000, state.stats.Assignments)
}

func BenchmarkSemiotics5_findMax(b *testing.B) {
	state, _ := newStateFromData("semiotics", "5")
	state.options.findMax = b.N
	state.solve()
}
