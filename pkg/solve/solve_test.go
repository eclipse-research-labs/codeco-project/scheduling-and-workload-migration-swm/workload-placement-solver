// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data"
	"siemens.com/qos-solver/pkg/textformat"
)

func prepStateFromText(input string) *state {
	s, err := newStateFromText("?", input)
	if err != nil {
		panic(err.Error())
	}
	if err = s.computeDomains(); err != nil {
		panic(err.Error())
	}
	return &s
}

func varListState() *state {
	return prepStateFromText(`/* one app, three nodes*/
		workload assignment v1
		model varList options implicit-reverse-path:no compound-names:no
		application A1
			workload W1 needs cpu:100 ram:1000
			workload W2 needs cpu:100 ram:1000 labels:(!C)
			workload W3 needs cpu:100 ram:1000 labels:(B)
			workload W4 needs cpu:100 ram:1000 labels:(A)
		channel W1—W2 needs lat:10 bw:100
		channel W2—W3 needs lat:10 bw:100
		channel W3—W4 needs lat:10 bw:100
		infrastructure
		node N1 provides cpu:1000 ram:8000 labels:(A)
		node N2 provides cpu:1000 ram:8000 labels:(B)
		node N3 provides cpu:1000 ram:8000 labels:(C)
		network ethernet
		link N1—N2,N1—N3,N2—N3 with lat:1 bw:1000
		paths(N1—N2 N1—N3 N2—N3)`)
}

func TestNewVarList(t *testing.T) {
	assert := assert.New(t)
	s := varListState()
	vl := newVarList(s)
	assert.Len(vl.is, 4)
	assert.Len(vl.ws, 4)
	assert.Len(vl.cs, 3*2)
	assert.Len(vl.ns, 3+2+1+1)
	assert.Len(vl.ps, 4+4+1+1+1+1)
	exp := []string{
		"(wlVarInfo (wlVar A1.W3 0@()) ())",
		"(wlVarInfo (wlVar A1.W4 0@()) ())",
		"(wlVarInfo (wlVar A1.W2 0@()) ())",
		"(wlVarInfo (wlVar A1.W1 0@()) ())"}
	for i := range vl.is {
		assert.Equal(exp[i], vl.is[i].String())
	}
	j := 0
	for i := range vl.ws {
		vl.ws[i].nodes = vl.ws[i].nodes[:1]
		assert.Equal(&vl.ns[j], &vl.ws[i].nodes[0])
		j += len(s.workloads[i].nodes)
	}
	assert.Equal(
		"(varList"+
			" (wlVarInfo (wlVar A1.W3 0@(nil)) ())"+
			" (wlVarInfo (wlVar A1.W4 0@(nil)) ())"+
			" (wlVarInfo (wlVar A1.W2 0@(nil)) ())"+
			" (wlVarInfo (wlVar A1.W1 0@(nil)) ()))",
		vl.String())
}

func TestVarListAddChannels(t *testing.T) {
	assert := assert.New(t)
	s := varListState()
	vl := newVarList(s)
	vl.addChannels()
	exp := []string{
		"(wlVarInfo (wlVar A1.W3 0@()) ())",
		"(wlVarInfo (wlVar A1.W4 0@()) " +
			"((chanVar A1.W3→A1.W4 0@()) (chanVar A1.W4→A1.W3 0@())))",
		"(wlVarInfo (wlVar A1.W2 0@()) " +
			"((chanVar A1.W2→A1.W3 0@()) (chanVar A1.W3→A1.W2 0@())))",
		"(wlVarInfo (wlVar A1.W1 0@()) " +
			"((chanVar A1.W1→A1.W2 0@()) (chanVar A1.W2→A1.W1 0@())))",
	}
	assert.Equal(len(vl.is), cap(vl.is))
	for i, exp := range exp {
		assert.Equal(exp, vl.is[i].String())
	}
	j := 0
	assert.Equal(len(vl.cs), cap(vl.cs))
	for i := range vl.cs {
		vl.cs[i].paths = vl.cs[i].paths[:1]
		assert.Equal(&vl.ps[j], &vl.cs[i].paths[0])
		for k := range s.channels[i].paths {
			j += len(s.channels[i].paths[k])
		}
	}
	assert.Equal(
		"(varList "+
			"(wlVarInfo (wlVar A1.W3 0@()) ()) "+
			"(wlVarInfo (wlVar A1.W4 0@())"+
			" ((chanVar A1.W3→A1.W4 0@(nil))"+
			" (chanVar A1.W4→A1.W3 0@(nil)))) "+
			"(wlVarInfo (wlVar A1.W2 0@())"+
			" ((chanVar A1.W2→A1.W3 0@(nil))"+
			" (chanVar A1.W3→A1.W2 0@(nil)))) "+
			"(wlVarInfo (wlVar A1.W1 0@())"+
			" ((chanVar A1.W1→A1.W2 0@(nil))"+
			" (chanVar A1.W2→A1.W1 0@(nil)))))",
		vl.String())
}

func TestVarListFlatten(t *testing.T) {
	assert := assert.New(t)
	s := varListState()
	vl := newVarList(s)
	vl.addChannels()
	vars := vl.flatten()
	exp := []string{
		"(wlVar A1.W3 0@())",
		"(wlVar A1.W4 0@())",
		"(chanVar A1.W3→A1.W4 0@())",
		"(chanVar A1.W4→A1.W3 0@())",
		"(wlVar A1.W2 0@())",
		"(chanVar A1.W2→A1.W3 0@())",
		"(chanVar A1.W3→A1.W2 0@())",
		"(wlVar A1.W1 0@())",
		"(chanVar A1.W1→A1.W2 0@())",
		"(chanVar A1.W2→A1.W1 0@())",
	}
	for i := range vars {
		assert.Equal(exp[i], fmt.Sprintf("%s", vars[i]))
	}
}

func BenchmarkVarListState(b *testing.B) {
	for i := 0; i < b.N; i++ {
		varListState()
	}
}

func BenchmarkVarListUsage(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s := varListState()
		vl := newVarList(s)
		vl.addChannels()
		vl.flatten()
	}
}

func justSolveM(model *grpc.Model) (state, error) {
	state, err := prepStateFromModel(model)
	if err == nil {
		err = state.solve()
	}
	return state, err
}

func justSolve(a, n interface{}, args []string) (state, error) {
	var name string
	var model grpc.Model
	switch x := a.(type) {
	case string:
		model = *data.GetModel(x, args...)
		name = x
	case *grpc.ApplicationGroup:
		model.AppGroup = x
	default:
		panic("unknown type")
	}
	switch x := n.(type) {
	case string:
		if x != name {
			m2 := data.GetModel(x, args...)
			model.Infrastructure = m2.Infrastructure
		}
	case *grpc.Infrastructure:
		model.Infrastructure = x
	default:
		panic("unknown type")
	}
	return justSolveM(&model)
}

func solve(a, n interface{}, args ...string) (assignment, error) {
	state, err := justSolve(a, n, args)
	if err != nil {
		return assignment{}, err
	}
	return state.assignment(), nil
}

func solveStable(a, n interface{}, args ...string) (assignment, error) {
	state, err := justSolve(a, n, args)
	if err != nil {
		return assignment{}, err
	}
	return state.assignmentStable(), nil
}

func TestAssignmentToGrpc(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	s := prepStateFromText(`
		workload assignment v1
		model test
		application A1
			workload W1,W2 needs cpu:100 ram:1000
		infrastructure
		node N1,N2 provides cpu:100 ram:8000`)
	s.x = []decisionVariable{
		makeWlVar(s, wlIndex(0), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(s, wlIndex(1), 1, nodeIndex(0), nodeIndex(1))}
	assert.Equal(t,
		&grpc.Assignment{
			Workloads: []*grpc.Assignment_Workload{
				{Application: "A1", Workload: "W1", Node: "N1"},
				{Application: "A1", Workload: "W2", Node: "N2"}}},
		s.assignment().toGrpc())
}

func TestAssignmentString(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	s := prepStateFromText(`
		workload assignment v1
		model test
		application A1
			workload W1,W2 needs cpu:100 ram:1000
		channel W1→W2 needs lat:3 bw:100
		node N1,N2 provides cpu:100 ram:8000
		network net link N1→N2 with lat:1 bw:1000 path:yes`)
	s.x = []decisionVariable{
		makeWlVar(s, wlIndex(0), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(s, wlIndex(1), 1, nodeIndex(0), nodeIndex(1)),
		makeChanVar(s, chanIndex(0), 0, pathIndex(0))}
	assert.Equal(t,
		"(assignment "+
			"(workloads A1.W1:N1 A1.W2:N2) "+
			"(channels A1.W1→A1.W2:net:N1→N2))",
		s.assignment().String())

	s.x = []decisionVariable{
		makeWlVar(s, wlIndex(0), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(s, wlIndex(1), 0),
		makeChanVar(s, chanIndex(0), 0)}
	assert.Equal(t,
		`(assignment (workloads A1.W1:N1) (channels))`,
		s.assignment().String())

	s.x = []decisionVariable{
		makeWlVar(s, wlIndex(0), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(s, wlIndex(1), 2, nodeIndex(0), nodeIndex(1)),
		makeChanVar(s, chanIndex(0), 0)}
	assert.Equal(t,
		`(assignment (workloads A1.W1:N1) (channels))`,
		s.assignment().String())
}

func TestStateRemoveLastApplication_channelsToLast(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	s := prepStateFromText(`
		workload assignment v1
		model channelsToLast
		application A1 workload W1 needs cpu:100 ram:5000
		application A2 workload W1 needs cpu:100 ram:5000
		application A3 workload W1 needs cpu:100 ram:5000
		channel A1•W1→A2•W1 needs lat:5 bw:100
		channel A2•W1→A3•W1 needs lat:5 bw:100
		node C1 provides cpu:1000 ram:8000
		network loopback link C1→C1 with lat:1 bw:10000 path:yes`)
	assert.NoError(t, s.pathsForChannels())
	assert.False(t, s.removeLastApplication())

	s = prepStateFromText(`
		workload assignment v1
		model channelsToLast
		application A1 workload W1 needs cpu:100 ram:5000
		application A2 workload W1 needs cpu:100 ram:5000
		application A3 workload W1 needs cpu:100 ram:5000
		channel A1•W1→A3•W1 needs lat:5 bw:100
		channel A2•W1→A1•W1 needs lat:5 bw:100
		node C1 provides cpu:1000 ram:8000
		network loopback link C1→C1 with lat:1 bw:10000 path:yes`)
	assert.NoError(t, s.pathsForChannels())
	assert.False(t, s.removeLastApplication())
}

func TestEmptyModel(t *testing.T) {
	check := func(ag *grpc.ApplicationGroup, im *grpc.Infrastructure) {
		asgmt, err := solve(ag, im)
		assert.Zero(t, len(asgmt.ws))
		assert.Zero(t, len(asgmt.cs))
		assert.ErrorContains(t, err, "empty application model")
	}
	check(nil, nil)
	check(&grpc.ApplicationGroup{}, &grpc.Infrastructure{})
}

func parse(input string) *grpc.Model {
	var model grpc.Model
	if err := textformat.Convert("varList", input, &model); err != nil {
		panic("cannot convert model: " + err.Error())
	}
	return &model
}
func parseAndSolve(input string) (assignment, error) {
	model := parse(input)
	return solve(model.AppGroup, model.Infrastructure)
}
func parseAndSolveStable(input string) (assignment, error) {
	model := parse(input)
	return solveStable(model.AppGroup, model.Infrastructure)
}

func TestSolve_errorWithNewState(t *testing.T) {
	assert := assert.New(t)
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := parseAndSolve(`
		workload assignment v1
		model errorWithNewState
		application A1 workload W1,W1
		node dummy`)
	assert.Len(asgmt.ws, 0)
	assert.Len(asgmt.cs, 0)
	assert.ErrorContains(err, `duplicate workload: A1.W1`)
}

func TestSolve_noNodesForWorkload(t *testing.T) {
	assert := assert.New(t)
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := parseAndSolve(`
		workload assignment v1
		model noNodesForWorkload
		application A1 workload W1 needs cpu:2000 ram:16000
		node C1 provides cpu:1000 ram:8000`)
	assert.Len(asgmt.ws, 0)
	assert.Len(asgmt.cs, 0)
	assert.ErrorContains(err, `no node for workload A1.W1`)
}

func TestSolve(t *testing.T) {
	assert := assert.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := parseAndSolve(`
		workload assignment v1
		model noNodesForWorkload
		application A1
		workload W1,W2,W3 needs cpu:500 ram:4000
		channel W1→W2,W2→W3,W3→W1 needs lat:5 bw:100
		node C1,C2,C3 provides cpu:1000 ram:8000
		network net link C1↔C2,C1↔C3,C2↔C3 with lat:3 bw:200 path:yes`)
	assert.NoError(err)
	assert.Equal(
		"(assignment "+
			"(workloads A1.W1:C1 A1.W2:C2 A1.W3:C3) "+
			"(channels A1.W1→A1.W2:net:C1→C2 A1.W2→A1.W3:net:C2→C3"+
			" A1.W3→A1.W1:net:C3→C1))",
		asgmt.String())
}

func TestSolve_MAppsNw(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	assert := assert.New(t)
	asgmt, err := solve("multiple-apps", "multiple-apps")
	assert.NoError(err)
	assert.Equal(
		"(assignment "+
			"(workloads App2.W3:C4 App3.W4:C6 App3.W5:C7 App1.W1:C3"+
			" App1.W2:C1 App3.W6:C5) "+
			"(channels App3.W4→App3.W5:eth:C6→C7 App3.W5→App3.W4:eth:C7→C6"+
			" App1.W1→App1.W2:eth:C3→C1 App1.W2→App1.W1:eth:C1→C3"+
			" App3.W5→App3.W6:eth:C7→C5 App3.W6→App3.W5:eth:C5→C7))",
		asgmt.String())
}

func TestSolveFourAndFour(t *testing.T) {
	assert := assert.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := parseAndSolve(`
		workload assignment v1
		model fourAndFour
		application A1
		workload W1,W2,W3,W4 needs cpu:900 ram:4000
		channel W1→W3,W3→W4 needs lat:1 bw:900
		channel W2→W3,W1→W4 needs lat:2 bw:150
		node C1,C2,C3,C4 provides cpu:1000 ram:8000
		network net
		link C2→C1,C3→C1,C4→C3,C4→C2
		     with lat:1 bw:1000 path:yes`)
	assert.ErrorContains(err, `no path for channel "A1.W1→A1.W3"`)
	assert.Equal(assignment{}, asgmt)
}

func TestSolveFourAndFour2(t *testing.T) {
	assert := assert.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := parseAndSolve(`
		workload assignment v1
		model FourAndFour2
		application A1
		workload W1,W2,W3,W4 needs cpu:900 ram:4000
		channel W1→W3,W2→W4 needs lat:1 bw:900
		channel W3→W2,W1→W4 needs lat:2 bw:50
		node C1,C2,C3,C4 provides cpu:1000 ram:8000
		network net
		link C2→C1,C4→C3 with lat:2 bw:100 path:yes
		link C3→C1,C2→C4 with lat:1 bw:1000 path:yes`)
	assert.NoError(err)
	assert.Equal(
		"(assignment "+
			"(workloads A1.W2:C3 A1.W3:C4 A1.W1:C2 A1.W4:C1) "+
			"(channels A1.W3→A1.W2:net:C4→C3 A1.W1→A1.W3:net:C2→C4"+
			" A1.W2→A1.W4:net:C3→C1 A1.W1→A1.W4:net:C2→C1))",
		asgmt.String())
}

func TestSolve_noSolution(t *testing.T) {
	_, err := parseAndSolve(`
		workload assignment v1
		model noSolution
		application A1 workload W1,W2 needs cpu:1000 ram:8000
		application A2 workload W1,W2 needs cpu:1000 ram:8000
		node C1 provides cpu:1500 ram:12000
		network loopback link C1→C1 with lat:1 bw:1000 path:yes`)
	assert.ErrorContains(t, err, `no assignment found`)
}

var twoIsolatedAppsAndNodes string = `
	workload assignment v1
	model twoIsolatedAppsAndNodes
	application A1
		workload W1 needs cpu:250 ram:1000
		workload W2 needs cpu:500 ram:2000
		channel W1→W2 needs lat:5 bw:100
	application A2
		workload W3 needs cpu:250 ram:1000 labels:(B)
		workload W4 needs cpu:500 ram:2000 labels:(B)
		channel W3→W4 needs lat:5 bw:100
	node N1 provides cpu:1000 ram:8000
	node N2 provides cpu:1000 ram:8000 labels:(B)
	network net
	link N1→N1,N2→N2 with lat:2 bw:1000 path:yes

	assignment
	workload A1•W1 on N1
	workload A1•W2 on N1
	workload A2•W3 on N2
	workload A2•W4 on N2
	channel A1•W1→A1•W2 on net•"N1→N1"
	channel A2•W3→A2•W4 on net•"N2→N2"`

func TestSolveTwoIsolatedApps(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	asgmt, err := parseAndSolve(twoIsolatedAppsAndNodes)
	assert.NoError(t, err)
	exp := assignment{
		stable: false,
		ws: []wlAssignment{
			{&asgmt.s.apps[1].workloads[0], &asgmt.s.nodes[1]},
			{&asgmt.s.apps[1].workloads[1], &asgmt.s.nodes[1]},
			{&asgmt.s.apps[0].workloads[0], &asgmt.s.nodes[0]},
			{&asgmt.s.apps[0].workloads[1], &asgmt.s.nodes[0]}},
		cs: []channelAssignment{
			{&asgmt.s.channels[1], &asgmt.s.networks[0].paths[1]},
			{&asgmt.s.channels[0], &asgmt.s.networks[0].paths[0]}}}
	asgmt.s = nil
	assert.Equal(t, exp, asgmt)

	asgmt, err = parseAndSolveStable(twoIsolatedAppsAndNodes)
	assert.NoError(t, err)
	exp = assignment{
		stable: true,
		ws: []wlAssignment{
			{&asgmt.s.apps[0].workloads[0], &asgmt.s.nodes[0]},
			{&asgmt.s.apps[0].workloads[1], &asgmt.s.nodes[0]},
			{&asgmt.s.apps[1].workloads[0], &asgmt.s.nodes[1]},
			{&asgmt.s.apps[1].workloads[1], &asgmt.s.nodes[1]}},
		cs: []channelAssignment{
			{&asgmt.s.channels[0], &asgmt.s.networks[0].paths[0]},
			{&asgmt.s.channels[1], &asgmt.s.networks[0].paths[1]}}}
	asgmt.s = nil
	assert.Equal(t, exp, asgmt)
}

func TestExportedSolve_happyCase(t *testing.T) {
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.CompoundNames, false)()
	model := parse(twoIsolatedAppsAndNodes)
	asgmt, err := Solve(model)
	assert.NoError(t, err)

	asgmt2, err := textformat.Parse("asgmts", twoIsolatedAppsAndNodes)
	require.NoError(t, err)
	assert.Equal(t, asgmt2.Assignment[0], asgmt)

	stats := Statistics()
	assert.Equal(t, 6, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)
}

func TestExportedSolve_errorYieldsEmptyAssignment(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	model := parse(`
		workload assignment v1
		model errorYieldsEmptyAssignment
		application A1
			workload W1,W2 needs cpu:500 ram:2000
			channel W1→Wx needs lat:5 bw:100
		node N1 provides cpu:1000 ram:8000
		network net link N1→N1 with lat:2 bw:1000 path:yes`)
	asgmt, err := Solve(model)
	assert.ErrorContains(t, err, "unknown workload: A1.Wx")
	assert.Nil(t, asgmt)
	stats := Statistics()
	assert.Zero(t, stats.Combinations)
	assert.Zero(t, stats.CheckLinks)

	model = parse(`
		workload assignment v1
		model noSolution
		application A1
			workload W1,W2 needs cpu:500 ram:5000
			channel W1→W2 needs lat:5 bw:100
		node N1 provides cpu:1000 ram:8000
		network net link N1→N1 with lat:2 bw:1000 path:yes`)
	asgmt, err = Solve(model)
	assert.ErrorContains(t, err, "no assignment found")
	assert.Nil(t, asgmt)
	stats = Statistics()
	assert.Equal(t, 2, stats.Combinations)
	assert.Zero(t, stats.CheckLinks)
}

func TestStatistics(t *testing.T) {
	defer restore(&textformat.CompoundNames, false)()
	problem, err := textformat.Parse("test-stats", `
		workload assignment v1
		model test-stats
		application A1
			workload W1,W2 needs cpu:500 ram:2000
			channel W1→W2 needs lat:5 bw:100
		node N1 provides cpu:1000 ram:8000
		network net link N1→N1 with lat:2 bw:1000 path:yes
		assignment
		workload A1•W1 on N1
		workload A1•W2 on N1
		channel A1•W1→A1•W2 on net•"N1→N1"`)
	require.NoError(t, err)
	sol, err := Solve(&grpc.Model{
		AppGroup:        problem.AppGroup,
		Infrastructure:  problem.Infrastructure,
		Recommendations: problem.Recommendations})
	assert.NoError(t, err)
	assert.Equal(t, problem.Assignment[0], sol)
	stats := Statistics()
	assert.Equal(t, 3, stats.Combinations)
	assert.Equal(t, 0, stats.CheckLinks)
}
