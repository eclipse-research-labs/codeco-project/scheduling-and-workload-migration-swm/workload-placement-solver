// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"bytes"
	"fmt"
)

type nodeInfo struct {
	node     *node
	priority priority
}

type wlVar struct {
	workload *workload
	s        *state     // context for variable
	curNode  int        // index of current node
	nodes    []nodeInfo // suitable nodes
}

func (v wlVar) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "(wlVar %s %d@(", v.s.nameOf(v.workload), v.curNode)
	for i, n := range v.nodes {
		if i > 0 {
			buf.WriteRune(' ')
		}
		if n.node == nil {
			buf.WriteString("nil")
		} else {
			fmt.Fprintf(&buf, "%s", n.node.name)
		}
	}
	fmt.Fprintf(&buf, "))")
	return buf.String()
}

// A decision variable for a workload is less than another variable if it
// has less suitable nodes.  If two variables have the same number of nodes,
// a variable is less than another variable if it is defined earlier.
func (v *wlVar) less(w *wlVar) bool {
	ni := len(v.workload.nodes)
	nj := len(w.workload.nodes)
	return ni < nj || ni == nj && v.workload.index < w.workload.index
}

func (v *wlVar) init(s *state, wl wlIndex, nodes []nodeInfo) *wlVar {
	*v = wlVar{s: s, workload: &s.workloads[wl],
		curNode: len(nodes), nodes: nodes}
	return v
}

func newWlVar(s *state, wl wlIndex) *wlVar {
	v := wlVar{}
	return v.init(s, wl, make([]nodeInfo, 0, len(s.workloads[wl].nodes)))
}

func (v *wlVar) nodeIndex() nodeIndex {
	if v.curNode >= len(v.nodes) {
		return -1
	}
	return v.nodes[v.curNode].node.index
}

func (v *wlVar) nodePriority(node *node) priority {
	if len(v.workload.priorities) > 0 {
		for i := range v.workload.priorities {
			if v.workload.priorities[i].node == node.index {
				return v.workload.priorities[i].value
			}
		}
		return v.workload.defaultPrio
	} else {
		return nodePriority(v.workload, node)
	}
}

func (v *wlVar) getDomain() {
	v.nodes, v.curNode = v.nodes[:len(v.workload.nodes)], 0
	for i := range v.workload.nodes {
		node := &v.s.nodes[v.workload.nodes[i]]
		p, j := v.nodePriority(node), i-1
		for ; 0 <= j && p > v.nodes[j].priority; j-- {
			v.nodes[j+1] = v.nodes[j]
		}
		v.nodes[j+1] = nodeInfo{node, p}
	}
}

func (v *wlVar) stateChange() bool {
	node := v.nodes[v.curNode].node
	ok := node.cpu.Use(v.workload.cpu)
	ok = node.ram.Use(v.workload.ram) && ok
	ok = node.in.Use(v.workload.in) && ok
	return node.out.Use(v.workload.out) && ok
}

func (v *wlVar) rollback() {
	node := v.nodes[v.curNode].node
	node.cpu.Free(v.workload.cpu)
	node.ram.Free(v.workload.ram)
	node.in.Free(v.workload.in)
	node.out.Free(v.workload.out)
}

func (v *wlVar) exhausted() bool {
	return v.curNode >= len(v.nodes)
}

func (v *wlVar) next() {
	if v.curNode < len(v.nodes) {
		v.curNode++
	}
}

func (v *wlVar) assignment() (a wlAssignment) {
	return wlAssignment{
		workload: v.workload,
		node:     v.nodes[v.curNode].node}
}
