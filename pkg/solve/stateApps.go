// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"fmt"
	"math"
	"sort"

	"siemens.com/qos-solver/internal/port/grpc"
)

type application struct {
	name      string
	base      wlIndex    // start of workloads
	workloads []workload // subset of state.workloads
	channels  []channel  // subset of state.channels
}

func (a *application) setChannels(cs []channel) {
	a.channels = cs
	i := 0
	for lo, hi := 0, 0; lo < len(cs); lo = hi {
		for hi < len(cs) && cs[hi].src == cs[lo].src {
			hi++
		}
		for a.base+wlIndex(i) < cs[lo].src {
			a.workloads[i].channels = nil
			i++
		}
		a.workloads[i].channels = cs[lo:hi:hi]
		i++
	}
	for ; i < len(a.workloads); i++ {
		a.workloads[i].channels = nil
	}
}

type milliCPU float64
type ramMB float64
type latency int
type bandwidth int
type serviceClass int

const (
	scBestEffort serviceClass = iota
	scAssured
)

func serviceClassFromGrpc(c grpc.ServiceClass) serviceClass {
	if c == grpc.ServiceClass_ASSURED {
		return scAssured
	}
	return scBestEffort
}

type nodePrio struct {
	node  nodeIndex
	value priority
}

type workload struct {
	name        string
	app         appIndex
	index       wlIndex
	cpu         milliCPU    // required CPU
	ram         ramMB       // required RAM
	lat         latency     // minimum latency of channels
	in          bandwidth   // incoming bandwidth
	out         bandwidth   // outgoing bandwidth
	channels    []channel   // outgoing channels
	labels      []nameIndex // negative index means "forbidden"
	nodes       []nodeIndex // suitable nodes
	priorities  []nodePrio  // user priorities (cost or recommendation)
	defaultPrio priority    // default user priority
	dv          *wlVar
}

func (w *workload) runsOn(node nodeIndex) bool {
	for _, worker := range w.nodes {
		if node == worker {
			return true
		}
	}
	return false
}

type channel struct {
	pair[wlIndex]
	name  string
	index chanIndex
	lat   latency      // required latency
	bw    bandwidth    // required bandwidth
	qos   serviceClass // required QoS class
	paths [][]pathIndex
	dv    *chanVar
}

func (s *state) allocateApps(ag *grpc.ApplicationGroup) {
	if ag == nil {
		return
	}
	wc := 0
	for i := range ag.Applications {
		wc += len(ag.Applications[i].Workloads)
	}
	s.apps = make([]application, 0, len(ag.Applications))
	s.workloads = make([]workload, 0, wc)
	s.channels = make([]channel, 0, len(ag.Channels))
}

func (s *state) addApplication(a *grpc.Application) error {
	err := s.checkAppName(a.Id)
	if err != nil {
		return err
	}
	if len(a.Workloads) == 0 {
		return empty("application", a.Id)
	}

	w1 := len(s.workloads)
	sa := application{name: a.Id, base: wlIndex(w1)}
	for n, w := range a.Workloads {
		err := s.checkWlName(&sa, w.Id)
		if err != nil {
			s.workloads = s.workloads[:w1]
			return err
		}
		sw := workload{app: appIndex(len(s.apps)), name: w.Id,
			index: wlIndex(w1 + n),
			cpu:   milliCPU(w.CPU),
			ram:   ramMB(w.Memory)}
		sw.labels, err = s.addLabels(w.RequiredLabels, w.ForbiddenLabels)
		if err != nil {
			s.workloads = s.workloads[:w1]
			return err
		}
		s.workloads = append(s.workloads, sw)
	}
	hi := w1 + len(a.Workloads)
	sa.workloads = s.workloads[w1:hi:hi]
	s.apps = append(s.apps, sa)
	return nil
}

func (s *state) findWorkload(app, workload string) (wlIndex, error) {
	base := wlIndex(0)
	for i := range s.apps {
		if s.apps[i].name == app {
			for j, w := range s.apps[i].workloads {
				if w.name == workload {
					return base + wlIndex(j), nil
				}
			}
			return -1, unknown("workload", app, ".", workload)
		}
		base += wlIndex(len(s.apps[i].workloads))
	}
	return -1, unknown("application", app)
}

func (s *state) addChannel(ch *grpc.Channel) error {
	src, err := s.findWorkload(ch.SourceApplication, ch.SourceWorkload)
	if err != nil {
		return err
	}
	dst, err := s.findWorkload(ch.TargetApplication, ch.TargetWorkload)
	if err != nil {
		return err
	}
	s.channels = append(s.channels,
		channel{pair: pair[wlIndex]{src, dst},
			name: ch.Id,
			lat:  latency(ch.Latency),
			bw:   bandwidth(ch.Bandwidth),
			qos:  serviceClassFromGrpc(ch.Qos)})
	return nil
}

func (s *state) addAppModel(am *grpc.ApplicationGroup) error {
	if am == nil {
		return nil
	}
	for _, a := range am.Applications {
		if err := s.addApplication(a); err != nil {
			return err
		}
	}
	for _, ch := range am.Channels {
		if err := s.addChannel(ch); err != nil {
			return err
		}
	}
	sort.Slice(s.channels, func(i, j int) bool {
		ci, cj := &s.channels[i], &s.channels[j]
		return ci.src < cj.src || ci.src == cj.src && ci.dst < cj.dst
	})
	if len(s.channels) > 0 {
		for i := range s.channels {
			s.channels[i].index = chanIndex(i)
		}
		chSrcApp := func(i chanIndex) appIndex {
			return s.workloads[s.channels[i].src].app
		}
		i, lo, hi := appIndex(0), chanIndex(0), chanIndex(0)
		for ; int(lo) < len(s.channels); lo = hi {
			app := chSrcApp(lo)
			for int(hi) < len(s.channels) && chSrcApp(hi) == app {
				hi++
			}
			for i < app {
				s.apps[i].setChannels(nil)
				i++
			}
			s.apps[i].setChannels(s.channels[lo:hi:hi])
			i++
		}
		for ; int(i) < len(s.apps); i++ {
			s.apps[i].setChannels(nil)
		}
	}
	return nil
}

func nodeFinder(s *state) func(string) (nodeIndex, error) {
	type result struct {
		idx nodeIndex
		err error
	}
	cache := make(map[string]result, len(s.nodes))
	return func(name string) (nodeIndex, error) {
		if r, ok := cache[name]; ok {
			return r.idx, r.err
		}
		idx, err := s.findNode(name)
		cache[name] = result{idx, err}
		return idx, err
	}
}

func (s *state) addCosts(cs *grpc.Cost) error {
	if cs.Empty() {
		return nil
	}
	M := 0.0
	findNode := nodeFinder(s)
	addNode := func(node *grpc.NodeValue, bs bitset) error {
		if node.Value < 0 {
			return fmt.Errorf("value out of bounds: %f", node.Value)
		}
		idx, err := findNode(node.Node)
		if err == nil {
			bs.set(uint(idx))
			M = math.Max(node.Value, M)
		}
		return err
	}
	nb := newBitset(uint(len(s.nodes)))
	for _, ni := range cs.Nodes {
		if err := addNode(ni, nb); err != nil {
			return err
		}
	}
	wb := newBitset(uint(len(s.nodes)))
	u := newBitset(uint(len(s.nodes)))
	cost2priority := func(value float64) priority {
		return priority(scale * (1.0 - value/M))
	}
	for _, wc := range cs.Workloads {
		wb.clear()
		for _, c := range wc.Nodes {
			if err := addNode(c, wb); err != nil {
				return err
			}
		}
		wi, err := s.findWorkload(wc.Application, wc.Workload)
		if err != nil {
			return err
		}
		u.assign(wb)
		u.union(nb)
		nr := make([]nodePrio, u.popCount())
		for k, ni := range wc.Nodes {
			idx, _ := findNode(ni.Node)
			nr[k] = nodePrio{idx, cost2priority(ni.Value)}
		}
		j := len(wc.Nodes)
		for _, ni := range cs.Nodes {
			if idx, _ := findNode(ni.Node); !wb.isSet(uint(idx)) {
				nr[j] = nodePrio{idx, cost2priority(ni.Value)}
				j++
			}
		}
		s.workloads[wi].priorities = nr
		s.workloads[wi].defaultPrio = scale
	}
	return nil
}

func (s *state) addRecs(rs *grpc.Recommendation) error {
	if rs.Empty() {
		return nil
	}
	n := 0
	for _, wr := range rs.Workloads {
		n += len(wr.Nodes)
	}
	findNode := nodeFinder(s)
	is, j := make([]nodePrio, n), 0
	rec2priority := func(value float64) priority {
		return priority(scale * (value / 100))
	}
	for _, wr := range rs.Workloads {
		wi, err := s.findWorkload(wr.Application, wr.Workload)
		if err != nil {
			return err
		}
		nr := is[j : j+len(wr.Nodes) : j+len(wr.Nodes)]
		for k := range wr.Nodes {
			if idx, err := findNode(wr.Nodes[k].Node); err != nil {
				return err
			} else {
				val := wr.Nodes[k].Value
				if val < 0 || 100 < val {
					return fmt.Errorf("value out of bounds: %f", val)
				}
				nr[k] = nodePrio{idx, rec2priority(val)}
			}
		}
		s.workloads[wi].priorities = nr
		s.workloads[wi].defaultPrio = 0
		j += len(nr)
	}
	return nil
}
