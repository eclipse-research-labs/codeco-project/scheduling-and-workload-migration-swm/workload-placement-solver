// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/data"
)

func killLeadingWhiteSpace(s string) string {
	re := regexp.MustCompile("[ \t]*\n[ \t]*")
	s = re.ReplaceAllString(s, " ")
	if len(s) > 0 {
		if s[0] == ' ' {
			return s[1:]
		}
	}
	return s
}

func TestKillLeadingWhiteSpace(t *testing.T) {
	assert.Equal(t, "", killLeadingWhiteSpace(""))
	assert.Equal(t, "", killLeadingWhiteSpace("\n  "))
	assert.Equal(t, "(x)", killLeadingWhiteSpace("\n  (x)"))
	assert.Equal(t,
		"(x y z)", killLeadingWhiteSpace("\n  (x  \n    y  \n   z)"))
}

func equalAsgmt(
	t *testing.T, expected string, asgmt assignment,
	msgAndArgs ...interface{},
) {
	t.Helper()
	assert.Equal(
		t, killLeadingWhiteSpace(expected), asgmt.String(), msgAndArgs...)
}

func solveBuiltIn(t *testing.T, name, asgmt string) {
	state, err := justSolveM(data.GetModel(name))
	t.Helper()
	require.NoError(t, err)
	equalAsgmt(t, asgmt, state.assignmentStable())
}

func TestSolver_tiny(t *testing.T) {
	solveBuiltIn(t, "tiny", `
		(assignment
		  (workloads a1.w1:c2 a1.w2:c1)
		  (channels a1.w1→a1.w2:Ethernet:c2→c1))`)
}

func TestSolver_multipleApps(t *testing.T) {
	solveBuiltIn(t, "multiple-apps", `
		(assignment
		  (workloads
			App1.W1:C3 App1.W2:C1
			App2.W3:C4
			App3.W4:C6 App3.W5:C7 App3.W6:C5)
		  (channels
			App1.W1→App1.W2:eth:C3→C1
			App1.W2→App1.W1:eth:C1→C3
			App3.W4→App3.W5:eth:C6→C7
			App3.W5→App3.W4:eth:C7→C6
			App3.W5→App3.W6:eth:C7→C5
			App3.W6→App3.W5:eth:C5→C7))`)
}

func TestSolver_qosDemo(t *testing.T) {
	solveBuiltIn(t, "qos-demo", `
		(assignment
		  (workloads
			app1.w1:c1 app1.w2:c2
			app2.w4:c1 app2.w5:c1 app2.w6:c1) 
		  (channels
			app1.w1→app1.w2:tsn:c1→c2
			app1.w2→app1.w1:Ethernet:c2→c1
			app2.w4→app2.w5:Loopback:c1→c1
			app2.w5→app2.w4:Loopback:c1→c1
			app2.w5→app2.w6:Loopback:c1→c1))`)
}

func TestSolve_sphereControl(t *testing.T) {
	solveBuiltIn(t, "sphere-control", `
		(assignment
		  (workloads
			sphere.webcam:micro
			sphere.processor:micro
			sphere.controller:nano
			dashboard.prometheus:rasp4
			dashboard.grafana:rasp4)
		  (channels
			sphere.webcam→sphere.processor:alo:micro→micro
			sphere.webcam→dashboard.prometheus:eth:micro→rasp4
			sphere.processor→sphere.controller:vlan:micro→nano
			sphere.processor→dashboard.prometheus:eth:micro→rasp4
			sphere.controller→dashboard.prometheus:eth:nano→rasp4
			dashboard.prometheus→sphere.webcam:eth:rasp4→micro
			dashboard.prometheus→sphere.processor:eth:rasp4→micro
			dashboard.prometheus→sphere.controller:eth:rasp4→nano
			dashboard.prometheus→dashboard.grafana:lo:rasp4→rasp4
			dashboard.grafana→dashboard.prometheus:lo:rasp4→rasp4))`)
}
