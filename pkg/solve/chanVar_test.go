// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/textformat"
)

func makeChanVar(s *state, ch chanIndex, cn int, paths ...pathIndex) *chanVar {
	v := newChanVar(s, ch)
	v.curPath = cn
	if len(paths) > 0 {
		v.paths = make([]*path, len(paths))
		for i := range paths {
			v.paths[i] = &s.networks[0].paths[i]
		}
	}
	return v
}

func getChState(t *testing.T) *state {
	t.Helper()
	s, err := newStateFromText("ch-state", `
		workload assignment v1
		model ch-state options implicit-reverse-path:no
		application A1
		workload W1,W2 needs cpu:100 ram:1000
		channel W1->W2 needs lat:3 bw:10
		node C1,C2,C3 provides cpu:1000 ram:8000
		network net
		node N1,N2
		link C1•C2,C1•C3,C1•N1,N1•C2,N1•N2,N2•C2 with lat:1 bw:100
		paths (C1•N1•C2 C1•N1•N2•C2 C1•C2 C1•C3)`)
	require.NoError(t, err)
	err = s.computeDomains()
	require.NoError(t, err)
	s.x = []decisionVariable{
		makeWlVar(&s, wlIndex(0), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(&s, wlIndex(1), 1, nodeIndex(0), nodeIndex(1))}
	return &s
}

func TestChVarGetDomain(t *testing.T) {
	defer restore(&textformat.ShortestPathsOnly, false)()
	s := getChState(t)
	v := newChanVar(s, chanIndex(0))
	v.getDomain()
	assert.Equal(t,
		[]*path{&s.networks[0].paths[2]},
		v.paths)
}

func getChState2(t *testing.T) *state {
	t.Helper()
	s, err := newStateFromText("ch-state-2", `
		workload assignment v1
		model ch-state-2 options implicit-reverse-path:no
		application A1
		workload W1 needs cpu:100 ram:1000
		workload W2 needs cpu:100 ram:1000
		workload W3 needs cpu:100 ram:1000
		channel W1→W3 needs lat:3 bw:75
		channel W2→W3 needs lat:3 bw:75
		node C1,C2 provides cpu:1000 ram:8000
		network net
		node N1,N2
		link C1•N1,N1•N2,N2•C2 with lat:1 bw:100
		paths (C1•N1•N2•C2)
		network lo link C1•C1,C2•C2 with lat:1 bw:1000 path:yes`)
	require.NoError(t, err)
	err = s.computeDomains()
	require.NoError(t, err)
	s.x = []decisionVariable{
		makeWlVar(&s, wlIndex(0), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(&s, wlIndex(1), 0, nodeIndex(0), nodeIndex(1)),
		makeWlVar(&s, wlIndex(2), 1, nodeIndex(0), nodeIndex(1))}
	return &s
}

func TestLinkOverflow(t *testing.T) {
	s := getChState2(t)
	v1 := newChanVar(s, chanIndex(0))
	v1.getDomain()
	assert.True(t, v1.stateChange())
	v2 := newChanVar(s, chanIndex(1))
	v2.getDomain()
	assert.False(t, v2.stateChange())
	v2.next()
	assert.True(t, v2.exhausted())
	assert.Equal(t, 1, s.stats.LinkOverload)
}

func TestChanVarString(t *testing.T) {
	assert := assert.New(t)
	s := getChState2(t)
	v := newChanVar(s, chanIndex(0))
	assert.Equal("(chanVar A1.W1→A1.W3 0@())", v.String())
	v.getDomain()
	assert.Equal("(chanVar A1.W1→A1.W3 0@(C1→³C2))", v.String())
	v.next()
	assert.Equal("(chanVar A1.W1→A1.W3 1@(C1→³C2))", v.String())

	// Check output of multiple paths
	v.curPath = 0
	net := &s.networks[0]
	v.paths = []*path{&net.paths[0], &net.paths[0], &net.paths[0]}
	assert.Equal("(chanVar A1.W1→A1.W3 0@(C1→³C2 C1→³C2 C1→³C2))", v.String())
	v.next()
	assert.Equal("(chanVar A1.W1→A1.W3 1@(C1→³C2 C1→³C2 C1→³C2))", v.String())
}
