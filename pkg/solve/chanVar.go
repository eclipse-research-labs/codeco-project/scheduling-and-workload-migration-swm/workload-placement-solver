// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"bytes"
	"fmt"
)

type chanVar struct {
	channel  *channel
	src, dst *wlVar
	s        *state  // context for variable
	curPath  int     // index of current path
	paths    []*path // suitable paths
}

func (v chanVar) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "(chanVar %s %d@(", v.s.nameOf(v.channel), v.curPath)
	for i, path := range v.paths {
		if i > 0 {
			buf.WriteRune(' ')
		}
		fmt.Fprintf(&buf, "%s", v.s.nameOf(path))
	}
	fmt.Fprintf(&buf, "))")
	return buf.String()
}

// A decision variable for a channel is less than another variable if it
// has less suitable paths.  If two variables have the same number of paths,
// a variable is less than another variable if its source workload is defined
// earlier.
func (v *chanVar) less(w *chanVar) bool {
	lv, lw := 0, 0
	for i := range v.channel.paths {
		lv += len(v.channel.paths[i])
		lw += len(w.channel.paths[i])
	}
	return lv < lw || lv == lw && v.src.workload.index < w.src.workload.index

}

func (v *chanVar) init(s *state, ci chanIndex, paths []*path) *chanVar {
	ch := &s.channels[ci]
	*v = chanVar{
		s:       s,
		channel: ch,
		src:     s.workloads[ch.src].dv,
		dst:     s.workloads[ch.dst].dv,
		paths:   paths}
	return v
}

func newChanVar(s *state, ci chanIndex) *chanVar {
	v, pc := chanVar{}, 0
	for i := range s.channels[ci].paths {
		pc += len(s.channels[ci].paths[i])
	}
	return v.init(s, ci, make([]*path, 0, pc))
}

func (v *chanVar) findPath() *path {
	v.s.stats.PathSearches++
	sd := pair[nodeIndex]{
		v.src.nodes[v.src.curNode].node.index,
		v.dst.nodes[v.dst.curNode].node.index}
	for i := range v.s.networks {
		if len(v.channel.paths[i]) == 0 {
			continue
		}
		if j := v.s.networks[i].pathMap.find(sd); j >= 0 {
			for _, pi := range v.channel.paths[i] {
				if j == pi {
					return &v.s.networks[i].paths[j]
				}
			}
		}
	}
	return nil
}

func (v *chanVar) getDomain() {
	v.paths, v.curPath = v.paths[:0], 0
	if path := v.findPath(); path == nil {
		v.s.stats.NoPathFound++
	} else {
		if v.channel.lat < path.lat {
			v.s.stats.NoPathFound++
		} else {
			v.paths = v.paths[:1]
			v.paths[0] = path
		}
	}
}

func (v *chanVar) stateChange() (ok bool) {
	ok = true
	for _, link := range v.paths[v.curPath].links {
		if !link.medium.Use(v.channel.bw) {
			ok = false
		}
	}
	if !ok {
		v.s.stats.LinkOverload++
	}
	return
}

func (v *chanVar) rollback() {
	for _, link := range v.paths[v.curPath].links {
		link.medium.Free(v.channel.bw)
	}
}

func (v *chanVar) exhausted() bool {
	return v.curPath >= len(v.paths)
}

func (v *chanVar) next() {
	if v.curPath < len(v.paths) {
		v.curPath++
	}
}

func (v *chanVar) assignment() channelAssignment {
	return channelAssignment{
		channel: v.channel,
		path:    v.paths[v.curPath]}
}
