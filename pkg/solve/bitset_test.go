// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewBitSet(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(17)
	assert.NotNil(b)
	assert.Len(b, 2)
	assert.Equal(uint(17), b.size())

	b = newBitset(1024)
	assert.NotNil(b)
	assert.Len(b, 17)
	assert.Equal(uint(1024), b.size())
}

func mkSet(words uint, pat uint64) bitset {
	b := newBitset(words * 64)
	for i := uint(1); i <= words; i++ {
		b[i] = pat
	}
	return b
}

func TestBitsetCopy(t *testing.T) {
	a := mkSet(2, 0x1234_5678_1234_5678)
	b := a.copy()
	assert.NotSame(t, &a[0], &b[0])
	for i := 0; i <= 2; i++ {
		assert.Equal(t, b[i], a[i], fmt.Sprintf("index: %d", i))
	}
	a = nil
	assert.Nil(t, a.copy())
}

func TestBitsetClear(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(128)
	b.not()
	b.clear()
	assert.Equal(uint(128), b.size())
	assert.Zero(b[1])
	assert.Zero(b[2])
}

func TestBitsetInit(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(16)
	b.not()
	b.init(2, 3, 5, 7, 11, 13, 17)
	assert.Equal(uint(16), b.size())
	assert.Equal(
		uint64(1<<2|1<<3|1<<5|1<<7|1<<11|1<<13),
		b[1])
}

func TestBitsetString(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(16)
	assert.Equal("{}", b.String())

	b.set(2)
	assert.Equal("{2}", b.String())

	b.init(2, 3, 5, 7, 11, 13)
	assert.Equal("{2,3,5,7,11,13}", b.String())
}

func TestBitsetSet(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(128)
	for i := uint(0); i < b.size(); i += 2 {
		b.set(i)
	}
	assert.Equal(uint(128), b.size())
	assert.Equal(uint64(0x5555555555555555), b[1])
	assert.Equal(uint64(0x5555555555555555), b[2])
}

func TestBitsetGet(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(128)
	for i := uint(0); i < b.size(); i += 2 {
		b.set(i)
	}
	for i := uint(0); i < b.size(); i++ {
		assert.Equal(uint(1-(i&1)), b.get(i), "bit %d", i)
	}
}

func TestBitsetNot(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(96)
	for i := uint(1); i < b.size(); i += 2 {
		b.set(i)
	}
	b.not()
	assert.Equal(uint64(0x5555555555555555), b[1])
	assert.Equal(uint64(0x0000000055555555), b[2])

	b = newBitset(32)
	for i := uint(1); i < b.size(); i += 2 {
		b.set(i)
	}
	b.not()
	assert.Equal(uint64(0x0000000055555555), b[1])
}

func TestBitsetAssign(t *testing.T) {
	a := mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b := mkSet(2, 0x3333_3333_3333_3333)
	a.assign(b)
	assert.Equal(t, b, a)

	a0 := &a[0]
	b = mkSet(1, 0xCCCC_CCCC_CCCC_CCCC)
	a.assign(b)
	assert.Equal(t, b, a)
	assert.Same(t, a0, &a[0])

	b = mkSet(2, 0x3333_3333_3333_3333)
	a.assign(b)
	assert.Equal(t, b, a)
	assert.Same(t, a0, &a[0])

	b = mkSet(3, 0xCCCC_CCCC_CCCC_CCCC)
	a.assign(b)
	assert.Equal(t, b, a)
	assert.NotSame(t, a0, &a[0])
}

func TestBitsetIntersect(t *testing.T) {
	a := mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b := mkSet(2, 0x3333_3333_3333_3333)
	a.intersect(b)
	assert.Equal(t, b, a)

	a = mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b = mkSet(3, 0x3333_3333_3333_3333)
	a.intersect(b)
	assert.Equal(t, mkSet(2, 0x3333_3333_3333_3333), a)

	a = mkSet(3, 0xFFFF_FFFF_FFFF_FFFF)
	b = mkSet(2, 0x3333_3333_3333_3333)
	c := mkSet(3, 0x3333_3333_3333_3333)
	c[3] = 0
	a.intersect(b)
	assert.Equal(t, c, a)
}

func TestBitsetIntersect_newSet(t *testing.T) {
	a := mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b := mkSet(2, 0x3333_3333_3333_3333)
	i := bitsetIntersection(a, b)
	assert.Equal(t, b, i)
	assert.NotSame(t, &a[0], &i[0])
	assert.NotSame(t, &b[0], &i[0])

	a = mkSet(3, 0xFFFF_FFFF_FFFF_FFFF)
	b = mkSet(2, 0x3333_3333_3333_3333)
	c := mkSet(3, 0x3333_3333_3333_3333)
	c[3] = 0
	i = bitsetIntersection(a, b)
	assert.Equal(t, c, i)
	assert.NotSame(t, &a[0], &i[0])
	assert.NotSame(t, &b[0], &i[0])
}

func TestBitsetDifference(t *testing.T) {
	a := mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b := mkSet(2, 0x3333_3333_3333_3333)
	c := mkSet(2, 0xCCCC_CCCC_CCCC_CCCC)
	a.difference(b)
	assert.Equal(t, c, a)

	a = mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b = mkSet(3, 0x3333_3333_3333_3333)
	a.difference(b)
	assert.Equal(t, c, a)

	a = mkSet(3, 0xFFFF_FFFF_FFFF_FFFF)
	b = mkSet(2, 0x3333_3333_3333_3333)
	c = mkSet(3, 0xCCCC_CCCC_CCCC_CCCC)
	c[3] = 0xFFFF_FFFF_FFFF_FFFF
	a.difference(b)
	assert.Equal(t, c, a)
}

func TestBitsetDifference_newSet(t *testing.T) {
	a := mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	b := mkSet(2, 0x3333_3333_3333_3333)
	c := mkSet(2, 0xCCCC_CCCC_CCCC_CCCC)
	d := bitsetDifference(a, b)
	assert.Equal(t, c, d)
	assert.NotSame(t, &a[0], &d[0])
	assert.NotSame(t, &b[0], &d[0])
}

func TestBitsetUnion(t *testing.T) {
	a := bitset{128 - 12, 0x3333_3333_3333_3333, 0x0003_3333_3333_3333}
	b := bitset{128 - 20, 0xCCCC_CCCC_CCCC_CCCC, 0x0000_0CCC_CCCC_CCCC}
	c := bitset{128 - 12, 0xFFFF_FFFF_FFFF_FFFF, 0x0003_3FFF_FFFF_FFFF}
	a.union(b)
	assert.Equal(t, c, a)

	a = bitset{128 - 20, 0x3333_3333_3333_3333, 0x0000_0333_3333_3333}
	b = bitset{128 - 12, 0xCCCC_CCCC_CCCC_CCCC, 0x000C_CCCC_CCCC_CCCC}
	c = bitset{128 - 12, 0xFFFF_FFFF_FFFF_FFFF, 0x000C_CFFF_FFFF_FFFF}
	a.union(b)
	assert.Equal(t, c, a)

	x := bitset{64 - 12, 0x0003_3333_3333_3333}
	a.assign(x)
	c = bitset{128 - 12, 0xCCCF_FFFF_FFFF_FFFF, 0x000C_CCCC_CCCC_CCCC}
	a.union(b)
	assert.Equal(t, c, a)

	a = x
	c = bitset{128 - 12, 0xCCCF_FFFF_FFFF_FFFF, 0x000C_CCCC_CCCC_CCCC}
	a.union(b)
	assert.Equal(t, c, a)
}

func TestBitsetUnion_newSet(t *testing.T) {
	a := mkSet(2, 0xCCCC_CCCC_CCCC_CCCC)
	b := mkSet(2, 0x3333_3333_3333_3333)
	c := mkSet(2, 0xFFFF_FFFF_FFFF_FFFF)
	i := bitsetUnion(a, b)
	assert.Equal(t, c, i)
	assert.NotSame(t, &a[0], &i[0])
	assert.NotSame(t, &b[0], &i[0])

	a = mkSet(3, 0xCCCC_CCCC_CCCC_CCCC)
	b = mkSet(2, 0x3333_3333_3333_3333)
	c = mkSet(3, 0xFFFF_FFFF_FFFF_FFFF)
	c[3] = 0xCCCC_CCCC_CCCC_CCCC
	i = bitsetUnion(a, b)
	assert.Equal(t, c, i)
	assert.NotSame(t, &a[0], &i[0])
	assert.NotSame(t, &b[0], &i[0])
}

func TestPop(t *testing.T) {
	assert.Equal(t, uint(0), pop(0))
	assert.Equal(t, uint(32), pop(0x5555_5555_5555_5555))
	assert.Equal(t, uint(32), pop(0x3333_3333_3333_3333))
	assert.Equal(t, uint(64), pop(^uint64(0)))
}

func TestPopCount(t *testing.T) {
	b := newBitset(256)
	assert.Equal(t, uint(0), b.popCount())
	b[1] = ^uint64(0)
	b[2] = b[1]
	b[3] = b[1]
	b[4] = b[1]
	assert.Equal(t, uint(256), b.popCount())
}

func TestBitmapIsSet(t *testing.T) {
	assert := assert.New(t)
	b := newBitset(128)
	for i := uint(1); i < 128; i += 2 {
		b.set(i)
	}
	for i := uint(0); i < 128; i++ {
		assert.Equal(i&1 == 1, b.isSet(i), "bit %d", i)
	}
}
