// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"fmt"
	"math"

	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

type node struct {
	name   string
	index  nodeIndex
	cpu    types.Resource[milliCPU]
	ram    types.Resource[ramMB]
	inM    [][]*medium
	outM   [][]*medium
	in     types.Resource[bandwidth]
	out    types.Resource[bandwidth]
	labels []nameIndex
}

func labelsMatch(wls, nls []nameIndex) bool {
	for _, wl := range wls {
		found := false
		for _, nl := range nls {
			if nl == wl || ^nl == wl {
				found = true
				break
			}
		}
		if wl >= 0 && !found || wl < 0 && found {
			return false
		}
	}
	return true
}

func (n node) canHost(w workload) bool {
	return w.cpu <= n.cpu.Cap && w.ram <= n.ram.Cap &&
		w.in <= n.in.Cap && w.out <= n.out.Cap &&
		labelsMatch(w.labels, n.labels)
}

type medium struct {
	name string
	types.Resource[bandwidth]
}

type link struct {
	pair[nodeIndex]
	name   string
	net    int
	lat    latency
	medium *medium
}

type path struct {
	pair[nodeIndex]
	name     string
	lat      latency // total latency
	smallest *medium // medium with smallest available capacity
	links    []*link // constituent links
}

func (x *path) equal(y *path) bool {
	if x.pair != y.pair || len(x.links) != len(y.links) {
		return false
	}
	for i, xi := range x.links {
		if xi != y.links[i] {
			return false
		}
	}
	return true
}

type network struct {
	name    string
	qos     serviceClass
	bearer  *network
	media   []medium
	links   []link
	paths   []path
	pathMap pathMap
}

func (n *network) getMedium(name string) *medium {
	for i := range n.media {
		if m := &n.media[i]; m.name == name {
			return m
		}
	}
	return nil
}

func (n *network) addMedium(m *grpc.Medium, idx int) error {
	err := n.checkMediumName(m.Id)
	if err != nil {
		return err
	}
	sm := medium{name: m.Id}
	cap := bandwidth(m.Bandwidth.Capacity)
	if n.bearer != nil {
		base := &n.bearer.media[idx]
		if cap > base.Cap {
			return fmt.Errorf("bandwidth too big: %s•%s", n.name, m.Id)
		}
		sm.Resource = *base.Bear(cap)
	} else {
		sm.Cap = cap
	}
	sm.Load(bandwidth(m.Bandwidth.Used))
	n.media = append(n.media, sm)
	return nil
}

func (n *network) findMedium(name string) (*medium, error) {
	if m := n.getMedium(name); m != nil {
		return m, nil
	}
	return nil, unknown("medium", name)
}

func (n *network) findLink(name string) *link {
	for i := range n.links {
		if l := &n.links[i]; l.name == name {
			return l
		}
	}
	return nil
}

func (n *network) findLinkByPair(src, dst nodeIndex) *link {
	for i := range n.links {
		if link := &n.links[i]; link.src == src && link.dst == dst {
			return link
		}
	}
	return nil
}

var infinity medium = medium{
	Resource: types.Resource[bandwidth]{Cap: math.MaxInt64}}

func (n *network) buildPath(p *grpc.Network_Path, s *state) (sp path, err error) {
	sp = path{smallest: &infinity}
	sp.links = make([]*link, len(p.Links))
	nodes := newBitset(uint(len(s.nodes)))
	for i, id := range p.Links {
		link := n.findLink(id)
		if link == nil {
			err = unknown("link", id)
			return
		}
		sp.lat += link.lat
		if link.medium.Available() < sp.smallest.Available() {
			sp.smallest = link.medium
		}
		sp.links[i] = link

		if i == 0 {
			sp.pair.src = link.src
			nodes.set(uint(link.src))
		} else if link.src != sp.pair.dst {
			err = ErrIllegalPath
			return
		}
		if nodes.isSet(uint(link.dst)) && len(p.Links) > 1 {
			err = duplicate("node", s.nodes[link.dst].name, " in ", p.Id)
			return
		}
		sp.pair.dst = link.dst
		nodes.set(uint(link.dst))
	}
	return
}

func (n *network) addPath(np *grpc.Network_Path, s *state) (p *path, e error) {
	if np == nil {
		e = ErrIllegalPath
		return
	}
	if e = n.checkPathName(np.Id); e != nil {
		return
	}
	var sp path
	if sp, e = n.buildPath(np, s); e == nil {
		sp.name = np.Id
		n.paths = append(n.paths, sp)
		p = &sp
	}
	return
}

func (n *network) reversePath(p *path) (*path, error) {
	N := len(p.links)
	q := &path{
		pair:     p.pair.swap(),
		name:     p.name + "\u1D63\u2091\u1D65",
		smallest: &infinity,
		links:    make([]*link, N)}
	for i := range p.links {
		// For consistency traverse links like command “find”
		to := p.links[N-1-i]
		if fro := n.findLinkByPair(to.dst, to.src); fro == nil {
			return nil, unknown("reverse link", to.name)
		} else {
			q.links[i] = fro
			q.lat += fro.lat
			if fro.medium.Available() < q.smallest.Available() {
				q.smallest = fro.medium
			}
		}
	}
	return q, nil
}

func (s *state) allocateInfra(im *grpc.Infrastructure) {
	if im == nil {
		return
	}
	lc, pc, mc := 0, 0, 0
	if len(im.Networks) > 0 {
		for _, n := range im.Networks {
			lc, pc, mc = lc+len(n.Links), pc+len(n.Paths), mc+len(n.Media)
		}
	}
	if s.options.implicitReversePath {
		pc *= 2
	}
	s.nodes = make([]node, 0, len(im.Nodes))
	s.networks = make([]network, len(im.Networks))
	s.nodeNws = make([][]*medium, 2*len(im.Nodes)*len(im.Networks))
	s.inM = make([]*medium, 0, lc)
	s.outM = make([]*medium, 0, lc)
}

func (s *state) addNode(n *grpc.Node) error {
	err := s.checkNodeName(n.Id)
	if err != nil {
		return err
	}
	sn := node{name: n.Id}
	if n.NodeType == grpc.Node_COMPUTE {
		sn.cpu = types.Resource[milliCPU]{Cap: milliCPU(n.CPU.Available())}
		sn.ram = types.Resource[ramMB]{Cap: ramMB(n.Memory.Available())}
		nc := len(s.networks)
		start := 2 * nc * len(s.nodes)
		n1, n2 := start+nc, start+2*nc
		sn.inM = s.nodeNws[start:n1:n1]
		sn.outM = s.nodeNws[n1:n2:n2]
		if sn.labels, err = s.addLabels(n.Labels, nil); err != nil {
			return err
		}
	}
	s.nodes = append(s.nodes, sn)
	return nil
}

func (s *state) findNode(node string) (nodeIndex, error) {
	for i := range s.nodes {
		if s.nodes[i].name == node {
			return nodeIndex(i), nil
		}
	}
	return -1, unknown("node", node)
}

func (s *state) findNetwork(net string) (*network, error) {
	for i := range s.networks {
		if n := &s.networks[i]; n.name == net {
			return n, nil
		}
	}
	return nil, unknown("network", net)
}

func (s *state) addLink(net int, l *grpc.Network_Link) error {
	n := &s.networks[net]
	err := n.checkLinkName(l.Id)
	if err != nil {
		return err
	}
	src, err := s.findNode(l.Source)
	if err != nil {
		return err
	}
	dst, err := s.findNode(l.Target)
	if err != nil {
		return err
	}
	m, err := n.findMedium(l.Medium)
	if err != nil {
		return err
	}
	n.links = append(n.links,
		link{name: l.Id, net: net,
			pair:   pair[nodeIndex]{src: src, dst: dst},
			lat:    latency(l.Latency),
			medium: m})
	return nil
}

func (s *state) addPath(n *network, p *grpc.Network_Path) (*path, error) {
	if p == nil {
		return nil, ErrIllegalPath
	}
	if err := n.checkPathName(p.Id); err != nil {
		return nil, err
	}
	if sp, err := n.buildPath(p, s); err != nil {
		return nil, err
	} else {
		sp.name = p.Id
		n.paths = append(n.paths, sp)
		return &sp, nil
	}
}

func (s *state) nodeNetworkAttributes() {
	for i := range s.networks {
		n := &s.networks[i]
		for i := range n.links {
			s.nodes[n.links[i].src].out.Cap += n.links[i].medium.Cap
			s.nodes[n.links[i].dst].in.Cap += n.links[i].medium.Cap
		}
	}
}

func (s *state) workerMediaIn() {
	less := func(x, y *link) bool {
		return x.dst < y.dst ||
			x.dst == y.dst &&
				(x.net < y.net || x.net == y.net && x.src < y.src)
	}
	lc := 0
	for i := range s.networks {
		lc += len(s.networks[i].links)
	}
	ls, pos := make([]*link, lc), 0
	for i := range s.networks {
		n := &s.networks[i]
		for i := range n.links {
			li := &n.links[i]
			if s.nodes[li.dst].cpu.Cap != 0 {
				k := pos - 1
				for ; k >= 0 && less(li, ls[k]); k-- {
					ls[k+1] = ls[k]
				}
				ls[k+1], pos = li, pos+1
			}
		}
	}
	for i := 0; i < pos; i++ {
		s.inM = append(s.inM, ls[i].medium)
	}
	for lo := 0; lo < pos; {
		hi, n, net := lo+1, ls[lo].dst, ls[lo].net
		for hi < pos && ls[hi].dst == n && ls[hi].net == net {
			hi++
		}
		s.nodes[n].inM[net], lo = s.inM[lo:hi:hi], hi
	}
}

func (s *state) workerMediaOut() {
	less := func(x, y *link) bool {
		return x.src < y.src ||
			x.src == y.src &&
				(x.net < y.net || x.net == y.net && x.dst < y.dst)
	}
	lc := 0
	for i := range s.networks {
		lc += len(s.networks[i].links)
	}
	ls, pos := make([]*link, lc), 0
	for i := range s.networks {
		n := &s.networks[i]
		for i := range n.links {
			li := &n.links[i]
			if s.nodes[li.src].cpu.Cap != 0 {
				k := pos - 1
				for ; k >= 0 && less(li, ls[k]); k-- {
					ls[k+1] = ls[k]
				}
				ls[k+1], pos = li, pos+1
			}
		}
	}
	for i := 0; i < pos; i++ {
		s.outM = append(s.outM, ls[i].medium)
	}
	for lo := 0; lo < pos; {
		hi, n, net := lo+1, ls[lo].src, ls[lo].net
		for hi < pos && ls[hi].src == n && ls[hi].net == net {
			hi++
		}
		s.nodes[n].outM[net], lo = s.outM[lo:hi:hi], hi
	}
}

func (s *state) workerNetworkMedia() {
	s.workerMediaIn()
	s.workerMediaOut()
}

func (s *state) addNetwork(i int, n *grpc.Network) (err error) {
	net := &s.networks[i]
	if n.Bearer != "" {
		if net.bearer, err = s.findNetwork(n.Bearer); err != nil {
			return
		}
	}
	net.name, net.qos = n.Id, serviceClass(n.Qos)
	net.media = make([]medium, 0, len(n.Media))
	for i, m := range n.Media {
		if err = net.addMedium(m, i); err != nil {
			return
		}
	}
	net.links = make([]link, 0, len(n.Links))
	for _, l := range n.Links {
		if err = s.addLink(i, l); err != nil {
			return
		}
	}
	net.paths = make([]path, 0, len(n.Paths))
	for _, p := range n.Paths {
		var pp *path
		if pp, err = net.addPath(p, s); err != nil {
			return
		}
		if err = net.checkPath(len(net.paths)-1, s); err != nil {
			net.paths = net.paths[:len(net.paths)-1]
			return
		}
		if s.options.implicitReversePath {
			var q *path
			if q, err = net.reversePath(pp); err != nil {
				return
			} else {
				q.name = textformat.PathName(
					s.nodes[q.src].name, s.nodes[q.dst].name,
					len(p.Links))
				net.paths = append(net.paths, *q)
			}
		}
	}
	return
}

func (s *state) addInfraModel(im *grpc.Infrastructure) (err error) {
	if im == nil {
		return
	}
	for i, n := range im.Nodes {
		if err = s.addNode(n); err != nil {
			return
		}
		s.nodes[i].index = nodeIndex(i)
	}
	if len(im.Networks) == 0 {
		return
	}
	for i, n := range im.Networks {
		if err = s.addNetwork(i, n); err != nil {
			return
		}
	}
	s.nodeNetworkAttributes()
	s.workerNetworkMedia()
	return
}

func (s *state) pathsForChannels() error {
	if len(s.workloads) == 0 {
		return ErrEmptyAppModel
	}
	for i := range s.channels {
		ch, pc := &s.channels[i], 0
		src, dst := &s.workloads[ch.src], &s.workloads[ch.dst]
		s.channels[i].paths = make([][]pathIndex, len(s.networks))
		for k := range s.networks {
			net := &s.networks[k]
			if net.qos != ch.qos {
				continue
			}
			ps := make([]pathIndex, 0, len(net.paths))
			for j, pth := range net.paths {
				if !src.runsOn(pth.src) || !dst.runsOn(pth.dst) {
					continue
				}
				if pth.lat <= ch.lat && pth.smallest.Available() >= ch.bw {
					ps = append(ps, pathIndex(j))
				}
			}
			ch.paths[k], pc = ps, pc+len(ps)
		}
		if pc == 0 {
			return fmt.Errorf("no path for channel %q", s.nameOf(ch))
		}
	}
	return nil
}

// Shortest path between nodes
func (s *state) pathBetweenNodes() {
	for i := range s.networks {
		n := &s.networks[i]
		pm := newPathMap(len(s.nodes), len(n.paths))
		for i, path := range n.paths {
			if pi := pm.find(path.pair); pi >= 0 {
				if len(n.paths[i].links) >= len(n.paths[pi].links) {
					continue
				}
			}
			pm.set(path.pair, pathIndex(i))
		}
		n.pathMap = pm
	}
}

func (s *state) workloadNetworkRequirements() {
	for i := range s.workloads {
		s.workloads[i].lat = math.MaxInt
	}
	for _, ch := range s.channels {
		if ch.lat < s.workloads[ch.src].lat {
			s.workloads[ch.src].lat = ch.lat
		}
		s.workloads[ch.src].out += ch.bw
		s.workloads[ch.dst].in += ch.bw
	}
}

func (s *state) workloadNodes() error {
	if len(s.nodes) == 0 {
		return ErrNoNodes
	}
	for i, wl := range s.workloads {
		nodes := make([]nodeIndex, 0, len(s.nodes))
		for k, n := range s.nodes {
			if n.canHost(wl) {
				nodes = append(nodes, nodeIndex(k))
			}
		}
		if len(nodes) == 0 {
			return fmt.Errorf("no node for workload %s", s.nameOf(&wl))
		}
		s.workloads[i].nodes = nodes
	}
	return nil
}

func (s *state) workloadSourceNodes(w wlIndex) bitset {
	b := newBitset(uint(len(s.nodes)))
	if cs := s.workloads[w].channels; len(cs) == 0 {
		b.not()
	} else {
		for i := range cs {
			ch := &cs[i]
			for ni := range ch.paths {
				net := &s.networks[ni]
				for _, pi := range ch.paths[ni] {
					b.set(uint(net.paths[pi].src))
				}
			}
		}
	}
	return b
}

func (s *state) nodesForWorkloads() error {
	if len(s.nodes) == 0 {
		return ErrNoNodes
	}
	for i := range s.workloads {
		sources := s.workloadSourceNodes(wlIndex(i))
		wi := &s.workloads[i]
		nodes, j := wi.nodes, 0
		for _, node := range nodes {
			if sources.isSet(uint(node)) {
				nodes[j] = node
				j++
			}
		}
		wi.nodes = nodes[:j]
		if j == 0 {
			return fmt.Errorf("no node for workload %s", s.nameOf(wi))
		}
	}
	return nil
}
