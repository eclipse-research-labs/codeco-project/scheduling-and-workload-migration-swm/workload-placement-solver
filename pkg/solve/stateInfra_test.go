// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestLabelsMatch(t *testing.T) {
	assert := assert.New(t)
	s_1_2 := []nameIndex{1, 2}
	s_1_2_3 := []nameIndex{1, 2, 3}
	s_1_n3 := []nameIndex{1, ^3}
	assert.True(labelsMatch(nil, nil))
	assert.True(labelsMatch(nil, s_1_2))
	assert.True(labelsMatch(s_1_2, s_1_2))
	assert.True(labelsMatch(s_1_2, s_1_2_3))
	assert.False(labelsMatch(s_1_2, nil))
	assert.False(labelsMatch(s_1_2_3, s_1_2))
	assert.True(labelsMatch(s_1_n3, s_1_2))
	assert.False(labelsMatch(s_1_n3, s_1_2_3))

	assert.False(labelsMatch([]nameIndex{0}, []nameIndex{9}))
	assert.True(labelsMatch([]nameIndex{0}, []nameIndex{0}))
}

func TestSNodeCanHost(t *testing.T) {
	assert := assert.New(t)
	node := func(labels ...nameIndex) node {
		return node{cpu: cpu(1000), ram: ram(8000), labels: labels}
	}
	workload := func(cpu, ram int, labels ...nameIndex) workload {
		return workload{cpu: milliCPU(cpu), ram: ramMB(ram), labels: labels}
	}
	assert.True(node().canHost(workload(100, 1000)))
	assert.False(node().canHost(workload(2000, 1000)))
	assert.False(node().canHost(workload(100, 10000)))
	assert.True(node(1).canHost(workload(100, 1000, 1)))
	assert.True(node().canHost(workload(100, 1000, ^1)))
	assert.False(node().canHost(workload(100, 1000, 1)))
	assert.False(node(1).canHost(workload(100, 1000, ^1)))
}

func TestNetworkReversePath(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	state, err := newStateFromText("reverse-path", `
		workload assignment v1
		model reverse-path options implicit-reverse-path:no
		application dummy workload dummy
		node N1,N2,N3,N4 provides cpu:1000
		network net
		link N1--N2,N2--N3,N2->N4 with lat:5 bw:100
		paths (N1•N2•N3 N1•N2•N4)`)
	require.NoError(err)
	require.Len(state.networks, 1)
	net := state.networks[0]
	p, err := net.reversePath(&net.paths[0])
	assert.NoError(err)
	assert.Equal(
		&path{pair: pair[nodeIndex]{2, 0},
			name:     "N1→²N3ᵣₑᵥ",
			lat:      10,
			smallest: &net.media[3],
			links:    []*link{&net.links[3], &net.links[1]}},
		p)
	p, err = net.reversePath(&net.paths[1])
	assert.ErrorContains(err, `unknown reverse link: N2→N4`)
	assert.Nil(p)
}

func TestStateAddNode(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("add-node", `
		workload assignment v1
		model add-node
		application dummy workload dummy
		node N1 provides cpu:1000 ram:8000
		node N2 provides cpu:2000 ram:16000
		node N3 provides cpu:3000 ram:24000 labels:(A B B A)`)
	require.NoError(t, err)
	require.Len(t, state.nodes, 3)
	assert.Equal(
		node{name: "N1", index: 0, cpu: cpu(1000), ram: ram(8000),
			inM: [][]*medium{}, outM: [][]*medium{}},
		state.nodes[0])
	assert.Equal(
		node{name: "N2", index: 1, cpu: cpu(2000), ram: ram(16000),
			inM: [][]*medium{}, outM: [][]*medium{}},
		state.nodes[1])
	assert.Equal(
		node{name: "N3", index: 2, cpu: cpu(3000), ram: ram(24000),
			inM: [][]*medium{}, outM: [][]*medium{},
			labels: []nameIndex{1, 2}},
		state.nodes[2])
}

func TestStateAddLink(t *testing.T) {
	assert := assert.New(t)
	var model grpc.Model
	err := textformat.Convert("test", `
		workload assignment v1 model app-errors
		application a workload w
		node N1 provides cpu:1000 ram:8000
		node N2 provides cpu:2000 ram:16000
		network net with type:radio`,
		&model)
	require.NoError(t, err)
	state, err := newStateFromModel(&model)
	require.NoError(t, err)
	net := model.Infrastructure.Networks[0]

	mkLink := func(src, dst string) *grpc.Network_Link {
		return &grpc.Network_Link{
			Id: textformat.LinkName(src, dst), Source: src, Target: dst,
			Medium: net.Media[0].Id, Latency: 5}
	}
	assert.NoError(state.addLink(0, mkLink("N1", "N2")))
	require.Len(t, state.networks, 1)
	nw := &state.networks[0]
	require.Len(t, nw.links, 1)
	assert.Equal(
		link{name: "N1→N2", pair: pair[nodeIndex]{src: 0, dst: 1}, lat: 5,
			medium: &nw.media[0]},
		nw.links[0])

	assert.ErrorContains(
		state.addLink(0, mkLink("Nx", "N2")), `unknown node: Nx`)
	assert.ErrorContains(
		state.addLink(0, mkLink("N1", "Ny")), `unknown node: Ny`)
}

func TestStateAddPath_happyCases(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	state, err := newStateFromText("add-path-happy", `
		workload assignment v1
		model add-path-happy
		application dummy workload dummy
		node N1,N2,N3 provides cpu:1000 ram:8000
		network net
		link N1•N2 with lat:5 bw:100
		link N2•N3 with lat:10 bw:10`)
	require.NoError(err)

	for i := range state.nodes {
		ni := &state.nodes[i]
		assert.Equal(len(ni.inM), cap(ni.inM))
		assert.Equal(len(ni.outM), cap(ni.outM))
	}

	net := &state.networks[0]
	_, err = state.addPath(net, makePath("N1•N2"))
	assert.NoError(err)
	require.Len(net.paths, 1)
	assert.Equal(
		path{name: "N1→N2", pair: pair[nodeIndex]{src: 0, dst: 1}, lat: 5,
			smallest: net.links[0].medium,
			links:    []*link{&net.links[0]}},
		net.paths[0])
	_, err = state.addPath(net, makePath("N1•N2•N3"))
	assert.NoError(err)
	require.Len(net.paths, 2)
	assert.Equal(
		path{name: "N1→²N3", pair: pair[nodeIndex]{src: 0, dst: 2}, lat: 15,
			smallest: net.links[1].medium,
			links:    []*link{&net.links[0], &net.links[1]}},
		net.paths[1])
}

func TestStateAddPath_ErrorCases(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	state, err := newStateFromText("add-path-errors", `
		workload assignment v1
		model add-path-errors
		application dummy workload dummy
		node N1,N2,N3,N4 provides cpu:1000 ram:8000
		network net link N1•N2,N2•N3,N3•N1 with lat:5 bw:100`)
	cases := []struct {
		path string
		err  string
	}{
		{"", `illegal path`},
		{"N1", `illegal path`},
		{"Nx•N2", `unknown link: Nx→N2`},
		{"N1•Nx", `unknown link: N1→Nx`},
		{"N1•N2•N3•N1", `duplicate node: N1 in N1→³N1`}}
	require.NoError(err)
	net := &state.networks[0]
	for _, tc := range cases {
		_, err := state.addPath(net, makePath(tc.path))
		assert.ErrorContains(err, tc.err, tc.path)
		require.Len(net.paths, 0)
	}
}

func TestStateAddInfraModel_happyCase(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("infra-happy-case", threeNodeModelText)
	require.NoError(t, err)
	require.Len(t, state.nodes, 3)
	require.Len(t, state.networks, 1)
	net := &state.networks[0]
	assert.Equal(
		node{name: "N1", index: 0, cpu: cpu(1000), ram: ram(8000),
			inM:  [][]*medium{{&net.media[1]}},
			outM: [][]*medium{{&net.media[0], &net.media[2]}},
			in:   bw(1000), out: bw(2000)},
		state.nodes[0])
	assert.Equal(
		node{name: "N2", index: 1, cpu: cpu(1000), ram: ram(8000),
			inM:  [][]*medium{{&net.media[0]}},
			outM: [][]*medium{{&net.media[1], &net.media[3]}},
			in:   bw(1000), out: bw(2000)},
		state.nodes[1])
	assert.Equal(
		node{name: "N3", index: 2, cpu: cpu(1000), ram: ram(8000),
			inM:  [][]*medium{{&net.media[2], &net.media[3]}},
			outM: [][]*medium{nil},
			in:   bw(2000), out: bw(0)},
		state.nodes[2])
	assert.Len(net.links, 4)
	assert.Len(net.paths, 2)
}

func TestStateAddInfraModel_loopbackPath(t *testing.T) {
	state, err := newStateFromText("loopback-path", `
		workload assignment v1
		model loopback-path
		application dummy workload dummy
		node C1 provides cpu:1000 ram:8000
		network net link C1->C1 with lat:1 bw:1000 path:yes`)
	assert.NoError(t, err)
	require.Len(t, state.networks, 1)
	assert.Len(t, state.networks[0].paths, 1)
}

func TestStateAddInfraModel_bearerNetwork(t *testing.T) {
	state, err := newStateFromText("bearer-network", `
		workload assignment v1
		model bearer-network
		application dummy workload dummy
		node c1,c2,c3
		network eth node sw
		link c1—sw,c2—sw,c3—sw
		find shortest paths (c1•c2 c1•c3 c2•c3)
		network vlan with qos:assured bearer:eth
		nodes (c1 c2)`)
	require.NoError(t, err)
	require.Len(t, state.networks, 2)
	net := &state.networks[0]
	assert.Equal(t, scBestEffort, net.qos)
	assert.Len(t, net.media, 6)
	assert.Len(t, net.links, 6)
	assert.Len(t, net.paths, 6)

	net = &state.networks[1]
	assert.Equal(t, scAssured, net.qos)
	assert.Len(t, net.media, 6)
	assert.Len(t, net.links, 4)
	assert.Len(t, net.paths, 2)
}

func TestStateAddInfraModel_errorCases(t *testing.T) {
	assert := assert.New(t)
	_, err := newStateFromText("dup-node", `
		workload assignment v1
		model dup-node
		application dummy workload dummy
		node N1 provides cpu:1000 ram:8000
		node N1 provides cpu:1000 ram:8000`)
	assert.ErrorContains(err, `duplicate node: N1`)

	_, err = newStateFromText("unknown-node", `
		workload assignment v1
		model unknown-node
		application dummy workload dummy
		node N1 provides cpu:1000 ram:8000
		network net link N1->Nx with lat:5 bw:100`)
	assert.ErrorContains(err, `unknown node: Nx`)

	_, err = newStateFromText("unknown-link", `
		workload assignment v1
		model unknown-link
		application dummy workload dummy
		node N1 provides cpu:1000 ram:8000
		network net paths (N1•Nx)`)
	assert.ErrorContains(err, `unknown link: N1→Nx`)
}

func TestStatePathsForChannels_noChannels(t *testing.T) {
	assert := assert.New(t)
	state := state{}
	assert.ErrorIs(state.pathsForChannels(), ErrEmptyAppModel)
	assert.Zero(len(state.channels))

	state, err := newStateFromModel(nil)
	assert.NoError(err)
	assert.ErrorIs(state.pathsForChannels(), ErrEmptyAppModel)
	assert.Zero(len(state.channels))
}

func TestStatePathsForChannels_happyCase(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("paths-for-channels", `
		workload assignment v1
		model paths-for-channels
		application A workload W needs cpu:100 ram:1000
		channel W—W needs lat:3,4 bw:500,1000
		node C provides cpu:1000 ram:2000`)
	require.NoError(t, err)
	media := []medium{
		{Resource: types.Resource[bandwidth]{Cap: 1000}},
		{Resource: types.Resource[bandwidth]{Cap: 100}}}
	state.networks = []network{{paths: []path{
		{lat: 2, smallest: &media[0]},
		{lat: 3, smallest: &media[1]},
		{lat: 4, smallest: &media[0]},
		{lat: 5, smallest: &media[0]}}}}
	assert.NoError(state.workloadNodes())
	assert.NoError(state.pathsForChannels())
	assert.Equal([][]pathIndex{{0}}, state.channels[0].paths)
	assert.Equal([][]pathIndex{{0, 2}}, state.channels[1].paths)
}

func TestStatePathsForChannels_noPath(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("no-path", `
		workload assignment v1
		model no-path
		application A workload W
		channel W->W needs lat:3 bw:500
		node C provides cpu:1000
		network N1 link 1:C->C with lat:2 bw:100  path:yes
		network N2 link 2:C->C with lat:4 bw:1000 path:yes`)
	require.NoError(t, err)
	assert.ErrorContains(
		state.pathsForChannels(), `no path for channel "A.W→A.W"`)
	assert.Len(state.channels[0].paths[0], 0)
}

func TestStatePathBetweenNodes(t *testing.T) {
	state, err := newStateFromModel(nil)
	require.NoError(t, err)

	state, err = newStateFromText("paths-explicit", `
		workload assignment v1
		model paths-explicit options implicit-reverse-path:no
		application A1 workload W1
		node C1 provides cpu:1000 ram:8000
		node C2 provides cpu:1000 ram:8000
		node N1
		network net
		link C1->C1 with lat:0 bw:1000 path:yes
		link C1--C2 with lat:1 bw:100
		link C1--N1 with lat:1 bw:200
		paths (C1•C2 C2•C1)`)
	require.NoError(t, err)
	state.pathBetweenNodes()
	assert.Equal(t,
		pathMap{
			nodes: 3,
			data: []pathIndex{
				1, 2, 0,
				3, 0, 0,
				0, 0, 0}},
		state.networks[0].pathMap)
}

func TestStateWorkloadNetworkRequirements_happyCase(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("wl-reqs-happy", `
		workload assignment v1
		model wl-reqs-happy
		application A1
		workload W1 needs cpu:100  ram:1000
		workload W2 needs cpu:1500 ram:2000
		workload W3 needs cpu:1500 ram:2000
		channel W1->W2 needs lat:4 bw:1
		channel W2->W1 needs lat:3 bw:2
		channel W1->W3 needs lat:2 bw:4
		node dummy`)
	assert.NoError(err)
	state.workloadNetworkRequirements()
	exp := []workload{
		{name: "W1", index: 0, cpu: 100, ram: 1000, lat: 2, in: 2, out: 5,
			channels: []channel{
				mkChannel(0, 1, "A1•W1→A1•W2", 0, 4, 1),
				mkChannel(0, 2, "A1•W1→A1•W3", 1, 2, 4)}},
		{name: "W2", index: 1, cpu: 1500, ram: 2000, lat: 3, in: 1, out: 2,
			channels: []channel{
				mkChannel(1, 0, "A1•W2→A1•W1", 2, 3, 2)}},
		{name: "W3", index: 2, cpu: 1500, ram: 2000,
			lat: math.MaxInt, in: 4, out: 0}}
	assert.Equal(exp, state.workloads)
}

func TestStateNodesForWorkloads_noNodes(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromModel(nil)
	assert.NoError(err)
	assert.ErrorIs(s.nodesForWorkloads(), ErrNoNodes)
}

func TestStateNodesForWorkloads_noApps(t *testing.T) {
	assert := assert.New(t)
	model := grpc.Model{
		Infrastructure: &grpc.Infrastructure{
			Nodes: []*grpc.Node{
				{Id: "C1",
					CPU:    &grpc.Resource{Capacity: 1000},
					Memory: &grpc.Resource{Capacity: 8000}}}}}
	s, err := newStateFromModel(&model)
	assert.NoError(err)
	assert.NoError(s.nodesForWorkloads())
}

func TestStateNodesForWorkloads_happyCase(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromText("nodes-happy", `
		workload assignment v1
		model nodes-happy
		application A1
		workload W1 needs cpu:100  ram:1000
		workload W2 needs cpu:1500 ram:2000
		application A2
		workload W1 needs cpu:100  ram:1000
		workload W2 needs cpu:1500 ram:2000
		node N1 provides cpu:1000 ram:8000
		node N2 provides cpu:50   ram:500
		node N3 provides cpu:2000 ram:16000`)
	require.NoError(t, err)
	assert.NoError(s.workloadNodes())
	assert.NoError(s.nodesForWorkloads())
	assert.Equal([]nodeIndex{0, 2}, s.workloads[0].nodes)
	assert.Equal([]nodeIndex{2}, s.workloads[1].nodes)
	assert.Equal([]nodeIndex{0, 2}, s.workloads[2].nodes)
	assert.Equal([]nodeIndex{2}, s.workloads[3].nodes)
}

func TestStateNodesForWorkloads_happyCase_recommendations(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromText("nodes-happy-recommendations", `
		workload assignment v1
		model nodes-happy-recommendations
		application A1
		workload W1 needs cpu:5 ram:10
		workload W2 needs cpu:100 ram:1000
		node N1,N2 provides cpu:50 ram:100
		node N3,N4 provides cpu:500 ram:2000
		recommendations
		workload A1•W1 (N1:75 N2:50 N3:25)
		workload A1•W2 (N1:75 N4:50 N3:25)`)
	assert.NoError(err)
	assert.NoError(s.workloadNodes())
	assert.Len(s.workloads[0].nodes, 4)
	assert.Len(s.workloads[1].nodes, 2)
	assert.NoError(s.nodesForWorkloads())
	assert.Equal(
		[]nodePrio{{0, 750}, {1, 500}, {2, 250}},
		s.workloads[0].priorities)
	assert.Equal(
		[]nodePrio{{0, 750}, {3, 500}, {2, 250}},
		s.workloads[1].priorities)
}

func TestStateNodesForWorkloads_happyCase_costs(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromText("nodes-happy-costs", `
		workload assignment v1
		model nodes-happy-costs
		application A1
		workload W1 needs cpu:5 ram:10
		workload W2 needs cpu:100 ram:1000
		node N1,N2 provides cpu:50 ram:100
		node N3,N4 provides cpu:500 ram:2000
		costs
		nodes (N1:100 N2:200)
		workload A1•W1 (N1:75 N2:50 N3:25)
		workload A1•W2 (N1:75 N4:50 N3:25)`)
	assert.NoError(err)
	assert.NoError(s.workloadNodes())
	assert.Len(s.workloads[0].nodes, 4)
	assert.Len(s.workloads[1].nodes, 2)
	assert.NoError(s.nodesForWorkloads())
	assert.Equal(
		[]nodePrio{{0, 625}, {1, 750}, {2, 875}},
		s.workloads[0].priorities)
	assert.Equal(
		[]nodePrio{{0, 625}, {3, 750}, {2, 875}, {1, 0}},
		s.workloads[1].priorities)
}

func TestStateNodesForWorkloads_noSuitableNode(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromText("no-suitable-nodes", `
		workload assignment v1
		model no-suitable-node
		application A1 workload W1 needs cpu:10000 ram:10000
		node N1 provides cpu:500 ram:16000
		node N2 provides cpu:2000 ram:8000`)
	assert.NoError(err)
	assert.ErrorContains(s.nodesForWorkloads(), "no node for workload A1.W1")
}
