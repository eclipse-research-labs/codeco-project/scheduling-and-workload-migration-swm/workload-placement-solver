// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"fmt"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data"
	"siemens.com/qos-solver/pkg/textformat"
)

type nameIndex int
type appIndex int
type wlIndex int
type chanIndex int
type nodeIndex int
type pathIndex int

type stateOptions struct {
	findMax             int
	compoundNames       bool
	implicitReversePath bool
	shortestPathsOnly   bool
}

type state struct {
	names     []string
	apps      []application
	workloads []workload
	channels  []channel
	nodes     []node
	nodeNws   [][]*medium
	inM, outM []*medium
	networks  []network
	stats     Stats
	options   stateOptions
	x         []decisionVariable
}

func (s *state) nameOf(obj interface{}) string {
	switch x := obj.(type) {
	case *application:
		if x == nil {
			return "nil"
		}
		return x.name
	case *workload:
		if x == nil {
			return "nil"
		}
		an, wn := s.apps[x.app].name, x.name
		if strings.HasPrefix(wn, an+"_") {
			return wn
		}
		return fmt.Sprintf("%s.%s", an, wn)
	case *channel:
		if x == nil {
			return "nil"
		}
		src := s.nameOf(&s.workloads[x.src])
		dst := s.nameOf(&s.workloads[x.dst])
		return fmt.Sprintf("%s→%s", src, dst)
	case *node:
		if x == nil {
			return "nil"
		}
		return x.name
	case *medium:
		if x == nil {
			return "nil"
		}
		return x.name
	case *link:
		if x == nil {
			return "nil"
		}
		return x.name
	case *path:
		if x == nil {
			return "nil"
		}
		return x.name
	default:
		return "???"
	}
}

func (s *state) addName(name string) nameIndex {
	if s.names == nil {
		s.names = make([]string, 0, 1024)
		s.names = append(s.names, "")
	}
	for i, n := range s.names {
		if n == name {
			return nameIndex(i)
		}
	}
	n := len(s.names)
	if n == cap(s.names) {
		s.names = append(make([]string, 0, n+1024), s.names...)
	}
	s.names = append(s.names, name)
	return nameIndex(n)
}

func (s *state) reset() {
	s.apps = s.apps[:cap(s.apps)]
	s.workloads = s.workloads[:cap(s.workloads)]
	s.channels = s.channels[:cap(s.channels)]
	s.x = s.x[:cap(s.x)]
	for i := range s.workloads {
		s.workloads[i].dv = nil
	}
	for i := range s.channels {
		s.channels[i].dv = nil
	}
	for i := range s.nodes {
		s.nodes[i].cpu.Reset()
		s.nodes[i].ram.Reset()
		s.nodes[i].in.Reset()
		s.nodes[i].out.Reset()
	}
	for i := range s.networks {
		n := &s.networks[i]
		for i := range n.media {
			n.media[i].Reset()
		}
	}
	s.stats = Stats{}
	s.x = nil
}

func (s *state) init(a *grpc.ApplicationGroup, i *grpc.Infrastructure) {
	s.options.compoundNames = textformat.CompoundNames
	s.options.implicitReversePath = textformat.ImplicitReversePath
	s.options.shortestPathsOnly = textformat.ShortestPathsOnly
	s.allocateApps(a)
	s.allocateInfra(i)
}

func newStateFromModel(m *grpc.Model) (state, error) {
	var s state
	if m == nil {
		s.init(nil, nil)
	} else {
		if !m.Costs.Empty() && !m.Recommendations.Empty() {
			return state{}, errRecsNotAllowed
		}
		s.init(m.AppGroup, m.Infrastructure)
		if err := s.addAppModel(m.AppGroup); err != nil {
			return state{}, err
		}
		if err := s.addInfraModel(m.Infrastructure); err != nil {
			return state{}, err
		}
		if err := s.addCosts(m.Costs); err != nil {
			return state{}, err
		}
		if err := s.addRecs(m.Recommendations); err != nil {
			return state{}, err
		}
	}
	return s, nil
}

func newStateFromAsgmt(asgmt textformat.Assignment) (state, error) {
	var s state
	s.options.compoundNames = asgmt.Options.CompoundNames
	s.options.implicitReversePath = asgmt.Options.ImplicitReversePath
	s.options.shortestPathsOnly = asgmt.Options.ShortestPathsOnly
	s.allocateApps(asgmt.AppGroup)
	s.allocateInfra(asgmt.Infrastructure)
	if err := s.addAppModel(asgmt.AppGroup); err != nil {
		return state{}, err
	}
	if err := s.addInfraModel(asgmt.Infrastructure); err != nil {
		return state{}, err
	}
	if err := s.addCosts(asgmt.Costs); err != nil {
		return state{}, err
	}
	if err := s.addRecs(asgmt.Recommendations); err != nil {
		return state{}, err
	}
	return s, nil
}

func newStateFromData(name string, args ...string) (state, error) {
	model := data.GetModel(name, args...)
	if model == nil {
		return state{}, errUnknownDataSet
	}
	return newStateFromAsgmt(textformat.Assignment{
		Name: name,
		Options: textformat.ModelOptions{
			CompoundNames:       textformat.CompoundNames,
			ImplicitReversePath: textformat.ImplicitReversePath,
			ShortestPathsOnly:   textformat.ShortestPathsOnly},
		AppGroup:        model.AppGroup,
		Infrastructure:  model.Infrastructure,
		Costs:           model.Costs,
		Recommendations: model.Recommendations})
}

func newStateFromText(model, input string) (state, error) {
	if asgmt, err := textformat.Parse(model, input); err != nil {
		return state{}, err
	} else {
		return newStateFromAsgmt(asgmt)
	}
}
