// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

func mkPath(s *state, nodes string) *path {
	n := &s.networks[0]
	p, err := n.buildPath(makePath(nodes), s)
	if err != nil {
		panic(err.Error())
	}
	return &p
}

func TestPathEqual(t *testing.T) {
	state, err := newStateFromText("path-equal", `
		workload assignment v1
		model path-equal
		application dummy workload dummy
		node n1,n2,n3,n4 provides cpu:1000 ram:8000
		network net
		link n1↔n2,n1↔n3,n1↔n4,n2↔n4,n3↔n4 with lat:1 bw:2`)
	require.NoError(t, err)

	p1 := mkPath(&state, "n1•n4")
	p2 := mkPath(&state, "n1•n2•n4")
	p3 := mkPath(&state, "n1•n3•n4")
	p4 := mkPath(&state, "n1•n2")
	assert := assert.New(t)
	assert.True(p1.equal(p1))
	assert.True(p2.equal(p2))
	assert.False(p1.equal(p3))
	assert.False(p2.equal(p3))
	assert.False(p1.equal(p4))
}

func cpu(cap int, load ...int) types.Resource[milliCPU] {
	r := types.Resource[milliCPU]{Cap: milliCPU(cap)}
	if len(load) > 0 {
		r.Load(milliCPU(load[0]))
	}
	return r
}
func ram(cap int, load ...int) types.Resource[ramMB] {
	r := types.Resource[ramMB]{Cap: ramMB(cap)}
	if len(load) > 0 {
		r.Load(ramMB(load[0]))
	}
	return r
}
func bw(cap int) types.Resource[bandwidth] {
	return types.Resource[bandwidth]{Cap: bandwidth(cap)}
}

func TestStateNameOf(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromText("name-of", `
		workload assignment v1
		model name-of
		application A1
		workload W1,W2 needs cpu:100 ram:1000
		channel W1->W2 needs lat:5 bw:100
		node N1,N2 provides cpu:1000 ram:8000
		network net link N1•N2 with lat:2 bw:1000 path:yes`)
	assert.NoError(err)

	var app *application = nil
	assert.Equal("A1", s.nameOf(&s.apps[0]))
	assert.Equal("nil", s.nameOf(app))

	var workload *workload = nil
	assert.Equal("A1.W1", s.nameOf(&s.workloads[0]))
	assert.Equal("nil", s.nameOf(workload))

	var channel *channel = nil
	assert.Equal("A1.W1→A1.W2", s.nameOf(&s.channels[0]))
	assert.Equal("nil", s.nameOf(channel))

	var node *node = nil
	assert.Equal("N1", s.nameOf(&s.nodes[0]))
	assert.Equal("nil", s.nameOf(node))

	var link *link = nil
	assert.Equal("N1→N2", s.nameOf(&s.networks[0].links[0]))
	assert.Equal("nil", s.nameOf(link))

	var path *path = nil
	assert.Equal("N1→N2", s.nameOf(&s.networks[0].paths[0]))
	assert.Equal("nil", s.nameOf(path))

	assert.Equal("???", s.nameOf("unknown type"))
}

const twoAppsModelText string = `
workload assignment v1
model two-apps
application A1
workload W1 needs cpu:500 ram:2000
workload W2 needs cpu:500 ram:2000
channel W1↔W2 needs lat:5 bw:100
application A2
workload W1 needs cpu:500 ram:2000
workload W2 needs cpu:500 ram:2000
channel W1↔W2 needs lat:5 bw:100
node dummy`

const threeNodeModelText string = `
workload assignment v1
model three-nodes
application dummy workload dummy
node N1,N2,N3 provides cpu:1000 ram:8000
network net
link N1↔N2,N1•N3,N2•N3 with lat:2 bw:1000
paths (N1•N2 N1•N3)`

func TestStateAddName(t *testing.T) {
	assert := assert.New(t)
	s := state{}
	assert.Equal(nameIndex(1), s.addName("A"))
	assert.Equal(2, len(s.names))
	assert.Equal(1024, cap(s.names))

	assert.Equal(nameIndex(2), s.addName("B"))
	assert.Equal(3, len(s.names))
	assert.Equal(1024, cap(s.names))

	assert.Equal(nameIndex(1), s.addName("A"))
	assert.Equal(3, len(s.names))
	assert.Equal(1024, cap(s.names))

	s.names = make([]string, 0, 2)
	assert.Equal(nameIndex(0), s.addName("A"))
	assert.Equal(nameIndex(1), s.addName("B"))
	assert.Equal(nameIndex(2), s.addName("C"))
	assert.Equal(1026, cap(s.names))
}

func TestNewState_happyCase(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("new-state", `
		workload assignment v1
		model new-state options implicit-reverse-path:no
		application A1
		workload W1 needs cpu:500 ram:2000
		workload W2 needs cpu:500 ram:2000
		channel W1↔W2 needs lat:5 bw:100
		application A2
		workload W1 needs cpu:500 ram:2000
		workload W2 needs cpu:500 ram:2000
		channel W1↔W2 needs lat:5 bw:100
		node N1,N2,N3 provides cpu:1000 ram:8000
		network net
		link N1↔N2,N1•N3,N2•N3 with lat:2 bw:1000
		paths (N1•N2 N1•N3)`)
	require.NoError(t, err)
	assert.Equal(2, len(state.apps))
	assert.Equal(2, cap(state.apps))
	assert.Equal(4, len(state.workloads))
	assert.Equal(4, cap(state.workloads))
	assert.Equal(4, len(state.channels))
	assert.Equal(4, cap(state.channels))
	assert.Equal(3, len(state.nodes))
	assert.Equal(3, cap(state.nodes))
	assert.Equal(1, len(state.networks))
	assert.Equal(1, cap(state.networks))
	assert.Equal(6, len(state.nodeNws))
	assert.Equal(6, cap(state.nodeNws))
	assert.Equal(4, len(state.inM))
	assert.Equal(4, cap(state.inM))
	assert.Equal(4, len(state.outM))
	assert.Equal(4, cap(state.outM))
	assert.Nil(state.x)
}

func TestNewState_errorCases(t *testing.T) {
	assert := assert.New(t)
	_, err := newStateFromText("duplicate-application", `
		workload assignment v1
		model duplicate-application
		application A1 workload W1
		application A1 workload A1
		node N1`)
	assert.ErrorContains(err, `duplicate application: A1`)

	_, err = newStateFromText("duplicate-node", `
		workload assignment v1
		model duplicate-node
		application A1 workload A1
		node N1,N1`)
	assert.ErrorContains(err, `duplicate node: N1`)
}

func TestNewStateFromModel(t *testing.T) {
	check := func(m *grpc.Model) {
		s, err := newStateFromModel(m)
		require.NoError(t, err)
		assert.Len(t, s.workloads, 0)
		assert.Len(t, s.channels, 0)
		assert.Len(t, s.nodes, 0)
		assert.Len(t, s.networks, 0)
		assert.Len(t, s.x, 0)
	}
	check(nil)

	var m grpc.Model
	check(&m)
}

func TestNewStateWithCosts(t *testing.T) {
	state, err := newStateFromText("with-costs", `
		workload assignment v1
		model with-costs
		application a1 workload w1,w2
		application a2 workload w1,w2
		node c1,c2,c3,c4
		costs
		nodes (c1:100 c2:200)
		workload a1•w1 (c1:75 c2:50 c3:25)
		workload a1•w2 (c1:75 c4:50 c3:25)`)
	require.NoError(t, err)
	require.Len(t, state.workloads, 4)
	require.Len(t, state.nodes, 4)
	assert.Equal(t,
		[]nodePrio{{0, 625}, {1, 750}, {2, 875}},
		state.workloads[0].priorities)
	assert.Equal(t,
		[]nodePrio{{0, 625}, {3, 750}, {2, 875}, {1, 0}},
		state.workloads[1].priorities)
}

func TestNetStateWithCosts_errors(t *testing.T) {
	check := func(cost, errStr string) {
		_, err := newStateFromText("with-costs-n-errors", `
			workload assignment v1
			model with-costs-n-errors
			application a1 workload w1,w2
			application a2 workload w1,w2
			node c1,c2,c3,c4
			costs`+"\n"+
			cost)
		assert.ErrorContains(t, err, errStr)
	}
	check("workload ax•w1 (c1:75 c2:50)", "unknown application: ax")
	check("workload a1•wx (c1:75 c2:50)", "unknown workload: a1.wx")
	check("workload a1•w1 (cx:75 c2:50)", "unknown node: cx")

	asgmt, err := textformat.Parse("with-costs-n-errors", `
		workload assignment v1
		model with-costs-n-errors
		application a1 workload w1,w2
		application a2 workload w1,w2
		node c1,c2,c3,c4
		costs
		workload ax•w1 (c1:75 c2:50)`)
	require.NoError(t, err)
	// Text format does not support negative values
	asgmt.Costs.Workloads[0].Nodes[0].Value = -1
	_, err = newStateFromAsgmt(asgmt)
	assert.ErrorContains(t, err, "value out of bounds: -1")
}

func TestNewStateWithRecommendations(t *testing.T) {
	state, err := newStateFromText("with-recommendations", `
		workload assignment v1
		model with-recommendations
		application a1 workload w1,w2
		application a2 workload w1,w2
		node c1,c2,c3,c4
		recommendations
		workload a1•w1 (c1:75 c2:50)
		workload a2•w2 (c4:25 c3:50 c2:75)`)
	require.NoError(t, err)
	require.Len(t, state.workloads, 4)
	require.Len(t, state.nodes, 4)
	assert.Equal(t,
		[]nodePrio{{0, 750}, {1, 500}},
		state.workloads[0].priorities)
	assert.Equal(t,
		[]nodePrio{{3, 250}, {2, 500}, {1, 750}},
		state.workloads[3].priorities)
}

func TestNetStateWithRecommendations_errors(t *testing.T) {
	check := func(rec, errStr string) {
		_, err := newStateFromText("with-recommendations-n-errors", `
			workload assignment v1
			model with-recommendations-n-errors
			application a1 workload w1,w2
			application a2 workload w1,w2
			node c1,c2,c3,c4
			recommendations`+"\n"+
			rec)
		assert.ErrorContains(t, err, errStr)
	}
	check("workload ax•w1 (c1:75 c2:50)", "unknown application: ax")
	check("workload a1•wx (c1:75 c2:50)", "unknown workload: a1.wx")
	check("workload a1•w1 (cx:75 c2:50)", "unknown node: cx")
	check("workload a1•w1 (c1:101)", "value out of bounds: 101")
}

func TestStateReset(t *testing.T) {
	checkReset := func() {
		expected, err := newStateFromData("multiple-apps")
		require.NoError(t, err)
		require.NoError(t, expected.computeDomains())
		state, _ := newStateFromData("multiple-apps")
		state.computeDomains()
		err = state.solve()
		assert.NoError(t, err)
		state.reset()
		assert.Equal(t, expected, state)
	}
	defer restore(&textformat.ImplicitReversePath, false)()
	checkReset()
	textformat.ImplicitReversePath = true
	checkReset()
}

func TestBandwidthMgmt(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	asgmt, err := textformat.Parse("input", `
		workload assignment v1
		model bandwidth-mgmt options shortest-path-only:no
		application a1 workload w1 workload w2 channel w1->w2
		application a2 workload w1 workload w2 channel w1->w2
		application a3 workload w1 workload w2 channel w1->w2
		node c1,c2 provides ram:8GiB
		network n1 link 1:c1->c2 with bw:1Mb path:yes
		network n2 link 2:c1->c2 with bw:1Mb path:yes
		network n3 link c1->c1,c2->c2 with bw:10Mb path:yes`)
	require.NoError(err)
	state, err := newStateFromAsgmt(asgmt)
	require.NoError(err)

	require.Len(state.inM, 4)
	require.Len(state.outM, 4)
	require.Len(state.nodes, 2)
	n := &state.nodes[0]
	require.Len(n.inM, 3)
	assert.Equal(1, len(n.inM[0])+len(n.inM[1])+len(n.inM[2]))
	assert.Same(state.inM[0], n.inM[2][0]) //  n3:c1->c1
	require.Len(n.outM, 3)
	assert.Equal(3, len(n.outM[0])+len(n.outM[1])+len(n.outM[2]))
	assert.Same(state.outM[0], n.outM[0][0]) // n1:c1->c2
	assert.Same(state.outM[1], n.outM[1][0]) // n2:c1->c2
	assert.Same(state.outM[2], n.outM[2][0]) // n3:c1->c1

	n = &state.nodes[1]
	require.Len(n.inM, 3)
	assert.Equal(3, len(n.inM[0])+len(n.inM[1])+len(n.inM[2]))
	assert.Same(state.inM[1], n.inM[0][0]) // n1:c1->c2
	assert.Same(state.inM[2], n.inM[1][0]) // n2:c1->c2
	assert.Same(state.inM[3], n.inM[2][0]) // n3:c2->c2
	require.Len(n.outM, 3)
	assert.Equal(1, len(n.outM[0])+len(n.outM[1])+len(n.outM[2]))
	assert.Same(state.outM[3], n.outM[2][0]) // n3:c2->c2
}

func TestBandwidthMgmt_networkNodes(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	asgmt, err := textformat.Parse("input", `
		workload assignment v1
		model bandwidth-mgmt options shortest-path-only:no
		application a1 workload w1 workload w2 channel w1->w2
		application a2 workload w1 workload w2 channel w1->w2
		application a3 workload w1 workload w2 channel w1->w2
		node n1
		node c1 provides ram:8GiB
		node n2
		node c2 provides ram:8GiB
		network N1
		link 1:c1->n1,2:n1->n2,3:n2->c2 with bw:1Mb
		paths (4:1,2,3)
		network N2
		link 5:c1->n1,6:n1->n2,7:n2->c2 with bw:1Mb
		paths (8:5,6,7)
		network N3
		link 9:c1->c1,10:c2->c2 with bw:10Mb path:yes`)
	require.NoError(err)
	state, err := newStateFromAsgmt(asgmt)
	require.NoError(err)

	require.Len(state.inM, 4)
	require.Len(state.outM, 4)
	require.Len(state.nodes, 4)
	n := &state.nodes[0]
	assert.Zero(len(n.inM) + len(n.outM))

	n = &state.nodes[1]
	require.Len(n.inM, 3)
	assert.Equal(1, len(n.inM[0])+len(n.inM[1])+len(n.inM[2]))
	assert.Same(state.inM[0], n.inM[2][0]) //  N3:c1->c1
	require.Len(n.outM, 3)
	assert.Equal(3, len(n.outM[0])+len(n.outM[1])+len(n.outM[2]))
	assert.Same(state.outM[0], n.outM[0][0]) // N1:c1->n1
	assert.Same(state.outM[1], n.outM[1][0]) // N2:c1->n1
	assert.Same(state.outM[2], n.outM[2][0]) // N3:c1->c1

	n = &state.nodes[2]
	assert.Zero(len(n.inM) + len(n.outM))

	n = &state.nodes[3]
	require.Len(n.inM, 3)
	assert.Equal(3, len(n.inM[0])+len(n.inM[1])+len(n.inM[2]))
	assert.Same(state.inM[1], n.inM[0][0]) // N1:n2->c2
	assert.Same(state.inM[2], n.inM[1][0]) // N2:n2->c2
	assert.Same(state.inM[3], n.inM[2][0]) // N3:c2->c2
	require.Len(n.outM, 3)
	assert.Equal(1, len(n.outM[0])+len(n.outM[1])+len(n.outM[2]))
	assert.Same(state.outM[3], n.outM[2][0]) // N3:c2->c2
}
