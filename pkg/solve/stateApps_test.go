// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestApplicationSetChannels(t *testing.T) {
	assert := assert.New(t)
	chanSrc := func(src wlIndex) channel {
		return channel{pair: pair[wlIndex]{src: src}}
	}
	cs := []channel{chanSrc(5), chanSrc(5), chanSrc(7), chanSrc(8)}
	app := application{
		base: 4,
		workloads: []workload{
			{name: "w1"}, {name: "w2"}, {name: "w3"}, {name: "w4"},
			{name: "w5"}, {name: "w6"}, {name: "w7"}}}
	app.setChannels(cs)
	for i := range app.workloads {
		wi := &app.workloads[i]
		assert.Equal(len(wi.channels), cap(wi.channels))
	}
	assert.Equal([]channel(nil), app.workloads[0].channels)
	assert.Equal(
		[]channel{chanSrc(5), chanSrc(5)},
		app.workloads[1].channels)
	assert.Equal([]channel(nil), app.workloads[2].channels)
	assert.Equal([]channel{chanSrc(7)}, app.workloads[3].channels)
	assert.Equal([]channel{chanSrc(8)}, app.workloads[4].channels)
	assert.Equal([]channel(nil), app.workloads[5].channels)
	assert.Equal([]channel(nil), app.workloads[6].channels)

	app.setChannels(nil)
	assert.Equal([]channel(nil), app.workloads[0].channels)
	assert.Equal([]channel(nil), app.workloads[1].channels)
	assert.Equal([]channel(nil), app.workloads[2].channels)
	assert.Equal([]channel(nil), app.workloads[3].channels)
	assert.Equal([]channel(nil), app.workloads[4].channels)
	assert.Equal([]channel(nil), app.workloads[5].channels)
	assert.Equal([]channel(nil), app.workloads[6].channels)
}

func TestStateAddApplication(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("add-application", `
		workload assignment v1
		model two-apps
		application A1 workload W1,W2 needs cpu:500 ram:2000
		application A2 workload W1,W2 needs cpu:500 ram:2000
		node dummy`)
	require.NoError(t, err)
	for i := range state.apps {
		ai := &state.apps[i]
		assert.Equal(len(ai.workloads), cap(ai.workloads))
	}
	require.Len(t, state.apps, 2)
	assert.Equal(
		application{
			name: "A1", base: 0,
			workloads: []workload{
				{app: 0, name: "W1", index: 0, cpu: 500, ram: 2000},
				{app: 0, name: "W2", index: 1, cpu: 500, ram: 2000}}},
		state.apps[0])
	assert.Equal(
		application{
			name: "A2", base: 2,
			workloads: []workload{
				{app: 1, name: "W1", index: 2, cpu: 500, ram: 2000},
				{app: 1, name: "W2", index: 3, cpu: 500, ram: 2000},
			}},
		state.apps[1])
	assert.Equal(
		[]workload{
			{app: 0, name: "W1", index: 0, cpu: 500, ram: 2000},
			{app: 0, name: "W2", index: 1, cpu: 500, ram: 2000},
			{app: 1, name: "W1", index: 2, cpu: 500, ram: 2000},
			{app: 1, name: "W2", index: 3, cpu: 500, ram: 2000}},
		state.workloads)
}

func TestStateFindWorkload(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("find-workload", `
		workload assignment v1
		model find-workload
		application A1 workload W1,W2
		application A2 workload W1,W2
		infrastructure node C`)
	require.NoError(t, err)
	idx, err := state.findWorkload("Ax", "Wy")
	assert.Equal(wlIndex(-1), idx)
	assert.ErrorContains(err, `unknown application: Ax`)
	idx, err = state.findWorkload("A1", "Wy")
	assert.Equal(wlIndex(-1), idx)
	assert.ErrorContains(err, `unknown workload: A1.Wy`)
	idx, err = state.findWorkload("A1", "W2")
	assert.Equal(wlIndex(1), idx)
	assert.NoError(err)
	idx, err = state.findWorkload("A2", "W2")
	assert.Equal(wlIndex(3), idx)
	assert.NoError(err)
}

func mkChannel(src, dst int, name string, index, lat, bw int) channel {
	return channel{
		pair:  pair[wlIndex]{src: wlIndex(src), dst: wlIndex(dst)},
		name:  name,
		index: chanIndex(index),
		lat:   latency(lat),
		bw:    bandwidth(bw)}
}

func TestSateAddChannel(t *testing.T) {
	assert := assert.New(t)
	model := func(ch string) string {
		return `workload assignment v1
		model add-channel
		application A1 workload W1,W2
		` + ch + "\nnode dummy"
	}
	state, err := newStateFromText("add-channel-happy",
		model("channel A1•W1->A1•W2 needs lat:5 bw:100 qos:assured"))
	require.NoError(t, err)
	require.Len(t, state.channels, 1)
	ch := state.channels[0]
	assert.Equal(latency(5), ch.lat)
	assert.Equal(bandwidth(100), ch.bw)
	assert.Equal(scAssured, ch.qos)

	_, err = newStateFromText("unknown-workload",
		model("channel Ax•W1->A1•W2 needs lat:5 bw:100"))
	assert.ErrorContains(err, `unknown application: Ax`)

	_, err = newStateFromText("unknown-workload",
		model("channel A1•Wx->A1•W2 needs lat:5 bw:100"))
	assert.ErrorContains(err, `unknown workload: A1.Wx`)
}

func TestStateAddAppModel_happyCase(t *testing.T) {
	assert := assert.New(t)
	state, err := newStateFromText("two-apps", twoAppsModelText)
	require.NoError(t, err)
	assert.Len(state.apps, 2)
	assert.Len(state.workloads, 4)
	assert.Len(state.channels, 4)
	for i := range state.apps {
		ai := &state.apps[i]
		assert.Equal(len(ai.channels), cap(ai.channels))
	}
	assert.Equal(
		[]channel{
			mkChannel(0, 1, "A1•W1→A1•W2", 0, 5, 100),
			mkChannel(1, 0, "A1•W2→A1•W1", 1, 5, 100)},
		state.apps[0].channels)
	assert.Equal(
		[]channel{
			mkChannel(2, 3, "A2•W1→A2•W2", 2, 5, 100),
			mkChannel(3, 2, "A2•W2→A2•W1", 3, 5, 100)},
		state.apps[1].channels)
}

func TestStateAddAppModel_errorCases(t *testing.T) {
	assert := assert.New(t)
	_, err := newStateFromText("duplicate-application", `
		workload assignment v1
		model duplicate-workload
		application A1 workload W1
		application A1 workload W1
		node N1`)
	assert.ErrorContains(err, `duplicate application: A1`)

	_, err = newStateFromText("duplicate-workload", `
		workload assignment v1
		model duplicate-workload
		application A1 workload W1 workload W1
		node N1`)
	assert.ErrorContains(err, `duplicate workload: A1.W1`)

	_, err = newStateFromText("unknown-application", `
		workload assignment v1
		model unknown-application
		application A1 workload W1 needs cpu:500 ram:1000
		channel X•W2->W1
		node N1`)
	assert.ErrorContains(err, `unknown application: X`)

	_, err = newStateFromText("unknown-workload", `
		workload assignment v1
		model unknown-workload
		application A1 workload W1 needs cpu:500 ram:1000
		channel W2->W1
		node N1`)
	assert.ErrorContains(err, `unknown workload: A1.W2`)
}
