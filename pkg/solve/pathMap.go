// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

type pair[I ~int] struct {
	src, dst I
}

func (x pair[I]) swap() pair[I] {
	return pair[I]{x.dst, x.src}
}

func (x pair[I]) compare(y pair[I]) int {
	if d := int(y.src) - int(x.src); d != 0 {
		return int(d)
	}
	return int(y.dst - x.dst)
}

type pathMap struct {
	nodes nodeIndex
	data  []pathIndex
}

func newPathMap(nodes, _ int) pathMap {
	return pathMap{
		nodes: nodeIndex(nodes), data: make([]pathIndex, nodes*nodes)}
}

func (pm *pathMap) set(p pair[nodeIndex], path pathIndex) int {
	if p.src < 0 || p.dst < 0 || p.src >= pm.nodes || p.dst >= pm.nodes {
		return -1
	}
	idx := int(p.src*pm.nodes + p.dst)
	pm.data[idx] = path + 1
	return idx
}

func (pm *pathMap) find(p pair[nodeIndex]) pathIndex {
	idx := int(p.src*pm.nodes + p.dst)
	return pm.data[idx] - 1
}
