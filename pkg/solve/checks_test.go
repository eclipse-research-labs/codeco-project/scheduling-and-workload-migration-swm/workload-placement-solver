// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestIsK8sName(t *testing.T) {
	assert := assert.New(t)
	assert.True(isK8sName("a"))
	assert.True(isK8sName(strings.Repeat("a", 253)))
	assert.False(isK8sName(""))
	assert.False(isK8sName(strings.Repeat("a", 254)))
	assert.False(isK8sName(` !"§$%&/()=?"`))
}

func TestStateAddApplication_appErrors(t *testing.T) {
	assert := assert.New(t)
	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model app-errors
		application "•" workload w
		application a1 workload w1
		application a2 workload w1
		application a3 workload w1
		node c`)
	require.NoError(t, err)
	apps := asgmt.AppGroup.Applications
	apps[3].Workloads = nil

	s := &state{}
	assert.ErrorContains(s.addApplication(apps[0]), "illegal name")
	assert.Len(s.names, 0)
	assert.NoError(s.addApplication(apps[1]))
	assert.NoError(s.addApplication(apps[2]))
	assert.ErrorContains(s.addApplication(apps[1]), "duplicate application")
	assert.ErrorContains(s.addApplication(apps[3]), "empty application")
}

func TestStateAddApplication_workloadErrors(t *testing.T) {
	assert := assert.New(t)
	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model workload-errors
		application a1 workload w1,w1
		application a2 workload w1 needs labels:("•")
		node c`)
	require.NoError(t, err)
	apps := asgmt.AppGroup.Applications

	s := &state{}
	assert.ErrorContains(s.addApplication(apps[0]), "duplicate workload")
	assert.ErrorContains(s.addApplication(apps[1]), "illegal name")
}

func TestStateAddLabels(t *testing.T) {
	assert := assert.New(t)
	s := &state{}
	ns, err := s.addLabels([]string{"a", "b", "b"}, []string{"c", "c"})
	assert.Equal([]nameIndex{1, 2, ^3}, ns)
	assert.NoError(err)

	ns, err = s.addLabels([]string{"•"}, []string{"a"})
	assert.Nil(ns)
	assert.ErrorContains(err, "illegal name")

	ns, err = s.addLabels([]string{"a"}, []string{"•"})
	assert.Nil(ns)
	assert.ErrorContains(err, "illegal name")

	ns, err = s.addLabels([]string{"a"}, []string{"a"})
	assert.Nil(ns)
	assert.ErrorContains(err, "label required and forbidden")
}

func TestStateAddNode_errors(t *testing.T) {
	assert := assert.New(t)
	asgmt, err := textformat.Parse("test", `
		workload assignment v1 model workload-errors
		application a workload w
		node "•" provides cpu:100 ram:8000
		node c1 provides cpu:100 ram:8000
		node c1 provides cpu:100 ram:8000
		node c2 provides cpu:100 ram:8000 labels:("•")
		node c2 provides cpu:100 ram:8000 labels:(A A)`)
	require.NoError(t, err)
	nodes := asgmt.Infrastructure.Nodes

	s := &state{}
	assert.ErrorContains(s.addNode(nodes[0]), "illegal name")
	assert.NoError(s.addNode(nodes[1]))
	assert.ErrorContains(s.addNode(nodes[2]), "duplicate node")
	assert.ErrorContains(s.addNode(nodes[3]), "illegal name")
	assert.NoError(s.addNode(nodes[4]))
}

func TestNetworkMediumMgmt(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	n := network{}
	m := &grpc.Medium{
		Id: "m1", Bandwidth: &grpc.Resource{Capacity: 2, Used: 1}}
	assert.NoError(n.addMedium(m, 0))
	require.Len(n.media, 1)
	assert.Equal("m1", n.media[0].name)
	b := types.Resource[bandwidth]{Cap: 2}
	b.Load(1)
	assert.Equal(b, n.media[0].Resource)

	assert.ErrorContains(n.addMedium(m, 0), "duplicate medium")
	assert.Len(n.media, 1)
}

func TestStateAddLink_errors(t *testing.T) {
	assert := assert.New(t)
	s, err := newStateFromText("add-link-errors", `
		workload assignment v1
		model add-link-errors
		application dummy workload dummy
		node n1,n2 provides cpu:1000 ram:8000
		network net link n1•n1 with lat:1 bw:2`)
	require.NoError(t, err)
	add := func(src, dst string) error {
		l := &grpc.Network_Link{Source: src, Target: dst}
		l.Medium = s.networks[0].media[0].name
		err := s.addLink(0, l)
		return err
	}
	assert.NoError(add("n1", "n2"))
	assert.ErrorContains(add("n1", "n2"), "duplicate link")
	assert.Len(s.networks[0].links, 2)
}

func makePath(nodes string) *grpc.Network_Path {
	if ns := strings.Split(nodes, "•"); len(ns) > 1 {
		N := len(ns) - 1
		ls := make([]string, N)
		for i := 1; i < len(ns); i++ {
			ls[i-1] = textformat.LinkName(ns[i-1], ns[i])
		}
		return &grpc.Network_Path{
			Id:    textformat.PathName(ns[0], ns[N], N),
			Links: ls}
	}
	return nil
}

func TestStateCheckPath(t *testing.T) {
	s, err := newStateFromText("check-path", `
		workload assignment v1
		model check-path options implicit-reverse-path:no
		application dummy workload dummy
		node n1,n2,n3 provides cpu:1000 ram:8000
		network net
		link n1↔n2,n1↔n3,n2↔n3 with lat:1 bw:2`)
	require.NoError(t, err)
	add := func(nodes string) (err error) {
		net := &s.networks[0]
		if _, err = s.addPath(net, makePath(nodes)); err == nil {
			if err = net.checkPath(len(net.paths)-1, &s); err != nil {
				net.paths = net.paths[:len(net.paths)-1]
			}
		}
		return
	}
	assert := assert.New(t)
	assert.NoError(add("n1•n2•n3"))
	assert.ErrorContains(add("n1•n2•n3"), "duplicate path")
	assert.ErrorContains(add("n1•n3"), "duplicate path")
	assert.ErrorContains(add("n1•n4"), "unknown link")
	assert.Len(s.networks[0].paths, 1)

	s.options.implicitReversePath = true
	assert.ErrorContains(add("n3•n2•n1"), "duplicate path")

	s.options.implicitReversePath = false
	assert.NoError(add("n3•n1"))
	assert.Len(s.networks[0].paths, 2)
}
