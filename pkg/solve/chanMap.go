// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

type chanInfo struct {
	pair[wlIndex]
	channels []chanIndex
}

type chanMap []chanInfo

func newChanMap(n int) chanMap {
	return make([]chanInfo, 0, n)
}

func (pm *chanMap) set(p pair[wlIndex], ch chanIndex) int {
	l := len(*pm)
	if l == 0 {
		*pm = (*pm)[:1]
		cs := make([]chanIndex, 1, 8)
		cs[0] = ch
		(*pm)[0] = chanInfo{p, cs}
		return 0
	}
	lo, hi := 0, l-1
	for lo <= hi {
		m := (lo + hi) / 2
		if d := p.compare((*pm)[m].pair); d == 0 {
			(*pm)[m].channels = append((*pm)[m].channels, ch)
			return m
		} else if d < 0 {
			lo = m + 1
		} else if d > 0 {
			hi = m - 1
		}
	}
	if l == cap(*pm) {
		return -1
	}
	*pm = (*pm)[:l+1]
	for i := l - 1; i >= lo; i-- {
		(*pm)[i+1] = (*pm)[i]
	}
	cs := make([]chanIndex, 1, 8)
	cs[0] = ch
	(*pm)[lo] = chanInfo{p, cs}
	return lo
}

func (pm *chanMap) find(p pair[wlIndex]) []chanIndex {
	lo, hi := 0, len(*pm)-1
	for lo <= hi {
		m := (lo + hi) / 2
		if d := p.compare((*pm)[m].pair); d == 0 {
			return (*pm)[m].channels
		} else if d < 0 {
			lo = m + 1
		} else if d > 0 {
			hi = m - 1
		}
	}
	return nil
}
