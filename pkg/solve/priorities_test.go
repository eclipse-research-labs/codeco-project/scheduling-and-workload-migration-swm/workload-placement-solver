// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNodePriority(t *testing.T) {
	assert := assert.New(t)
	w := workload{cpu: 1000, ram: 8000}
	n := node{cpu: cpu(1000), ram: ram(8000)}
	assert.Equal(priority(3000), nodePriority(&w, &n))

	w = workload{cpu: 100, ram: 800}
	n = node{cpu: cpu(1000, 500), ram: ram(8000, 4000)}
	assert.Equal(priority(1600), nodePriority(&w, &n))

	w = workload{cpu: 250, ram: 2000}
	assert.Equal(priority(1750), nodePriority(&w, &n))

	w = workload{cpu: 500, ram: 4000}
	assert.Equal(priority(2000), nodePriority(&w, &n))

	w = workload{cpu: 5, ram: 8000}
	assert.Equal(priority(1007), nodePriority(&w, &n))
}
