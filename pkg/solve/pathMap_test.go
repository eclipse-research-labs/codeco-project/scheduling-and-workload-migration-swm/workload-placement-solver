// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPairSwap(t *testing.T) {
	p1 := pair[int]{1, 2}
	p2 := pair[int]{2, 1}
	assert.Equal(t, p2, p1.swap())
}

func TestPairCompare(t *testing.T) {
	assert := assert.New(t)
	pi_0_1 := pair[int]{src: 0, dst: 1}
	pi_1_0 := pair[int]{src: 1, dst: 0}
	pi_1_1 := pair[int]{src: 1, dst: 1}
	pi_1_2 := pair[int]{src: 1, dst: 2}
	pi_2_0 := pair[int]{src: 2, dst: 0}
	assert.Equal(-1, pi_1_1.compare(pi_0_1))
	assert.Equal(-1, pi_1_1.compare(pi_1_0))
	assert.Equal(0, pi_1_1.compare(pi_1_1))
	assert.Equal(1, pi_1_1.compare(pi_1_2))
	assert.Equal(1, pi_1_1.compare(pi_2_0))
}

func TestPathMapSet(t *testing.T) {
	assert := assert.New(t)
	pm := newPathMap(3, 6)
	assert.Equal(4, pm.set(pair[nodeIndex]{1, 1}, 17))
	assert.Equal(5, pm.set(pair[nodeIndex]{1, 2}, 17))
	assert.Equal(1, pm.set(pair[nodeIndex]{0, 1}, 17))
	assert.Equal(3, pm.set(pair[nodeIndex]{1, 0}, 17))
	assert.Equal(3, pm.set(pair[nodeIndex]{1, 0}, 18))
	assert.Equal(pathIndex(18), pm.find(pair[nodeIndex]{1, 0}))
	assert.Equal(6, pm.set(pair[nodeIndex]{2, 0}, 17))
	assert.Equal(-1, pm.set(pair[nodeIndex]{1, 3}, 17))

	assert.Equal(3, pm.set(pair[nodeIndex]{1, 0}, 19))
	assert.Equal(pathIndex(19), pm.find(pair[nodeIndex]{1, 0}))
}

func TestPathMapFind(t *testing.T) {
	assert := assert.New(t)
	pm := newPathMap(3, 3)
	assert.Equal(1, pm.set(pair[nodeIndex]{0, 1}, 10))
	assert.Equal(4, pm.set(pair[nodeIndex]{1, 1}, 20))
	assert.Equal(6, pm.set(pair[nodeIndex]{2, 0}, 30))

	assert.Equal(pathIndex(-1), pm.find(pair[nodeIndex]{0, 0}))
	assert.Equal(pathIndex(10), pm.find(pair[nodeIndex]{0, 1}))
	assert.Equal(pathIndex(-1), pm.find(pair[nodeIndex]{1, 0}))
	assert.Equal(pathIndex(20), pm.find(pair[nodeIndex]{1, 1}))
	assert.Equal(pathIndex(-1), pm.find(pair[nodeIndex]{1, 2}))
	assert.Equal(pathIndex(30), pm.find(pair[nodeIndex]{2, 0}))
	assert.Equal(pathIndex(-1), pm.find(pair[nodeIndex]{2, 1}))
}

func BenchmarkPathMapFind(b *testing.B) {
	pm := newPathMap(3, 3)
	pm.set(pair[nodeIndex]{0, 1}, 10)
	pm.set(pair[nodeIndex]{1, 1}, 20)
	pm.set(pair[nodeIndex]{2, 0}, 30)
	for n := 0; n < b.N; n++ {
		pm.find(pair[nodeIndex]{0, 0})
		pm.find(pair[nodeIndex]{0, 1})
		pm.find(pair[nodeIndex]{1, 0})
		pm.find(pair[nodeIndex]{1, 1})
		pm.find(pair[nodeIndex]{1, 2})
		pm.find(pair[nodeIndex]{2, 0})
		pm.find(pair[nodeIndex]{2, 1})
	}
}
