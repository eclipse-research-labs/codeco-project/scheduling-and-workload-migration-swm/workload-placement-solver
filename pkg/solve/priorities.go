// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import "math"

const scale = 1000

type priority uint32

func workloadLRP(_ *workload, n *node) float64 {
	cpu := float64(n.cpu.Used() / n.cpu.Cap)
	ram := float64(n.ram.Used() / n.ram.Cap)
	return 1.0 - (cpu+ram)*0.5
}

func workloadMRP(w *workload, n *node) float64 {
	cpu := float64(w.cpu / n.cpu.Cap)
	ram := float64(w.ram / n.ram.Cap)
	return (cpu + ram) * 0.5
}

func workloadBRA(w *workload, n *node) float64 {
	cpu := float64(w.cpu / n.cpu.Cap)
	ram := float64(w.ram / n.ram.Cap)
	return 1.0 - math.Abs(cpu-ram)
}

const (
	weightLRP = 1.0
	weightMRP = 1.0
	weightBRA = 1.0
)

func nodePriority(w *workload, n *node) priority {
	score := scale *
		(weightLRP*workloadLRP(w, n) +
			weightMRP*workloadMRP(w, n) +
			weightBRA*workloadBRA(w, n))
	return priority(math.Floor(score))
}
