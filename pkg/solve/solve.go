// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"bytes"
	"fmt"
	"sort"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
)

type Stats struct {
	Combinations int // number of combinations checked
	Assignments  int // number of assignments found
	NoNodeFound  int // number of times workload could has no node
	CheckLinks   int // number of checks for network constraints
	NoPathFound  int // network checks stopped due to missing path
	LinkOverload int // network checks stopped due to link overload
	PathSearches int // number of path searched between nodes
}

var stats Stats

type decisionVariable interface {
	getDomain()
	exhausted() bool
	stateChange() bool
	rollback()
	next()
}

type wlAssignment struct {
	workload *workload
	node     *node
}
type channelAssignment struct {
	channel *channel
	path    *path
}

type assignment struct {
	s      *state
	stable bool
	ws     []wlAssignment
	cs     []channelAssignment
}

func (a assignment) toGrpc() *grpc.Assignment {
	ga := &grpc.Assignment{}
	was := a.ws
	if !a.stable {
		was = make([]wlAssignment, len(a.ws))
		copy(was, a.ws)
		sort.Slice(was, func(i, j int) bool {
			wi, wj := was[i].workload, was[j].workload
			return wi.app < wj.app || wi.app == wj.app && wi.index < wj.index
		})
	}
	if l := len(a.ws); l > 0 {
		ga.Workloads =
			make([]*grpc.Assignment_Workload, l)
		for i := range was {
			wa := &was[i]
			ga.Workloads[i] = &grpc.Assignment_Workload{
				Application: a.s.apps[wa.workload.app].name,
				Workload:    wa.workload.name,
				Node:        wa.node.name}
		}
	}
	if l := len(a.cs); l > 0 {
		ga.Channels = make([]*grpc.Assignment_Channel, l)
		for i := range a.cs {
			ca := &a.cs[i]
			ga.Channels[i] = &grpc.Assignment_Channel{
				Channel: ca.channel.name,
				Network: a.s.networks[ca.path.links[0].net].name,
				Path:    ca.path.name}
			if a.s.options.implicitReversePath {
				ga.Channels[i].Source = a.s.nodes[ca.path.src].name
				ga.Channels[i].Target = a.s.nodes[ca.path.dst].name
			}
		}
	}
	return ga
}

func (s *state) assignment() (a assignment) {
	a = assignment{s: s,
		ws: make([]wlAssignment, 0, len(s.workloads)),
		cs: make([]channelAssignment, 0, len(s.channels))}
	for _, v := range s.x {
		if v.exhausted() {
			break
		}
		switch v := v.(type) {
		case *wlVar:
			a.ws = append(a.ws, v.assignment())
		case *chanVar:
			a.cs = append(a.cs, v.assignment())
		}
	}
	return
}

func (s *state) assignmentStable() (asgmt assignment) {
	asgmt = assignment{s: s, stable: true,
		ws: make([]wlAssignment, 0, len(s.workloads)),
		cs: make([]channelAssignment, 0, len(s.channels))}
	for a := range s.apps {
		app := &s.apps[a]
		for w := range app.workloads {
			if dv := app.workloads[w].dv; !dv.exhausted() {
				asgmt.ws = append(asgmt.ws, dv.assignment())
			}
		}
	}
	for c := range s.channels {
		if dv := s.channels[c].dv; !dv.exhausted() {
			asgmt.cs = append(asgmt.cs, dv.assignment())
		}
	}
	return
}

func (a assignment) String() string {
	var buf strings.Builder
	fmt.Fprintf(&buf, "(assignment (workloads")
	for _, wa := range a.ws {
		fmt.Fprintf(&buf, " %s:%s",
			a.s.nameOf(wa.workload),
			wa.node.name)
	}
	fmt.Fprintf(&buf, ") (channels")
	for _, ch := range a.cs {
		fmt.Fprintf(&buf, " %s:%s:%s→%s",
			a.s.nameOf(ch.channel),
			a.s.networks[ch.path.links[0].net].name,
			a.s.nodes[ch.path.src].name,
			a.s.nodes[ch.path.dst].name)
	}
	fmt.Fprintf(&buf, "))")
	return buf.String()
}

type wlVarInfo struct {
	dv *wlVar
	cs []chanVar
}

func (wi wlVarInfo) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "(wlVarInfo %s (", wi.dv)
	for i, v := range wi.cs {
		if i > 0 {
			buf.WriteRune(' ')
		}
		fmt.Fprintf(&buf, "%s", v)
	}
	fmt.Fprintf(&buf, "))")
	return buf.String()
}

type varList struct {
	s  *state
	is []wlVarInfo
	ws []wlVar
	cs []chanVar
	ns []nodeInfo
	ps []*path
}

func (vl varList) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "(varList ")
	for i := range vl.is {
		if i > 0 {
			buf.WriteRune(' ')
		}
		fmt.Fprintf(&buf, "%s", vl.is[i])
	}
	fmt.Fprintf(&buf, ")")
	return buf.String()
}

func newVarList(s *state) varList {
	nc, nn := 0, 0
	for i := range s.channels {
		for j := range s.channels[i].paths {
			nc += len(s.channels[i].paths[j])
		}
	}
	for i := range s.workloads {
		nn += len(s.workloads[i].nodes)
	}
	vl := varList{s: s,
		is: make([]wlVarInfo, len(s.workloads)),
		ws: make([]wlVar, len(s.workloads)),
		cs: make([]chanVar, len(s.channels)),
		ns: make([]nodeInfo, nn),
		ps: make([]*path, nc)}
	lo := 0
	for i := range s.workloads {
		dv := vl.ws[i].init(s, wlIndex(i), vl.ns[lo:lo])
		j, info := i-1, wlVarInfo{dv: dv}
		for ; 0 <= j && dv.less(vl.is[j].dv); j-- {
			vl.is[j+1] = vl.is[j]
		}
		vl.is[j+1] = info
		s.workloads[i].dv = dv
		lo += len(s.workloads[i].nodes)
	}
	return vl
}

func (vl *varList) addChannels() {
	for i, lo, hi, p := 0, 0, 0, 0; i < len(vl.is); i, lo = i+1, hi {
		vi := &vl.is[i]
		addFromTo := func(src, dst *workload) {
			for k := range src.channels {
				ck := &src.channels[k]
				if ck.src == src.index && ck.dst == dst.index {
					v, j := chanVar{}, hi-1
					v.init(vl.s, ck.index, vl.ps[p:p])
					for ; lo <= j && v.less(&vl.cs[j]); j-- {
						vl.cs[j+1] = vl.cs[j]
					}
					vl.cs[j+1] = v
					for i := range v.channel.paths {
						p += len(v.channel.paths[i])
					}
					hi++
				}
			}
		}
		dst := vi.dv.workload
		for j := 0; j < i; j++ {
			src := vl.is[j].dv.workload
			addFromTo(src, dst)
			addFromTo(dst, src)
		}
		for j := lo; j < hi; j++ {
			dv := &vl.cs[j]
			dv.channel.dv = dv
		}
		vi.cs = vl.cs[lo:hi:hi]
	}
}

func (vl *varList) flatten() []decisionVariable {
	vs := make([]decisionVariable, len(vl.s.workloads)+len(vl.s.channels))
	for i, j := 0, 0; i < len(vl.ws); i++ {
		vs[j] = vl.is[i].dv
		j++
		channels := vl.is[i].cs
		for k := range channels {
			vs[j] = &channels[k]
			j++
		}
	}
	return vs
}

func (s *state) setUpVariables() {
	varList := newVarList(s)
	varList.addChannels()
	s.x = varList.flatten()
}

func (s *state) removeLastApplication() bool {
	if len(s.apps) <= 1 {
		return false
	}
	dead := appIndex(len(s.apps) - 1)
	for i := dead - 1; i >= 0; i -= 1 {
		for _, ch := range s.apps[i].channels {
			if s.workloads[ch.dst].app >= dead {
				dead = i
				break
			}
		}
	}
	s.apps = s.apps[:dead]
	nw, nc := 0, 0
	for i := range s.apps {
		nw += len(s.apps[i].workloads)
		nc += len(s.apps[i].channels)
	}
	s.workloads, s.channels = s.workloads[:nw], s.channels[:nc]
	return dead > 0
}

func (s *state) computeDomains() error {
	s.workloadNetworkRequirements()
	s.workloadNodes()
	s.pathBetweenNodes()
	if err := s.pathsForChannels(); err != nil {
		return err
	}
	return s.nodesForWorkloads()
}

func prepStateFromModel(m *grpc.Model) (s state, e error) {
	if s, e = newStateFromModel(m); e == nil {
		e = s.computeDomains()
	}
	return
}

func prepStateFromData(name string, args ...string) (s state, e error) {
	if s, e = newStateFromData(name, args...); e == nil {
		e = s.computeDomains()
	}
	return
}

func (s *state) visit() error {
	s.stats.Assignments++
	if s.stats.Assignments < s.options.findMax {
		return nil
	}
	return errAssignmentFound
}

func (s *state) algorithmB() (err error) {
	const enter_level_l = 2
	const try_x_l = 3
	const try_again = 4
	const backtrack = 5

	s.setUpVariables()
	for l, step := 0, enter_level_l; l >= 0; {
		switch step {
		case enter_level_l:
			if l == len(s.x) {
				if err = s.visit(); err != nil {
					return
				}
				step = backtrack
				continue
			} else {
				s.x[l].getDomain()
			}
		case try_x_l:
			if s.x[l].exhausted() {
				step = backtrack
				continue
			}
			s.stats.Combinations++
			if s.x[l].stateChange() {
				l, step = l+1, enter_level_l
				continue
			}
		case try_again:
			s.x[l].rollback()
			s.x[l].next()
			if !s.x[l].exhausted() {
				step = try_x_l
				continue
			}
		case backtrack:
			if l -= 1; l >= 0 {
				step = try_again
				continue
			}
		}
		step += 1
	}
	return ErrNoAssignmentFound
}

func (s *state) solve() error {
	s.stats = Stats{}
	defer func() { stats = s.stats }()
	for {
		switch err := s.algorithmB(); err {
		case errAssignmentFound:
			return nil
		case ErrNoAssignmentFound:
			if !s.removeLastApplication() {
				return err
			}
		default:
			return fmt.Errorf("internal error: %w", err)
		}
	}
}

func Solve(m *grpc.Model) (*grpc.Assignment, error) {
	state, err := prepStateFromModel(m)
	if err != nil {
		stats = Stats{}
		return nil, err
	}
	if err = state.solve(); err != nil {
		return nil, err
	}
	return state.assignmentStable().toGrpc(), nil
}

func Statistics() Stats {
	return stats
}
