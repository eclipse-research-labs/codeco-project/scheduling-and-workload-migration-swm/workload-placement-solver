// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func makeWlVar(s *state, wl wlIndex, cn int, nodes ...nodeIndex) *wlVar {
	v := newWlVar(s, wl)
	v.curNode = cn
	if len(nodes) > 0 {
		v.nodes = make([]nodeInfo, len(nodes))
		for i, n := range nodes {
			v.nodes[i].node = &s.nodes[n]
		}
	}
	s.workloads[wl].dv = v
	return v
}

func getWlState(t *testing.T) *state {
	t.Helper()
	s, err := newStateFromText("test-model", `
		workload assignment v1
		model test-model
		application A1 workload W1 needs cpu:500 ram:4000
		infrastructure
		node C1 provides cpu:3000 ram:24000 // priority 2166
		node C2 provides cpu:1000 ram:8000  // priority 2500
		node C3 provides cpu:2000 ram:16000 // priority 2250`)
	require.NoError(t, err)
	s.workloadNodes()
	require.NoError(t, s.nodesForWorkloads())
	return &s
}

func TestWlVarGetDomain(t *testing.T) {
	s := getWlState(t)
	v := newWlVar(s, wlIndex(0))
	v.getDomain()
	assert.Equal(t,
		[]nodeInfo{
			{node: &s.nodes[1], priority: 2500},
			{node: &s.nodes[2], priority: 2250},
			{node: &s.nodes[0], priority: 2166}},
		v.nodes)
}

func TestWlVarUsage(t *testing.T) {
	assert := assert.New(t)
	s := getWlState(t)
	v := newWlVar(s, wlIndex(0))
	v.getDomain()
	assert.False(v.exhausted())
	assert.Equal(nodeIndex(1), v.nodeIndex())
	v.next()
	assert.False(v.exhausted())
	assert.Equal(nodeIndex(2), v.nodeIndex())
	v.next()
	assert.False(v.exhausted())
	assert.Equal(nodeIndex(0), v.nodeIndex())
	v.next()
	assert.True(v.exhausted())
	assert.Equal(nodeIndex(-1), v.nodeIndex())
}

func TestWlVarString(t *testing.T) {
	assert := assert.New(t)
	s := getWlState(t)
	v := newWlVar(s, wlIndex(0))
	assert.Equal("(wlVar A1.W1 0@())", v.String())
	v.getDomain()
	assert.Equal("(wlVar A1.W1 0@(C2 C3 C1))", v.String())
	v.next()
	assert.Equal("(wlVar A1.W1 1@(C2 C3 C1))", v.String())
	v.next()
	assert.Equal("(wlVar A1.W1 2@(C2 C3 C1))", v.String())
	v.next()
	assert.Equal("(wlVar A1.W1 3@(C2 C3 C1))", v.String())
}
