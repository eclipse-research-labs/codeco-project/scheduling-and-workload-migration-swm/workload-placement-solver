// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package solve

import (
	"bytes"
	"strconv"
)

type bitset []uint64

func newBitset(size uint) bitset {
	b := make([]uint64, (size+63)/64+1)
	b[0] = uint64(size)
	return b
}

func (b bitset) copy() bitset {
	if b == nil {
		return nil
	}
	c := make([]uint64, len(b))
	copy(c, b)
	return c
}

func (b bitset) String() string {
	var buf bytes.Buffer
	buf.WriteRune('{')
	for i, first := uint(0), true; i < b.size(); i++ {
		if b.isSet(i) {
			if !first {
				buf.WriteRune(',')
			}
			buf.WriteString(strconv.Itoa(int(i)))
			first = false
		}
	}
	buf.WriteRune('}')
	return buf.String()
}

func bitsetOp(b1, b2 bitset, op func(*bitset, bitset)) bitset {
	var b bitset
	if len(b1) >= len(b2) {
		b = b1.copy()
		op(&b, b2)
	} else {
		b = b2.copy()
		op(&b, b1)
	}
	return b
}

func bitsetIntersection(b1, b2 bitset) bitset {
	return bitsetOp(b1, b2, (*bitset).intersect)
}

func bitsetDifference(b1, b2 bitset) bitset {
	return bitsetOp(b1, b2, (*bitset).difference)
}

func bitsetUnion(b1, b2 bitset) bitset {
	return bitsetOp(b1, b2, (*bitset).union)
}

func (b *bitset) assign(b2 bitset) {
	if len(b2) <= cap(*b) {
		*b = (*b)[:len(b2)]
	} else {
		*b = make([]uint64, len(b2))
	}
	copy(*b, b2)
}

func (b *bitset) clear() {
	l := len(*b) - 1
	for i := 1; i <= l; i++ {
		(*b)[i] = 0
	}
}

func (b *bitset) init(bits ...uint) {
	b.clear()
	for _, bit := range bits {
		b.set(bit)
	}
}

func (b *bitset) size() uint {
	return uint((*b)[0])
}

func (b *bitset) set(bit uint) {
	if uint64(bit) < (*b)[0] {
		(*b)[1+bit>>6] |= 1 << (bit & 0x3F)
	}
}

func (b *bitset) get(bit uint) uint {
	if uint64(bit) < (*b)[0] && (*b)[1+bit>>6]&(1<<(bit&0x3F)) != 0 {
		return 1
	}
	return 0
}

func (b *bitset) not() {
	l := len(*b) - 1
	for i := 1; i <= l; i++ {
		(*b)[i] = ^(*b)[i]
	}
	if n := (*b)[0] & 0x3F; n > 0 {
		m := (uint64(1) << n) - 1
		(*b)[l] &= m
	}
}

func (b *bitset) intersect(b2 bitset) {
	l, l2 := len(*b)-1, len(b2)-1
	for i := 1; i <= l && i <= l2; i++ {
		(*b)[i] = (*b)[i] & (b2)[i]
	}
	for i := l2 + 1; i <= l; i++ {
		(*b)[i] = 0
	}
}

func (b *bitset) difference(b2 bitset) {
	l, l2 := len(*b)-1, len(b2)-1
	for i := 1; i <= l && i <= l2; i++ {
		(*b)[i] = (*b)[i] & ^((*b)[i] & (b2)[i])
	}
}

func (b *bitset) union(b2 bitset) {
	if len(b2) <= cap(*b) {
		if l := len(*b); len(b2) > l {
			*b = (*b)[:len(b2)]
			for i := l; i < len(b2); i++ {
				(*b)[i] = 0
			}
		}
		if (*b)[0] < b2[0] {
			(*b)[0] = b2[0]
		}
		for i := 1; i < len(b2); i++ {
			(*b)[i] |= b2[i]
		}
	} else {
		u, i := make([]uint64, len(b2)), 1
		u[0] = b2[0]
		for ; i < len(*b); i++ {
			u[i] = (*b)[i] | b2[i]
		}
		for ; i < len(b2); i++ {
			u[i] = b2[i]
		}
		*b = u
	}
}

func pop(x uint64) uint {
	x = (x & 0x5555_5555_5555_5555) + ((x >> 1) & 0x5555_5555_5555_5555)
	x = (x & 0x3333_3333_3333_3333) + ((x >> 2) & 0x3333_3333_3333_3333)
	x = (x + (x >> 4)) & 0x0F0F_0F0F_0F0F_0F0F
	x = x + (x >> 8)
	x = x + (x >> 16)
	x = x + (x >> 32)
	return uint(x & 0x7F)
}

func (b *bitset) popCount() (N uint) {
	l := len(*b)
	for i := 1; i < l; i++ {
		N += pop((*b)[i])
	}
	return
}

func (b *bitset) isSet(bit uint) bool {
	return b.get(bit) == 1
}
