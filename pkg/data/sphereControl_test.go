// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestSphereControl_appGroup(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.CompoundNames, false)()
	ag, _, err := sphereControl()
	require.NoError(err)

	checkApp := func(idx int, prefix string, names ...string) {
		app := ag.Applications[idx]
		require.Len(app.Workloads, len(names))
		for i, name := range names {
			assert.Equal(prefix+name, app.Workloads[i].Id, name)
		}
	}
	checkChan := func(to, fro int, preSrc, src, preDst, dst string) {
		ch := ag.Channels[to]
		assert.Equal(preSrc+src, ch.SourceWorkload)
		assert.Equal(preDst+dst, ch.TargetWorkload)
		if fro > to {
			ch = ag.Channels[fro]
			assert.Equal(preDst+dst, ch.SourceWorkload)
			assert.Equal(preSrc+src, ch.TargetWorkload)
		}
	}
	require.Len(ag.Applications, 2)
	checkApp(0, "", "webcam", "processor", "controller")
	checkApp(1, "", "prometheus", "grafana")
	require.Len(ag.Channels, 10)
	checkChan(0, 0, "", "webcam", "", "processor")
	checkChan(1, 0, "", "processor", "", "controller")
	checkChan(2, 3, "", "prometheus", "", "processor")
	checkChan(4, 5, "", "prometheus", "", "controller")
	checkChan(6, 7, "", "prometheus", "", "webcam")
	checkChan(8, 9, "", "prometheus", "", "grafana")

	textformat.CompoundNames = true
	ag, _, err = sphereControl()
	require.NoError(err)
	require.Len(ag.Applications, 2)
	checkApp(0, "sphere_", "webcam", "processor", "controller")
	checkApp(1, "dashboard_", "prometheus", "grafana")

	require.Len(ag.Channels, 10)
	checkChan(0, 0, "sphere_", "webcam", "sphere_", "processor")
	checkChan(1, 0, "sphere_", "processor", "sphere_", "controller")
	checkChan(2, 3, "dashboard_", "prometheus", "sphere_", "processor")
	checkChan(4, 5, "dashboard_", "prometheus", "sphere_", "controller")
	checkChan(6, 7, "dashboard_", "prometheus", "sphere_", "webcam")
	checkChan(8, 9, "dashboard_", "prometheus", "dashboard_", "grafana")
}

func TestSphereControl_infrastructure(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()

	_, infra, err := sphereControl()
	require.NoError(err)
	assert.Len(infra.Nodes, 4)
	require.Len(infra.Networks, 4)
	assert.Len(infra.Networks[0].Links, 3)
	assert.Len(infra.Networks[0].Paths, 3)
	assert.Len(infra.Networks[1].Links, 2)
	assert.Len(infra.Networks[1].Paths, 2)
	assert.Len(infra.Networks[2].Links, 6)
	assert.Len(infra.Networks[2].Paths, 6)
	assert.Len(infra.Networks[3].Links, 4)
	assert.Len(infra.Networks[3].Paths, 2)

	textformat.ImplicitReversePath = true
	_, infra, err = sphereControl()
	require.NoError(err)
	assert.Len(infra.Nodes, 4)
	require.Len(infra.Networks, 4)
	assert.Len(infra.Networks[0].Links, 3)
	assert.Len(infra.Networks[0].Paths, 3)
	assert.Len(infra.Networks[1].Links, 2)
	assert.Len(infra.Networks[1].Paths, 2)
	assert.Len(infra.Networks[2].Links, 6)
	assert.Len(infra.Networks[2].Paths, 3)
	assert.Len(infra.Networks[3].Links, 4)
	assert.Len(infra.Networks[3].Paths, 1)
}
