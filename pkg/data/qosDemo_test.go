// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestQosDemo_appGroup(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.CompoundNames, false)()

	ag, _, err := qosDemo()
	require.NoError(err)
	require.Len(ag.Applications, 2)
	require.Len(ag.Applications[0].Workloads, 2)
	assert.Equal("w1", ag.Applications[0].Workloads[0].Id)
	require.Len(ag.Channels, 5)
	assert.Equal("w1", ag.Channels[0].SourceWorkload)
	assert.Equal("w2", ag.Channels[0].TargetWorkload)

	textformat.CompoundNames = true
	ag, _, err = qosDemo()
	require.NoError(err)
	require.Len(ag.Applications, 2)
	require.Len(ag.Applications[0].Workloads, 2)
	assert.Equal("app1_w1", ag.Applications[0].Workloads[0].Id)
	require.Len(ag.Channels, 5)
	assert.Equal("app1_w1", ag.Channels[0].SourceWorkload)
	assert.Equal("app1_w2", ag.Channels[0].TargetWorkload)
}

func TestQosDemo_infrastructure(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()

	_, infra, err := qosDemo()
	require.NoError(err)
	assert.Len(infra.Nodes, 3)
	require.Len(infra.Networks, 3)
	assert.Len(infra.Networks[0].Links, 1)
	assert.Len(infra.Networks[0].Media, 1)
	assert.Len(infra.Networks[0].Paths, 1)
	assert.Len(infra.Networks[1].Links, 3)
	assert.Len(infra.Networks[1].Media, 3)
	assert.Len(infra.Networks[1].Paths, 3)
	assert.Len(infra.Networks[2].Links, 2)
	assert.Len(infra.Networks[2].Media, 2)
	assert.Len(infra.Networks[2].Paths, 2)

	textformat.ImplicitReversePath = true
	_, infra, err = qosDemo()
	require.NoError(err)
	assert.Len(infra.Nodes, 3)
	require.Len(infra.Networks, 3)
	assert.Len(infra.Networks[0].Links, 1)
	assert.Len(infra.Networks[0].Media, 1)
	assert.Len(infra.Networks[0].Paths, 1)
	assert.Len(infra.Networks[1].Links, 3)
	assert.Len(infra.Networks[1].Media, 3)
	assert.Len(infra.Networks[1].Paths, 3)
	assert.Len(infra.Networks[2].Links, 2)
	assert.Len(infra.Networks[2].Media, 2)
	assert.Len(infra.Networks[2].Paths, 2)
}
