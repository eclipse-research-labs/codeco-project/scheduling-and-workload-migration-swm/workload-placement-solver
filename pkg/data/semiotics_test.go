// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data/new"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestSemioticsAppModel(t *testing.T) {
	assert := assert.New(t)
	model := semioticsAppModel(0)
	assert.Len(model.Applications, 5)
	assert.Len(model.Channels, 37)

	model = semioticsAppModel(1)
	assert.Len(model.Applications, 5+1)
	assert.Len(model.Channels, 37+7)

	l := len(model.Applications)
	assert.Equal(
		[]string{"AccSens", "M1"},
		model.Applications[l-1].Workloads[0].RequiredLabels)
	assert.Equal(
		[]string{"TurbSens", "M1"},
		model.Applications[l-1].Workloads[1].RequiredLabels)
	assert.Equal(
		[]string{"Mic", "M1"},
		model.Applications[l-1].Workloads[2].RequiredLabels)
	assert.Equal(
		[]string{"Camera", "M1"},
		model.Applications[l-1].Workloads[3].RequiredLabels)

	model = semioticsAppModel(5)
	assert.Len(model.Applications, 5+5)
	assert.Len(model.Channels, 37+5*7)
}

func TestFindPathsBidiForModelSemiotics(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList(
			"C1•N1•N2•C2",
			"C2•N2•N1•C1"),
		new.FindShortestPathsBidi(model, "C1•C2"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C3",
			"C3•N7•N4•N3•N2•N1•C1"),
		new.FindShortestPathsBidi(model, "C1•C3"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C4",
			"C4•N7•N4•N3•N2•N1•C1"),
		new.FindShortestPathsBidi(model, "C1•C4"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C5",
			"C5•N8•N5•N4•N3•N2•N1•C1"),
		new.FindShortestPathsBidi(model, "C1•C5"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C6",
			"C6•N8•N5•N4•N3•N2•N1•C1"),
		new.FindShortestPathsBidi(model, "C1•C6"))
}

func TestFindAllPathsBidiForModelSemiotics(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList(
			"C1•N1•N2•C2",
			"C2•N2•N1•C1"),
		new.FindAllPathsBidi(model, "C1•C2"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C3",
			"C1•N1•N2•N3•N6•N5•N4•N7•C3",
			"C3•N7•N4•N3•N2•N1•C1",
			"C3•N7•N4•N5•N6•N3•N2•N1•C1"),
		new.FindAllPathsBidi(model, "C1•C3"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C4",
			"C1•N1•N2•N3•N6•N5•N4•N7•C4",
			"C4•N7•N4•N3•N2•N1•C1",
			"C4•N7•N4•N5•N6•N3•N2•N1•C1"),
		new.FindAllPathsBidi(model, "C1•C4"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C5",
			"C1•N1•N2•N3•N6•N5•N8•C5",
			"C5•N8•N5•N4•N3•N2•N1•C1",
			"C5•N8•N5•N6•N3•N2•N1•C1"),
		new.FindAllPathsBidi(model, "C1•C5"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C6",
			"C1•N1•N2•N3•N6•N5•N8•C6",
			"C6•N8•N5•N4•N3•N2•N1•C1",
			"C6•N8•N5•N6•N3•N2•N1•C1"),
		new.FindAllPathsBidi(model, "C1•C6"))
}

func TestFindAllPathsBidiForModelSemiotics_regression(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList(
			"N6•N3•N4•N7•C3",
			"N6•N5•N4•N7•C3",
			"C3•N7•N4•N3•N6",
			"C3•N7•N4•N5•N6"),
		new.FindAllPathsBidi(model, "N6•C3"))
	assert.Equal(
		new.PathList(
			"N6•N3•N4•N7•C4",
			"N6•N5•N4•N7•C4",
			"C4•N7•N4•N3•N6",
			"C4•N7•N4•N5•N6"),
		new.FindAllPathsBidi(model, "N6•C4"))
	assert.Equal(
		new.PathList(
			"N6•N3•N4•N7•C3",
			"N6•N5•N4•N7•C3"),
		new.FindAllBidiPaths(model, "N6•C3"))
	assert.Equal(
		new.PathList(
			"N6•N3•N4•N7•C4",
			"N6•N5•N4•N7•C4"),
		new.FindAllBidiPaths(model, "N6•C4"))
	assert.Equal(
		new.PathList("N6•N3•N4•N7•C3"),
		new.FindShortestBidiPaths(model, "N6•C3"))
	assert.Equal(
		new.PathList("N6•N3•N4•N7•C4"),
		new.FindShortestBidiPaths(model, "N6•C4"))
}

func TestFindBidiPathsForModelSemiotics(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList(
			"C1•N1•N2•C2"),
		new.FindShortestBidiPaths(model, "C1•C2"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C3"),
		new.FindShortestBidiPaths(model, "C1•C3"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C4"),
		new.FindShortestBidiPaths(model, "C1•C4"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C5"),
		new.FindShortestBidiPaths(model, "C1•C5"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C6"),
		new.FindShortestBidiPaths(model, "C1•C6"))
}

func TestAllFindBidiPathsForModelSemiotics(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList(
			"C1•N1•N2•C2"),
		new.FindAllBidiPaths(model, "C1•C2"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C3",
			"C1•N1•N2•N3•N6•N5•N4•N7•C3"),
		new.FindAllBidiPaths(model, "C1•C3"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N7•C4",
			"C1•N1•N2•N3•N6•N5•N4•N7•C4"),
		new.FindAllBidiPaths(model, "C1•C4"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C5",
			"C1•N1•N2•N3•N6•N5•N8•C5"),
		new.FindAllBidiPaths(model, "C1•C5"))
	assert.Equal(
		new.PathList(
			"C1•N1•N2•N3•N4•N5•N8•C6",
			"C1•N1•N2•N3•N6•N5•N8•C6"),
		new.FindAllBidiPaths(model, "C1•C6"))
}

func TestFindPathsForModelSemioticsMillNodes(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList("M1.C1•N1•C1"),
		new.FindShortestPaths(model, "M1.C1•C1"))
	assert.Equal(
		new.PathList("M1.C1•N1•N2•M1.C2"),
		new.FindShortestPaths(model, "M1.C1•M1.C2"))
	assert.Equal(
		new.PathList("M1.C1•N1•N2•C2"),
		new.FindShortestPaths(model, "M1.C1•C2"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N7•C3"),
		new.FindShortestPaths(model, "M1.C1•C3"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N7•C4"),
		new.FindShortestPaths(model, "M1.C1•C4"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N5•N8•C5"),
		new.FindShortestPaths(model, "M1.C1•C5"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N5•N8•C6"),
		new.FindShortestPaths(model, "M1.C1•C6"))
	assert.Equal(
		new.PathList("M1.C2•N2•N1•C1"),
		new.FindShortestPaths(model, "M1.C2•C1"))
	assert.Equal(
		new.PathList("M1.C2•N2•C2"),
		new.FindShortestPaths(model, "M1.C2•C2"))
	assert.Equal(
		new.PathList(
			"M1.C2•N2•N3•N4•N7•C3"),
		new.FindShortestPaths(model, "M1.C2•C3"))
}

func TestFindAllPathsForModelSemioticsMillNodes(t *testing.T) {
	assert := assert.New(t)
	model := semioticsInfraModelWithoutPaths(1)
	assert.Equal(
		new.PathList("M1.C1•N1•C1"),
		new.FindAllPaths(model, "M1.C1•C1"))
	assert.Equal(
		new.PathList("M1.C1•N1•N2•M1.C2"),
		new.FindAllPaths(model, "M1.C1•M1.C2"))
	assert.Equal(
		new.PathList("M1.C1•N1•N2•C2"),
		new.FindAllPaths(model, "M1.C1•C2"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N7•C3",
			"M1.C1•N1•N2•N3•N6•N5•N4•N7•C3"),
		new.FindAllPaths(model, "M1.C1•C3"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N7•C4",
			"M1.C1•N1•N2•N3•N6•N5•N4•N7•C4"),
		new.FindAllPaths(model, "M1.C1•C4"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N5•N8•C5",
			"M1.C1•N1•N2•N3•N6•N5•N8•C5"),
		new.FindAllPaths(model, "M1.C1•C5"))
	assert.Equal(
		new.PathList(
			"M1.C1•N1•N2•N3•N4•N5•N8•C6",
			"M1.C1•N1•N2•N3•N6•N5•N8•C6"),
		new.FindAllPaths(model, "M1.C1•C6"))
	assert.Equal(
		new.PathList("M1.C2•N2•N1•C1"),
		new.FindAllPaths(model, "M1.C2•C1"))
	assert.Equal(
		new.PathList("M1.C2•N2•C2"),
		new.FindAllPaths(model, "M1.C2•C2"))
	assert.Equal(
		new.PathList(
			"M1.C2•N2•N3•N4•N7•C3",
			"M1.C2•N2•N3•N6•N5•N4•N7•C3"),
		new.FindAllPaths(model, "M1.C2•C3"))
}

func TestMillSections(t *testing.T) {
	sects := millSections("M1 M1", "0")
	assert.Len(t, sects, 0)
	sects = millSections("M1 M1", "x")
	assert.Len(t, sects, 0)

	sects = millSections("M1 M1", "1")
	assert.Equal(t, []string{"M1 M1"}, sects)
	sects = millSections("M1 M1", "2")
	assert.Equal(t, []string{"M1 M1", "M2 M2"}, sects)
	sects = millSections("M1 M1", "100")
	assert.Len(t, sects, 20)
}

func checkSemiotics(t *testing.T, mills int) {
	apps, infra, err := semiotics(strconv.Itoa(mills))
	require.NoError(t, err)
	require.NotNil(t, apps)
	require.NotNil(t, infra)
	am := semioticsAppModel(mills)
	assert.Equal(t, am.Applications, apps.Applications)
	assert.Equal(t, am.Channels, apps.Channels)
	im := semioticsInfraModel(mills)
	assertNodesEqualSorted(t, im.Nodes, infra.Nodes)
	assert.Equal(t, im.Networks[0].Links, infra.Networks[0].Links)
	assertPathsEqualSorted(t, im.Networks[0].Paths, infra.Networks[0].Paths)
}

func TestSemioticsAssignment(t *testing.T) {
	checkSemiotics(t, 0)
	checkSemiotics(t, 1)
	checkSemiotics(t, 2)
	checkSemiotics(t, 5)
}

func TestSemiotics_appGroup(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.CompoundNames, false)()

	ag, _, err := semiotics("0")
	require.NoError(err)
	require.Len(ag.Applications, 5)
	require.Len(ag.Applications[0].Workloads, 5)
	assert.Equal("Control", ag.Applications[0].Workloads[0].Id)
	require.Len(ag.Channels, 37)
	assert.Equal("Control", ag.Channels[0].SourceWorkload)
	assert.Equal("Agent1", ag.Channels[0].TargetWorkload)

	textformat.CompoundNames = true
	ag, _, err = semiotics("0")
	require.NoError(err)
	require.Len(ag.Applications, 5)
	require.Len(ag.Applications[0].Workloads, 5)
	assert.Equal("SDN_Control", ag.Applications[0].Workloads[0].Id)
	require.Len(ag.Channels, 37)
	assert.Equal("SDN_Control", ag.Channels[0].SourceWorkload)
	assert.Equal("SDN_Agent1", ag.Channels[0].TargetWorkload)
}

func smokeTestNodes(t *testing.T, model *grpc.Infrastructure) {
	assert := assert.New(t)
	l := len(model.Nodes)
	for i := 0; i < 10; i++ {
		assert.Equal(grpc.Node_COMPUTE, model.Nodes[i].NodeType)
	}
	for i := 10; i < l-4; i += 2 {
		assert.Equal(grpc.Node_COMPUTE, model.Nodes[i].NodeType)
		mn := fmt.Sprintf("M%d", ((i-10)/2)+1)
		assert.Equal(
			[]string{"TurbSens", "Mic", "Camera", mn},
			model.Nodes[i].Labels)
		assert.Equal(grpc.Node_COMPUTE, model.Nodes[i+1].NodeType)
		assert.Equal(
			[]string{"AccSens", mn},
			model.Nodes[i+1].Labels)
	}
	for i := 1; i <= 4; i++ {
		assert.Equal(grpc.Node_NETWORK, model.Nodes[l-1].NodeType)
	}
}

func TestSemiotics_infrastructureExplicit(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()
	defer restore(&textformat.ShortestPathsOnly, false)()

	nodes := func(mills int) int { return 14 + mills*2 }
	lbLinks := func(mills int) int { return 6 + mills*2 }
	ethLinks := func(mills int) int { return 28 + mills*2*2 }
	ethPaths := func(mills int) int { return 138 + mills*2*21 }

	check := func(mills int) {
		_, infra, err := semiotics(strconv.Itoa(mills))
		require.NoError(err)
		assert.Len(infra.Nodes, nodes(mills))
		require.Len(infra.Networks, 2)
		assert.Len(infra.Networks[0].Links, ethLinks(mills), mills)
		assert.Len(infra.Networks[0].Media, ethLinks(mills), mills)
		assert.Len(infra.Networks[0].Paths, ethPaths(mills), mills)
		assert.Len(infra.Networks[1].Links, lbLinks(mills), mills)
		assert.Len(infra.Networks[1].Media, lbLinks(mills), mills)
		assert.Len(infra.Networks[1].Paths, lbLinks(mills), mills)
		smokeTestNodes(t, infra)
	}
	check(0)
	check(1)
	check(5)
}

func TestSemiotics_infrastructureImplicit(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.ImplicitReversePath, true)()
	defer restore(&textformat.ShortestPathsOnly, false)()

	nodes := func(mills int) int { return 14 + mills*2 }
	lbLinks := func(mills int) int { return 6 + mills*2 }
	ethLinks := func(mills int) int { return 28 + mills*2*2 }
	ethPaths := func(mills int) int { return 69 + mills*21 }

	check := func(mills int) {
		_, infra, err := semiotics(strconv.Itoa(mills))
		require.NoError(err)
		assert.Len(infra.Nodes, nodes(mills))
		require.Len(infra.Networks, 2)
		assert.Len(infra.Networks[0].Links, ethLinks(mills), mills)
		assert.Len(infra.Networks[0].Media, ethLinks(mills), mills)
		assert.Len(infra.Networks[0].Paths, ethPaths(mills), mills)
		assert.Len(infra.Networks[1].Links, lbLinks(mills), mills)
		assert.Len(infra.Networks[1].Media, lbLinks(mills), mills)
		assert.Len(infra.Networks[1].Paths, lbLinks(mills), mills)
		smokeTestNodes(t, infra)
	}
	check(0)
	check(1)
	check(5)
}
