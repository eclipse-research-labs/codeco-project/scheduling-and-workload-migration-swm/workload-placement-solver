// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/data/new"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestGetMillCount(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(5, getMillCount([]interface{}{}))
	assert.Equal(1, getMillCount([]interface{}{1}))
	assert.Equal(2, getMillCount([]interface{}{"2"}))
	assert.PanicsWithValue("unsupported type", func() {
		getMillCount([]interface{}{'?'})
	})
}

func TestGetAppNames(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{}, getAppNames([]interface{}{5}))
	assert.Equal([]string{}, getAppNames([]interface{}{5, nil}))
	assert.Equal([]string{"name"}, getAppNames([]interface{}{5, "name"}))
	apps := []string{"eins", "zwei", "drei"}
	assert.Equal(apps, getAppNames([]interface{}{5, apps}))
	assert.PanicsWithValue("unsupported type", func() {
		getAppNames([]interface{}{5, '?'})
	})
}

func TestGetAppModel_happyCase(t *testing.T) {
	assert := assert.New(t)
	assert.Len(GetAppModel("semiotics").Applications, 10)
	assert.Len(GetAppModel("SEMIOTICS").Applications, 10)
	assert.Len(GetAppModel("semiotics-test").Applications, 10)

	var ag new.AppGroup
	assert.Equal(ag.AsGrpc(), GetAppModel("unknown"))
}

func TestGetInfraModel(t *testing.T) {
	assert := assert.New(t)
	assert.Len(GetInfraModel("semiotics", 0).Nodes, 14)
	assert.Len(GetInfraModel("SEMIOTICS", 0).Nodes, 14)

	var infrastructure new.Infrastructure
	assert.Equal(infrastructure.AsGrpc(), GetInfraModel("unknown"))
}

func TestGetAssignment(t *testing.T) {
	assert := assert.New(t)
	for _, mi := range ListModels() {
		name := strings.Replace(mi.Name, "(n)", "", 1)
		apps, infra := GetAssignment(name)
		assert.NotNil(apps, mi.Name)
		assert.NotNil(infra, mi.Name)
	}
	apps, infra := GetAssignment("unknown")
	assert.Nil(apps)
	assert.Nil(infra)
}

func TestGetAssignment_compoundNames(t *testing.T) {
	assert := assert.New(t)
	defer restore(&textformat.CompoundNames, false)()
	a, _ := GetAssignment("multiple-apps")
	app := a.Applications[0]
	wl := app.Workloads[0]
	assert.False(strings.HasPrefix(wl.Id, app.Id+"_"))

	textformat.CompoundNames = true
	a, _ = GetAssignment("multiple-apps")
	app = a.Applications[0]
	wl = app.Workloads[0]
	assert.True(strings.HasPrefix(wl.Id, app.Id+"_"))
}

func TestListModels(t *testing.T) {
	names := []string{"multiple-apps", "qos-demo", "semiotics(n)",
		"sphere-control", "tiny"}
	models := ListModels()
	require.Len(t, models, len(names))
	for i, name := range names {
		assert.Equal(t, name, models[i].Name)
	}
}

func TestGetSource(t *testing.T) {
	assert.NotEmpty(t, GetSource("tiny"))
	assert.NotEmpty(t, GetSource("qos-demo"))
	assert.NotEmpty(t, GetSource("multiple-apps"))
	assert.NotEmpty(t, GetSource("sphere-control"))
	assert.NotEmpty(t, GetSource("semiotics", "0"))
	assert.Empty(t, GetModel("unknown"))
}
