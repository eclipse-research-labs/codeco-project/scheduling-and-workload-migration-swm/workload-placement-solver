// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data/new"
	"siemens.com/qos-solver/pkg/textformat"
)

var appSet []string = []string{"SDN", "SCADA", "Analysis", "MDSP", "AppIntf"}

func millName(i int, name ...string) string {
	if name != nil {
		return fmt.Sprintf("M%d.%s", i, name[0])
	}
	return fmt.Sprintf("M%d", i)
}

func index(a []string, str string) int {
	for i, s := range a {
		if str == s {
			return i
		}
	}
	return -1
}

func appSDN(ag *new.AppGroup, apps []string) {
	ag.Application("SDN").
		Workload("Control", 2000, 500).
		Workload("Agent1", 100, 100, "SDN").
		Workload("Agent2", 100, 100, "SDN").
		Workload("Agent3", 100, 100, "SDN").
		Workload("Agent4", 100, 100, "SDN")
	ag.BidiChannel("SDN.Control•SDN.Agent1", 600, 1, 600, 10)
	ag.BidiChannel("SDN.Control•SDN.Agent2", 600, 1, 600, 10)
	ag.BidiChannel("SDN.Control•SDN.Agent3", 600, 1, 600, 10)
	ag.BidiChannel("SDN.Control•SDN.Agent4", 600, 1, 600, 10)
	if index(apps, "AppIntf") >= 0 {
		ag.BidiChannel("SDN.Control•AppIntf.PatOrch", 2000, 10, 2000, 1)
	}
}

func appSCADA(ag *new.AppGroup, apps []string) {
	ag.Application("SCADA").
		Workload("SCADA", 3000, 4000).
		Workload("PlcEmerg", 100, 200).
		Workload("PlcData", 0, 0, "PLC")
	ag.BidiChannel("SCADA.PlcData•SCADA.PlcEmerg", 10, 50, 10, 1)
	ag.BidiChannel("SCADA.PlcEmerg•SCADA.SCADA", 1000, 50, 1000, 1)
	if index(apps, "Analysis") >= 0 {
		ag.BidiChannel("SCADA.PlcEmerg•Analysis.Accel", 50, 1)
	}
}

func appAnalysis(ag *new.AppGroup, apps []string) {
	ag.Application("Analysis").
		Workload("Accel", 1000, 1000).
		Workload("DCorr", 500, 200).
		Workload("Audio", 500, 200).
		Workload("Video", 2000, 1000)
	if index(apps, "MDSP") >= 0 {
		ag.Channel("Analysis.Accel•MDSP.Connect", 2000, 10)
		ag.Channel("Analysis.DCorr•MDSP.Connect", 2000, 10)
		ag.Channel("Analysis.Audio•MDSP.Connect", 2000, 10)
		ag.Channel("Analysis.Video•MDSP.Connect", 2000, 10)
	}
	if index(apps, "AppIntf") >= 0 {
		ag.BidiChannel("Analysis.Accel•AppIntf.SemGW", 2000, 1)
		ag.BidiChannel("Analysis.DCorr•AppIntf.SemGW", 2000, 1)
		ag.BidiChannel("Analysis.Audio•AppIntf.SemGW", 2000, 1)
		ag.BidiChannel("Analysis.Video•AppIntf.SemGW", 2000, 1)
	}
}

func appMDSP(ag *new.AppGroup, apps []string) {
	ag.Application("MDSP").
		Workload("Connect", 400, 400).
		Workload("CloudIntf", 500, 500, "MDSP")
	ag.Channel("MDSP.Connect•MDSP.CloudIntf", 5000, 50)
}

func appAppIntf(ag *new.AppGroup, apps []string) {
	ag.Application("AppIntf").
		Workload("SemGW", 500, 500).
		Workload("PatOrch", 1000, 1000).
		Workload("RecipeFW", 2000, 2000).
		Workload("KnowRep", 2000, 4000)
	ag.BidiChannel("AppIntf.SemGW•AppIntf.PatOrch", 2000, 1, 2000, 10)
	ag.BidiChannel("AppIntf.RecipeFW•AppIntf.KnowRep", 2000, 1)
	ag.BidiChannel("AppIntf.RecipeFW•AppIntf.PatOrch", 2000, 1)
	ag.BidiChannel("AppIntf.KnowRep•AppIntf.PatOrch", 2000, 1)
}

func appMill(ag *new.AppGroup, i int, apps []string) {
	mill := millName(i)
	ag.Application(millName(i)).
		Workload("Accel", 200, 100, "AccSens", mill).
		Workload("TSens", 200, 100, "TurbSens", mill).
		Workload("Audio", 200, 100, "Mic", mill).
		Workload("Video", 500, 300, "Camera", mill)
	if index(apps, "SCADA") >= 0 {
		ag.Channel(millName(i, "Accel")+"•SCADA.SCADA", 1000, 100)
		ag.Channel(millName(i, "TSens")+"•SCADA.SCADA", 1000, 50)
	}
	if index(apps, "Analysis") >= 0 {
		ag.Channel(millName(i, "Accel")+"•Analysis.Accel", 500, 100)
		ag.Channel(millName(i, "Accel")+"•Analysis.DCorr", 1000, 100)
		ag.Channel(millName(i, "TSens")+"•Analysis.DCorr", 1000, 50)
		ag.Channel(millName(i, "Audio")+"•Analysis.Audio", 500, 320)
		ag.Channel(millName(i, "Video")+"•Analysis.Video", 500, 4000)
	}
}

func appGroup(mills int, apps []string) *new.AppGroup {
	ag := &new.AppGroup{}
	for _, app := range apps {
		var proc func(*new.AppGroup, []string)
		switch app {
		case "SDN":
			proc = appSDN
		case "SCADA":
			proc = appSCADA
		case "Analysis":
			proc = appAnalysis
		case "MDSP":
			proc = appMDSP
		case "AppIntf":
			proc = appAppIntf
		}
		if proc != nil {
			proc(ag, apps)
		}
	}
	for i := 1; i <= mills; i++ {
		appMill(ag, i, apps)
	}
	return ag
}

func semioticsAppModel(mills int) *grpc.ApplicationGroup {
	ag := appGroup(mills, appSet)
	if textformat.CompoundNames {
		ag.MakeCompoundNames()
	}
	return ag.AsGrpc()
}

func semioticsAppModelTest(mills int, apps []string) *grpc.ApplicationGroup {
	if len(apps) == 0 {
		apps = appSet
	}
	ag := appGroup(mills, apps)
	if textformat.CompoundNames {
		ag.MakeCompoundNames()
	}
	return ag.AsGrpc()
}

func semioticsInfraModelWithoutPaths(mills int) *grpc.Infrastructure {
	nodes := []*grpc.Node{
		new.Node("C1", 500, 500, "PLC"),
		new.Node("C2", 4000, 4000),
		new.Node("C3", 4000, 8000),
		new.Node("C4", 4000, 8000),
		new.Node("C5", 8000, 16000, "MDSP"),
		new.Node("C6", 8000, 16000, "MDSP"),
		new.Node("N3", 100, 100, "SDN"),
		new.Node("N4", 100, 100, "SDN"),
		new.Node("N5", 100, 100, "SDN"),
		new.Node("N6", 100, 100, "SDN")}
	loopback := new.Net("Loopback").
		Loopback("C1", 0, 1_000_000_000). // is also a path…
		Loopback("C2", 0, 1_000_000_000).
		Loopback("C3", 0, 1_000_000_000).
		Loopback("C4", 0, 1_000_000_000).
		Loopback("C5", 0, 1_000_000_000).
		Loopback("C6", 0, 1_000_000_000)
	ethernet := new.Net("Ethernet").
		NetworkNode("N1").
		NetworkNode("N2").
		NetworkNode("N7").
		NetworkNode("N8").
		BidiLink("C1•N1", 2, 100_000).
		BidiLink("N1•N2", 1, 1_000_000).
		BidiLink("C2•N2", 2, 100_000).
		BidiLink("N2•N3", 10, 1_000_000).
		BidiLink("N3•N4", 10, 1_000_000).
		BidiLink("N4•N5", 10, 1_000_000).
		BidiLink("N5•N6", 10, 1_000_000).
		BidiLink("N3•N6", 10, 1_000_000).
		BidiLink("N4•N7", 20, 1_000_000).
		BidiLink("C3•N7", 0, 1_000_000_000).
		BidiLink("C4•N7", 0, 1_000_000_000).
		BidiLink("N5•N8", 500, 2_000, 500, 10_000).
		BidiLink("C5•N8", 0, 1_000_000_000).
		BidiLink("C6•N8", 0, 1_000_000_000)
	for i := 1; i <= mills; i++ {
		n1, n2 := millName(i, "C1"), millName(i, "C2")
		nodes = append(nodes,
			new.Node(n1, 2000, 1000, "TurbSens", "Mic", "Camera", millName(i)),
			new.Node(n2, 2000, 1000, "AccSens", millName(i)))
		ethernet.BidiLink(n1+"•N1", 2, 100_000).
			BidiLink(n2+"•N2", 2, 100_000)
		loopback.Loopback(n1, 0, 1_000_000_000).
			Loopback(n2, 0, 1_000_000_000)
	}
	var infrastructure new.Infrastructure
	infrastructure.
		Nodes(nodes).
		Nets(ethernet, loopback)
	return infrastructure.AsGrpc()
}

func semioticsInfraModel(mills int) *grpc.Infrastructure {
	model := semioticsInfraModelWithoutPaths(mills)
	pathFinder := getFindPathsFunc()
	net := model.Networks[0]
	net.Paths = append(net.Paths, pathFinder(model,
		"C1•C2", "C1•C3", "C1•C4", "C1•C5", "C1•C6",
		"C2•C3", "C2•C4", "C2•C5", "C2•C6",
		"C3•C4", "C3•C5", "C3•C6",
		"C4•C5", "C4•C6",
		"C5•C6",
		"N3•C1", "N3•C2", "N3•C3", "N3•C4", "N3•C5", "N3•C6",
		"N4•C1", "N4•C2", "N4•C3", "N4•C4", "N4•C5", "N4•C6",
		"N5•C1", "N5•C2", "N5•C3", "N5•C4", "N5•C5", "N5•C6",
		"N6•C1", "N6•C2", "N6•C3", "N6•C4", "N6•C5", "N6•C6")...)
	// Mill nodes do not talk to network nodes (SDN)
	for i := 1; i <= mills; i++ {
		n1, n2 := millName(i, "C1"), millName(i, "C2")
		net.Paths = append(net.Paths, pathFinder(model,
			n1+"•"+n2, n1+"•C1", n1+"•C2", n1+"•C3", n1+"•C4",
			n1+"•C5", n1+"•C6")...)
		net.Paths = append(net.Paths, pathFinder(model,
			n2+"•C1", n2+"•C2", n2+"•C3", n2+"•C4",
			n2+"•C5", n2+"•C6")...)
	}
	return model
}

//go:embed semiotics.asgmt
var semioticsTF string

var millSection *regexp.Regexp = regexp.MustCompile(
	`(Section M1\n(?:.*\n)+)`)

func millSections(ms, n string) (ss []string) {
	if n, err := strconv.Atoi(n); err == nil && n > 0 {
		if n > 20 {
			n = 20
		}
		ss = make([]string, n)
		for i := range ss {
			ss[i] = strings.ReplaceAll(ms, "M1", fmt.Sprintf("M%d", i+1))
		}
	}
	return
}

func semioticsSource(args ...string) string {
	src := semioticsTF
	if len(args) > 0 {
		src = strings.Join(append(
			[]string{millSection.ReplaceAllString(src, "")},
			millSections(millSection.FindString(src), args[0])...),
			"\n")
	}
	return src
}

func semiotics(
	args ...string,
) (*grpc.ApplicationGroup, *grpc.Infrastructure, error) {
	semiotics := semioticsSource(args...)

	var model grpc.Model
	if err := textformat.Convert("semiotics", semiotics, &model); err != nil {
		return nil, nil, err
	}
	return model.AppGroup, model.Infrastructure, nil
}
