// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"strconv"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data/new"
	"siemens.com/qos-solver/pkg/textformat"
)

type pathsFinder func(
	*grpc.Infrastructure, ...string) []*grpc.Network_Path

func getFindPathsFunc() pathsFinder {
	if textformat.ShortestPathsOnly {
		if textformat.ImplicitReversePath {
			return new.FindShortestBidiPaths
		}
		return new.FindShortestPathsBidi
	}
	if textformat.ImplicitReversePath {
		return new.FindAllBidiPaths
	}
	return new.FindAllPathsBidi
}

func getMillCount(data []interface{}) int {
	if len(data) > 0 {
		switch x := data[0].(type) {
		case int:
			return x
		case string:
			if n, err := strconv.Atoi(x); err == nil {
				return n
			}
		default:
			panic("unsupported type")
		}
	}
	return 5
}

func getAppNames(data []interface{}) []string {
	if len(data) > 1 {
		switch apps := data[1].(type) {
		case []string:
			return apps
		case string:
			return []string{apps}
		case nil:
			return []string{}
		default:
			panic("unsupported type")
		}
	}
	return []string{}
}

func GetAppModel(name string, data ...interface{}) *grpc.ApplicationGroup {
	var a *grpc.ApplicationGroup
	switch strings.ToLower(name) {
	case "semiotics":
		a = semioticsAppModel(getMillCount(data))
	case "semiotics-test":
		a = semioticsAppModelTest(getMillCount(data), getAppNames(data))
	default:
		a = &grpc.ApplicationGroup{}
	}
	return a
}

func GetInfraModel(name string, data ...interface{}) *grpc.Infrastructure {
	switch strings.ToLower(name) {
	case "semiotics":
		return semioticsInfraModel(getMillCount(data))
	}
	return &grpc.Infrastructure{}
}

func GetAssignment(
	name string, args ...string,
) (*grpc.ApplicationGroup, *grpc.Infrastructure) {
	if m := GetModel(name, args...); m != nil {
		return m.AppGroup, m.Infrastructure
	}
	return nil, nil
}

func GetModel(name string, args ...string) *grpc.Model {
	var err error
	m := &grpc.Model{}
	switch strings.ToLower(name) {
	case "multiple-apps":
		m.AppGroup, m.Infrastructure, err = multipleApps()
	case "qos-demo":
		m.AppGroup, m.Infrastructure, err = qosDemo()
	case "semiotics":
		m.AppGroup, m.Infrastructure, err = semiotics(args...)
	case "sphere-control":
		m.AppGroup, m.Infrastructure, err = sphereControl()
	case "tiny":
		m.AppGroup, m.Infrastructure, err = tiny()
	default:
		return nil
	}
	if err != nil {
		return nil
	}
	return m
}

func GetSource(name string, args ...string) string {
	switch strings.ToLower(name) {
	case "multiple-apps":
		return multipleAppsSource()
	case "qos-demo":
		return qosDemoSource()
	case "semiotics":
		return semioticsSource(args...)
	case "sphere-control":
		return sphereControlSource()
	case "tiny":
		return tinySource()
	default:
		return ""
	}
}

type ModelInfo struct {
	Name, Note string
}

func ListModels() []ModelInfo {
	return []ModelInfo{
		{Name: "multiple-apps",
			Note: "Example from white paper"},
		{Name: "qos-demo",
			Note: "QoS Scheduler’s demo application"},
		{Name: "semiotics(n)",
			Note: "SEMIoTICs wind park; N is the number of mills"},
		{Name: "sphere-control",
			Note: "QoS demo application “Sphere on Plate”"},
		{Name: "tiny",
			Note: "A tiny model for testing purposes"}}
}
