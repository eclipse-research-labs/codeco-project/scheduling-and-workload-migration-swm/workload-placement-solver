// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	_ "embed"

	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/textformat"
)

//go:embed multipleApps.asgmt
var multipleAppsTF string

func multipleAppsSource() string {
	return multipleAppsTF
}

func multipleApps() (*grpc.ApplicationGroup, *grpc.Infrastructure, error) {
	var model grpc.Model
	err := textformat.Convert("multiple-apps", multipleAppsTF, &model)
	if err != nil {
		return nil, nil, err
	}
	return model.AppGroup, model.Infrastructure, nil
}
