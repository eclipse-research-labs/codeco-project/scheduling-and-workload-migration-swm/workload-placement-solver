// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/data/new"
)

func pathAsString(p *grpc.Network_Path) string {
	var buf bytes.Buffer
	if p != nil {
		for i, n := range p.Links {
			if i > 0 {
				buf.WriteRune('•')
			}
			buf.WriteString(n)
		}
	}
	return buf.String()
}

func sortPaths(ps []*grpc.Network_Path) []string {
	sps := make([]string, len(ps))
	for i := range ps {
		s, j := pathAsString(ps[i]), i-1
		for ; j >= 0 && s < sps[j]; j-- {
			sps[j+1] = sps[j]
		}
		sps[j+1] = s
	}
	return sps
}

func TestSortPaths(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{}, sortPaths(new.PathList()))
	assert.Equal(
		[]string{"A→B•B→C", "A→B•B→C•C→E", "M→N•N→O", "U→V•V→W"},
		sortPaths(new.PathList("M•N•O", "U•V•W", "A•B•C", "A•B•C•E")))
}

func assertPathsEqualSorted(
	t *testing.T, exp, act []*grpc.Network_Path,
	args ...interface{},
) bool {
	t.Helper()
	return assert.Equal(t, sortPaths(exp), sortPaths(act), args...)
}

func nodeAsString(n *grpc.Node) string {
	var buf bytes.Buffer
	if n != nil {
		buf.WriteString(n.Id)
		if n.CPU != nil && n.Memory != nil {
			fmt.Fprintf(&buf, ":%d:%d", n.CPU.Capacity, n.Memory.Capacity)
		}
	}
	return buf.String()
}

func sortNodes(ns []*grpc.Node) []string {
	sns := make([]string, len(ns))
	for i := range ns {
		s, j := nodeAsString(ns[i]), i-1
		for ; j >= 0 && s < sns[j]; j-- {
			sns[j+1] = sns[j]
		}
		sns[j+1] = s
	}
	return sns
}

func TestSortNodes(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{}, sortNodes(nil))
	assert.Equal(
		[]string{"N1:1000:8000", "N2:1000:8000", "N3:1000:8000", "N4"},
		sortNodes([]*grpc.Node{
			new.Node("N3", 1000, 8000),
			new.Node("N1", 1000, 8000),
			new.Node("N2", 1000, 8000),
			new.NetworkNode("N4")}))
}

func assertNodesEqualSorted(
	t *testing.T, exp, act []*grpc.Node,
	args ...interface{},
) bool {
	t.Helper()
	return assert.Equal(t, sortNodes(exp), sortNodes(act), args...)
}
