// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/textformat"
)

func restore[T any](who *T, value T) func() {
	store := *who
	*who = value
	return func() {
		*who = store
	}
}

func TestMultipleApps_appGroup(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.CompoundNames, false)()

	ag, _, err := multipleApps()
	require.NoError(err)
	require.Len(ag.Applications, 5)
	require.Len(ag.Applications[0].Workloads, 2)
	assert.Equal("W1", ag.Applications[0].Workloads[0].Id)
	require.Len(ag.Channels, 16)
	assert.Equal("W1", ag.Channels[0].SourceWorkload)
	assert.Equal("W2", ag.Channels[0].TargetWorkload)

	textformat.CompoundNames = true
	ag, _, err = multipleApps()
	require.NoError(err)
	require.Len(ag.Applications, 5)
	require.Len(ag.Applications[0].Workloads, 2)
	assert.Equal("App1_W1", ag.Applications[0].Workloads[0].Id)
	require.Len(ag.Channels, 16)
	assert.Equal("App1_W1", ag.Channels[0].SourceWorkload)
	assert.Equal("App1_W2", ag.Channels[0].TargetWorkload)
}

func TestMultipleApps_infrastructure(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()

	_, infra, err := multipleApps()
	require.NoError(err)
	assert.Len(infra.Nodes, 10)
	assert.Len(infra.Networks[0].Links, 18)
	assert.Len(infra.Networks[0].Paths, 42)

	textformat.ImplicitReversePath = true
	_, infra, err = multipleApps()
	require.NoError(err)
	assert.Len(infra.Nodes, 10)
	assert.Len(infra.Networks[0].Links, 18)
	assert.Len(infra.Networks[0].Paths, 21)
}
