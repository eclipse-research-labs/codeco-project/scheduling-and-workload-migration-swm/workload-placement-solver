// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	_ "embed"

	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/pkg/textformat"
)

//go:embed qosDemo.asgmt
var qosDemoTF string

func qosDemoSource() string {
	return qosDemoTF
}

func qosDemo() (*grpc.ApplicationGroup, *grpc.Infrastructure, error) {
	var model grpc.Model
	if err := textformat.Convert("qosDemo", qosDemoTF, &model); err != nil {
		return nil, nil, err
	}
	return model.AppGroup, model.Infrastructure, nil
}
