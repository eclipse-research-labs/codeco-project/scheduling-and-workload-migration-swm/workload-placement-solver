// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

workload assignment v1
model sphere-control

application sphere
workload webcam needs cpu:200 ram:200MiB labels:(ipc camera)
workload processor needs cpu:500 ram:200MiB labels:(ipc)
workload controller needs cpu:200 ram:100MiB labels:(ipc robot)

channel webcam->processor needs lat:1ms bw:30*64KiB qos:assured
channel processor->controller needs lat:1ms bw:30*200 qos:assured

application dashboard
workload prometheus needs cpu:500 ram:1GiB labels:(!ipc)
workload grafana needs cpu:500 ram:1GiB labels:(!ipc)

channel prometheus—sphere•processor needs bw:2*2KiB,2*16KiB
channel prometheus—sphere•controller needs bw:2*2KiB,2*16KiB
channel prometheus—sphere•webcam needs bw:2*2KiB,2*16KiB
channel prometheus—grafana needs bw:100Kb

infrastructure
node nano provides cpu:2000-200 ram:4GiB-400MiB labels:(ipc robot)
node micro provides cpu:4000-400 ram:16GiB-1600MiB labels:(ipc camera)
node rasp4 provides cpu:4000 ram:8GiB-128MiB labels:(ARM)

network lo with bw:1Gb lat:100ns
link nano->nano,micro->micro,rasp4->rasp4 with path:yes
network alo with bearer:lo qos:assured
nodes (nano micro)

network eth with bw:1Gb lat:100µs
node switch
link nano--switch,micro--switch,rasp4--switch
find shortest paths (nano•micro nano•rasp4 micro•rasp4)

network vlan with bearer:eth qos:assured
nodes (nano micro)
