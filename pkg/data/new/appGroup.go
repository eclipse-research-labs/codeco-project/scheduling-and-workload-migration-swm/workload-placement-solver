// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"fmt"
	"regexp"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
)

type workloadProps struct {
	Node   string
	CPU    int32
	RAM    int64
	Labels []string
}

type AppGroup struct {
	apps     []*grpc.Application
	channels []*grpc.Channel
}

func (ag *AppGroup) Apps() []*grpc.Application { return ag.apps }
func (ag *AppGroup) Channels() []*grpc.Channel { return ag.channels }

func (ag *AppGroup) AsGrpc() *grpc.ApplicationGroup {
	return &grpc.ApplicationGroup{
		Applications: ag.apps,
		Channels:     ag.channels}
}

func compound(app, wl string) string {
	return app + "_" + wl
}
func (ag *AppGroup) MakeCompoundNames() {
	for _, app := range ag.apps {
		for _, w := range app.Workloads {
			w.Id = compound(app.Id, w.Id)
		}
	}
	for _, ch := range ag.channels {
		ch.SourceWorkload = compound(ch.SourceApplication, ch.SourceWorkload)
		ch.TargetWorkload = compound(ch.TargetApplication, ch.TargetWorkload)
	}
}

type Application struct{ app *grpc.Application }

func (a *Application) AsGrpc() *grpc.Application { return a.app }

func (ag *AppGroup) Application(
	name string, workloads ...*grpc.Workload,
) *Application {
	app := &grpc.Application{Id: name}
	if len(workloads) > 0 {
		app.Workloads = workloads
	}
	ag.apps = append(ag.apps, app)
	return &Application{app: app}
}

func (a *Application) workloadWithProps(
	name string, props *workloadProps,
) *Application {
	w := &grpc.Workload{
		Id:     name,
		Node:   props.Node,
		CPU:    props.CPU,
		Memory: props.RAM}
	if len(props.Labels) > 0 {
		w.RequiredLabels = make([]string, 0, len(props.Labels))
		w.ForbiddenLabels = make([]string, 0, len(props.Labels))
		for _, l := range props.Labels {
			if l[0] == '-' || l[0] == '!' || l[0] == '~' || l[0] == '¬' {
				w.ForbiddenLabels = append(w.ForbiddenLabels, l[1:])
			} else {
				w.RequiredLabels = append(w.RequiredLabels, l)
			}
		}
	}
	a.app.Workloads = append(a.app.Workloads, w)
	return a
}

func (a *Application) Workload(
	name string, cpu int32, ram int64, labels ...string,
) *Application {
	props := workloadProps{CPU: cpu, RAM: ram, Labels: labels}
	return a.workloadWithProps(name, &props)
}

func (ag *AppGroup) channel(src, dst, path string, lat, bw int64) {
	c := grpc.Channel{Path: path, Latency: lat, Bandwidth: bw}

	split := func(ref string) (string, string, bool) {
		m := strings.Split(ref, "•")
		if len(m) == 1 || m[0] == "" || m[1] == "" {
			return "", "", false
		}
		return m[0], m[1], true
	}
	var ok bool
	if c.SourceApplication, c.SourceWorkload, ok = split(src); !ok {
		return
	}
	if c.TargetApplication, c.TargetWorkload, ok = split(dst); !ok {
		return
	}
	c.Id = fmt.Sprintf("%s•%s→%s•%s",
		c.SourceApplication, c.SourceWorkload,
		c.TargetApplication, c.TargetWorkload)
	ag.channels = append(ag.channels, &c)
}

var chanNames = regexp.MustCompile(`^(\w+)\.(\w+)(?:•|->)(\w+)\.(\w+)$`)

func (ag *AppGroup) Channel(spec string, lat, bw int64) {
	m := chanNames.FindStringSubmatch(spec)
	if m == nil {
		return
	}
	ag.channel(m[1]+"•"+m[2], m[3]+"•"+m[4], "", lat, bw)
}

func (ag *AppGroup) BidiChannel(spec string, lat, bw int64, args ...int64) {
	m := chanNames.FindStringSubmatch(spec)
	if m == nil {
		return
	}
	ag.channel(m[1]+"•"+m[2], m[3]+"•"+m[4], "", lat, bw)
	if len(args) >= 1 {
		lat = args[0]
	}
	if len(args) >= 2 {
		bw = args[1]
	}
	ag.channel(m[3]+"•"+m[4], m[1]+"•"+m[2], "", lat, bw)
}
