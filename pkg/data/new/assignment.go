// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"regexp"
	"strings"

	"siemens.com/qos-solver/internal/port/grpc"
)

type ChannelAsgmtProps struct {
	Name    string
	Network string
	Path    string
}

type Assignment struct {
	Workloads []*grpc.Assignment_Workload
	Channels  []*grpc.Assignment_Channel
}

func (a *Assignment) MakeCompoundNames() {
	for _, w := range a.Workloads {
		w.Workload = compound(w.Application, w.Workload)
	}
}

func (a *Assignment) AsGrpc() *grpc.Assignment {
	return &grpc.Assignment{
		Workloads: a.Workloads,
		Channels:  a.Channels}
}

var wlSpec = regexp.MustCompile(`^([\w.-]+)•([\w.-]+):([\w.-]+)$`)

func (a *Assignment) Workload(asgmt string) *Assignment {
	if m := wlSpec.FindStringSubmatch(asgmt); m != nil {
		a.Workloads = append(a.Workloads, &grpc.Assignment_Workload{
			Application: m[1], Workload: m[2], Node: m[3]})
	}
	return a
}

func (a *Assignment) ChannelWithProps(props *ChannelAsgmtProps) *Assignment {
	a.Channels = append(a.Channels, &grpc.Assignment_Channel{
		Channel: props.Name,
		Network: props.Network,
		Path:    props.Path})
	return a
}

func (a *Assignment) Channel(asgmt string) *Assignment {
	m := strings.Split(asgmt, ":")
	var ca *grpc.Assignment_Channel
	switch {
	case len(m) == 3:
		ca = &grpc.Assignment_Channel{
			Channel: strings.TrimSpace(m[0]),
			Network: strings.TrimSpace(m[1]),
			Path:    strings.TrimSpace(m[2])}
	case len(m) == 5:
		ca = &grpc.Assignment_Channel{
			Channel: strings.TrimSpace(m[0]),
			Network: strings.TrimSpace(m[1]),
			Path:    strings.TrimSpace(m[2]),
			Source:  strings.TrimSpace(m[3]),
			Target:  strings.TrimSpace(m[4])}
	}
	if ca != nil {
		a.Channels = append(a.Channels, ca)
	}
	return a
}
