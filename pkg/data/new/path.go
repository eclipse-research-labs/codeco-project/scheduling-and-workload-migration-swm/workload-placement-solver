// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"regexp"

	"siemens.com/qos-solver/internal/port/grpc"
)

type nodeInfo struct {
	index map[string]int
	links [][]int
}

func nodeInfos(
	nodes []*grpc.Node,
	links []*grpc.Network_Link,
) nodeInfo {
	ni := nodeInfo{
		index: make(map[string]int, len(nodes)),
		links: make([][]int, len(nodes))}
	for i := range nodes {
		ni.index[nodes[i].Id] = i
	}
	for i, l := range links {
		src := l.Source
		srcI, ok := ni.index[src]
		if !ok {
			continue
		}
		if _, ok := ni.index[l.Target]; !ok {
			continue
		}
		ni.links[srcI] = append(ni.links[srcI], i)
		ls := ni.links[srcI]
		needsSwap := func(j int) bool {
			x := ni.index[links[ls[j-1]].Target]
			y := ni.index[links[ls[j]].Target]
			return x > y
		}
		for k := len(ls) - 1; k > 0 && needsSwap(k); k-- {
			ls[k-1], ls[k] = ls[k], ls[k-1]
		}
	}
	return ni
}

type searchState struct {
	nodes     []*grpc.Node
	links     []*grpc.Network_Link
	allPaths  bool
	nodeInfos nodeInfo
}

func newSearchState(
	nodes []*grpc.Node,
	links []*grpc.Network_Link,
	allPaths bool,
) searchState {
	s := searchState{
		nodes:     nodes,
		links:     links,
		allPaths:  allPaths,
		nodeInfos: nodeInfos(nodes, links)}
	return s
}

func (s *searchState) findLink(src, dst int) *grpc.Network_Link {
	for _, li := range s.nodeInfos.links[src] {
		l := s.links[li]
		if n, ok := s.nodeInfos.index[l.Target]; ok && n == dst {
			return l
		}
	}
	panic("link not found")
}

func (s *searchState) makePath(
	node int, parents []int,
) *grpc.Network_Path {
	ls := make([]string, len(parents))
	for i := 0; i < len(parents)-1; i++ {
		ls[i] = s.findLink(parents[i], parents[i+1]).Id
	}
	ls[len(ls)-1] = s.findLink(parents[len(parents)-1], node).Id
	return linksToPath(ls)

	/*p := grpc.Network_Path{Links: make([]string, len(parents))}
	for i := 0; i < len(parents)-1; i++ {
		p.Links[i] = s.findLink(parents[i], parents[i+1]).Id
	}
	p.Links[len(p.Links)-1] = s.findLink(parents[len(parents)-1], node).Id
	return &p*/
}

func contains(a []int, x int) bool {
	for _, n := range a {
		if n == x {
			return true
		}
	}
	return false
}

func (s *searchState) pathsBetween(
	src, dst string,
) (ps []*grpc.Network_Path) {
	srcIdx, ok := s.nodeInfos.index[src]
	if !ok {
		return
	}
	dstIdx, ok := s.nodeInfos.index[dst]
	if !ok {
		return
	}
	q := make(queue, 0, len(s.nodes))
	q.push(srcIdx, []int{})
	for !q.isEmpty() {
		qeNode, qeParents := q.pop()
		if qeNode == dstIdx {
			ps = append(ps, s.makePath(qeNode, qeParents))
			if s.allPaths {
				continue
			}
			break
		}
		for _, l := range s.nodeInfos.links[qeNode] {
			dst, ok := s.nodeInfos.index[s.links[l].Target]
			if !ok || dst == qeNode {
				continue
			}
			if !contains(qeParents, dst) {
				parents := append(qeParents, qeNode)
				q.push(dst, parents)
			}
		}
	}
	return
}

var fromTo = regexp.MustCompile(`^([\w.-]+)(?:•|->)([\w.-]+)$`)

func findPaths(
	s searchState, specs []string,
) []*grpc.Network_Path {
	ps := make([]*grpc.Network_Path, 0, len(specs))
	for _, spec := range specs {
		if m := fromTo.FindStringSubmatch(spec); m != nil {
			ps = append(ps, s.pathsBetween(m[1], m[2])...)
		}
	}
	if len(ps) == 0 {
		return nil
	}
	return ps
}

func FindShortestPaths(
	im *grpc.Infrastructure, specs ...string,
) []*grpc.Network_Path {
	return findPaths(
		newSearchState(im.Nodes, im.Networks[0].Links, false), specs)
}

func FindAllPaths(
	im *grpc.Infrastructure, specs ...string,
) []*grpc.Network_Path {
	return findPaths(
		newSearchState(im.Nodes, im.Networks[0].Links, true), specs)
}

func findPathsBidi(
	s searchState, specs []string,
) []*grpc.Network_Path {
	ps := make([]*grpc.Network_Path, 0, len(specs))
	for _, spec := range specs {
		if m := linkNodes.FindStringSubmatch(spec); m != nil {
			ps = append(ps, s.pathsBetween(m[1], m[2])...)
			ps = append(ps, s.pathsBetween(m[2], m[1])...)
		}
	}
	if len(ps) == 0 {
		return nil
	}
	return ps
}

func FindShortestPathsBidi(
	im *grpc.Infrastructure, specs ...string,
) []*grpc.Network_Path {
	return findPathsBidi(
		newSearchState(im.Nodes, im.Networks[0].Links, false), specs)
}

func FindAllPathsBidi(
	im *grpc.Infrastructure, specs ...string,
) []*grpc.Network_Path {
	return findPathsBidi(
		newSearchState(im.Nodes, im.Networks[0].Links, true), specs)
}

func isReverseLink(x, y string) bool {
	if x := reLinkName.FindStringSubmatch(x); x != nil {
		if y := reLinkName.FindStringSubmatch(y); y != nil {
			return x[3] == y[5] && x[5] == y[3]
		}
	}
	return false
}

func isReverse(p, q *grpc.Network_Path) bool {
	if p == nil {
		return q == nil || len(q.Links) == 0
	} else if q == nil {
		return len(p.Links) == 0
	}
	if l := len(p.Links) - 1; l == len(q.Links)-1 {
		for i, n := range p.Links {
			if !isReverseLink(n, q.Links[l-i]) {
				return false
			}
		}
		return true
	}
	return false
}

func bidiPaths(
	to, fro []*grpc.Network_Path,
) []*grpc.Network_Path {
	j := 0
	for i := range to {
		p := to[i]
		for k := range fro {
			if isReverse(fro[k], p) {
				to[j], fro[k] = p, nil
				j++
				break
			}
		}
	}
	if j == 0 {
		return nil
	}
	return to[:j]
}

func findBidiPaths(
	s searchState, specs []string,
) []*grpc.Network_Path {
	ps := make([]*grpc.Network_Path, 0, len(specs))
	for _, spec := range specs {
		if m := linkNodes.FindStringSubmatch(spec); m != nil {
			ps = append(ps, bidiPaths(
				s.pathsBetween(m[1], m[2]),
				s.pathsBetween(m[2], m[1]))...)
		}
	}
	if len(ps) == 0 {
		return nil
	}
	return ps
}

func FindShortestBidiPaths(
	im *grpc.Infrastructure, specs ...string,
) []*grpc.Network_Path {
	return findBidiPaths(
		newSearchState(im.Nodes, im.Networks[0].Links, false), specs)
}

func FindAllBidiPaths(
	im *grpc.Infrastructure, specs ...string,
) []*grpc.Network_Path {
	return findBidiPaths(
		newSearchState(im.Nodes, im.Networks[0].Links, true), specs)
}

func FindNetworkPaths(
	nodes []*grpc.Node, links []*grpc.Network_Link, kind string,
	implicitReversePath bool, specs ...string,
) []*grpc.Network_Path {
	proc := findPathsBidi
	if implicitReversePath {
		proc = findBidiPaths
	}
	return proc(
		newSearchState(nodes, links, kind == "all"),
		specs)
}
