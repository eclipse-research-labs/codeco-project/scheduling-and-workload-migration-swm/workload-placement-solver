// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQueuePush(t *testing.T) {
	assert := assert.New(t)
	q := make(queue, 0, 10)
	node, parents := 17, []int{1, 2, 3}
	q.push(node, parents)
	assert.Equal(node, q[0].node)
	assert.Equal(parents, q[0].parents)
	parents[0] = 19
	assert.Equal(1, q[0].parents[0])
}

func TestQueue(t *testing.T) {
	assert := assert.New(t)
	q := make(queue, 0, 10)
	assert.True(q.isEmpty())
	q.push(1, []int{})
	assert.False(q.isEmpty())
	assert.Len(q, 1)
	assert.Equal(1, q[0].node)
	assert.Equal([]int{}, q[0].parents)

	q.push(4, []int{})
	assert.Len(q, 2)
	assert.Equal(4, q[1].node)
	assert.Equal([]int{}, q[1].parents)

	node, parents := q.pop()
	assert.Equal(1, node)
	assert.Equal([]int{}, parents)
	assert.Len(q, 1)

	node, parents = q.pop()
	assert.Equal(4, node)
	assert.Equal([]int{}, parents)
	assert.True(q.isEmpty())
}
