// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
)

func TestAssignmentMakeCompoundNames(t *testing.T) {
	asgmt := Assignment{}
	asgmt.Workload("a•w:n").Workload("b•v:m")
	asgmt.MakeCompoundNames()
	assert.Equal(t, "a_w", asgmt.Workloads[0].Workload)
	assert.Equal(t, "b_v", asgmt.Workloads[1].Workload)
}

func TestAssignmentWorkload(t *testing.T) {
	var a Assignment
	assert.Same(t, &a, a.Workload("app•workload:node"))
	require.Len(t, a.Workloads, 1)
	assert.Equal(t,
		&grpc.Assignment_Workload{
			Application: "app", Workload: "workload", Node: "node"},
		a.Workloads[0])
	a.Workload("illegal")
	assert.Len(t, a.Workloads, 1)
}
func TestAssignmentChannel(t *testing.T) {
	var a Assignment
	assert.Same(t, &a, a.Channel("channel:net:path"))
	require.Len(t, a.Channels, 1)
	assert.Equal(t,
		&grpc.Assignment_Channel{
			Channel: "channel", Network: "net", Path: "path"},
		a.Channels[0])

	a.Channel("c:nw:p:n:m")
	require.Len(t, a.Channels, 2)
	assert.Equal(t,
		&grpc.Assignment_Channel{
			Channel: "c", Network: "nw", Path: "p", Source: "n", Target: "m"},
		a.Channels[1])

	a.Channel("illegal")
	assert.Len(t, a.Channels, 2)
}
