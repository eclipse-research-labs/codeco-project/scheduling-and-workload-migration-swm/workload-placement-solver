// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"fmt"
	"regexp"
	"strings"

	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
)

type Infrastructure struct {
	nodes    []*grpc.Node
	networks []*network
}

func (i *Infrastructure) Networks() []*network { return i.networks }

func qosToServiceClass(qos string) grpc.ServiceClass {
	qos = strings.ReplaceAll(strings.ToUpper(qos), "-", "_")
	if sc, ok := grpc.ServiceClass_value[qos]; ok {
		return grpc.ServiceClass(sc)
	}
	return grpc.ServiceClass_BEST_EFFORT
}

func (i *Infrastructure) AsGrpc() *grpc.Infrastructure {
	gi := &grpc.Infrastructure{Nodes: i.nodes}
	if len(i.networks) > 0 {
		gi.Networks = make([]*grpc.Network, len(i.networks))
		for i, n := range i.networks {
			gi.Networks[i] = &grpc.Network{
				Id:     n.Name,
				Bearer: n.Bearer,
				Qos:    qosToServiceClass(n.Options.qos.Value()),
				Links:  n.Links, Paths: n.Paths, Media: n.Media}
			if len(n.Switches) > 0 && n.Bearer == "" {
				gi.Nodes = append(gi.Nodes, n.Switches...)
			}
		}
	}
	return gi
}

func (i *Infrastructure) addNode(n *grpc.Node) {
	i.nodes = append(i.nodes, n)
}

func (i *Infrastructure) Nodes(ns []*grpc.Node) *Infrastructure {
	for _, n := range ns {
		i.addNode(n)
	}
	return i
}

func (i *Infrastructure) Node(
	name string, cpu int32, ram int64, labels ...string,
) *Infrastructure {
	n := &grpc.Node{
		Id:       name,
		NodeType: grpc.Node_COMPUTE,
		CPU:      &grpc.Resource{Capacity: int64(cpu)},
		Memory:   &grpc.Resource{Capacity: ram}}
	if len(labels) > 0 {
		n.Labels = labels
	}
	i.addNode(n)
	return i
}

func makeNetworkNode(name string, lb ...int64) *grpc.Node {
	node := &grpc.Node{Id: name, NodeType: grpc.Node_NETWORK}
	if len(lb) > 0 {
		node.Latency = lb[0]
		if len(lb) > 1 {
			node.Bandwidth = &grpc.Resource{Capacity: lb[1]}
		}
	}
	return node
}

func (i *Infrastructure) NetworkNode(
	name string, lb ...int64,
) *Infrastructure {
	i.nodes = append(i.nodes, makeNetworkNode(name, lb...))
	return i
}

func (i *Infrastructure) addNetwork(n *network) {
	i.networks = append(i.networks, n)
}

func (i *Infrastructure) Add(data interface{}) *Infrastructure {
	switch item := data.(type) {
	case *grpc.Node:
		i.addNode(item)
	case *network:
		i.addNetwork(item)
	default:
		panic("unsupported type")
	}
	return i
}

func (i *Infrastructure) Nets(nets ...*network) *Infrastructure {
	i.networks = append(i.networks, nets...)
	return i
}

func Node(
	name string, cpu int32, ram int64, labels ...string,
) (n *grpc.Node) {
	n = &grpc.Node{
		Id:       name,
		NodeType: grpc.Node_COMPUTE,
		CPU:      &grpc.Resource{Capacity: int64(cpu)},
		Memory:   &grpc.Resource{Capacity: ram}}
	if len(labels) > 0 {
		n.Labels = labels
	}
	return
}

func NetworkNode(name string, lb ...int64) *grpc.Node {
	return &grpc.Node{
		Id:       name,
		NodeType: grpc.Node_NETWORK}
}

func NodeLoad(name string, cpu int32, ram int64) *grpc.NodeLoad {
	return &grpc.NodeLoad{Id: name, CPU: cpu, RAM: ram}
}

func NetworkNodeLoad(name string, bandwidth int64) *grpc.NetworkNodeLoad {
	return &grpc.NetworkNodeLoad{Id: name, Cap: bandwidth}
}

type networkOptions struct {
	// mtu       types.Option[int]
	// bearer    types.Option[string]
	qos       types.Option[string]
	latency   types.Option[int64]
	bandwidth types.Option[int64]
	wire      types.Option[bool]
	duplex    types.Option[bool]
}

func defaultNetworkOptions() networkOptions {
	return networkOptions{
		qos:       types.NewOption(defaults.NetworkQoS),
		latency:   types.NewOption(defaults.LatencyLink),
		bandwidth: types.NewOption(defaults.BandwidthLink),
		wire:      types.NewOption(defaults.NetworkType == "wire"),
		duplex:    types.NewOption(defaults.NetworkLinks == "duplex")}
}

type network struct {
	Name     string
	Bearer   string
	serials  map[string]int
	Options  networkOptions
	Workers  []*grpc.Node
	Switches []*grpc.Node
	Media    []*grpc.Medium
	Links    []*grpc.Network_Link
	Paths    []*grpc.Network_Path
}

func (n *network) findLink(src, dst string) *grpc.Network_Link {
	for _, l := range n.Links {
		if l.Source == src && l.Target == dst {
			return l
		}
	}
	return nil
}

func (n *network) findMedium(name string) *grpc.Medium {
	if n != nil {
		for _, medium := range n.Media {
			if medium.Id == name {
				return medium
			}
		}
	}
	return nil
}

func pathIndex(i int) string {
	return "\u207D" + itoaSup(i) + "\u207E"
}

func (n *network) makePathNamesUnique() {
	cache := make(map[string][]int, len(n.Paths))
	for i, p := range n.Paths {
		ns, ok := cache[p.Id]
		if !ok {
			ns = make([]int, 0, 4)
		}
		cache[p.Id] = append(ns, i)
	}
	for id, ns := range cache {
		if len(ns) < 2 {
			continue
		}
		for i := range ns {
			n.Paths[ns[i]].Id = id + pathIndex(i+1)
		}
	}
}

func Net(name string, opts ...networkOptions) *network {
	n := &network{Name: name, Options: defaultNetworkOptions()}
	if len(opts) > 0 {
		n.Options = opts[0]
	}
	if !n.Options.wire.Value() {
		bw := defaults.BandwidthLink
		if n.Options.bandwidth.WasSet() {
			bw = n.Options.bandwidth.Value()
		}
		n.Medium(name, bw)
	}
	return n
}

func (n *network) defaultLinkProps() (linkProps, linkProps) {
	to := linkProps{latency: n.Options.latency.Value(),
		bandwidth: types.Resource[int64]{Cap: n.Options.bandwidth.Value()}}
	fro := to
	if n.Options.wire.Value() {
		to.wire, fro.wire = true, n.Options.duplex.Value()
	}
	return to, fro
}

func (n *network) uniqueName(pre string) string {
	if n.serials == nil {
		n.serials = map[string]int{"L": 0, "M": 0, "P": 0}
	}
	s := n.serials[pre]
	s++
	n.serials[pre] = s
	return fmt.Sprintf("%s%d", pre, s)
}

func (n *network) setNodes(nodes []*grpc.Node, names []string) {
	n.Switches = make([]*grpc.Node, 0, len(nodes))
	if len(names) > 0 {
		for _, node := range nodes {
			for _, name := range names {
				if node.Id == name {
					n.Switches = append(n.Switches, node)
					break
				}
			}
		}
	} else {
		n.Switches = append(n.Switches, nodes...)
	}
}

func (n *network) NetworkNode(name string, lb ...int64) *network {
	n.Switches = append(n.Switches, makeNetworkNode(name, lb...))
	return n
}

func (n *network) add(obj interface{}) {
	switch item := obj.(type) {
	case *grpc.Node:
		n.Switches = append(n.Switches, item)
	case *grpc.Network_Link:
		n.Links = append(n.Links, item)
	case *grpc.Network_Path:
		n.Paths = append(n.Paths, item)
	case []interface{}:
		for _, obj := range item {
			n.add(obj)
		}
	default:
		panic("unsupported type")
	}
}

var linkNodes = regexp.MustCompile(`^([\w.-]+)(?:->|•)([\w.-]+)$`)

func (n *network) Medium(owner string, cap int64) *network {
	var mn string
	if owner != "" {
		mn = owner + "\u2098"
	} else {
		mn = n.uniqueName("M")
	}
	n.Media = append(n.Media, &grpc.Medium{
		Id:        mn,
		Bandwidth: &grpc.Resource{Capacity: cap}})
	return n
}

type linkProps struct {
	name      string
	src, dst  string
	wire      bool
	path      bool
	latency   int64
	bandwidth types.Resource[int64]
}

var namedLink = regexp.MustCompile(
	`^(?:([\w.-]+|[0-9]+)(?:,([\w.-]+|[0-9]+))?:)?([\w.-]+)(?:->|•)([\w.-]+)$`)

func splitLinkSpec(spec string) (bool, string, string, string, string) {
	if m := namedLink.FindStringSubmatch(spec); m != nil {
		return true, m[1], m[2], m[3], m[4]
	}
	return false, "", "", "", ""
}

func linkName(src, dst string) string {
	return src + "→" + dst
}

func (n *network) link(props *linkProps) *grpc.Network_Link {
	if n.Bearer != "" {
		l := n.findLink(props.src, props.dst)
		if l == nil {
			panic("link not found")
		}
		l.Latency = props.latency
		if props.wire {
			m := n.findMedium(l.Medium)
			if m == nil {
				panic("medium not found")
			}
			m.Bandwidth.Capacity = props.bandwidth.Cap
		}
		return l
	}
	name := props.name
	if name == "" {
		name = linkName(props.src, props.dst)
	}
	l := &grpc.Network_Link{
		Id:     name,
		Source: props.src, Target: props.dst,
		Latency: props.latency}
	if props.wire {
		n.Medium(l.Id, props.bandwidth.Available())
	}
	l.Medium = n.Media[len(n.Media)-1].Id
	n.Links = append(n.Links, l)
	return l
}

func (n *network) Link(spec string, lat, cap int64) *network {
	if ok, n1, _, src, dst := splitLinkSpec(spec); ok {
		props := linkProps{
			name: n1, src: src, dst: dst, wire: true, latency: lat,
			bandwidth: types.Resource[int64]{Cap: cap}}
		n.link(&props)
	}
	return n
}

func (n *network) BidiLink(
	spec string, lat, cap int64, args ...int64,
) *network {
	ok, n1, n2, src, dst := splitLinkSpec(spec)
	if !ok {
		return n
	}
	props := linkProps{
		name: n1, src: src, dst: dst, wire: true, latency: lat,
		bandwidth: types.Resource[int64]{Cap: cap}}
	n.link(&props)
	if len(args) >= 1 {
		props.latency = args[0]
	}
	if len(args) >= 2 {
		props.bandwidth.Cap = args[1]
	}
	props.name, props.src, props.dst = n2, dst, src
	n.link(&props)
	return n
}

func (n *network) linkWithProps(props *linkProps) *network {
	l := n.link(props)
	if props.path && n.Bearer == "" {
		n.path("", l.Id)
	}
	return n
}

func Link(spec string, lat, cap int64) *grpc.Network_Link {
	ok, n1, _, src, dst := splitLinkSpec(spec)
	if !ok {
		return &grpc.Network_Link{}
	}
	name := n1
	if name == "" {
		name = linkName(src, dst)
	}
	return &grpc.Network_Link{
		Id: name, Source: src, Target: dst,
		Latency: lat} //??, Bandwidth: cap
}

func LinkLoad(spec string, cap int64) *grpc.LinkLoad {
	if m := linkNodes.FindStringSubmatch(spec); m != nil {
		return &grpc.LinkLoad{Src: m[1], Dst: m[2], Cap: cap}
	}
	return &grpc.LinkLoad{}
}

func (n *network) linkName(src, dst string) string {
	for _, l := range n.Links {
		if l.Source == src && l.Target == dst {
			return l.Id
		}
	}
	return linkName(src, dst)
}

func (n *network) nodesToLinks(nodes string) (ls []string) {
	if ns := strings.Split(nodes, "•"); len(ns) > 1 {
		ls = make([]string, len(ns)-1)
		for i := 1; i < len(ns); i++ {
			ls[i-1] = n.linkName(ns[i-1], ns[i])
		}
	}
	return
}

var reLinkName *regexp.Regexp = regexp.MustCompile(
	`^(?:([[:alnum:].-]+)(?:,([[:alnum:].-]+))?:)?` +
		`([[:alnum:]](?:[[:alnum:].-]*[[:alnum:]])?)` +
		`(->|→)` +
		`([[:alnum:]](?:[[:alnum:].-]*[[:alnum:]])?)$`)

var digitsSup []rune = []rune{
	'\u2070', '\u00B9', '\u00B2', '\u00B3', '\u2074',
	'\u2075', '\u2076', '\u2077', '\u2078', '\u2079'}

func itoaSup(n int) string {
	return strings.Map(
		func(d rune) rune {
			return digitsSup[d-'0']
		},
		fmt.Sprintf("%d", n))
}

func PathName(src, dst string, links int) string {
	var lc string
	if links > 1 {
		lc = itoaSup(links)
	}
	return fmt.Sprintf("%s→%s%s", src, lc, dst)

}

func pathName(links []string) string {
	if len(links) == 0 {
		return "path"
	}
	var src, dst, lc string
	if m := reLinkName.FindStringSubmatch(links[0]); m != nil {
		src = m[3]
	}
	if m := reLinkName.FindStringSubmatch(links[len(links)-1]); m != nil {
		dst = m[5]
	}
	if src != "" && dst != "" {
		return PathName(src, dst, len(links))
	}
	if len(links) == 1 {
		return links[0]
	}
	if len(links) > 2 {
		lc = itoaSup(len(links) - 1)
	}
	return fmt.Sprintf("%s∘%s%s", links[0], lc, links[len(links)-1])
}

func linksToPath(links []string) (p *grpc.Network_Path) {
	if len(links) > 0 {
		p = &grpc.Network_Path{Id: pathName(links), Links: links}
	}
	return
}

func (n *network) path(name string, links ...string) *network {
	if p := linksToPath(links); p != nil {
		if name != "" {
			p.Id = name
		}
		n.Paths = append(n.Paths, p)
	}
	return n
}

var pathSpec *regexp.Regexp = regexp.MustCompile(
	`^(?:([^,]+)(?:,([^:]+))?:)?(.+)$`)

func (n *network) Path(spec string) *network {
	if m := pathSpec.FindStringSubmatch(spec); m != nil {
		if ls := n.nodesToLinks(m[3]); ls != nil {
			return n.path(m[1], ls...)
		}
	}
	return n
}

func (n *network) PathList(specs ...string) *network {
	for _, spec := range specs {
		n.Path(spec)
	}
	return n
}

func (n *network) BidiPath(spec string) *network {
	if m := pathSpec.FindStringSubmatch(spec); m != nil {
		if ls := n.nodesToLinks(m[3]); ls != nil {
			return n.path(m[1], ls...).path(m[2], reverseLinks(ls)...)
		}
	}
	return n
}

func (n *network) LinkPath(spec string) *network {
	if m := pathSpec.FindStringSubmatch(spec); m != nil {
		if ls := strings.Split(m[3], "•"); len(ls) > 0 {
			return n.path(m[1], ls...)
		}
	}
	return n
}

func (n *network) Loopback(node string, lat, cap int64) *network {
	props := linkProps{
		src: node, dst: node,
		wire: true, path: true, latency: lat,
		bandwidth: types.Resource[int64]{Cap: cap}}
	return n.linkWithProps(&props)
}

func Path(nodes string) *grpc.Network_Path {
	net := &network{}
	return linksToPath(net.nodesToLinks(nodes))
}

func PathList(specs ...string) []*grpc.Network_Path {
	if len(specs) == 0 {
		return nil
	}
	ps := make([]*grpc.Network_Path, len(specs))
	for i, spec := range specs {
		ps[i] = Path(spec)
	}
	return ps
}

func reverse(src []string) []string {
	srcLen := len(src)
	dst := make([]string, srcLen)
	for i := range src {
		dst[srcLen-1-i] = src[i]
	}
	return dst
}

func reverseLink(l string) string {
	if m := reLinkName.FindStringSubmatch(l); m != nil {
		return m[5] + m[4] + m[3]
	}
	return l
}

func reverseLinks(links []string) []string {
	links = reverse(links)
	for i := range links {
		links[i] = reverseLink(links[i])
	}
	return links
}

/*func findLink(im *grpc.Infrastructure, src, dst string) *grpc.Network_Link {
	for _, nw := range im.Networks {
		for _, l := range nw.Links {
			if l.Source == src && l.Target == dst {
				return l
			}
		}
	}
	return nil
}*/
