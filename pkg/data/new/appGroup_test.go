// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/port/grpc"
)

func TestAppGroupApplication(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	var ag AppGroup
	ag.Application("app").
		Workload("w1", 1000, 8000).
		Workload("w2", 2000, 16000)
	require.Len(ag.apps, 1)
	assert.Equal("app", ag.apps[0].Id)
	require.Len(ag.apps[0].Workloads, 2)
	assert.Equal("w1", ag.apps[0].Workloads[0].Id)
	assert.Equal("w2", ag.apps[0].Workloads[1].Id)
}

func TestAppGroupChannel(t *testing.T) {
	var ag AppGroup
	ag.Channel("aa.bb•cc.dd", 1, 2)
	assert.Equal(t,
		[]*grpc.Channel{
			{Id: "aa•bb→cc•dd",
				SourceApplication: "aa", SourceWorkload: "bb",
				TargetApplication: "cc", TargetWorkload: "dd",
				Latency: 1, Bandwidth: 2}},
		ag.Channels())
	ag.Channel("A.•C.D", 1, 2)
	assert.Len(t, ag.channels, 1)
}

func TestAppGroupBidiChannel(t *testing.T) {
	var ag AppGroup
	ag.BidiChannel("A.B•C.D", 1, 2, 3, 4)
	assert.Equal(t,
		[]*grpc.Channel{
			{Id: "A•B→C•D",
				SourceApplication: "A", SourceWorkload: "B",
				TargetApplication: "C", TargetWorkload: "D",
				Latency: 1, Bandwidth: 2},
			{Id: "C•D→A•B",
				SourceApplication: "C", SourceWorkload: "D",
				TargetApplication: "A", TargetWorkload: "B",
				Latency: 3, Bandwidth: 4}},
		ag.Channels())
}

func TestAppGroupMakeCompoundNames(t *testing.T) {
	ag := AppGroup{
		apps: []*grpc.Application{
			{Id: "A1",
				Workloads: []*grpc.Workload{
					{Id: "W1"},
					{Id: "W2"}}}},
		channels: []*grpc.Channel{
			{SourceApplication: "A1", SourceWorkload: "W1",
				TargetApplication: "A1", TargetWorkload: "W2"}}}
	ag.MakeCompoundNames()
	assert.Equal(t,
		AppGroup{
			apps: []*grpc.Application{
				{Id: "A1",
					Workloads: []*grpc.Workload{
						{Id: "A1_W1"},
						{Id: "A1_W2"}}}},
			channels: []*grpc.Channel{
				{SourceApplication: "A1", SourceWorkload: "A1_W1",
					TargetApplication: "A1", TargetWorkload: "A1_W2"}}},
		ag)
}

func TestApplicationWorkload(t *testing.T) {
	assert := assert.New(t)
	make := func(n string, c int32, r int64, ls ...string) *grpc.Workload {
		var ag AppGroup
		app := ag.Application("a")
		app.Workload(n, c, r, ls...)
		return app.app.Workloads[len(app.app.Workloads)-1]
	}
	assert.Equal(
		&grpc.Workload{Id: "W1", CPU: 1000, Memory: 8000},
		make("W1", 1000, 8000))
	assert.Equal(
		&grpc.Workload{Id: "W1",
			CPU: 1000, Memory: 8000,
			RequiredLabels:  []string{"A", "B"},
			ForbiddenLabels: []string{"C"}},
		make("W1", 1000, 8000, "A", "B", "!C"))
	assert.Equal(
		&grpc.Workload{Id: "W1",
			CPU: 1000, Memory: 8000,
			RequiredLabels:  []string{"A", "B"},
			ForbiddenLabels: []string{"C"}},
		make("W1", 1000, 8000, "A", "B", "-C"))
	assert.Equal(
		&grpc.Workload{Id: "W1",
			CPU: 1000, Memory: 8000,
			RequiredLabels:  []string{"A", "B"},
			ForbiddenLabels: []string{"C"}},
		make("W1", 1000, 8000, "A", "B", "-C"))
}
