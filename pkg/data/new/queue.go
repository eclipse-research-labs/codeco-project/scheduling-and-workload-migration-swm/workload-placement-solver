// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

type queueEntry struct {
	node    int
	parents []int
}
type queue []queueEntry

func (q queue) isEmpty() bool {
	return len(q) == 0
}

func (q *queue) push(node int, parents []int) {
	p := make([]int, 0, len(parents))
	p = append(p, parents...)
	*q = append(*q, queueEntry{node: node, parents: p})
}

func (q *queue) pop() (node int, parents []int) {
	node, parents = (*q)[0].node, (*q)[0].parents
	for i := 1; i < len(*q); i++ {
		(*q)[i-1] = (*q)[i]
	}
	*q = (*q)[:len(*q)-1]
	return
}
