// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/internal/defaults"
	"siemens.com/qos-solver/internal/port/grpc"
	"siemens.com/qos-solver/internal/types"
)

func TestInfrastructureNode(t *testing.T) {
	i := &Infrastructure{}
	assert.Same(t, i, i.Node("N", 1000, 8000))
	require.Len(t, i.nodes, 1)
	assert.Equal(t,
		&grpc.Node{
			Id:       "N",
			NodeType: grpc.Node_COMPUTE,
			CPU:      &grpc.Resource{Capacity: 1000},
			Memory:   &grpc.Resource{Capacity: 8000}},
		i.nodes[0])

	assert.Same(t, i, i.Node("M", 1000, 8000, "A", "B"))
	require.Len(t, i.nodes, 2)
	assert.Equal(t,
		&grpc.Node{
			Id:       "M",
			NodeType: grpc.Node_COMPUTE,
			CPU:      &grpc.Resource{Capacity: 1000},
			Memory:   &grpc.Resource{Capacity: 8000},
			Labels:   []string{"A", "B"}},
		i.nodes[1])
}

func TestInfrastructureNetworkNode(t *testing.T) {
	i := &Infrastructure{}
	assert.Same(t, i, i.NetworkNode("N1"))
	require.Len(t, i.nodes, 1)
	assert.Equal(t,
		&grpc.Node{
			Id:       "N1",
			NodeType: grpc.Node_NETWORK},
		i.nodes[0])

	assert.Same(t, i, i.NetworkNode("N2", 17, 19))
	require.Len(t, i.nodes, 2)
	assert.Equal(t,
		&grpc.Node{
			Id:        "N2",
			NodeType:  grpc.Node_NETWORK,
			Latency:   17,
			Bandwidth: &grpc.Resource{Capacity: 19}},
		i.nodes[1])
}

func TestNode(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(
		&grpc.Node{
			Id:       "W1",
			NodeType: grpc.Node_COMPUTE,
			CPU:      &grpc.Resource{Capacity: 1000},
			Memory:   &grpc.Resource{Capacity: 8000}},
		Node("W1", 1000, 8000))

	assert.Nil(linksToPath([]string{}))
	assert.Equal(
		&grpc.Node{
			Id:       "W1",
			NodeType: grpc.Node_COMPUTE,
			CPU:      &grpc.Resource{Capacity: 1000},
			Memory:   &grpc.Resource{Capacity: 8000},
			Labels:   []string{"A", "B"}},
		Node("W1", 1000, 8000, "A", "B"))
}

func TestNet(t *testing.T) {
	assert.Equal(t,
		&network{
			Name:    "net",
			Options: defaultNetworkOptions()},
		Net("net"))
}

func TestNetMediaMgmt(t *testing.T) {
	net := Net("x", networkOptions{wire: types.NewOption(false)})
	assert.Len(t, net.Media, 1)
	to, fro := net.defaultLinkProps()
	assert.False(t, to.wire)
	assert.False(t, fro.wire)

	net = Net("x", networkOptions{wire: types.NewOption(true)})
	assert.Len(t, net.Media, 0)
	to, fro = net.defaultLinkProps()
	assert.True(t, to.wire)
	assert.False(t, fro.wire)

	net = Net("x", networkOptions{
		wire: types.NewOption(true), duplex: types.NewOption(true)})
	assert.Len(t, net.Media, 0)
	to, fro = net.defaultLinkProps()
	assert.True(t, to.wire)
	assert.True(t, fro.wire)
}

func TestReLinkName(t *testing.T) {
	check := func(link, n1, n2, src, op, dst string) {
		t.Helper()
		assert.Equal(t,
			[]string{link, n1, n2, src, op, dst},
			reLinkName.FindStringSubmatch(link))
	}
	check("a->b", "", "", "a", "->", "b")
	check("a→b", "", "", "a", "→", "b")
	check("a-x-y→b", "", "", "a-x-y", "→", "b")
	check("a-x-y->b", "", "", "a-x-y", "->", "b")
	check("a→b-x-y", "", "", "a", "→", "b-x-y")

	check("n1:a→b", "n1", "", "a", "→", "b")
	check("n1,n2:a→b", "n1", "n2", "a", "→", "b")
}

func TestNodesToLinks(t *testing.T) {
	assert := assert.New(t)
	net := network{}
	assert.Nil(net.nodesToLinks(""))
	assert.Nil(net.nodesToLinks("N1"))
	assert.Equal([]string{"N1→N2"}, net.nodesToLinks("N1•N2"))
	assert.Equal([]string{"N1→N2", "N2→N3"}, net.nodesToLinks("N1•N2•N3"))

	net.Links = []*grpc.Network_Link{
		{Id: "1", Source: "N1", Target: "N2"},
		{Id: "2", Source: "N2", Target: "N3"}}
	assert.Equal([]string{"1", "2"}, net.nodesToLinks("N1•N2•N3"))
}

func TestLinksToPath(t *testing.T) {
	assert.Nil(t, linksToPath([]string{}))
	assert.Equal(t,
		&grpc.Network_Path{Id: "N1→N2", Links: []string{"N1→N2"}},
		linksToPath([]string{"N1→N2"}))
	assert.Equal(t,
		&grpc.Network_Path{
			Id: "N1→²N3", Links: []string{"N1→N2", "N2→N3"}},
		linksToPath([]string{"N1→N2", "N2→N3"}))
}

func TestPathIndex(t *testing.T) {
	assert.Equal(t,
		"\u207D\u00B9\u00B2\u00B3\u2074\u2075"+ // superscript "(12345"
			"\u2076\u2077\u2078\u2079\u2070\u207E", // superscript "67890)"
		pathIndex(1234567890))
}

func TestNetworkMakePathsNamesUnique(t *testing.T) {
	assert := assert.New(t)
	net := network{Paths: []*grpc.Network_Path{
		{Id: "a→b"},
		{Id: "c→\u2074d"},
		{Id: "c→\u2074d"},
		{Id: "c→\u2074d"},
		{Id: "e→\u00B2f"}}}
	net.makePathNamesUnique()
	assert.Equal("a→b", net.Paths[0].Id)
	assert.Equal("c→\u2074d\u207D\u00B9\u207E", net.Paths[1].Id)
	assert.Equal("c→\u2074d\u207D\u00B2\u207E", net.Paths[2].Id)
	assert.Equal("c→\u2074d\u207D\u00B3\u207E", net.Paths[3].Id)
	assert.Equal("e→\u00B2f", net.Paths[4].Id)
}

func TestNetworkUniqueName(t *testing.T) {
	assert := assert.New(t)
	n := network{}
	assert.Equal("L1", n.uniqueName("L"))
	assert.Equal("L2", n.uniqueName("L"))
	assert.Equal("L3", n.uniqueName("L"))
	assert.Equal("M1", n.uniqueName("M"))
	assert.Equal("P1", n.uniqueName("P"))
	assert.Equal("M2", n.uniqueName("M"))
	assert.Equal("L4", n.uniqueName("L"))
	assert.Equal("X1", n.uniqueName("X"))
}

func TestNetworkMedium(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.Medium("", 17))
	require.Len(t, n.Media, 1)
	assert.Equal(t, "M1", n.Media[0].Id)
	assert.Equal(t, int64(17), n.Media[0].Bandwidth.Capacity)
}

func TestLinkName(t *testing.T) {
	assert.Equal(t, "a→b", linkName("a", "b"))
}

func TestNetworkLink(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.Link("n•m", 17, 19))
	require.Len(t, n.Media, 1)
	assert.Equal(t, int64(19), n.Media[0].Bandwidth.Capacity)
	require.Len(t, n.Links, 1)
	assert.Equal(t, "n→m", n.Links[0].Id)
	assert.Equal(t, "n", n.Links[0].Source)
	assert.Equal(t, "m", n.Links[0].Target)
	assert.Equal(t, int64(17), n.Links[0].Latency)

	n = &network{}
	assert.Same(t, n, n.Link("illegal", 17, 19))
	assert.Len(t, n.Media, 0)
	assert.Len(t, n.Links, 0)
}

func TestNetworkBidiLink(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.BidiLink("n•m", 17, 19))
	require.Len(t, n.Media, 2)
	assert.Equal(t, int64(19), n.Media[0].Bandwidth.Capacity)
	assert.Equal(t, int64(19), n.Media[1].Bandwidth.Capacity)
	require.Len(t, n.Links, 2)
	assert.Equal(t, "n→m", n.Links[0].Id)
	assert.Equal(t, "n", n.Links[0].Source)
	assert.Equal(t, "m", n.Links[0].Target)
	assert.Equal(t, int64(17), n.Links[0].Latency)
	assert.Equal(t, "m→n", n.Links[1].Id)
	assert.Equal(t, "m", n.Links[1].Source)
	assert.Equal(t, "n", n.Links[1].Target)
	assert.Equal(t, int64(17), n.Links[1].Latency)

	n = &network{}
	assert.Same(t, n, n.BidiLink("n•m", 17, 19, 23, 29))
	require.Len(t, n.Media, 2)
	assert.Equal(t, int64(19), n.Media[0].Bandwidth.Capacity)
	assert.Equal(t, int64(29), n.Media[1].Bandwidth.Capacity)
	require.Len(t, n.Links, 2)
	assert.Equal(t, "n→m", n.Links[0].Id)
	assert.Equal(t, "n", n.Links[0].Source)
	assert.Equal(t, "m", n.Links[0].Target)
	assert.Equal(t, int64(17), n.Links[0].Latency)
	assert.Equal(t, "m→n", n.Links[1].Id)
	assert.Equal(t, "m", n.Links[1].Source)
	assert.Equal(t, "n", n.Links[1].Target)
	assert.Equal(t, int64(23), n.Links[1].Latency)

	n = &network{}
	assert.Same(t, n, n.BidiLink("illegal", 17, 19))
	assert.Len(t, n.Media, 0)
	assert.Len(t, n.Links, 0)
}

func TestNetworkLinkProps(t *testing.T) {
	net := network{Options: defaultNetworkOptions()}
	p1, p2 := net.defaultLinkProps()
	ps := linkProps{wire: true, latency: defaults.LatencyLink,
		bandwidth: types.Resource[int64]{Cap: defaults.BandwidthLink}}
	assert.Equal(t, ps, p1)
	assert.Equal(t, ps, p2)

	net.Options.latency = types.NewOption(int64(17))
	net.Options.bandwidth = types.NewOption(int64(19))
	p1, p2 = net.defaultLinkProps()
	ps = linkProps{wire: true, latency: 17,
		bandwidth: types.Resource[int64]{Cap: 19}}
	assert.Equal(t, ps, p1)
	assert.Equal(t, ps, p2)
}

func TestNetworkLinkWithProps(t *testing.T) {
	makeLink := func(isPath bool) *network {
		n := &network{}
		props := linkProps{
			src: "n", dst: "m", wire: true, path: isPath, latency: 17,
			bandwidth: types.Resource[int64]{Cap: 19}}
		assert.Same(t, n, n.linkWithProps(&props))
		require.Len(t, n.Media, 1)
		assert.Equal(t, int64(19), n.Media[0].Bandwidth.Capacity)
		require.Len(t, n.Links, 1)
		assert.Equal(t, "n→m", n.Links[0].Id)
		assert.Equal(t, "n", n.Links[0].Source)
		assert.Equal(t, "m", n.Links[0].Target)
		assert.Equal(t, int64(17), n.Links[0].Latency)
		assert.Equal(t, n.Media[0].Id, n.Links[0].Medium)
		return n
	}
	net := makeLink(false)
	assert.Len(t, net.Paths, 0)
	net = makeLink(true)
	require.Len(t, net.Paths, 1)
	assert.Equal(t, "n→m", net.Paths[0].Id)
	assert.Equal(t, []string{"n→m"}, net.Paths[0].Links)
}

func TestPathName(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("a→b", PathName("a", "b", 1))
	assert.Equal("a→²b", PathName("a", "b", 2))
}

func TestPathName_internal(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("path", pathName([]string{}))
	assert.Equal("a→b", pathName([]string{"a→b"}))
	assert.Equal("a→²c", pathName([]string{"a→b", "b→c"}))
	assert.Equal("a→³d", pathName([]string{"a→b", "b→c", "c→d"}))

	assert.Equal("a", pathName([]string{"a"}))
	assert.Equal("a∘b", pathName([]string{"a", "b"}))
	assert.Equal("a∘²c", pathName([]string{"a", "b", "c"}))
	assert.Equal("a∘³d", pathName([]string{"a", "b", "c", "d"}))
}

func TestRePathSpec(t *testing.T) {
	m := pathSpec.FindStringSubmatch("m•o")
	require.Len(t, m, 4)
	assert.Equal(t, "", m[1])
	assert.Equal(t, "", m[2])
	assert.Equal(t, "m•o", m[3])

	m = pathSpec.FindStringSubmatch("n:m•o")
	require.Len(t, m, 4)
	assert.Equal(t, "n", m[1])
	assert.Equal(t, "", m[2])
	assert.Equal(t, "m•o", m[3])

	m = pathSpec.FindStringSubmatch("n1,n2:m•o")
	require.Len(t, m, 4)
	assert.Equal(t, "n1", m[1])
	assert.Equal(t, "n2", m[2])
	assert.Equal(t, "m•o", m[3])
}

func TestNetworkPath_private(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.path(""))
	assert.Len(t, n.Paths, 0)

	assert.Same(t, n, n.path("", "a->b"))
	require.Len(t, n.Paths, 1)
	assert.Equal(t, "a→b", n.Paths[0].Id)
	assert.Equal(t, []string{"a->b"}, n.Paths[0].Links)

	assert.Same(t, n, n.path("", "a→b"))
	require.Len(t, n.Paths, 2)
	assert.Equal(t, "a→b", n.Paths[1].Id)
	assert.Equal(t, []string{"a→b"}, n.Paths[1].Links)

	assert.Same(t, n, n.path("", "a->b", "b->c"))
	require.Len(t, n.Paths, 3)
	assert.Equal(t, "a→²c", n.Paths[2].Id)
	assert.Equal(t, []string{"a->b", "b->c"}, n.Paths[2].Links)
}

func TestNetworkPath(t *testing.T) {
	n := &network{}
	n.Link("N1•N2", 17, 19)
	n.Link("N2•N3", 17, 19)
	check := func(input, id string, links ...string) {
		expLen := len(n.Paths) + 1
		n.Path(input)
		require.Len(t, n.Paths, expLen)
		assert.Equal(t,
			&grpc.Network_Path{Id: id, Links: links},
			n.Paths[expLen-1])
	}
	check("N1•N2", "N1→N2", "N1→N2")
	check("N1•N2•N3", "N1→²N3", "N1→N2", "N2→N3")
	check("p:N2•N3", "p", "N2→N3")

	n = &network{}
	n.Link("1:N1•N2", 17, 19)
	n.Link("2:N2•N3", 17, 19)
	n.Link("3:N3•N4", 17, 19)
	check("N1•N2", "1", "1")
	check("N1•N2•N3", "1∘2", "1", "2")
	check("N1•N2•N3•N4", "1∘²3", "1", "2", "3")
}

func TestNetworkBidiPath(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.BidiPath("N1•N2"))
	assert.Equal(t,
		[]*grpc.Network_Path{
			{Id: "N1→N2", Links: []string{"N1→N2"}},
			{Id: "N2→N1", Links: []string{"N2→N1"}}},
		n.Paths)

	n = &network{}
	assert.Same(t, n, n.BidiPath("a:N1•N2"))
	assert.Equal(t,
		[]*grpc.Network_Path{
			{Id: "a", Links: []string{"N1→N2"}},
			{Id: "N2→N1", Links: []string{"N2→N1"}}},
		n.Paths)

	n = &network{}
	assert.Same(t, n, n.BidiPath("a,b:N1•N2"))
	assert.Equal(t,
		[]*grpc.Network_Path{
			{Id: "a", Links: []string{"N1→N2"}},
			{Id: "b", Links: []string{"N2→N1"}}},
		n.Paths)
}

func TestNetworkLinkPath(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.LinkPath("L1"))
	assert.Equal(t,
		[]*grpc.Network_Path{{Id: "L1", Links: []string{"L1"}}},
		n.Paths)

	n = &network{}
	assert.Same(t, n, n.LinkPath("1•2•3"))
	assert.Equal(t,
		[]*grpc.Network_Path{{Id: "1∘²3", Links: []string{"1", "2", "3"}}},
		n.Paths)

	n = &network{}
	assert.Same(t, n, n.LinkPath("a:L1•L2"))
	assert.Equal(t,
		[]*grpc.Network_Path{{Id: "a", Links: []string{"L1", "L2"}}},
		n.Paths)
}

func TestNetworkLoopback(t *testing.T) {
	n := &network{}
	assert.Same(t, n, n.Loopback("n", 17, 19))
	require.Len(t, n.Media, 1)
	assert.Equal(t, "n→nₘ", n.Media[0].Id)
	assert.Equal(t, int64(19), n.Media[0].Bandwidth.Capacity)
	require.Len(t, n.Links, 1)
	assert.Equal(t, "n→n", n.Links[0].Id)
	assert.Equal(t, int64(17), n.Links[0].Latency)
	require.Len(t, n.Paths, 1)
	assert.Equal(t, "n→n", n.Paths[0].Id)
	assert.Equal(t, []string{"n→n"}, n.Paths[0].Links)
}

func TestReverseLink(t *testing.T) {
	assert.Equal(t, "b->a", reverseLink("a->b"))
	assert.Equal(t, "b→a", reverseLink("a→b"))
}

func TestReverseLinks(t *testing.T) {
	assert.Equal(t,
		[]string{"d->c", "c->b", "b->a"},
		reverseLinks([]string{"a->b", "b->c", "c->d"}))
}

func TestSplitLinkSpec(t *testing.T) {
	check := func(spec string, ps ...string) {
		ok, m1, m2, m3, m4 := splitLinkSpec(spec)
		if len(ps) == 0 {
			assert.False(t, ok)
		} else {
			assert.Equal(t, ps, []string{m1, m2, m3, m4})
		}
	}
	check("illegal")
	check("N1•N2", "", "", "N1", "N2")
	check("L:N1•N2", "L", "", "N1", "N2")
	check("1:N1•N2", "1", "", "N1", "N2")
	check("L1,L2:N1•N2", "L1", "L2", "N1", "N2")
	check("1,2:N1•N2", "1", "2", "N1", "N2")
}

func TestLink(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(
		&grpc.Network_Link{
			Id: "A→B", Source: "A", Target: "B", Latency: 1},
		Link("A•B", 1, 2))
	assert.Equal(
		&grpc.Network_Link{
			Id: "L", Source: "A", Target: "B", Latency: 1},
		Link("L:A•B", 1, 2))
	assert.Equal(
		&grpc.Network_Link{
			Id: "1", Source: "A", Target: "B", Latency: 1},
		Link("1:A•B", 1, 2))
	assert.Equal(
		&grpc.Network_Link{},
		Link("illegal", 1, 2))
}

func TestReverse(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{}, reverse([]string{}))
	assert.Equal([]string{"A"}, reverse([]string{"A"}))
	assert.Equal([]string{"B", "A"}, reverse([]string{"A", "B"}))
	assert.Equal([]string{"C", "B", "A"}, reverse([]string{"A", "B", "C"}))
}

func TestNetworkNode(t *testing.T) {
	assert.Equal(t,
		&grpc.Node{
			Id:       "N1",
			NodeType: grpc.Node_NETWORK},
		NetworkNode("N1"))
	assert.Equal(t,
		&grpc.Node{
			Id:       "N2",
			NodeType: grpc.Node_NETWORK},
		NetworkNode("N2", 17, 19))
}

func TestNetworkNodeLoad(t *testing.T) {
	assert.Equal(t,
		&grpc.NetworkNodeLoad{Id: "N1", Cap: 17},
		NetworkNodeLoad("N1", 17))
}

func TestNetworkSetNodes(t *testing.T) {
	net := network{}
	nodes := []*grpc.Node{{Id: "N1"}, {Id: "N2"}}
	net.setNodes(nodes, []string{})
	require.Len(t, net.Switches, 2)
	assert.Equal(t, nodes[0], net.Switches[0])
	assert.Equal(t, nodes[1], net.Switches[1])

	net.setNodes(nodes, []string{"N2"})
	require.Len(t, net.Switches, 1)
	assert.Equal(t, nodes[1], net.Switches[0])
}

func TestNetworkAdd(t *testing.T) {
	args := []interface{}{
		&grpc.Node{},
		&grpc.Network_Link{},
		&grpc.Network_Path{}}
	net := network{}
	net.add(args)
	require.Len(t, net.Switches, 1)
	assert.Equal(t, args[0], net.Switches[0])
	require.Len(t, net.Links, 1)
	assert.Equal(t, args[1], net.Links[0])
	require.Len(t, net.Paths, 1)
	assert.Equal(t, args[2], net.Paths[0])
	assert.PanicsWithValue(t, "unsupported type", func() { net.add(17) })
}

func TestLinkLoad(t *testing.T) {
	assert.Equal(t,
		&grpc.LinkLoad{Src: "C1", Dst: "C2", Cap: 25},
		LinkLoad("C1•C2", 25))
	assert.Equal(t,
		&grpc.LinkLoad{},
		LinkLoad("illegal", 25))
}
