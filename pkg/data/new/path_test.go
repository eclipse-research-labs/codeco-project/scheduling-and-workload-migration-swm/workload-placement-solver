// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package new

import (
	"math/rand"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"siemens.com/qos-solver/internal/port/grpc"
)

func fourNodesModel() *grpc.Infrastructure {
	var infrastructure Infrastructure
	infrastructure.
		Node("C1", 1000, 8000).
		Node("C2", 1000, 8000).
		Node("C3", 1000, 8000).
		Node("C4", 1000, 8000).
		NetworkNode("N1").
		NetworkNode("N2").
		NetworkNode("N3").
		NetworkNode("N4").
		Add(Net("net").
			Link("C1•N1", 1, 100).
			Link("C1•C4", 1, 100).
			Link("N1•N2", 1, 100).
			Link("N1•N3", 1, 100).
			Link("N2•C2", 1, 100).
			Link("N2•N4", 1, 100).
			Link("N3•N4", 1, 100).
			Link("N4•C3", 1, 100).
			Link("C2•C3", 1, 100))
	return infrastructure.AsGrpc()
}

func fourNodesModelBidi() *grpc.Infrastructure {
	var infrastructure Infrastructure
	infrastructure.
		Node("C1", 1000, 8000).
		Node("C2", 1000, 8000).
		Node("C3", 1000, 8000).
		Node("C4", 1000, 8000).
		NetworkNode("N1").
		NetworkNode("N2").
		NetworkNode("N3").
		NetworkNode("N4").
		Add(Net("net").
			BidiLink("C1•N1", 1, 100).
			BidiLink("C1•C4", 1, 100).
			BidiLink("N1•N2", 1, 100).
			BidiLink("N1•N3", 1, 100).
			BidiLink("N2•C2", 1, 100).
			BidiLink("N2•N4", 1, 100).
			BidiLink("N3•N4", 1, 100).
			BidiLink("N4•C3", 1, 100).
			BidiLink("C2•C3", 1, 100))
	return infrastructure.AsGrpc()
}

func linkList(model *grpc.Infrastructure, specs ...string) []int {
	ls := make([]int, len(specs))
	for i, spec := range specs {
		ps := strings.Split(spec, "•")
		ls[i] = model.FindLink(ps[0], ps[1])
	}
	return ls
}

func TestNodeInfos(t *testing.T) {
	model := fourNodesModel()
	assert.Equal(t,
		nodeInfo{
			index: map[string]int{
				"C1": 0, "C2": 1, "C3": 2, "C4": 3,
				"N1": 4, "N2": 5, "N3": 6, "N4": 7},
			links: [][]int{
				linkList(model, "C1•C4", "C1•N1"),
				linkList(model, "C2•C3"),
				nil,
				nil,
				linkList(model, "N1•N2", "N1•N3"),
				linkList(model, "N2•C2", "N2•N4"),
				linkList(model, "N3•N4"),
				linkList(model, "N4•C3")}},
		nodeInfos(model.Nodes, model.Networks[0].Links))

	var infrastructure Infrastructure
	infrastructure.
		Node("C1", 1000, 8000).
		Node("C2", 1000, 8000).
		Add(Net("net").
			Link("C1•C2", 1, 100).
			Link("Cx•C2", 1, 100).
			Link("C1•Cx", 1, 100))
	model = infrastructure.AsGrpc()
	assert.Equal(t,
		nodeInfo{
			index: map[string]int{"C1": 0, "C2": 1},
			links: [][]int{
				linkList(model, "C1•C2"),
				nil}},
		nodeInfos(model.Nodes, model.Networks[0].Links))
}

func TestNodeInfos_shuffle(t *testing.T) {
	model := fourNodesModelBidi()
	for i := 0; i < 10; i++ {
		ls := model.Networks[0].Links
		rand.Shuffle(len(ls), func(i, j int) {
			ls[i], ls[j] = ls[j], ls[i]
		})
		assert.Equal(t,
			nodeInfo{
				index: map[string]int{
					"C1": 0, "C2": 1, "C3": 2, "C4": 3,
					"N1": 4, "N2": 5, "N3": 6, "N4": 7},
				links: [][]int{
					linkList(model, "C1•C4", "C1•N1"),
					linkList(model, "C2•C3", "C2•N2"),
					linkList(model, "C3•C2", "C3•N4"),
					linkList(model, "C4•C1"),
					linkList(model, "N1•C1", "N1•N2", "N1•N3"),
					linkList(model, "N2•C2", "N2•N1", "N2•N4"),
					linkList(model, "N3•N1", "N3•N4"),
					linkList(model, "N4•C3", "N4•N2", "N4•N3")}},
			nodeInfos(model.Nodes, model.Networks[0].Links),
			"round %d", i)
	}
}

func TestNewSearchState(t *testing.T) {
	im := fourNodesModel()
	assert.Equal(t,
		searchState{
			nodes:     im.Nodes,
			links:     im.Networks[0].Links,
			allPaths:  true,
			nodeInfos: nodeInfos(im.Nodes, im.Networks[0].Links)},
		newSearchState(im.Nodes, im.Networks[0].Links, true))
}

func TestPathsBetween(t *testing.T) {
	assert := assert.New(t)
	model := fourNodesModel()
	s := newSearchState(model.Nodes, model.Networks[0].Links, true)
	assert.Equal(
		PathList("C1•N1•N2•C2"),
		s.pathsBetween("C1", "C2"))
	assert.Equal(
		PathList("C1•C4"),
		s.pathsBetween("C1", "C4"))

	assert.Equal(PathList(), s.pathsBetween("Cx", "C4"))
	assert.Equal(PathList(), s.pathsBetween("C1", "Cx"))
	model.Networks[0].Links[0].Target = "Nx"
	assert.Equal(PathList(), s.pathsBetween("C1", "C2"))
}

func modelC2N4Bidi() *grpc.Infrastructure {
	var infrastructure Infrastructure
	infrastructure.
		Node("C1", 1000, 8000).
		Node("C2", 1000, 8000).
		NetworkNode("N1").
		NetworkNode("N2").
		NetworkNode("N3").
		NetworkNode("N4").
		Add(Net("net").
			BidiLink("C1•N1", 1, 100).
			BidiLink("C1•N2", 1, 100).
			BidiLink("N1•N3", 1, 100).
			BidiLink("N1•N4", 1, 100).
			BidiLink("N2•N3", 1, 100).
			BidiLink("N2•N4", 1, 100).
			BidiLink("N3•C2", 1, 100).
			BidiLink("N4•C2", 1, 100))
	return infrastructure.AsGrpc()
}

func TestPathsBetweenForModelC2N4Bidi(t *testing.T) {
	im := modelC2N4Bidi()
	s := newSearchState(im.Nodes, im.Networks[0].Links, true)
	assert.Equal(t,
		PathList(
			"C1•N1•N3•C2",
			"C1•N1•N4•C2",
			"C1•N2•N3•C2",
			"C1•N2•N4•C2",
			"C1•N1•N3•N2•N4•C2",
			"C1•N1•N4•N2•N3•C2",
			"C1•N2•N3•N1•N4•C2",
			"C1•N2•N4•N1•N3•C2"),
		s.pathsBetween("C1", "C2"))
	assert.Equal(t,
		PathList(
			"C2•N3•N1•C1",
			"C2•N3•N2•C1",
			"C2•N4•N1•C1",
			"C2•N4•N2•C1",
			"C2•N3•N1•N4•N2•C1",
			"C2•N3•N2•N4•N1•C1",
			"C2•N4•N1•N3•N2•C1",
			"C2•N4•N2•N3•N1•C1"),
		s.pathsBetween("C2", "C1"))
}

func TestFindShortestPathsBidiForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	pathsC1C2 := PathList(
		"C1•N1•N3•C2",
		"C2•N3•N1•C1")
	assert.Equal(t,
		pathsC1C2,
		FindShortestPaths(model, "C1•C2", "C2•C1"))
	assert.Equal(t,
		PathList(),
		FindShortestPaths(model, "C1::C2"))

	assert.Equal(t,
		pathsC1C2,
		FindShortestPathsBidi(model, "C1•C2"))
	assert.Equal(t,
		PathList(),
		FindShortestPathsBidi(model, "C1::C2"))
}

func TestFindAllPathsBidiForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	pathsC1C2 := PathList(
		"C1•N1•N3•C2",
		"C1•N1•N4•C2",
		"C1•N2•N3•C2",
		"C1•N2•N4•C2",
		"C1•N1•N3•N2•N4•C2",
		"C1•N1•N4•N2•N3•C2",
		"C1•N2•N3•N1•N4•C2",
		"C1•N2•N4•N1•N3•C2",
		"C2•N3•N1•C1",
		"C2•N3•N2•C1",
		"C2•N4•N1•C1",
		"C2•N4•N2•C1",
		"C2•N3•N1•N4•N2•C1",
		"C2•N3•N2•N4•N1•C1",
		"C2•N4•N1•N3•N2•C1",
		"C2•N4•N2•N3•N1•C1")
	assert.Equal(t,
		pathsC1C2,
		FindAllPaths(model, "C1•C2", "C2•C1"))
	assert.Equal(t,
		PathList(),
		FindAllPaths(model, "C1::C2"))

	assert.Equal(t,
		pathsC1C2,
		FindAllPathsBidi(model, "C1•C2"))
	assert.Equal(t,
		PathList(),
		FindAllPathsBidi(model, "C1::C2"))
}

func TestIsReverseLink(t *testing.T) {
	assert.True(t, isReverseLink("n2->n1", "n1->n2"))
	assert.True(t, isReverseLink("n2->n1", "n1→n2"))
	assert.True(t, isReverseLink("n2→n1", "n1->n2"))
	assert.True(t, isReverseLink("n2→n1", "n1→n2"))
	assert.False(t, isReverseLink("x→a", "a→b"))
	assert.False(t, isReverseLink("b→x", "a→b"))
}

func TestIsReverse(t *testing.T) {
	p1 := &grpc.Network_Path{Links: []string{}}
	p2 := &grpc.Network_Path{Links: []string{"A->B", "B->C"}}
	p3 := &grpc.Network_Path{Links: []string{"C->B", "B->A"}}
	p4 := &grpc.Network_Path{Links: []string{"C->B", "X->A"}}
	assert.True(t, isReverse(nil, nil))
	assert.True(t, isReverse(nil, p1))
	assert.True(t, isReverse(p1, nil))
	assert.True(t, isReverse(p1, p1))
	assert.True(t, isReverse(p2, p3))
	assert.False(t, isReverse(nil, p2))
	assert.False(t, isReverse(p2, nil))
	assert.False(t, isReverse(p2, p4))
	assert.False(t, isReverse(p1, p2))
}

func TestBidiPaths(t *testing.T) {
	assert.Equal(t,
		PathList("A•N1•C", "A•N2•C"),
		bidiPaths(
			PathList("A•N1•B", "A•N2•B", "A•N1•C", "A•N2•C"),
			PathList("C•N1•A", "C•N2•A")))
	assert.Equal(t,
		PathList(),
		bidiPaths(
			PathList("A•N1•B", "A•N2•B"),
			PathList()))
}

func TestFindShortestBidiPathsForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	assert.Equal(t,
		PathList(
			"C1•N1•N3•C2"),
		FindShortestBidiPaths(model, "C1•C2"))
	assert.Equal(t,
		PathList(),
		FindShortestBidiPaths(model, "C1::C2"))
}

func TestFindAllBidiPathsForModelC2N4(t *testing.T) {
	model := modelC2N4Bidi()
	assert.Equal(t,
		PathList(
			"C1•N1•N3•C2",
			"C1•N1•N4•C2",
			"C1•N2•N3•C2",
			"C1•N2•N4•C2",
			"C1•N1•N3•N2•N4•C2",
			"C1•N1•N4•N2•N3•C2",
			"C1•N2•N3•N1•N4•C2",
			"C1•N2•N4•N1•N3•C2"),
		FindAllBidiPaths(model, "C1•C2"))
	assert.Equal(t,
		PathList(),
		FindAllBidiPaths(model, "C1::C2"))
}
