// SPDX-FileCopyrightText: 2023 Siemens AG
// SPDX-License-Identifier: Apache-2.0

package data

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"siemens.com/qos-solver/pkg/textformat"
)

func TestTiny_appGroup(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.CompoundNames, false)()

	ag, _, err := tiny()
	require.NoError(err)
	require.Len(ag.Applications, 1)
	require.Len(ag.Applications[0].Workloads, 2)
	assert.Equal("w1", ag.Applications[0].Workloads[0].Id)
	assert.Equal("w2", ag.Applications[0].Workloads[1].Id)
	require.Len(ag.Channels, 1)
	assert.Equal("w1", ag.Channels[0].SourceWorkload)
	assert.Equal("w2", ag.Channels[0].TargetWorkload)

	textformat.CompoundNames = true
	ag, _, err = tiny()
	require.NoError(err)
	require.Len(ag.Applications, 1)
	require.Len(ag.Applications[0].Workloads, 2)
	assert.Equal("a1_w1", ag.Applications[0].Workloads[0].Id)
	assert.Equal("a1_w2", ag.Applications[0].Workloads[1].Id)
	require.Len(ag.Channels, 1)
	assert.Equal("a1_w1", ag.Channels[0].SourceWorkload)
	assert.Equal("a1_w2", ag.Channels[0].TargetWorkload)
}

func TestTiny_infrastructure(t *testing.T) {
	assert, require := assert.New(t), require.New(t)
	defer restore(&textformat.ImplicitReversePath, false)()

	_, infra, err := tiny()
	require.NoError(err)
	assert.Len(infra.Nodes, 2)
	require.Len(infra.Networks, 1)
	assert.Len(infra.Networks[0].Links, 4)
	assert.Len(infra.Networks[0].Paths, 2)

	textformat.ImplicitReversePath = true
	_, infra, err = tiny()
	require.NoError(err)
	assert.Len(infra.Nodes, 2)
	require.Len(infra.Networks, 1)
	assert.Len(infra.Networks[0].Links, 4)
	assert.Len(infra.Networks[0].Paths, 1)
}
