# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

/^ ✖ / {if ($2>0) {printf("%s\n",FILENAME)}}
