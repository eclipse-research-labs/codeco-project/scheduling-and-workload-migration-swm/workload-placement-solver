# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

function indent() {
    printf("\n")
    for (i=1;i<=tabs;i++) { printf("\t") }
}
function beginTag(name,props,closer) {
    indent()
    printf("<%s",name)
    for (i in props) {
        printf(" %s=\"%s\"",i,props[i])
    }
    printf(closer)
    delete props
}
function openTag(name,props) {
    beginTag(name,props,">")
    tabs+=1
}
function emptyTag(name,props) {
    beginTag(name,props,"/>")
}
function endTag(name,newLine) {
    tabs-=1
    if (newLine) { indent() }
    printf("</%s>",name)
}
function testcase(suite,test,result,msg) {
    props["name"]=test
    props["classname"]=suite
    props["time"]="0.000"
    if (result == "PASS") {
        emptyTag("testcase",props)
    } else {
        openTag("testcase",props)
        props["message"]=msg
        props["type"]="Assertion error"
        emptyTag("failure",props)
        endTag("testcase",1)
    }
}
function testsuite(name,results,msg) {
    successes=0
    for (i in results) {
        if (results[i] == "PASS") { successes++ }
    }

    props["name"]=name
    props["tests"]=cases
    props["failures"]=cases-successes
    props["errors"]=0
    props["time"]="0.000"
    if (length(timestamp)>0) { props["timestamp"]=timestamp }
    openTag("testsuite",props)
    for (i in file) {
        testcase(name,file[i],results[i],msg)
    }
    endTag("testsuite",1)
}
BEGIN { FS=":" }
{
    file[NR]=$1
    static[NR]=index($2,"statically linked")>0 ? "PASS" : "FAIL"
    stripped[NR]=index($2,"not stripped")>0 ? "FAIL" : "PASS"
}
END {
    tabs=0;cases=NR-1
    printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
    props["tests"]=2*cases
    props["time"]="0.000"
    openTag("testsuites",props)
    testsuite("link-type",static,"Executable is not statically linked.")
    testsuite("stripped",stripped,"Executable not stripped.")
    endTag("testsuites",1)
}
