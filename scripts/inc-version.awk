# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

/^[0-9.]+/{
	if (kind=="major") {
		$1++;$2=0;$3=0
	} else if (kind=="minor") {
		$2++;$3=0
	} else {
		$3++
	};
	printf("%d.%d.%d\n",$1,$2,$3)
}
