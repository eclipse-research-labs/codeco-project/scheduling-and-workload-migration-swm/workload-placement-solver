#!/bin/sh
# SPDX-FileCopyrightText: 2023 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# Parameters
#   1 - image name                  -- solve       | solve
#   2 - image name in repository    -- solve       | solve
#   3 - image tag                   -- 0.3.1       | 0.3.1_arm64
#   4 - image platform              -- linux/amd64 | liunx/arm64
#   5 - version of base image       -- "", "3.18"
#
# Variables:
#   BUILD_ARGS - arguments for building image (form: key=value)

imageDir=${imageDir:-tmp/img}
buildArgs() {
    if [ -n "${1}" ] ; then
        echo "VERSION=${1} ${2}"
    else
        echo "${2}"
    fi | sed -Ee "s/([^ ]+)/--build-arg \\1/g" -e "s/^ +//"
}
imageTags() {
    printf "%s %s:%s" "${1}" "${2}" "${3}"
    if [ "${CI_COMMIT_REF_SLUG}" = main ] || [ -z "${CI_COMMIT_REF_SLUG}" ]
    then
        if [ -z "$(printf "%s" "${3}" | tr -dc _)" ]
        then
            printf " %s %s:%s" "${1}" "${2}" latest
        fi
    fi
}
buildWithKaniko() {
    mkdir -p /kaniko/.docker
    printf '{"auths":{"%s":{"username":"%s","password":"%s"}}' \
        "${CI_REGISTRY}" "${CI_REGISTRY_USER}" \
        "${CI_REGISTRY_PASSWORD}" >/kaniko/.docker/config.json
    KFLAGS=
    if [ "${http_proxy}" ] || [ "${https_proxy}" ] ; then
        # shellcheck disable=SC2154 # intentional: no_proxy is global
        printf ',"proxies":{"default":{"http_proxy":"%s","https_proxy":"%s","no_proxy":"%s"}}' \
            "${http_proxy}" "${https_proxy}" "${no_proxy}" \
            >>/kaniko/.docker/config.json
        KFLAGS="--build-arg https_proxy=${https_proxy}"
        KFLAGS="${KFLAGS} --build-arg http_proxy=${http_proxy}"
        KFLAGS="${KFLAGS} --build-arg no_proxy=${no_proxy}"
    fi
    echo "}" >>/kaniko/.docker/config.json
    if [ -n "${4}" ] ; then
        platform=--custom-platform=${4}
    fi
    cd "${imageDir}/${1}" || return
    # shellcheck disable=SC2086,SC2046 # intentional: word splitting
    /kaniko/executor -c . ${platform} ${KFLAGS} \
        $(buildArgs "${5}" "${BUILD_ARGS}") \
        $(imageTags -d "${CI_REGISTRY_IMAGE}/${2}" "${3}") \
        --image-name-tag-with-digest-file iid.txt
}
buildWithDocker() {
    if [ "${CI}" = "local" ] ; then
        img=${CI_REGISTRY_IMAGE}/${2}
    else
        img=${2}
    fi
    if [ -n "${4}" ] ; then
        platform=--platform=${4}
    fi
    cd "${imageDir}/${1}" || return
    # shellcheck disable=SC2086,SC2046 # intentional: word splitting
    docker build -q $(buildArgs "${5}" "${BUILD_ARGS}") \
        ${platform} --iidfile iid.txt \
        $(imageTags -t "${img}" "${3}") .
}
if [ -z "${CI}" ] || [ "${CI}" = "local" ] ; then
    buildWithDocker "${@}"
else
    buildWithKaniko "${@}"
fi
