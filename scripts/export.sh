#!/bin/sh
# SPDX-FileCopyrightText: 2024 Siemens AG
# SPDX-License-Identifier: Apache-2.0

# Parameters
#   1 - target repo
#   2 - version (default: current version)

# shellcheck disable=SC1111 # Unicode characters are OK

. scripts/gitlab-infos.sh

if output=$(git status --untracked-files=no --porcelain) && [ -n "${output}" ] ; then
	echo "Working directory not clean; stop."
	exit 1
fi
target=${1}
case "${target}" in
	codeco|eclipse)
		;;
	*)
		echo "Unknown target: ${target}"
		exit 1
		;;
esac
if [ "$(git symbolic-ref --short HEAD)" != main ] && [ -z "${2}" ] ; then
	echo "Version not given; stop."
	exit 1
fi
version=v${2:-$(cat VERSION)}
relTag="release-${version}"
if [ -z "$(git tag --list "${relTag}")" ] ; then
	echo "Release for ${version} does not exist; stop."
	exit 1
fi

# Get archive for files in release
git checkout -q -b __export__ "${relTag}"
git ls-tree -r __export__ --name-only >tmp/tracked-files.txt
tar -cf tmp/solver.tar -T tmp/tracked-files.txt
msg="Export to $(field "${target}" host)"
git tag -a "${target}$(date "+-%F")" -m "${msg}"

# Clone target repository
git clone -q "$(gitlab_url "${target}" solver)" "$(repo)"
cd "$(repo)" || exit 1

# Update branch for release imports
name=$(target_field branch)
git checkout -q -B "${name}"
git ls-tree -r "${name}" --name-only | xargs -n 400 rm
find . -empty -print0 | xargs -0n 400 rm -rf
tar -xf ../solver.tar
rm ../solver.tar ../tracked-files.txt

# Fix README.md
url="$(gitlab_url "${target}" scheduler)"
url=${url%.git}
sed -Ei "" -e "/\\[1\\]:/s|^(\\[1\\]: ).+$|\\1${url}|" README.md

# Increment version
git checkout -q VERSION
awk -v FS=. -f scripts/inc-version.awk VERSION >VERSION.new
mv VERSION.new VERSION

# Commit and push changes
git add --all
msg=$(printf "Import %s\n\nImport %s from %s as v%s." \
			 "${version}" "${version}" "$(field siemens host)" \
			 "$(cat VERSION)")
git commit -q -m "${msg}"
git push --set-upstream origin "${name}"
cd ../..
rm -rf "$(repo)"
git switch -q main
git push --tags
git branch -q -D __export__
