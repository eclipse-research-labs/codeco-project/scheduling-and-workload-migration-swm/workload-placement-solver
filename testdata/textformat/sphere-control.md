<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<!-- workload assignment v1 -->
# Model "sphere-control"

This is an example of a small application controlling a robot deployed to
a cluster with two IPCs.  This was shown at the Hannover fair in 2023.
Special about this setup is that the whole time-critical loop controlling the
robot is *not* run on the machine that controls the robot but in a Kubernetes
cluster.  One of the workloads in the cluster sends commands to the robot.

## Applications

There are two applications: [application “sphere”][1] controls the robot and
[application “dashboard”][2] provides a small monitoring dashboard.

[1]: #application-sphere
[2]: #application-dashboard

### Application “sphere”

Using a camera to track the current position of the ball, application “sphere”
controls the robot so that the ball follows a predefined track.  The default
track is a circle.

The control loop is executed 30 times a second and performs the steps shown
below.  The control loop is executed once for each frame in the video stream
and the camera delivers 30 frames per second.

1. Workload “webcam” captures a frames from the camera and forwards to
   workload “processor” for further processing and analysis.
2. Workload “processor” computes the ball’s current position given a frame and
   determines the corrective action needed to keep the ball on the defined
   track.  It computes a new position for the plate so that the ball moves in
   the right direction and sends it to workload “controller”.
3. Workload “controller” sends the corrective action to the robot.  Note that
   this workload does *not* run on the robot’s controlling PC but on an IPC in
   the cluster.

With 30 updates per second the ball follows the predefined track without any
big deviation.  For a human observer the circle seems to be perfect.

| Workload   | CPU | RAM    | Labels     |
|------------|-----|--------|------------|
| webcam     | 200 | 200MiB | ipc camera |
| processor  | 500 | 200MiB | ipc        |
| controller | 200 | 100MiB | ipc robot  |

The video stream has 30 images that need to be delivered with a certain
latency.  This applies as well to the commands sent the the robot.  To keep
bandwidth and processing requirements reasonable, the camera sends only a
“low-resolution” image that fits into 64KiB of memory.

| Channel               | Lat | BW       | QoS     |
|-----------------------|-----|----------|---------|
| webcam->processor     | 1ms | 30*64KiB | assured |
| processor->controller | 1ms | 30*200   | assured |

### Application “dashboard”

There is a small dashboard showing monitoring data about the application.
The dashboard is built using Prometheus and Grafana.  The dashboard may *not*
run on the IPCs so that the control loop for the robot is not disturbed.

| Workload   | CPU | RAM    | Labels     |
|------------|-----|--------|------------|
| prometheus | 500 | 1GiB   | !ipc       |
| grafana    | 500 | 1GiB   | !ipc       |

Communication between Prometheus and the workloads controlling the ball
happens two times a second an transfers only small packets.  The channel
between Prometheus and Grafana defines a small bandwidth requirement.  The
communication for the dashboard is not time-critical so we do not specify
a latency.

| Channel                      | BW             |
|------------------------------|----------------|
| prometheus—sphere•processor  | 2×2KiB,2×16KiB |
| prometheus—sphere•controller | 2×2KiB,2×16KiB |
| prometheus—sphere•webcam     | 2×2KiB,2×16KiB |
| prometheus—grafana           | 100Kb          |

## Infrastructure

The cluster consists of two IPCs and and one Raspberry Pi.  Note that the
Raspberry Pi has a different processor architecture than the IPCs.  Note that
on each machine a certain amount of CPU and memory is not available for the
application.

| Node  | CPU      | RAM           | Labels     |
|-------|----------|---------------|------------|
| nano  | 2000-200 | 4GiB-400MiB   | ipc robot  |
| micro | 4000-400 | 16GiB-1600MiB | ipc camera |
| rasp4 | 4000     | 8GiB-128MiB   | ARM        |

### Network “lo”

All worker nodes provide a loopback interface.  Without the loopback interface
it is impossible to deploy the application onto the cluster.

| Option | Value |
|--------|-------|
| bw     | 1Gb   |
| lat    | 100ns |

We specify bandwidth and latency for network “lo” so it is not necessary to
specify this for each link.  Each link in the network is also a path.

| Link         | Path |
|--------------|------|
| nano->nano   | yes  |
| micro->micro | yes  |
| rasp4->rasp4 | yes  |

### Network “alo”

Solver can only find a solution for the [assignment][3] if some links in the
network "loopback are defined to have QoS class “assured”.  Network “alo”
(short for “assured loopback”) builds on [network “lo”][4] but has QoS class
“assured”.

[3]: #applications
[4]: #network-lo

| Option | Value   |
|--------|---------|
| bearer | lo      |
| qos    | assured |

The loopback links in the IPCs are considered to be of class “assured”.  As
no “secondary” workloads are running on these hosts, the loopback interface
very likely has no problem in meeting the latency and bandwidth requirements.

| Nodes      |
|------------|
| nano micro |

### Network “eth”

Network “eth” is the network that connect all worker nodes.  It is a Gigabit
network of service class “best-effort”.

| Option | Value |
|--------|-------|
| bw     | 1Gb   |
| lat    | 100µs |

The network introduces a switch switch actually connects the works.  There is
a link from each worker node to the switch and the switch is the “bus” that
connecting the nodes.  This is the conventional setup nowadays.

| Node   |
|--------|
| switch |

Link latency and bandwidth are uniform for all link adn specified as a option
to the network.

| Link          |
|---------------|
| nano--switch  |
| micro--switch |
| rasp4--switch |

Each path between two worker node goes through node “switch”.  Solver’s
command ~find path~ determines these paths.

| Find Shortest Paths |
|---------------------|
| nano•micro          |
| nano•rasp4          |
| micro•rasp4         |

### Network “vlan”

Channels between the workloads of [application “sphere”][1] need a network
with service class “assured”.  The links in [network “alo”][5] are not
sufficient to find a solution for the assignment.

[5]: #network-alo

Network “vlan” is a network of service class “assured” that is implemented
using the links of [network “eth”][6].  Strictly speaking a network using
VLANs to separate and prioritize network traffic is *not* of service class
“assured” as there are no guarantees, but for the purposes of the application
at hand the requirements are fulfilled.

[6]: #network-eth

| Option | Value   |
|--------|---------|
| bearer | eth     |
| qos    | assured |

Network “vlan” contains only the IPCs.  Communication with and between
workloads of [application “dashboard”][2] is of service class “best-effort”.

| Nodes |
|-------|
| nano  |
| micro |
