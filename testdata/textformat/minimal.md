<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<!-- workload assignment v1 -->
# Model "minimal"

This is a very small model for an application with a single workload that
gets deployed onto a cluster with a single node.  There are no network
requirements for the application.

## Applications

There is only one application in this assignment.

### Application “A”

Application “A” has one tiny workload.

| Workload | CPU | RAM  |
|----------|-----|------|
| W        | 100 | 20MB |

## Infrastructure

This section defines the cluster’s infrastructure.  It starts with the nodes
(a.k.a. *worker nodes*) that make up the cluster.  There is only one node in
this cluster.  After defining the nodes there are sections for the networks
present in the cluster.  There is one section per network.

| Node | CPU  | RAM  |
|------|------|------|
| C    | 1000 | 2GiB |

## Network “Loopback”

This section defines network “loopback” that allows workload on the same on to
talk to each other.  Without this network workloads that have channels between
them may not reside on the same node.  The options below specify the type of
of network “loopback” and how its bandwidth is used.

| Option | Value       |
|--------|-------------|
| qos    | best-effort |
| links  | half-duplex |

Each link in network “loopback” is also a path, it allows two workloads on
node C to talk to each other.

| Link | Lat | BW  | path |
|------|-----|-----|------|
| C•C  | 1µs | 1Gb | yes  |
