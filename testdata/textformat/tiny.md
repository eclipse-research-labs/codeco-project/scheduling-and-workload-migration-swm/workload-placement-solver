<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<!-- workload assignment v1 -->
# Model “tiny”

## Applications

### Application “a1”

| Workload | CPU | RAM    |
|----------|-----|--------|
| w1       | 100 | 20MiB  |
| w2       | 100 | 100MiB |

| Channel | Lat   | BW  |
|---------|-------|-----|
| w1→w2   | 100µs | 5Mb |

## Infrastructure

| Node | CPU  | RAM  |
|------|------|------|
| c1   | 1000 | 2GiB |
| c2   | 2000 | 4GiB |

### Network “Ethernet”

| Option | Value       |
|--------|-------------|
| qos    | best-effort |

| Link  | Lat  | BW       |
|-------|------|----------|
| c1↔c2 | 10µs | 5Mb      |
| c1•c1 | 1µs  | 1Gb      |
| c2•c2 | 1µs  | 1Gb      |

| Find Paths |
|------------|
| c1•c2      |
