<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<!-- workload assignment v1 -->
# Model “semiotics”

This is an example for an application monitoring a set of windmills in a wind
park. It contains only a single mill (see [section “M1”][1]), but it is easy
to add more windmills by cloning section “M1” appropriately.

[1]: #section-m1

**Note:** The network requirements are not realistic.

## Applications

There are two sets of applications in this model: a set of applications that
provide the core functionality and a set of applications installed for each
[windmill][1] in the wind park.

### Application “SDN”

The network contains a few nodes that implement a software-defined network.
We deploy the software on these nodes in the same way as we deploy “normal”
work packages.  This software can run only on these special nodes.  Workload
“Control” provides the user an interface for the network.

| Workload | CPU  | RAM  | Labels |
|----------|------|------|--------|
| Control  | 2000 | 500  |        |
| Agent1   | 100  | 100  | SDN    |
| Agent2   | 100  | 100  | SDN    |
| Agent3   | 100  | 100  | SDN    |
| Agent4   | 100  | 100  | SDN    |

| Channel                   | Lat  | BW   |
|---------------------------|------|------|
| Control ↔ Agent1          | 600  | 1,10 |
| Control ↔ Agent2          | 600  | 1,10 |
| Control ↔ Agent3          | 600  | 1,10 |
| Control ↔ Agent4          | 600  | 1,10 |
| Control ↔ AppIntf•PatOrch | 2000 | 10,1 |

### Application “SCADA”

Application “SCADA“ monitors data from the windmills and shows the user
alerts as appropriate.

| Workload | CPU  | RAM  | Labels |
|----------|------|------|--------|
| SCADA    | 3000 | 4000 |        |
| PlcEmerg | 100  | 200  |        |
| PlcData  | 0    | 0    | PLC    |

| Channel                         | Lat  | BW   |
|---------------------------------|------|------|
| PlcData  ↔ PlcEmerg             | 10   | 50,1 |
| PlcEmerg ↔ SCADA                | 1000 | 50,1 |
| SCADA•PlcEmerg ↔ Analysis•Accel | 50   | 1    |

### Application “Analysis”

Application “Analysis” considers several data streams and generates some
aggregate data from it.  It considers acceleration, audio, and video sensors
attached to the windmills.

| Workload | CPU  | RAM  |
|----------|------|------|
| Accel    | 1000 | 1000 |
| DCorr    | 500  | 200  |
| Audio    | 500  | 200  |
| Video    | 2000 | 1000 |

| Channel               | Lat  | BW |
|-----------------------|------|----|
| Accel → MDSP•Connect  | 2000 | 10 |
| DCorr → MDSP•Connect  | 2000 | 10 |
| Audio → MDSP•Connect  | 2000 | 10 |
| Video → MDSP•Connect  | 2000 | 10 |
| Accel ↔ AppIntf•SemGW | 2000 | 1  |
| DCorr ↔ AppIntf•SemGW | 2000 | 1  |
| Audio ↔ AppIntf•SemGW | 2000 | 1  |
| Video ↔ AppIntf•SemGW | 2000 | 1  |

### Application “MDSP”

Application “MDSP” sends some information to MindSphere.

| Workload  | CPU | RAM | Labels |
|-----------|-----|-----|--------|
| Connect   | 400 | 400 |        |
| CloudIntf | 500 | 500 | MDSP   |

| Channel             | Lat  | BW |
|---------------------|------|----|
| Connect → CloudIntf | 5000 | 50 |

### Application “AppIntf”

Application “AppIntf“ provides an interface for a human and an API for other
software entities.

| Workload | CPU  | RAM  |
|----------|------|------|
| SemGW    | 500  | 500  |
| PatOrch  | 1000 | 1000 |
| RecipeFW | 2000 | 2000 |
| KnowRep  | 2000 | 4000 |

| Channel            | Lat  | BW   |
|--------------------|------|------|
| SemGW    ↔ PatOrch | 2000 | 1,10 |
| RecipeFW ↔ KnowRep | 2000 | 1    |
| RecipeFW ↔ PatOrch | 2000 | 1    |
| KnowRep  ↔ PatOrch | 2000 | 1    |

## Infrastructure

The cluster of this application is heterogeneous where some nodes are in the
cloud (e.g., at AWS), some are in a local “data centre” (i.e., some machines
installed directly on wind park’s site), and some are attached to each wind
mill (see [section “M1”][1]).  Node N1 through N4 are the nodes for the SDN
(see [section “Application ‘SDN’"][2]). They are *not* used to run ordinary
workloads.

[2]: #application-sdn

| Node | CPU  | RAM   | Labels |
|------|------|-------|--------|
| C1   | 500  | 500   | PLC    |
| C2   | 4000 | 4000  |        |
| C3   | 4000 | 8000  |        |
| C4   | 4000 | 8000  |        |
| C5   | 8000 | 16000 | MDSP   |
| C6   | 8000 | 16000 | MDSP   |
| N3   | 100  | 100   | SDN    |
| N4   | 100  | 100   | SDN    |
| N5   | 100  | 100   | SDN    |
| N6   | 100  | 100   | SDN    |

### Network “Ethernet”

Network Ethernet connects all nodes.  It introduces some network nodes
(switches and routers) that define the network topology together with the
nodes used for the SDN.  As the cluster is heterogeneous the properties of
network links differ.

| Option | Value  |
|--------|--------|
| links  | duplex |

| Node |
|------|
| N1   |
| N2   |
| N7   |
| N8   |

| Link  | Lat | BW       |
|-------|-----|----------|
| C1↔N1 | 2   | 100Kb    |
| N1↔N2 | 1   | 1Mb      |
| C2↔N2 | 2   | 100Kb    |
| N2↔N3 | 10  | 1Mb      |
| N3↔N4 | 10  | 1Mb      |
| N4↔N5 | 10  | 1Mb      |
| N5↔N6 | 10  | 1Mb      |
| N3↔N6 | 10  | 1Mb      |
| N4↔N7 | 20  | 1Mb      |
| C3↔N7 | 0   | 1Gb      |
| C4↔N7 | 0   | 1Gb      |
| N5↔N8 | 500 | 2Kb,10Kb |
| C5↔N8 | 0   | 1Gb      |
| C6↔N8 | 0   | 1Gb      |

| Find Paths                          |
|-------------------------------------|
| C1•C2 C1•C3 C1•C4 C1•C5 C1•C6       |
| C2•C3 C2•C4 C2•C5 C2•C6             |
| C3•C4 C3•C5 C3•C6                   |
| C4•C5 C4•C6                         |
| C5•C6                               |
| N3•C1 N3•C2 N3•C3 N3•C4 N3•C5 N3•C6 |
| N4•C1 N4•C2 N4•C3 N4•C4 N4•C5 N4•C6 |
| N5•C1 N5•C2 N5•C3 N5•C4 N5•C5 N5•C6 |
| N6•C1 N6•C2 N6•C3 N6•C4 N6•C5 N6•C6 |

### Network “Loopback”

Define network “loopback” so that workload communicating with each other can
be put onto the same worker node.

| Option | Value       |
|--------|-------------|
| qos    | best-effort |

| Nodes             |
|-------------------|
| C1 C2 C3 C4 C5 C6 |

| Link  | Lat | BW  | Path |
|-------|-----|-----|------|
| C1•C1 | 0   | 1Gb | yes  |
| C2•C2 | 0   | 1Gb | yes  |
| C3•C3 | 0   | 1Gb | yes  |
| C4•C4 | 0   | 1Gb | yes  |
| C5•C5 | 0   | 1Gb | yes  |
| C6•C6 | 0   | 1Gb | yes  |

## Section “M1”

This section is for a windmill.  It defines workloads and infrastructure for
a single windmill.  Clone this section appropriately to define additional
windmills.

### Application “M1”

The application for a windmill contains the workloads needed to collect sensor
information and forward it to the appropriate workloads making use of it.
Some workloads depends on sensors that only certain hosts support.

| Workload | CPU | RAM | Labels      |
|----------|-----|-----|-------------|
| Accel    | 200 | 100 | AccSens M1  |
| TSens    | 200 | 100 | TurbSens M1 |
| Audio    | 200 | 100 | Mic M1      |
| Video    | 500 | 300 | Camera M1   |

| Channel                | Lat  | BW   |
|------------------------|------|------|
| Accel → SCADA•SCADA    | 1000 | 100  |
| TSens → SCADA•SCADA    | 1000 | 50   |
| Accel → Analysis•Accel | 500  | 100  |
| Accel → Analysis•DCorr | 1000 | 100  |
| TSens → Analysis•DCorr | 1000 | 50   |
| Audio → Analysis•Audio | 500  | 320  |
| Video → Analysis•Video | 500  | 4000 |

### Infrastructure

For each windmill there are two nodes for running  its workloads.

| Node  | CPU  | RAM  | Labels                 |
|-------|------|------|------------------------|
| M1.C1 | 2000 | 1000 | TurbSens Mic Camera M1 |
| M1.C2 | 2000 | 1000 | AccSens M1             |

#### Network “Ethernet” (continued)

Each windmill adds its nodes and network links to [network “Ethernet”][3] that
contains all nodes.

[3]: #network-ethernet

| Nodes |
|-------|
| M1.C1 |
| M1.C2 |

| Link     | Lat | BW    |
|----------|-----|-------|
| M1.C1↔N1 | 2   | 100Kb |
| M1.C2↔N2 | 2   | 100Kb |

| Find Paths                                      |
|-------------------------------------------------|
| M1.C1•M1.C2 M1.C1•C1 M1.C1•C2 M1.C1•C3 M1.C1•C4 |
| M1.C1•C5 M1.C1•C6                               |
| M1.C2•C1 M1.C2•C2 M1.C2•C3 M1.C2•C4             |
| M1.C2•C5 M1.C2•C6                               |

#### Network “Loopback” (continued)

Extend [network “loopback”][4] so that workloads connected by a channel for
this windmill can be placed on the same node.

[4]: #network-loopback

| Nodes |
|-------|
| M1.C1 |
| M1.C2 |

| Link        | Lat | BW  | Path |
|-------------|-----|-----|------|
| M1.C1•M1.C1 | 0   | 1Gb | yes  |
| M1.C2•M1.C2 | 0   | 1Gb | yes  |
