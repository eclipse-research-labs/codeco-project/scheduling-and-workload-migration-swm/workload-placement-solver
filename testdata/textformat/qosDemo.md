<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<!-- workload assignment v1 -->
# Model “qos-demo”

Option “implicit-reverse-path” may *not* be set as the link between node c1
and c2 has a different QoS class for each direction.

| Option                | Value |
|-----------------------|-------|
| implicit-reverse-path | no    |

## Applications

### Application “app1”

| Workload | CPU | RAM   |
|----------|-----|-------|
| w1       | 100 | 20MiB |
| w2       | 100 | 20MiB |

| Channel | Lat   | BW    | QoS     |
|---------|-------|-------|---------|
| w1→w2   | 100µs | 5Mbit | assured |
| w2→w1   | 150µs | 2Mbit |         |

### Application “app2”

| Workload | CPU | RAM   |
|----------|-----|-------|
| w4       | 100 | 20MiB |
| w5       | 100 | 20MiB |
| w6       | 100 | 20MiB |

| Channel | Lat       | BW          |
|---------|-----------|-------------|
| w4—w5   | 10µs,10µs | 4Mbit,1Mbit |
| w5→w6   | 10µs      | 4Mbit       |

## Infrastructure

For node “kind-control-plane” no CPU and RAM resources are specified so the
default values apply.

| Node               | CPU  | RAM  |
|--------------------|------|------|
| c1                 | 1000 | 2GiB |
| c2                 | 1000 | 2GiB | 
| kind-control-plane |      |      |

### Network “tsn”

Network “tsn” is an assured network, i.e., latency and bandwidth are
guaranteed by the network.

| Option | Value   |
|--------|---------|
| qos    | assured |

For TSN special network equipment is necessary and only some nodes have the
equipment installed.

| Nodes |
|-------|
| c1 c2 |

This network provides only a single link from node c1 to node c2.

| Link  | Lat  | BW    |
|-------|------|-------|
| c1→c2 | 10µs | 5Mbit |

The network only has a single path connection worker nodes c1 and c2.

| Paths |
|-------|
| c1→c2 |

### Network “Ethernet”

Network “Ethernet” contains all nodes.  The nodes are connected directly to
each other.

| Link                  | Lat  | BW    |
|-----------------------|------|-------|
| c2→c1                 | 10µs | 5Mbit |
| c1—kind-control-plane | 10µs | 5Mbit |

| Find Paths                  |
|-----------------------------|
| c1•c2 c1•kind-control-plane |

### Network “Loopback”

Only the worker nodes provide a loopback interface.

| Nodes |
|-------|
| c1 c2 |

| Link  | Lat   | BW    | Path |
|-------|-------|-------|------|
| c1→c1 | 100ns | 1Gb   | yes  |
| c2→c2 | 100ns | 1Gb   | yes  |
