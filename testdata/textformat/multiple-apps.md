<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<!-- workload assignment v1 -->
# Model “multiple-apps”

This model has a set of application with workloads that all use the amount of
CPU and memory to make it easy to check where the solution returned by Solver
is correct.

## Applications

### Application “App1”

The workloads of application “App1” may not reside on a node intended for
databases.

| Workload | CPU  | RAM  | Labels |
|----------|------|------|--------|
| W1       | 1000 | 8000 | ~DB    |
| W2       | 1000 | 8000 | ~DB    |

The channel between workloads W1 and W2 is asymmetric.  The bandwidth from
W1 to W2 is 5 and the bandwidth from W2 to W1 is 2.

| Channel | Lat | BW  |
|---------|-----|-----|
| W1—W2   | 10  | 5,2 |

### Application “App2”

The workloads of application “App2” run on hosts intended for databases.

| Workload | CPU  | RAM  | Labels |
|----------|------|------|--------|
| W3       | 1000 | 8000 | DB     |

### Application “App3”

Some workloads of application “App3” need GPU support.

| Workload | CPU  | RAM  | Labels |
|----------|------|------|--------|
| W4       | 1000 | 8000 | GPU    |
| W5       | 1000 | 8000 | GPU    |
| W6       | 1000 | 8000 |        |

| Channel | Lat | BW  |
|---------|-----|-----|
| W4—W5   | 10  | 7,1 |
| W5—W6   | 10  | 7,1 |

### Application “App4”

| Workload | CPU  | RAM  |
|----------|------|------|
| W7       | 1000 | 8000 |
| W8       | 1000 | 8000 |
| W9       | 1000 | 8000 |

| Channel | Lat | BW  |
|---------|-----|-----|
| W7—W8   | 10  | 2   |
| W7—W9   | 10  | 2,1 |

### Application “App5”

| Workload | CPU  | RAM  | Labels |
|----------|------|------|--------|
| W10      | 1000 | 8000 | Sens   |
| W11      | 1000 | 8000 | GPU    |
| W12      | 1000 | 8000 | GPU    |

| Channel | Lat | BW  |
|---------|-----|-----|
| W10—W11 | 10  |5,1  |
| W10—W12 | 10  |10,2 |
| W11—W12 | 10  |3,1  |

# Infrastructure

The cluster’s nodes have not enough capacity to host all applications as some
nodes have already workloads (or other software) deployed on them.

| Node | CPU      | RAM        | Labels |
|------|----------|------------|--------|
| C1   | 2000-500 | 16000-1000 |        |
| C2   | 2000-500 | 16000-1000 |        |
| C3   | 2000     | 16000      |        |
| C4   | 1500     | 16000      | DB GPU |
| C5   | 1000     | 8000       | Sens   |
| C6   | 1000     | 8000       | GPU    |
| C7   | 1000     | 8000       | GPU    |

## Network “eth”

Network “eth” introduces three *network nodes*, i.e., nodes that define the
notwork topology but are not used for running workloads.

| Node     |
|----------|
| N1,N2,N3 |

Some bandwidth of the network links is already used by the workloads already
deployed onto the cluster.

| Link  | Lat | BW       |
|-------|-----|----------|
| C1↔N1 | 1   | 1000-100 |
| C2↔N1 | 1   | 1000-100 |
| C3↔N1 | 1   | 1000     |
| C4↔N2 | 1   | 1000     |
| C6↔N3 | 3   | 100      |
| C7↔N3 | 3   | 100      |
| C5↔N3 | 3   | 100      |
| N1↔N2 | 5   | 10       |
| N2↔N3 | 5   | 10       |

Instead of specifying the paths in the network explicitly, Solver’s command
“find path” determines the paths between the worker nodes.  For each pair of
nodes, Solver determines paths for going from for instance C1 ro C2 and from
C2 to C1.

| Find Paths                          |
|-------------------------------------|
| C1•C2 C1•C3 C1•C4 C1•C5 C1•C6 C1•C7 |
| C2•C3 C2•C4 C2•C5 C2•C6 C2•C7       |
| C3•C4 C3•C5 C3•C6 C3•C7             |
| C4•C5 C4•C6 C4•C7                   |
| C5•C6 C5•C7                         |
| C6•C7                               |
