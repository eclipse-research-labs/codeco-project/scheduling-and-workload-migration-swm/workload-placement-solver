# Solve built-in sample semiotic” (no mills)
solveE semiotics(0)
cmp stdout solution.txt

# implicit reverse paths
solveI semiotics(0)
cmp stdout solution.txt

-- solution.txt --
Node Usage
C1: cpu:100/500, ram:200/500, workloads: 2
   SCADA•PlcEmerg, SCADA•PlcData
C2: cpu:4000/4000, ram:2.441KiB/3.906KiB, workloads: 2
   SDN•Control, AppIntf•RecipeFW
C3: cpu:3000/4000, ram:3.906KiB/7.812KiB, workloads: 1
   SCADA•SCADA
C4: cpu:3000/4000, ram:4.883KiB/7.812KiB, workloads: 2
   Analysis•Accel, AppIntf•KnowRep
C5: cpu:2400/8000, ram:2.051KiB/15.62KiB, workloads: 4
   Analysis•DCorr, MDSP•Connect, MDSP•CloudIntf, AppIntf•PatOrch
C6: cpu:3000/8000, ram:1.66KiB/15.62KiB, workloads: 3
   Analysis•Audio, Analysis•Video, AppIntf•SemGW
N3: cpu:100/100, ram:100/100, workloads: 1
   SDN•Agent1
N4: cpu:100/100, ram:100/100, workloads: 1
   SDN•Agent2
N5: cpu:100/100, ram:100/100, workloads: 1
   SDN•Agent3
N6: cpu:100/100, ram:100/100, workloads: 1
   SDN•Agent4

Link Usage
Network “Ethernet”
C1→N1: lat:2ns, cap:51/100Kb, channels: 2
   SCADA•PlcEmerg→SCADA•SCADA, SCADA•PlcEmerg→Analysis•Accel
N1→C1: lat:2ns, cap:2/100Kb, channels: 2
   SCADA•SCADA→SCADA•PlcEmerg, Analysis•Accel→SCADA•PlcEmerg
N1→N2: lat:1ns, cap:51/1Mb, channels: 2
   SCADA•PlcEmerg→SCADA•SCADA, SCADA•PlcEmerg→Analysis•Accel
N2→N1: lat:1ns, cap:2/1Mb, channels: 2
   SCADA•SCADA→SCADA•PlcEmerg, Analysis•Accel→SCADA•PlcEmerg
C2→N2: lat:2ns, cap:16/100Kb, channels: 7
   SDN•Control→SDN•Agent1, SDN•Control→SDN•Agent2, SDN•Control→SDN•Agent3,
   SDN•Control→SDN•Agent4, SDN•Control→AppIntf•PatOrch,
   AppIntf•RecipeFW→AppIntf•PatOrch, AppIntf•RecipeFW→AppIntf•KnowRep
N2→C2: lat:2ns, cap:43/100Kb, channels: 7
   SDN•Agent1→SDN•Control, SDN•Agent2→SDN•Control, SDN•Agent3→SDN•Control,
   SDN•Agent4→SDN•Control, AppIntf•PatOrch→SDN•Control,
   AppIntf•PatOrch→AppIntf•RecipeFW, AppIntf•KnowRep→AppIntf•RecipeFW
N2→N3: lat:10ns, cap:67/1Mb, channels: 9
   SDN•Control→SDN•Agent1, SDN•Control→SDN•Agent2, SDN•Control→SDN•Agent3,
   SDN•Control→SDN•Agent4, SDN•Control→AppIntf•PatOrch,
   SCADA•PlcEmerg→SCADA•SCADA, SCADA•PlcEmerg→Analysis•Accel,
   AppIntf•RecipeFW→AppIntf•PatOrch, AppIntf•RecipeFW→AppIntf•KnowRep
N3→N2: lat:10ns, cap:45/1Mb, channels: 9
   SDN•Agent1→SDN•Control, SDN•Agent2→SDN•Control, SDN•Agent3→SDN•Control,
   SDN•Agent4→SDN•Control, SCADA•SCADA→SCADA•PlcEmerg,
   Analysis•Accel→SCADA•PlcEmerg, AppIntf•PatOrch→SDN•Control,
   AppIntf•PatOrch→AppIntf•RecipeFW, AppIntf•KnowRep→AppIntf•RecipeFW
N3→N4: lat:10ns, cap:65/1Mb, channels: 7
   SDN•Control→SDN•Agent2, SDN•Control→SDN•Agent3, SDN•Control→AppIntf•PatOrch,
   SCADA•PlcEmerg→SCADA•SCADA, SCADA•PlcEmerg→Analysis•Accel,
   AppIntf•RecipeFW→AppIntf•PatOrch, AppIntf•RecipeFW→AppIntf•KnowRep
N4→N3: lat:10ns, cap:25/1Mb, channels: 7
   SDN•Agent2→SDN•Control, SDN•Agent3→SDN•Control, SCADA•SCADA→SCADA•PlcEmerg,
   Analysis•Accel→SCADA•PlcEmerg, AppIntf•PatOrch→SDN•Control,
   AppIntf•PatOrch→AppIntf•RecipeFW, AppIntf•KnowRep→AppIntf•RecipeFW
N4→N5: lat:10ns, cap:24/1Mb, channels: 6
   SDN•Control→SDN•Agent3, SDN•Control→AppIntf•PatOrch,
   Analysis•Accel→MDSP•Connect, Analysis•Accel→AppIntf•SemGW,
   AppIntf•RecipeFW→AppIntf•PatOrch, AppIntf•KnowRep→AppIntf•PatOrch
N5→N4: lat:10ns, cap:14/1Mb, channels: 5
   SDN•Agent3→SDN•Control, AppIntf•SemGW→Analysis•Accel,
   AppIntf•PatOrch→SDN•Control, AppIntf•PatOrch→AppIntf•RecipeFW,
   AppIntf•PatOrch→AppIntf•KnowRep
N3→N6: lat:10ns, cap:1/1Mb, channels: 1
   SDN•Control→SDN•Agent4
N6→N3: lat:10ns, cap:10/1Mb, channels: 1
   SDN•Agent4→SDN•Control
N4→N7: lat:20ns, cap:54/1Mb, channels: 5
   SCADA•PlcEmerg→SCADA•SCADA, SCADA•PlcEmerg→Analysis•Accel,
   AppIntf•SemGW→Analysis•Accel, AppIntf•PatOrch→AppIntf•KnowRep,
   AppIntf•RecipeFW→AppIntf•KnowRep
N7→N4: lat:20ns, cap:15/1Mb, channels: 6
   SCADA•SCADA→SCADA•PlcEmerg, Analysis•Accel→SCADA•PlcEmerg,
   Analysis•Accel→MDSP•Connect, Analysis•Accel→AppIntf•SemGW,
   AppIntf•KnowRep→AppIntf•PatOrch, AppIntf•KnowRep→AppIntf•RecipeFW
C3→N7: lat:0s, cap:1/1Gb, channels: 1
   SCADA•SCADA→SCADA•PlcEmerg
N7→C3: lat:0s, cap:50/1Gb, channels: 1
   SCADA•PlcEmerg→SCADA•SCADA
C4→N7: lat:0s, cap:14/1Gb, channels: 5
   Analysis•Accel→SCADA•PlcEmerg, Analysis•Accel→MDSP•Connect,
   Analysis•Accel→AppIntf•SemGW, AppIntf•KnowRep→AppIntf•PatOrch,
   AppIntf•KnowRep→AppIntf•RecipeFW
N7→C4: lat:0s, cap:4/1Gb, channels: 4
   SCADA•PlcEmerg→Analysis•Accel, AppIntf•SemGW→Analysis•Accel,
   AppIntf•PatOrch→AppIntf•KnowRep, AppIntf•RecipeFW→AppIntf•KnowRep
N5→N8: lat:500ns, cap:23/2Kb, channels: 5
   SDN•Control→AppIntf•PatOrch, Analysis•Accel→MDSP•Connect,
   Analysis•Accel→AppIntf•SemGW, AppIntf•RecipeFW→AppIntf•PatOrch,
   AppIntf•KnowRep→AppIntf•PatOrch
N8→N5: lat:500ns, cap:4/10Kb, channels: 4
   AppIntf•SemGW→Analysis•Accel, AppIntf•PatOrch→SDN•Control,
   AppIntf•PatOrch→AppIntf•RecipeFW, AppIntf•PatOrch→AppIntf•KnowRep
C5→N8: lat:0s, cap:14/1Gb, channels: 5
   Analysis•DCorr→AppIntf•SemGW, AppIntf•PatOrch→SDN•Control,
   AppIntf•PatOrch→AppIntf•SemGW, AppIntf•PatOrch→AppIntf•RecipeFW,
   AppIntf•PatOrch→AppIntf•KnowRep
N8→C5: lat:0s, cap:44/1Gb, channels: 8
   SDN•Control→AppIntf•PatOrch, Analysis•Accel→MDSP•Connect,
   Analysis•Audio→MDSP•Connect, Analysis•Video→MDSP•Connect,
   AppIntf•SemGW→Analysis•DCorr, AppIntf•SemGW→AppIntf•PatOrch,
   AppIntf•RecipeFW→AppIntf•PatOrch, AppIntf•KnowRep→AppIntf•PatOrch
C6→N8: lat:0s, cap:23/1Gb, channels: 5
   Analysis•Audio→MDSP•Connect, Analysis•Video→MDSP•Connect,
   AppIntf•SemGW→Analysis•Accel, AppIntf•SemGW→Analysis•DCorr,
   AppIntf•SemGW→AppIntf•PatOrch
N8→C6: lat:0s, cap:12/1Gb, channels: 3
   Analysis•Accel→AppIntf•SemGW, Analysis•DCorr→AppIntf•SemGW,
   AppIntf•PatOrch→AppIntf•SemGW

Network “Loopback”
C1→C1: lat:0s, cap:51/1Gb, channels: 2
   SCADA•PlcEmerg→SCADA•PlcData, SCADA•PlcData→SCADA•PlcEmerg
C5→C5: lat:0s, cap:60/1Gb, channels: 2
   Analysis•DCorr→MDSP•Connect, MDSP•Connect→MDSP•CloudIntf
C6→C6: lat:0s, cap:4/1Gb, channels: 4
   Analysis•Audio→AppIntf•SemGW, Analysis•Video→AppIntf•SemGW,
   AppIntf•SemGW→Analysis•Audio, AppIntf•SemGW→Analysis•Video
