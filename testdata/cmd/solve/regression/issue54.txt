# Verify that a channel’s path has a network
solveE --query --format=text,json issue54.asgmt
stdin stdout
exec jq -SM .
cmp stdout has-network.json

-- issue54.asgmt --
workload assignment v1
model issue54
application a1
workload w1,w2 on c1,c2 needs cpu:100 ram:128MiB
channel w1—w2 on eth•1,eth•2 needs lat:10ms bw:1Mb
infrastructure
node c1,c2 provides cpu:1000 ram:2GiB
network eth
node n1
link 1,2:c1—n1,3,4:n1—c2 with lat:1µs bw:100Mb
paths (1:1,3 2:4,2)
-- has-network.json --
{
  "appGroup": {
    "applications": [
      {
        "id": "a1",
        "workloads": [
          {
            "CPU": 100,
            "id": "w1",
            "memory": "134217728",
            "node": "c1"
          },
          {
            "CPU": 100,
            "id": "w2",
            "memory": "134217728",
            "node": "c2"
          }
        ]
      }
    ],
    "channels": [
      {
        "bandwidth": "1000000",
        "id": "a1•w1→a1•w2",
        "latency": "10000000",
        "network": "eth",
        "path": "1",
        "sourceApplication": "a1",
        "sourceWorkload": "w1",
        "targetApplication": "a1",
        "targetWorkload": "w2"
      },
      {
        "bandwidth": "1000000",
        "id": "a1•w2→a1•w1",
        "latency": "10000000",
        "network": "eth",
        "path": "2",
        "sourceApplication": "a1",
        "sourceWorkload": "w2",
        "targetApplication": "a1",
        "targetWorkload": "w1"
      }
    ]
  },
  "infrastructure": {
    "networks": [
      {
        "id": "eth",
        "links": [
          {
            "id": "1",
            "latency": "1000",
            "medium": "1ₘ",
            "source": "c1",
            "target": "n1"
          },
          {
            "id": "2",
            "latency": "1000",
            "medium": "2ₘ",
            "source": "n1",
            "target": "c1"
          },
          {
            "id": "3",
            "latency": "1000",
            "medium": "3ₘ",
            "source": "n1",
            "target": "c2"
          },
          {
            "id": "4",
            "latency": "1000",
            "medium": "4ₘ",
            "source": "c2",
            "target": "n1"
          }
        ],
        "media": [
          {
            "bandwidth": {
              "capacity": "100000000"
            },
            "id": "1ₘ"
          },
          {
            "bandwidth": {
              "capacity": "100000000"
            },
            "id": "2ₘ"
          },
          {
            "bandwidth": {
              "capacity": "100000000"
            },
            "id": "3ₘ"
          },
          {
            "bandwidth": {
              "capacity": "100000000"
            },
            "id": "4ₘ"
          }
        ],
        "paths": [
          {
            "id": "1",
            "links": [
              "1",
              "3"
            ]
          },
          {
            "id": "2",
            "links": [
              "4",
              "2"
            ]
          }
        ]
      }
    ],
    "nodes": [
      {
        "CPU": {
          "capacity": "1000"
        },
        "id": "c1",
        "memory": {
          "capacity": "2147483648"
        }
      },
      {
        "CPU": {
          "capacity": "1000"
        },
        "id": "c2",
        "memory": {
          "capacity": "2147483648"
        }
      },
      {
        "bandwidth": {},
        "id": "n1",
        "nodeType": "NETWORK"
      }
    ]
  }
}
