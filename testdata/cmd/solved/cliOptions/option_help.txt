# Option “--help”
exec solved --help
stdout '^\nUsage.+\n\nReturn an assignment...'
stdout '-help\n +\tShow this help text and exit.\n'
stdout '-version\n +\tShow version and exit.\n'
stdout '-host string\n +\tAddress to bind server to. \(default "localhost"\)'
stdout '-port int\n +\tPort to listen for incoming requests. \(default 5000\)'
stdout '-verbose\n +\tEnable verbose logging.\n'
stdout '-max-requests value\n +\tMaximum number request to handle.\n'
