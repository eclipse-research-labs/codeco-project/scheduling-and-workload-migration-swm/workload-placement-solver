<!---
SPDX-FileCopyrightText: 2023 Siemens AG
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Project “QoS Solver”

Project “QoS Solver” is a simple solver for the [QoS Scheduler][1].  The
solver finds an assignment for a set of applications that respects all quality
of service constraints.  If the solver finds an assignment, it is guaranteed
to respect all requirements, but it may not be the “best” assignment of the
pods.  The solver has no cost function for the solution it finds.

[1]: https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/scheduling-and-workload-migration-swm/qos-scheduler

**Note:** The solver is not very advanced in searching for a solution and
stops at the first solution it finds.  If Solver cannot find a solution at
all, it may run for quite some time.

## Documentation

Solver supports a [little language][2] for describing an assignment problem.
It allows to compute a solution for an assignment problem with Solver outside
of a Kubernetes cluster.

[2]: docs/textformat.md

## License

All code files are licensed under Apache license version 2.0.  All
documentation is licensed under Creative Commons Attribution-ShareAlike 4.0
International.
